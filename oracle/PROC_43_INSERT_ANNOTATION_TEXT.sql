CREATE OR REPLACE PROCEDURE INSERT_ANNOTATION_TEXT 
(
	p_ann_id IN NUMBER,
	p_language IN VARCHAR2,
	p_text IN NVARCHAR2,
	p_pk OUT NUMBER) 
AS
BEGIN
	insert into ANNOTATION_TEXT (ANN_ID, LANGUAGE, TEXT) VALUES (p_ann_id, p_language, p_text);
	select ANNOTATION_TEXT_SEQ.currval into p_pk from dual;
END;
/
;
