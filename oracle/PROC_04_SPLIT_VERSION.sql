create or replace 
procedure SPLIT_VERSION(
    p_version IN varchar2,
    p_version1 OUT NUMBER,
    p_version2 OUT NUMBER,
    p_version3 OUT NUMBER
)
IS
point1 number;
point2 number;
BEGIN
    p_version1 := getVersion(p_version,1);
    p_version2 := getVersion(p_version,2);
    p_version3 := getVersion(p_version,3);
    if p_version1 is null then
         p_version1 := 0;
    end if;
    if p_version2 is null then
        p_version2 := 0;
    end if;
END;
/
;
