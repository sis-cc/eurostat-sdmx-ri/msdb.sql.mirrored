CREATE TABLE REGISTRATION (
    DR_ID NUMBER(18) NOT NULL,
    ID VARCHAR2(50) NULL,
    VALID_FROM TIMESTAMP NULL,
    VALID_TO TIMESTAMP NULL,
    LAST_UPDATED TIMESTAMP NULL,
    INDEX_TIME_SERIES NUMBER(1) DEFAULT 0 NOT NULL,
    INDEX_DATA_SET NUMBER(1) DEFAULT 0 NOT NULL,
    INDEX_ATTRIBUTES NUMBER(1) DEFAULT 0 NOT NULL,
    INDEX_REPORTING_PERIOD NUMBER(1) DEFAULT 0 NOT NULL,
    PA_ID NUMBER(18) NOT NULL,
    DATA_SOURCE_ID NUMBER(18) NOT NULL,
    PRIMARY KEY(DR_ID)
) 
;
