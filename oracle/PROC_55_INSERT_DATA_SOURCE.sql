create or replace procedure INSERT_DATA_SOURCE (
	p_data_url IN nvarchar2,
	p_wsdl_url IN nvarchar2,
	p_wadl_url IN nvarchar2,
	p_is_simple IN NUMBER,
	p_is_rest IN NUMBER,
	p_is_ws IN NUMBER,
	p_pk OUT NUMBER)
AS
BEGIN
	insert into DATA_SOURCE (DATA_URL, WSDL_URL, WADL_URL, IS_SIMPLE, IS_REST, IS_WS) 
    values (p_data_url, p_wsdl_url, p_wadl_url, p_is_simple, p_is_rest, p_is_ws);
  select DATA_SOURCE_ID_SEQ.currval into p_pk from dual;
END;
/
;
