CREATE OR REPLACE PROCEDURE INSERT_TEMPLATE_MAPPING(
p_column_name IN varchar2,
p_col_desc IN nvarchar2,
p_component_id IN varchar2,
p_component_type IN varchar2,
p_con_id IN number,
p_item_scheme_id IN number,
p_pk OUT number)
AS
BEGIN
		INSERT_COMPONENT_COMMON(p_pk => p_pk);
		INSERT INTO TEMPLATE_MAPPING (TEMPLATE_ID,COLUMN_NAME,COL_DESC,COMPONENT_ID,COMPONENT_TYPE,CON_ID,ITEM_SCHEME_ID) VALUES (p_pk, p_column_name, p_col_desc, p_component_id, p_component_type, p_con_id, p_item_scheme_id);
END;
/
;
