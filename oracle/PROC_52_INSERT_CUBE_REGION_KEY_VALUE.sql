CREATE OR REPLACE PROCEDURE INSERT_CUBE_REGION_KEY_VALUE(
	p_cube_region_id IN NUMBER,
	p_member_id IN varchar2,
	p_component_type IN varchar2,
	p_include IN NUMBER,
	p_pk OUT NUMBER,
	p_start_period IN varchar2,
	p_end_period IN varchar2,
	p_start_inclusive IN NUMBER,
	p_end_inclusive IN NUMBER)
AS
BEGIN
	INSERT INTO CUBE_REGION_KEY_VALUE
           (CUBE_REGION_ID, MEMBER_ID, COMPONENT_TYPE, INCLUDE,START_PERIOD,END_PERIOD,START_INCLUSIVE,END_INCLUSIVE)
     VALUES
           (p_cube_region_id, p_member_id, p_component_type, p_include,p_start_period,p_end_period,p_start_inclusive,p_end_inclusive);
	select CUBE_REGION_KEY_VALUE_SEQ.currval into p_pk from dual;
END;
/
;
