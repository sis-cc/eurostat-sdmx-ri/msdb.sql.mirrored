CREATE OR REPLACE PROCEDURE INSERT_CUBE_REGION(
	p_cont_cons_id IN NUMBER,
	p_include IN NUMBER,
	p_pk OUT NUMBER)
AS
BEGIN
	INSERT INTO CUBE_REGION
           (CONT_CONS_ID, INCLUDE)
     VALUES
           (p_cont_cons_id, p_include);
	select CUBE_REGION_SEQ.currval into p_pk from dual;
END;
/
;
