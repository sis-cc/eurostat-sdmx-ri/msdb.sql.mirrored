CREATE OR REPLACE PROCEDURE INSERT_CONTENT_CONSTRAINT(
	p_id IN varchar2,
	p_version IN varchar2,
	p_agency IN varchar2,
	p_valid_from IN  timestamp DEFAULT NULL,
	p_valid_to IN  timestamp DEFAULT NULL,
	p_is_final IN number DEFAULT NULL,
	p_uri IN nvarchar2,
	p_last_modified IN timestamp DEFAULT NULL,
	p_actual_data NUMBER DEFAULT 1,
	p_periodicity varchar2 DEFAULT NULL,
	p_offset varchar2 DEFAULT NULL,
	p_tolerance varchar2 DEFAULT NULL,
	p_attach_type varchar2 DEFAULT 'Dataflow',
	dp_id number DEFAULT null,
	set_id varchar2 DEFAULT null,
	simple_data_source varchar2 DEFAULT null,
	p_start_time IN  timestamp DEFAULT NULL,
	p_end_time IN  timestamp DEFAULT NULL,
	p_pk OUT NUMBER)
AS
BEGIN
  INSERT_ARTEFACT(p_id => p_id, p_version => p_version, p_agency => p_agency, p_valid_from => p_valid_from, p_valid_to => p_valid_to, p_is_final => p_is_final, p_uri => p_uri, p_last_modified => p_last_modified, p_is_stub => 0, p_service_url => null, p_structure_url => null, p_artefact_type => 'ContentConstraint', p_pk => p_pk);
	
	INSERT INTO CONTENT_CONSTRAINT
           (CONT_CONS_ID, ACTUAL_DATA, PERIODICITY, OFFSET, TOLERANCE,ATTACH_TYPE,DP_ID,SET_ID,SIMPLE_DATA_SOURCE,START_TIME,END_TIME)
     VALUES
           (p_pk, p_actual_data, p_periodicity, p_offset, p_tolerance,p_attach_type,dp_id,set_id,simple_data_source,p_start_time,p_end_time);
END;
/
;
