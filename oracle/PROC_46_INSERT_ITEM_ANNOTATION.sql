CREATE OR REPLACE PROCEDURE INSERT_ITEM_ANNOTATION 
(
	p_item_id IN NUMBER,
	p_id IN NVARCHAR2,
	p_title IN NVARCHAR2,
	p_type IN NVARCHAR2,
	p_url IN NVARCHAR2,
	p_pk OUT NUMBER) 
AS
BEGIN
	INSERT_ANNOTATION (p_id => p_id, p_title => p_title, p_type => p_type, p_url => p_url, p_pk => p_pk);
	insert into ITEM_ANNOTATION (ANN_ID, ITEM_ID) VALUES (p_pk, p_item_id);
END;
/
;
