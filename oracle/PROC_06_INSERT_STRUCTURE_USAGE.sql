CREATE OR REPLACE PROCEDURE INSERT_STRUCTURE_USAGE(
 p_id IN varchar2,
 p_version IN  varchar2,
 p_agency IN varchar2,
 p_valid_from IN   timestamp DEFAULT NULL, 
 p_valid_to IN   timestamp DEFAULT NULL,
 p_is_final IN  number,
 p_uri IN  nvarchar2,
 p_last_modified IN  timestamp DEFAULT NULL,
 p_is_stub IN number,
 p_service_url IN nvarchar2,
 p_structure_url IN nvarchar2,
 p_artefact_type IN varchar2,
 p_pk OUT  number)
IS
BEGIN
		INSERT_ARTEFACT(p_id => p_id, p_version => p_version, p_agency => p_agency, p_valid_from => p_valid_from, p_valid_to => p_valid_to, p_is_final => p_is_final, p_uri => p_uri, p_last_modified => p_last_modified, p_is_stub => p_is_stub, p_service_url => p_service_url, p_structure_url => p_structure_url, p_artefact_type => p_artefact_type, p_pk => p_pk);
     insert into STRUCTURE_USAGE (SU_ID) VALUES (p_pk);
END;
/
;
