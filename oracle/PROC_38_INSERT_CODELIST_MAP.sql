CREATE OR REPLACE PROCEDURE INSERT_CODELIST_MAP(
	p_id IN ITEM.ID%type,
	p_ss_id IN STRUCTURE_SET.SS_ID%type,
	p_source_cl_id IN CODELIST_MAP.SOURCE_CL_ID%type,
	p_target_cl_id IN CODELIST_MAP.TARGET_CL_ID%type,
	p_pk OUT ITEM.ITEM_ID%type)
AS
BEGIN

		insert into ITEM (ITEM_ID, ID) VALUES (ITEM_SEQ.nextval, p_id);
		select ITEM_SEQ.currval into p_pk from dual;
		insert into CODELIST_MAP (CLM_ID, SS_ID, SOURCE_CL_ID, TARGET_CL_ID) VALUES (p_pk, p_ss_id ,p_source_cl_id, p_target_cl_id);
END;
/
;
