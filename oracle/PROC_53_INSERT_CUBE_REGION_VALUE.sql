CREATE OR REPLACE PROCEDURE INSERT_CUBE_REGION_VALUE(
	p_cube_region_key_value_id IN NUMBER,
	p_member_value IN varchar2,
	p_include IN NUMBER)
AS
BEGIN
	INSERT INTO CUBE_REGION_VALUE
           (CUBE_REGION_KEY_VALUE_ID, MEMBER_VALUE, INCLUDE)
     VALUES
           (p_cube_region_key_value_id, p_member_value, p_include);
END;
/
;
