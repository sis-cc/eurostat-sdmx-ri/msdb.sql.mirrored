CREATE OR REPLACE PROCEDURE INSERT_DSD_CODE(
	p_id IN item.id%type,
	p_cl_id IN number,
	p_parent_code_id IN number DEFAULT NULL,
	p_pk OUT item.item_id%type)
AS
BEGIN

		insert into ITEM (ITEM_ID, ID) VALUES (ITEM_SEQ.nextval, p_id);
		select ITEM_SEQ.currval into p_pk from dual;
		insert into DSD_CODE (LCD_ID, CL_ID, PARENT_CODE_ID) VALUES (p_pk, p_cl_id, p_parent_code_id);
END;
/
;
