



CREATE OR REPLACE PROCEDURE INSERT_PARTY(
p_id IN varchar2,
p_header_id IN number,
p_type IN varchar2,
p_pk OUT number)
AS
BEGIN
		INSERT INTO PARTY (ID,HEADER_ID,TYPE) VALUES (p_id, p_header_id, p_type) RETURNING PARTY_ID INTO p_pk;
END;
/
;
