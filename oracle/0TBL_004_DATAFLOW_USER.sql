
CREATE TABLE DATAFLOW_USER ( 
	DF_ID NUMBER(18) NOT NULL,
	USER_ID NUMBER(18) NOT NULL
)
;

ALTER TABLE DATAFLOW_USER ADD CONSTRAINT PK_DATAFLOW_USER 
	PRIMARY KEY (DF_ID, USER_ID)
;
