


CREATE OR REPLACE PROCEDURE INSERT_MA_USER(
  p_username IN varchar2,
  p_password IN varchar2,
  p_user_type IN number,
  p_salt IN varchar2,
  p_algorithm IN varchar2,
  p_pk OUT number)
AS
BEGIN
   INSERT INTO MA_USER (USERNAME,PASSWORD,USER_TYPE,SALT,ALGORITHM) VALUES (p_username, p_password, p_user_type, p_salt, p_algorithm) RETURNING USER_ID INTO p_pk;
END;
/
;
