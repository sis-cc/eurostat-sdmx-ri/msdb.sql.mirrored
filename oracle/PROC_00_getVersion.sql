

-- functions

--- Gets the particle of a version string
create or replace function getVersion(p_version in varchar2, particle in number) 
RETURN number DETERMINISTIC
AS
point number;
currentParticle number;
returnValue varchar2(50);
tmp_version varchar2(50);
BEGIN
	point := instr(p_version, '.');
	currentParticle := 0;
	returnValue := null;

    tmp_version := p_version;
	WHILE returnValue is null and tmp_version is not null and currentParticle < particle LOOP
		currentParticle := currentParticle + 1;
		if currentParticle = particle then
			if point > 0 then
				returnValue := substr(tmp_version,1,point - 1);
			else
				returnValue := tmp_version;
			end if;
		elsif point > 0 then
			tmp_version := substr(tmp_version, (point + 1));
			point := instr(tmp_version, '.');
		else
			tmp_version := null;
		end if;
	END LOOP;
	return to_number(returnValue);
END;
/
;
