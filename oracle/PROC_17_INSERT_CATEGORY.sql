CREATE OR REPLACE PROCEDURE INSERT_CATEGORY(
	p_id IN item.id%type,
	p_cat_sch_id IN number,
	p_parent_cat_id IN CATEGORY.PARENT_CAT_ID%type DEFAULT NULL,
	p_pk OUT item.item_id%type)
AS
BEGIN

		insert into ITEM (ITEM_ID, ID) VALUES (ITEM_SEQ.nextval, p_id);
		select ITEM_SEQ.currval into p_pk from dual;
		insert into CATEGORY (CAT_ID, CAT_SCH_ID, PARENT_CAT_ID) VALUES (p_pk, p_cat_sch_id, p_parent_cat_id);
END;
/
;
