
CREATE OR REPLACE PROCEDURE INSERT_TEXT_FORMAT(
	p_compid IN NUMBER, 
	p_facet_type_enum IN NUMBER, 
	p_facet_value IN NVARCHAR2 DEFAULT NULL,
	p_pk OUT NUMBER)
AS
BEGIN
	insert into TEXT_FORMAT (FAC_ID, COMP_ID, FACET_TYPE_ENUM, FACET_VALUE) values (TEXT_FORMAT_SEQ.nextval, p_compid, p_facet_type_enum, p_facet_value);
	select TEXT_FORMAT_SEQ.currval into p_pk from dual;
END;
/
;
