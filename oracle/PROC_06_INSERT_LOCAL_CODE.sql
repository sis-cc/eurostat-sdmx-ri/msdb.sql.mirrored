CREATE OR REPLACE PROCEDURE INSERT_LOCAL_CODE(
	p_id IN varchar2, 
	p_column_id IN number,
	p_pk OUT NUMBER) 
AS
BEGIN
		insert into ITEM (ITEM_ID, ID) VALUES (ITEM_SEQ.nextval, p_id);
		select ITEM_SEQ.currval into p_pk from dual;
		insert into LOCAL_CODE (LCD_ID, COLUMN_ID) VALUES (p_pk, p_column_id);
END;
/
;
