
CREATE TABLE PARTY ( 
	PARTY_ID NUMBER(18) NOT NULL,
	ID NVARCHAR2(255),
	HEADER_ID NUMBER(18),
	TYPE VARCHAR2(50)
)
;

ALTER TABLE PARTY ADD CONSTRAINT PK_PARTY 
	PRIMARY KEY (PARTY_ID)
;
