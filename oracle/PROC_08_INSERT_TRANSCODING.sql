CREATE OR REPLACE PROCEDURE INSERT_TRANSCODING(
	p_map_id IN number, 
	p_expression IN varchar2 DEFAULT NULL,
	p_pk OUT NUMBER)
AS
BEGIN
	insert into TRANSCODING (MAP_ID, EXPRESSION) values (p_map_id,p_expression);
	select TRANSCODING_TR_ID_SEQ.currval into p_pk from dual;
END;
/
;
