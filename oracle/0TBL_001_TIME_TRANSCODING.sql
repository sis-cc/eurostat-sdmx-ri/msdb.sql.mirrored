
create table TIME_TRANSCODING ( 
  TR_ID number(18) NOT NULL,
  FREQ varchar2(5) NOT NULL,
  YEAR_COL_ID number(18) NULL,
  PERIOD_COL_ID number(18) NULL,
  DATE_COL_ID number(18) NULL,
  EXPRESSION varchar2(150) NULL
)
;

ALTER TABLE TIME_TRANSCODING ADD CONSTRAINT PK_TIME_TRANSCODING
    PRIMARY KEY(TR_ID, FREQ)
;
