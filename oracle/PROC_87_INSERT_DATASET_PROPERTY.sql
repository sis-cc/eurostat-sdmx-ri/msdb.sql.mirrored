
CREATE OR REPLACE PROCEDURE INSERT_DATASET_PROPERTY
(
	p_dataset_id IN number, 
	p_property_type_enum IN number, 
	p_property_value IN varchar2,
	p_pk OUT number
)
AS
BEGIN
	INSERT 
	  INTO DATASET_PROPERTY
	       (DATASET_ID, PROPERTY_TYPE_ENUM, PROPERTY_VALUE) 
	VALUES (p_dataset_id, p_property_type_enum, p_property_value)
	RETURNING DATASET_PROPERTY_ID INTO p_pk;
END;
/
;
