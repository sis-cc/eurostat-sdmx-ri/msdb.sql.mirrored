CREATE OR REPLACE PROCEDURE INSERT_HIERACHY(
	p_id IN varchar2,
	p_version IN varchar2,
	p_agency IN varchar2,
	p_valid_from IN  timestamp DEFAULT NULL,
	p_valid_to IN  timestamp DEFAULT NULL,
	p_is_final IN number DEFAULT NULL,
	p_uri IN nvarchar2,
	p_last_modified IN timestamp DEFAULT NULL,
	p_hcl_id IN number,
	p_pk OUT NUMBER)
AS
BEGIN

		INSERT_ARTEFACT(p_id => p_id, p_version => p_version, p_agency => p_agency, p_valid_from => p_valid_from, p_valid_to => p_valid_to, p_is_final => p_is_final, p_uri => p_uri, p_last_modified => p_last_modified, p_is_stub => 0, p_service_url => null, p_structure_url => null, p_artefact_type => null, p_pk => p_pk);
	
	INSERT INTO HIERARCHY
           (H_ID
           ,HCL_ID)
     VALUES
           (p_pk
           ,p_hcl_id);
END;
/
;
