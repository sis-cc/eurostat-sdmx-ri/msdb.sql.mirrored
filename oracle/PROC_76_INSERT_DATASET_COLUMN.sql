


CREATE OR REPLACE PROCEDURE INSERT_DATASET_COLUMN(
p_name IN varchar2,
p_description IN varchar2,
p_ds_id IN number,
p_lastretrieval IN timestamp,
p_pk OUT number)
AS
BEGIN
		INSERT INTO DATASET_COLUMN (NAME,DESCRIPTION,DS_ID,LASTRETRIEVAL) VALUES (p_name, p_description, p_ds_id, p_lastretrieval) RETURNING COL_ID INTO p_pk;
END;
/
;
