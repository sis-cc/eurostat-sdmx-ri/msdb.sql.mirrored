CREATE OR REPLACE TRIGGER SET_TRANSCODING_RUL_TR_RULE_ID
BEFORE INSERT
ON TRANSCODING_RULE
FOR EACH ROW
BEGIN
  SELECT TRANSCODING_RUL_TR_RULE_ID_SEQ.NEXTVAL
  INTO :NEW.TR_RULE_ID
  FROM DUAL;
END;
/
;
