CREATE OR REPLACE PROCEDURE INSERT_CODE_MAP(
	p_clm_id IN NUMBER,
	p_source_lcd_id IN NUMBER,
	p_target_lcd_id IN NUMBER,
	p_pk OUT NUMBER) 
AS
BEGIN
		insert into CODE_MAP (CLM_ID, SOURCE_LCD_ID, TARGET_LCD_ID) VALUES (p_clm_id, p_source_lcd_id, p_target_lcd_id);
		select CODE_MAP_SEQ.currval into p_pk from dual;
END;
/
;
