

CREATE OR REPLACE PROCEDURE INSERT_DESC_SOURCE(
p_desc_table IN varchar2,
p_related_field IN varchar2,
p_desc_field IN varchar2,
p_col_id IN number,
p_pk OUT number)
AS
BEGIN
		INSERT INTO DESC_SOURCE (DESC_TABLE,RELATED_FIELD,DESC_FIELD,COL_ID) VALUES (p_desc_table, p_related_field, p_desc_field, p_col_id) RETURNING DESC_SOURCE_ID INTO p_pk;
END;
/
;
