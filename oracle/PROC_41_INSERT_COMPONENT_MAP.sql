CREATE OR REPLACE PROCEDURE INSERT_COMPONENT_MAP(
	p_sm_id IN NUMBER,
	p_source_comp_id IN NUMBER,
	p_target_comp_id IN NUMBER,
	p_pk OUT NUMBER) 
AS
BEGIN
		insert into COMPONENT_MAP (SM_ID, SOURCE_COMP_ID, TARGET_COMP_ID) VALUES (p_sm_id, p_source_comp_id, p_target_comp_id);
		select COMPONENT_MAP_SEQ.currval into p_pk from dual;
END;
/
;
