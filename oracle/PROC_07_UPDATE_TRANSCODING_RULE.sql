CREATE OR REPLACE PROCEDURE UPDATE_TRANSCODING_RULE(
	p_tr_id IN number,
	p_tr_rule_id IN number,
	p_lcd_id IN number,
	p_cd_id IN number,
	p_uncoded_value IN nvarchar2,
	p_pk OUT NUMBER) 
AS
BEGIN
		if (p_tr_rule_id) is null
		then
			INSERT INTO TRANSCODING_RULE (TR_ID,UNCODED_VALUE) VALUES (p_tr_id, p_uncoded_value) RETURNING TR_RULE_ID INTO p_pk;
		else
			-- clean up
			delete from TRANSCODING_RULE_DSD_CODE where TR_RULE_ID = p_tr_rule_id;
			delete from TRANSCODING_RULE_LOCAL_CODE where TR_RULE_ID = p_tr_rule_id;
			update TRANSCODING_RULE SET UNCODED_VALUE=p_uncoded_value where TR_RULE_ID = p_tr_rule_id;
			p_pk := p_tr_rule_id;
		end if;
		
		-- p_cd_id could be null if p_uncoded_value is used
		if (p_cd_id) is not null
		then
			insert into TRANSCODING_RULE_DSD_CODE (TR_RULE_ID, CD_ID) VALUES (p_pk, p_cd_id);
		end if;
		insert into TRANSCODING_RULE_LOCAL_CODE (TR_RULE_ID, LCD_ID) VALUES (p_pk, p_lcd_id);		
END;
/
;
