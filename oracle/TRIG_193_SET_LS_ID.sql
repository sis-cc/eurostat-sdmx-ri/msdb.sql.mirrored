-- because of some issues of utf-8 strings (not confirmed with sp) we don't always use sp for localised_string
CREATE OR REPLACE TRIGGER SET_LS_ID
BEFORE INSERT
ON LOCALISED_STRING
FOR EACH ROW
BEGIN
  select LOCALISED_STRING_SEQ.NEXTVAL into :NEW.LS_ID FROM DUAL;
END;
/
;
