CREATE OR REPLACE TRIGGER SET_TIME_PARTICLE_CONF_TPC_ID
BEFORE INSERT
ON TIME_PARTICLE_CONFIG
FOR EACH ROW
BEGIN
  SELECT TIME_PARTICLE_CONF_TPC_ID_SEQ.NEXTVAL
  INTO :NEW.TPC_ID
  FROM DUAL;
END;
/
;
