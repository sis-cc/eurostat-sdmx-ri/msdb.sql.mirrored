CREATE OR REPLACE PROCEDURE INSERT_CONCEPT_TEXT_FORMAT(
	p_conceptid IN NUMBER, 
	p_facet_type_enum IN NUMBER, 
	p_facet_value IN NVARCHAR2 DEFAULT NULL)
AS
BEGIN
	insert into CONCEPT_TEXT_FORMAT (CON_ID, FACET_TYPE_ENUM, FACET_VALUE) values (p_conceptid, p_facet_type_enum, p_facet_value);
END;
/
;