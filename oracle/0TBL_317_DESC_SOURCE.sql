
CREATE TABLE DESC_SOURCE
	(
	DESC_SOURCE_ID NUMBER(18) NOT NULL, 
	DESC_TABLE VARCHAR(255) NOT NULL,
	RELATED_FIELD VARCHAR(255) NOT NULL,
	DESC_FIELD VARCHAR(255) NULL,
	COL_ID NUMBER (18) NOT NULL
	) 
;

ALTER TABLE DESC_SOURCE ADD CONSTRAINT PK_DESC_SOURCE
	PRIMARY KEY (DESC_SOURCE_ID)
;
