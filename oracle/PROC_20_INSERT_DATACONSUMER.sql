CREATE OR REPLACE PROCEDURE INSERT_DATACONSUMER(
	p_id IN item.id%type,
	p_datacons_sch_id IN DATACONSUMER.DC_SCH_ID%type,
	p_pk OUT item.item_id%type)
AS
BEGIN

		insert into ITEM (ITEM_ID, ID) VALUES (ITEM_SEQ.nextval, p_id);
		select ITEM_SEQ.currval into p_pk from dual;
		insert into DATACONSUMER (DC_ID, DC_SCH_ID) VALUES (p_pk, p_datacons_sch_id);
END;
/
;
