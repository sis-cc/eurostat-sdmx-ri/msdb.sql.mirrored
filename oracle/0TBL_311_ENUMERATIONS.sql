-- CREATE statements with their CONSTRAINTS

-- ENUMERATIONS
CREATE TABLE ENUMERATIONS
	(
	ENUM_ID NUMBER(18) NOT NULL,
	ENUM_NAME VARCHAR2(30) NULL,
	ENUM_VALUE VARCHAR2(50) NULL,
	ENUM_VALUE_DATATYPE VARCHAR2(20) NULL
	) 
;
ALTER TABLE ENUMERATIONS ADD CONSTRAINT
	PK_ENUMERATIONS PRIMARY KEY (ENUM_ID) 
;
