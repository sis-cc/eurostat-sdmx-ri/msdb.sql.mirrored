CREATE OR REPLACE PROCEDURE INSERT_TRANSCODING_SCRIPT(
	p_tr_id IN number, 
	p_title IN varchar2,
	p_content TRANSCODING_SCRIPT.SCRIPT_CONTENT%Type,
	p_pk OUT NUMBER)
AS
BEGIN
	insert into TRANSCODING_SCRIPT (TR_ID, SCRIPT_TITLE, SCRIPT_CONTENT) values (p_tr_id, p_title, p_content);
	select TR_SCRIPT_TR_SCRIPT_ID_SEQ.currval into p_pk from dual;
END;
/
;
