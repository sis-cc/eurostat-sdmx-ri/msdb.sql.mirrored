


CREATE OR REPLACE PROCEDURE INSERT_HEADER_LOCALISED_STRING(
p_type IN varchar2,
p_header_id IN number,
p_party_id IN number,
p_contact_id IN number,
p_language IN varchar2,
p_text IN varchar2,
p_pk OUT number)
AS
BEGIN
		INSERT INTO HEADER_LOCALISED_STRING (TYPE,HEADER_ID,PARTY_ID,CONTACT_ID,LANGUAGE,TEXT) VALUES (p_type, p_header_id, p_party_id, p_contact_id, p_language, p_text) RETURNING HLS_ID INTO p_pk;
END;
/
;
