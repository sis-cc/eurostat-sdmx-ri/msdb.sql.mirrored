


CREATE OR REPLACE PROCEDURE INSERT_DATASET(
p_name IN varchar2,
p_query IN CLOB,
p_order_by_clause IN CLOB,
p_connection_id IN number,
p_description IN varchar2,
p_xml_query IN CLOB,
p_user_id IN number,
p_editor_type IN varchar2,
p_pk OUT number)
AS
BEGIN
		INSERT INTO DATASET (NAME,QUERY,ORDER_BY_CLAUSE,CONNECTION_ID,DESCRIPTION,XML_QUERY,USER_ID,EDITOR_TYPE) VALUES (p_name, p_query, p_order_by_clause, p_connection_id, p_description, p_xml_query, p_user_id, p_editor_type) RETURNING DS_ID INTO p_pk;
END;
/
;
