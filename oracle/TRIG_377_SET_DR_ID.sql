CREATE OR REPLACE TRIGGER SET_DR_ID
BEFORE INSERT
ON REGISTRATION
FOR EACH ROW
BEGIN
  select REGISTRATION_DR_ID_SEQ.NEXTVAL into :NEW.DR_ID FROM DUAL;
END;
/
;
