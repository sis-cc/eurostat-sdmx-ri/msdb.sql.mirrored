CREATE OR REPLACE PROCEDURE PROCESS_CONSTRAINTS_INTERNAL(
	p_art_sys_id IN NUMBER,
	p_comp_id IN VARCHAR2)
AS
BEGIN
	PROCESS_INCLUSIVE_KEY_VALUES (p_art_sys_id => p_art_sys_id, p_comp_id => p_comp_id);
	PROCESS_EXCLUSIVE_KEY_VALUES (p_art_sys_id => p_art_sys_id, p_comp_id => p_comp_id);
END;
/
;
