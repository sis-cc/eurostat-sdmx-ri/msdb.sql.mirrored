CREATE OR REPLACE PROCEDURE INSERT_ORGANISATION_UNIT(
	p_id IN item.id%type,
	p_org_unit_sch_id IN ORGANISATION_UNIT_SCHEME.ORG_UNIT_SCH_ID%type,
	p_parent_org_unit_id IN ORGANISATION_UNIT.PARENT_ORG_UNIT_ID%type,
	p_pk OUT item.item_id%type)
AS
BEGIN

		insert into ITEM (ITEM_ID, ID) VALUES (ITEM_SEQ.nextval, p_id);
		select ITEM_SEQ.currval into p_pk from dual;
		insert into ORGANISATION_UNIT (ORG_UNIT_ID, ORG_UNIT_SCH_ID,PARENT_ORG_UNIT_ID) VALUES (p_pk, p_org_unit_sch_id,p_parent_org_unit_id);
END;
/
;
