create or replace function isEqualVersion(x1 in number, y1  in number, z1  in number, x2  in number, y2  in number, z2  in number)
RETURN NUMBER DETERMINISTIC
as
returnValue number;
xx1 number;
xx2 number;
yy1 number;
yy2 number;
BEGIN
  xx1 := x1;
  xx2 := x2;
  yy1 := y1;
  yy2 := y2;
	if xx1 is null then
		xx1 := 0;
	end if;
	if xx2 is null then
		xx2 := 0;
	end if;
	if yy1 is null then
		yy1 := 0;
	end if;
	if yy2 is null then
		yy2 := 0;
	end if;
  
  if ( xx1 = xx2 ) and ( yy1 = yy2 ) and (((z1 is null) and (z2 is null)) or (((z1 is not null) and (z2 is not null)) and ( z1 = z2 ))) then
    returnValue := 1;
  else
    returnValue := 0;  
  end if;
  return returnValue;
END;
/
;
