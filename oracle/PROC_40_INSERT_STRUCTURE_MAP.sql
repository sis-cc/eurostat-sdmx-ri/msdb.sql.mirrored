CREATE OR REPLACE PROCEDURE INSERT_STRUCTURE_MAP(
	p_id IN ITEM.ID%type,
	p_ss_id IN STRUCTURE_SET.SS_ID%type,
	p_source_str_id IN STRUCTURE_MAP.SOURCE_STR_ID%type,
	p_target_str_id IN STRUCTURE_MAP.TARGET_STR_ID%type,
	p_pk OUT ITEM.ITEM_ID%type)
AS
BEGIN

		insert into ITEM (ITEM_ID, ID) VALUES (ITEM_SEQ.nextval, p_id);
		select ITEM_SEQ.currval into p_pk from dual;
		insert into STRUCTURE_MAP (SM_ID, SS_ID, SOURCE_STR_ID, TARGET_STR_ID) VALUES (p_pk, p_ss_id ,p_source_str_id, p_target_str_id);
END;
/
;
