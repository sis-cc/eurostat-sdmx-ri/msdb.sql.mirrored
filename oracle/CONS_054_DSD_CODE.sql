ALTER TABLE DSD_CODE ADD CONSTRAINT CLS_ID 
	FOREIGN KEY (CL_ID) REFERENCES CODELIST (CL_ID)
ON DELETE CASCADE
;
ALTER TABLE DSD_CODE ADD CONSTRAINT DC_ID 
	FOREIGN KEY (LCD_ID) REFERENCES ITEM (ITEM_ID)
ON DELETE CASCADE
;
ALTER TABLE DSD_CODE ADD CONSTRAINT FK_DSD_CODE_DSD_CODE 
	FOREIGN KEY(PARENT_CODE_ID) REFERENCES DSD_CODE (LCD_ID)
	ON DELETE SET NULL
;
