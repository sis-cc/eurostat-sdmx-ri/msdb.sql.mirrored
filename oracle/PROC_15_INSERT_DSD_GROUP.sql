CREATE OR REPLACE PROCEDURE INSERT_DSD_GROUP(
	p_id IN varchar2,
	p_dsd_id IN number ,
	p_pk OUT NUMBER) 
AS
BEGIN
	insert into DSD_GROUP (GR_ID, ID, DSD_ID) 
		values (DSD_GROUP_SEQ.nextval, p_id, p_dsd_id);
	select DSD_GROUP_SEQ.currval into p_pk from dual;
END;
/
;
