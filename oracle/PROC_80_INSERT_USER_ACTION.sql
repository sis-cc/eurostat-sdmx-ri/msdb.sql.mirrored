CREATE OR REPLACE PROCEDURE INSERT_USER_ACTION(
p_action_when IN timestamp,
p_operation_type IN varchar2,
p_username IN nvarchar2,
p_entity_type IN varchar2,
p_entity_id IN number,
p_entity_name IN nvarchar2,
p_pk OUT number)
AS
BEGIN
		INSERT INTO USER_ACTION (ACTION_WHEN,OPERATION_TYPE,USERNAME,ENTITY_TYPE,ENTITY_ID,ENTITY_NAME) VALUES (p_action_when, p_operation_type, p_username, p_entity_type, p_entity_id, p_entity_name) RETURNING UA_ID INTO p_pk;
END;
/
;
