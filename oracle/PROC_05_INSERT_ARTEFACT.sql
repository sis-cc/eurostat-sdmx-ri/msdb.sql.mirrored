CREATE OR REPLACE PROCEDURE INSERT_ARTEFACT(
	p_id IN varchar2,
	p_version IN varchar2,
	p_agency IN varchar2,
	p_valid_from IN  timestamp DEFAULT NULL,
	p_valid_to IN  timestamp DEFAULT NULL,
	p_is_final IN number DEFAULT NULL,
	p_uri IN nvarchar2,
	p_last_modified IN timestamp DEFAULT NULL,
	p_is_stub IN number,
	p_service_url IN nvarchar2,
	p_structure_url IN nvarchar2,
	p_artefact_type IN varchar2,
	p_pk OUT NUMBER)
AS
  P_VERSION1 NUMBER;
  P_VERSION2 NUMBER;
  P_VERSION3 NUMBER;
BEGIN
    SPLIT_VERSION(
        P_VERSION => P_VERSION,
        P_VERSION1 => P_VERSION1,
        P_VERSION2 => P_VERSION2,
        P_VERSION3 => P_VERSION3
    );
	INSERT INTO ARTEFACT (ART_ID, ID,VERSION1,VERSION2,VERSION3,AGENCY,VALID_FROM,VALID_TO,IS_FINAL,URI,LAST_MODIFIED,IS_STUB,SERVICE_URL,STRUCTURE_URL,ARTEFACT_TYPE) VALUES (ARTEFACT_SEQ.nextval, p_id, p_version1, p_version2, p_version3, p_agency, p_valid_from, p_valid_to, p_is_final, p_uri, p_last_modified, p_is_stub, p_service_url, p_structure_url, p_artefact_type);
	select ARTEFACT_SEQ.currval into p_pk from dual;
END;
/
;
