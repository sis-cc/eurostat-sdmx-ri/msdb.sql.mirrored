CREATE OR REPLACE TRIGGER SET_PARTY_PARTY_ID
BEFORE INSERT
ON PARTY
FOR EACH ROW
BEGIN
  SELECT PARTY_PARTY_ID_SEQ.NEXTVAL
  INTO :NEW.PARTY_ID
  FROM DUAL;
END;
/
;
