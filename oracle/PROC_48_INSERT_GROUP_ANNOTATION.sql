CREATE OR REPLACE PROCEDURE INSERT_GROUP_ANNOTATION 
(
	p_gr_id IN NUMBER,
	p_id IN NVARCHAR2,
	p_title IN NVARCHAR2,
	p_type IN NVARCHAR2,
	p_url IN NVARCHAR2,
	p_pk OUT NUMBER) 
AS
BEGIN
	INSERT_ANNOTATION (p_id => p_id, p_title => p_title, p_type => p_type, p_url => p_url, p_pk => p_pk);
	insert into GROUP_ANNOTATION (ANN_ID, GR_ID) VALUES (p_pk, p_gr_id);
END;
/
;
