CREATE OR REPLACE PROCEDURE INSERT_LOCALISED_STRING(
	p_item_id IN number DEFAULT NULL,
	p_art_id IN number DEFAULT NULL,
	p_text IN nvarchar2,
	p_type IN varchar2,
	p_language IN varchar2,
	p_pk OUT NUMBER) 
AS
BEGIN
	insert into LOCALISED_STRING (ART_ID, ITEM_ID, TEXT, TYPE, LANGUAGE) values (p_art_id, p_item_id, p_text, p_type, p_language);
	select LOCALISED_STRING_SEQ.currval into p_pk from dual;
END;
/
;
