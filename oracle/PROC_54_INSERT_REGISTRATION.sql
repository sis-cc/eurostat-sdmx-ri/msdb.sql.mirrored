create or replace procedure INSERT_REGISTRATION (
	p_id IN varchar2,
	p_valid_from IN TIMESTAMP,
	p_valid_to IN TIMESTAMP,
	p_last_updated IN TIMESTAMP,
	p_index_ts IN NUMBER,
	p_index_ds IN NUMBER,
	p_index_attributes IN NUMBER,
	p_index_reporting_period IN NUMBER,
	p_pa_id IN NUMBER,
	p_datasource_id IN NUMBER,
	p_pk OUT NUMBER)
AS
BEGIN
	insert into REGISTRATION (ID, VALID_FROM, VALID_TO, LAST_UPDATED, INDEX_TIME_SERIES, INDEX_DATA_SET, INDEX_ATTRIBUTES, INDEX_REPORTING_PERIOD, PA_ID, DATA_SOURCE_ID) 
    values (p_id, p_valid_from, p_valid_to, p_last_updated, p_index_ts, p_index_ds, p_index_attributes, p_index_reporting_period, p_pa_id, p_datasource_id);
	select REGISTRATION_DR_ID_SEQ.currval into p_pk from dual;
END;
/
;
