CREATE OR REPLACE PROCEDURE INSERT_COMPONENT(
	p_type IN varchar2,
	p_id IN varchar2,
	p_dsd_id IN number,
	p_con_id IN number,
	p_cl_id IN number DEFAULT NULL,
	p_con_sch_id IN number DEFAULT NULL,
	p_is_freq_dim IN number DEFAULT NULL,
	p_is_measure_dim IN number DEFAULT NULL,
	p_att_ass_level IN varchar2 DEFAULT NULL,
	p_att_status IN varchar2 DEFAULT NULL,
	p_att_is_time_format IN number DEFAULT NULL,
	p_xs_attlevel_ds IN number DEFAULT NULL,
	p_xs_attlevel_group IN number DEFAULT NULL,
	p_xs_attlevel_section IN number DEFAULT NULL,
	p_xs_attlevel_obs IN number DEFAULT NULL,
	p_xs_measure_code IN varchar2 DEFAULT NULL,
	p_pk OUT NUMBER) 
AS
BEGIN
	INSERT_COMPONENT_COMMON(p_pk);
	insert into COMPONENT (COMP_ID,ID, TYPE, DSD_ID, CON_ID, CL_ID, IS_FREQ_DIM, IS_MEASURE_DIM, ATT_ASS_LEVEL, ATT_STATUS, ATT_IS_TIME_FORMAT, XS_ATTLEVEL_DS, XS_ATTLEVEL_GROUP, XS_ATTLEVEL_SECTION, XS_ATTLEVEL_OBS, XS_MEASURE_CODE, CON_SCH_ID) 
		values (p_pk,p_id, p_type, p_dsd_id, p_con_id, p_cl_id, p_is_freq_dim, p_is_measure_dim, p_att_ass_level, p_att_status, p_att_is_time_format, p_xs_attlevel_ds, p_xs_attlevel_group, p_xs_attlevel_section, p_xs_attlevel_obs, p_xs_measure_code, p_con_sch_id);
END;
/
;
