CREATE OR REPLACE PROCEDURE INSERT_ANNOTATION 
(
	p_id IN NVARCHAR2,
	p_title IN NVARCHAR2,
	p_type IN NVARCHAR2,
	p_url IN NVARCHAR2,
	p_pk OUT NUMBER) 
AS
BEGIN
	insert into ANNOTATION (ID, TITLE, TYPE, URL) VALUES (p_id, p_title, p_type, p_url);
	select ANNOTATION_SEQ.currval into p_pk from dual;
END;
/
;
