CREATE OR REPLACE PROCEDURE INSERT_METADATA_TARGET(
 p_id IN  varchar2,
 p_MSD_ID IN number,
 p_pk OUT  number)
IS
BEGIN
  insert into ITEM (ITEM_ID, ID) VALUES (ITEM_SEQ.nextval, p_id);
		select ITEM_SEQ.currval into p_pk from dual;
		insert into METADATA_TARGET (MDT_ID,MSD_ID) VALUES (p_pk,p_MSD_ID);
END;
/
;
