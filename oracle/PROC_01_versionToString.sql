create or replace function versionToString(particle1 in number, particle2 in number, particle3 in number) 
RETURN varchar2 DETERMINISTIC
AS
returnVersion varchar2(50);
p1 number;
p2 number;
BEGIN
	p1 := particle1;
  p2 := particle2;
	if p1 is null then
		p1 := 0;
	end if;
	if p2 is null then
		p2 := 0;
	end if;
	
	if particle3 is null then
		returnVersion := p1 || '.' || p2;
	else
		returnVersion := p1 || '.' || p2 || '.' || particle3; 
	end if;

	return returnVersion;
END;
/
;
