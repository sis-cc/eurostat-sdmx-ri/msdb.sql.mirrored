


CREATE OR REPLACE PROCEDURE INSERT_CONTACT_DETAIL(
p_contact_id IN number,
p_type IN varchar2,
p_value IN varchar2,
p_pk OUT number)
AS
BEGIN
		INSERT INTO CONTACT_DETAIL (CONTACT_ID,TYPE,VALUE) VALUES (p_contact_id, p_type, p_value) RETURNING CD_ID INTO p_pk;
END;
/
;
