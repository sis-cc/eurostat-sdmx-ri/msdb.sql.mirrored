CREATE OR REPLACE PROCEDURE INSERT_CONCEPT(
	p_id IN item.id%type,
	p_con_sch_id IN CONCEPT.CON_SCH_ID%type,
	p_parent_con_id IN CONCEPT.PARENT_CONCEPT_ID%type,
	p_cl_id IN number,
	p_pk OUT item.item_id%type)
AS
BEGIN

		insert into ITEM (ITEM_ID, ID) VALUES (ITEM_SEQ.nextval, p_id);
		select ITEM_SEQ.currval into p_pk from dual;
		insert into CONCEPT (CON_ID, CON_SCH_ID,PARENT_CONCEPT_ID,CL_ID) VALUES (p_pk, p_con_sch_id,p_parent_con_id,p_cl_id);
END;
/
;
