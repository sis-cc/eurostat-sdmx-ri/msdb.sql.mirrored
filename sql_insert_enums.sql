insert into ENUMERATIONS (enum_id, enum_name, enum_value, enum_value_datatype) values (1,'DataType','String','String')
;

insert into ENUMERATIONS (enum_id, enum_name, enum_value, enum_value_datatype) values (2,'DataType','BigInteger','String')
;

insert into ENUMERATIONS (enum_id, enum_name, enum_value, enum_value_datatype) values (3,'DataType','Integer','String')
;

insert into ENUMERATIONS (enum_id, enum_name, enum_value, enum_value_datatype) values (4,'DataType','Long','String')
;

insert into ENUMERATIONS (enum_id, enum_name, enum_value, enum_value_datatype) values (5,'DataType','Short','String')
;

insert into ENUMERATIONS (enum_id, enum_name, enum_value, enum_value_datatype) values (6,'DataType','Decimal','String')
;

insert into ENUMERATIONS (enum_id, enum_name, enum_value, enum_value_datatype) values (7,'DataType','Float','String')
;

insert into ENUMERATIONS (enum_id, enum_name, enum_value, enum_value_datatype) values (8,'DataType','Double','String')
;

insert into ENUMERATIONS (enum_id, enum_name, enum_value, enum_value_datatype) values (9,'DataType','Boolean','String')
;

insert into ENUMERATIONS (enum_id, enum_name, enum_value, enum_value_datatype) values (10,'DataType','DateTime','String')
;

insert into ENUMERATIONS (enum_id, enum_name, enum_value, enum_value_datatype) values (11,'DataType','Time','String')
;

insert into ENUMERATIONS (enum_id, enum_name, enum_value, enum_value_datatype) values (12,'DataType','Date','String')
;

insert into ENUMERATIONS (enum_id, enum_name, enum_value, enum_value_datatype) values (13,'DataType','Year','String')
;

insert into ENUMERATIONS (enum_id, enum_name, enum_value, enum_value_datatype) values (14,'DataType','Month','String')
;

insert into ENUMERATIONS (enum_id, enum_name, enum_value, enum_value_datatype) values (15,'DataType','Day','String')
;

insert into ENUMERATIONS (enum_id, enum_name, enum_value, enum_value_datatype) values (16,'DataType','MonthDay','String')
;

insert into ENUMERATIONS (enum_id, enum_name, enum_value, enum_value_datatype) values (17,'DataType','YearMonth','String')
;

insert into ENUMERATIONS (enum_id, enum_name, enum_value, enum_value_datatype) values (18,'DataType','Duration','String')
;

insert into ENUMERATIONS (enum_id, enum_name, enum_value, enum_value_datatype) values (19,'DataType','Timespan','String')
;

insert into ENUMERATIONS (enum_id, enum_name, enum_value, enum_value_datatype) values (20,'DataType','URI','String')
;

insert into ENUMERATIONS (enum_id, enum_name, enum_value, enum_value_datatype) values (21,'DataType','Count','String')
;

insert into ENUMERATIONS (enum_id, enum_name, enum_value, enum_value_datatype) values (22,'DataType','InclusiveValueRange','String')
;

insert into ENUMERATIONS (enum_id, enum_name, enum_value, enum_value_datatype) values (23,'DataType','ExclusiveValueRange','String')
;

insert into ENUMERATIONS (enum_id, enum_name, enum_value, enum_value_datatype) values (24,'DataType','Incremental','String')
;

insert into ENUMERATIONS (enum_id, enum_name, enum_value, enum_value_datatype) values (25,'DataType','ObservationalTimePeriod','String')
;

insert into ENUMERATIONS (enum_id, enum_name, enum_value, enum_value_datatype) values (26,'DataType','Base64Binary','String')
;

insert into ENUMERATIONS (enum_id, enum_name, enum_value, enum_value_datatype) values (27,'DataType','Alpha','String')
;

insert into ENUMERATIONS (enum_id, enum_name, enum_value, enum_value_datatype) values (28,'DataType','AlphaNumeric','String')
;

insert into ENUMERATIONS (enum_id, enum_name, enum_value, enum_value_datatype) values (29,'DataType','Numeric','String')
;

insert into ENUMERATIONS (enum_id, enum_name, enum_value, enum_value_datatype) values (30,'DataType','StandardTimePeriod','String')
;

insert into ENUMERATIONS (enum_id, enum_name, enum_value, enum_value_datatype) values (31,'DataType','BasicTimePeriod','String')
;

insert into ENUMERATIONS (enum_id, enum_name, enum_value, enum_value_datatype) values (32,'DataType','GregorianTimePeriod','String')
;

insert into ENUMERATIONS (enum_id, enum_name, enum_value, enum_value_datatype) values (33,'DataType','GregorianYear','String')
;

insert into ENUMERATIONS (enum_id, enum_name, enum_value, enum_value_datatype) values (34,'DataType','GregorianYearMonth','String')
;

insert into ENUMERATIONS (enum_id, enum_name, enum_value, enum_value_datatype) values (35,'DataType','GregorianDay','String')
;

insert into ENUMERATIONS (enum_id, enum_name, enum_value, enum_value_datatype) values (36,'DataType','ReportingTimePeriod','String')
;

insert into ENUMERATIONS (enum_id, enum_name, enum_value, enum_value_datatype) values (37,'DataType','ReportingYear','String')
;

insert into ENUMERATIONS (enum_id, enum_name, enum_value, enum_value_datatype) values (38,'DataType','ReportingSemester','String')
;

insert into ENUMERATIONS (enum_id, enum_name, enum_value, enum_value_datatype) values (39,'DataType','ReportingTrimester','String')
;

insert into ENUMERATIONS (enum_id, enum_name, enum_value, enum_value_datatype) values (40,'DataType','ReportingQuarter','String')
;

insert into ENUMERATIONS (enum_id, enum_name, enum_value, enum_value_datatype) values (41,'DataType','ReportingMonth','String')
;

insert into ENUMERATIONS (enum_id, enum_name, enum_value, enum_value_datatype) values (42,'DataType','ReportingWeek','String')
;

insert into ENUMERATIONS (enum_id, enum_name, enum_value, enum_value_datatype) values (43,'DataType','ReportingDay','String')
;

insert into ENUMERATIONS (enum_id, enum_name, enum_value, enum_value_datatype) values (44,'DataType','TimeRange','String')
;

insert into ENUMERATIONS (enum_id, enum_name, enum_value, enum_value_datatype) values (45,'DataType','XHTML','String')
;

insert into ENUMERATIONS (enum_id, enum_name, enum_value, enum_value_datatype) values (46,'DataType','KeyValues','String')
;

insert into ENUMERATIONS (enum_id, enum_name, enum_value, enum_value_datatype) values (47,'DataType','IdentifiableReference','String')
;

insert into ENUMERATIONS (enum_id, enum_name, enum_value, enum_value_datatype) values (48,'DataType','DataSetReference','String')
;

insert into ENUMERATIONS (enum_id, enum_name, enum_value, enum_value_datatype) values (49,'DataType','AttachmentConstraintReference','String')
;

insert into ENUMERATIONS (enum_id, enum_name, enum_value, enum_value_datatype) values (50,'FacetType','isSequence','Boolean')
;

insert into ENUMERATIONS (enum_id, enum_name, enum_value, enum_value_datatype) values (51,'FacetType','isInclusive','Boolean')
;

insert into ENUMERATIONS (enum_id, enum_name, enum_value, enum_value_datatype) values (52,'FacetType','minLength','Integer')
;

insert into ENUMERATIONS (enum_id, enum_name, enum_value, enum_value_datatype) values (53,'FacetType','maxLength','Integer')
;

insert into ENUMERATIONS (enum_id, enum_name, enum_value, enum_value_datatype) values (54,'FacetType','minValue','String')
;

insert into ENUMERATIONS (enum_id, enum_name, enum_value, enum_value_datatype) values (55,'FacetType','maxValue','String')
;

insert into ENUMERATIONS (enum_id, enum_name, enum_value, enum_value_datatype) values (56,'FacetType','startValue','String')
;

insert into ENUMERATIONS (enum_id, enum_name, enum_value, enum_value_datatype) values (57,'FacetType','endValue','String')
;

insert into ENUMERATIONS (enum_id, enum_name, enum_value, enum_value_datatype) values (58,'FacetType','decimals','Integer')
;

insert into ENUMERATIONS (enum_id, enum_name, enum_value, enum_value_datatype) values (59,'FacetType','interval','Double')
;

insert into ENUMERATIONS (enum_id, enum_name, enum_value, enum_value_datatype) values (60,'FacetType','timeInterval','Duration')
;

insert into ENUMERATIONS (enum_id, enum_name, enum_value, enum_value_datatype) values (61,'FacetType','pattern','String')
;

insert into ENUMERATIONS (enum_id, enum_name, enum_value, enum_value_datatype) values (62,'FacetType','enumeration','ItemScheme')
;

insert into ENUMERATIONS (enum_id, enum_name, enum_value, enum_value_datatype) values (63,'FacetType','startTime','String')
;

insert into ENUMERATIONS (enum_id, enum_name, enum_value, enum_value_datatype) values (64,'FacetType','endTime','String')
;

insert into ENUMERATIONS (enum_id, enum_name, enum_value, enum_value_datatype) values (65,'FacetType','isMultiLingual','Boolean')
;

INSERT INTO DB_TIME_FORMAT (FORMAT) VALUES ('Annual')
;

INSERT INTO DB_TIME_FORMAT (FORMAT) VALUES ('Monthly')
;

INSERT INTO DB_TIME_FORMAT (FORMAT) VALUES ('Weekly')
;

INSERT INTO DB_TIME_FORMAT (FORMAT) VALUES ('Quarterly')
;

INSERT INTO DB_TIME_FORMAT (FORMAT) VALUES ('Semester')
;

INSERT INTO DB_TIME_FORMAT (FORMAT) VALUES ('Trimester')
;

INSERT INTO DB_TIME_FORMAT (FORMAT) VALUES ('TimestampOrDate')
;

INSERT INTO DB_TIME_FORMAT (FORMAT) VALUES ('Iso8601Compatible')
;

INSERT INTO TIME_COLUMN_TYPE (COLUMN_TYPE) VALUES ('YEAR')
;

INSERT INTO TIME_COLUMN_TYPE (COLUMN_TYPE) VALUES ('PERIOD')
;

INSERT INTO TIME_COLUMN_TYPE (COLUMN_TYPE) VALUES ('DATE')
;
Insert into ATTACH_TYPES values ('DataProvider')
;
Insert into ATTACH_TYPES values ('DataSet')
;
Insert into ATTACH_TYPES values ('MetadataSet')
;
Insert into ATTACH_TYPES values ('SimpleDataSource')
;
Insert into ATTACH_TYPES values ('DataStructure')
;
Insert into ATTACH_TYPES values ('MetadataStructure')
;
Insert into ATTACH_TYPES values ('Dataflow')
;
Insert into ATTACH_TYPES values ('Metadataflow')
;
Insert into ATTACH_TYPES values ('ProvisionAgreement')
;

INSERT INTO REGISTRY (URL, TECHNOLOGY, NAME, IS_PUBLIC, UPGRADES, PROXY) VALUES ('https://registry.sdmxcloud.org/ws/public/sdmxapi/rest', 'REST', 'Fusion Cloud Registry', 1, 1, 0)
;

INSERT INTO REGISTRY (URL, TECHNOLOGY, NAME, IS_PUBLIC, UPGRADES, PROXY) VALUES ('https://registry.sdmx.org/ws/public/sdmxapi/rest/', 'REST', 'SDMX Global Registry', 1, 1, 0)
;

INSERT INTO REGISTRY (URL, TECHNOLOGY, NAME, IS_PUBLIC, UPGRADES, PROXY) VALUES ('https://ec.europa.eu/tools/cspa_services_global/sdmxregistry/v2.1/SdmxRegistryServicePS?wsdl', 'SoapV21', 'Euro Registry (2.1)', 1, 1, 0)
;

INSERT INTO REGISTRY (URL, TECHNOLOGY, NAME, IS_PUBLIC, UPGRADES, PROXY) VALUES ('https://ec.europa.eu/tools/cspa_services_global/sdmxregistry/v2.0/SdmxRegistryServicePS?wsdl', 'SoapV20', 'Euro Registry (2.0 extended)', 1, 1, 0)
;

INSERT INTO REGISTRY (URL, TECHNOLOGY, NAME, IS_PUBLIC, UPGRADES, PROXY) VALUES ('https://ec.europa.eu/tools/cspa_services_global/sdmxregistry/v2.0std/SdmxRegistryServicePS?wsdl', 'SoapV20', 'Euro Registry (2.0 standard)', 1, 1, 0)
;

INSERT INTO REGISTRY (URL, TECHNOLOGY, NAME, IS_PUBLIC, UPGRADES, PROXY) VALUES ('https://sdmxcentral.imf.org/ws/public/sdmxapi/rest', 'REST', 'IMF Registry', 1, 1, 0)
;

INSERT INTO DATASET_PROPERTY_TYPE(PROPERTY_ID, PROPERTY_NAME, PROPERTY_DATATYPE) VALUES (1, 'OrderByTimePeriodAsc', 'String')
;
 
INSERT INTO DATASET_PROPERTY_TYPE(PROPERTY_ID, PROPERTY_NAME, PROPERTY_DATATYPE) VALUES (2, 'OrderByTimePeriodDesc', 'String')
;

INSERT INTO DATASET_PROPERTY_TYPE(PROPERTY_ID, PROPERTY_NAME, PROPERTY_DATATYPE) VALUES (3, 'CrossApplyColumn', 'String')
;

INSERT INTO DATASET_PROPERTY_TYPE(PROPERTY_ID, PROPERTY_NAME, PROPERTY_DATATYPE) VALUES (4, 'CrossApplySupported', 'Boolean')
;
