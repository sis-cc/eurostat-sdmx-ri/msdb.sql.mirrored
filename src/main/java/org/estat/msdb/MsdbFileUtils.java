/**
 * Copyright (c) 2015 European Commission.
 *
 * Licensed under the EUPL, Version 1.1 or – as soon they
 * will be approved by the European Commission - subsequent
 * versions of the EUPL (the "Licence");
 * You may not use this work except in compliance with the
 * Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl5
 *
 * Unless required by applicable law or agreed to in
 * writing, software distributed under the Licence is
 * distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied.
 * See the Licence for the specific language governing
 * permissions and limitations under the Licence.
 */
package org.estat.msdb;

import sun.misc.Launcher;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Collection;
import java.util.Enumeration;
import java.util.TreeSet;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class MsdbFileUtils {

    /**
     * @param path directory path ending with "/"
     * @return Collection of filenames in provided path sorted alphabetically
     */
    public static Collection<String> getFileNamesFromJarFolder(String path) throws IOException {

        final File jarFile = new File(MsdbFileUtils.class.getProtectionDomain().getCodeSource().getLocation().getPath());

        Collection<String> filenames = new TreeSet<String>();

        if(jarFile.isFile()) {  // Run with JAR file
            JarFile jar;

            jar = new JarFile(jarFile);

            final Enumeration<JarEntry> entries = jar.entries(); //gives ALL entries in jar
            while(entries.hasMoreElements()) {
                final String name = entries.nextElement().getName();


                if (name.startsWith(path) && name.contains(".")) {//filter according to the path and also for being a file
                    filenames.add(name);
                }
            }
            jar.close();

        } else { // Run with IDE (just for local debugging through IDE, this is experimental, may not work
            final URL url = Launcher.class.getResource( path);
            if (url != null) {
                try {
                    final File apps = new File(url.toURI());
                    for (File app : apps.listFiles()) {
                        System.out.println(app);
                    }
                } catch (URISyntaxException ex) {
                    // never happens
                }
            }
        }
        return filenames;
    }
}
