﻿// -----------------------------------------------------------------------
// <copyright file="ResourceDataLocation.cs" company="EUROSTAT">
//   Date Created : 2017-04-27
//   Copyright (c) 2009, 2017 by the European Commission, represented by Eurostat.   All rights reserved.
// 
// Licensed under the EUPL, Version 1.1 or – as soon they
// will be approved by the European Commission - subsequent
// versions of the EUPL (the "Licence");
// You may not use this work except in compliance with the
// Licence.
// You may obtain a copy of the Licence at:
// 
// https://joinup.ec.europa.eu/software/page/eupl 
// 
// Unless required by applicable law or agreed to in
// writing, software distributed under the Licence is
// distributed on an "AS IS" basis,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
// express or implied.
// See the Licence for the specific language governing
// permissions and limitations under the Licence.
// </copyright>
// -----------------------------------------------------------------------
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Estat.Sri.Msdb.Sql {
    public class ResourceDataLocation {
        /// <summary>
        /// The assembly
        /// </summary>
        private readonly Assembly _assembly;

        /// <summary>
        /// The root namespace
        /// </summary>
        private readonly string _rootNamespace;

        /// <summary>
        /// The manifest resource names
        /// </summary>
        private readonly string[] _manifestResourceNames;

        /// <summary>
        /// The normalized resource names
        /// </summary>
        private readonly Dictionary<string, string> _normalizedResourceNames;

        public ResourceDataLocation () {
            var type = this.GetType ();
            this._assembly = type.Assembly;
            this._rootNamespace = type.Namespace;
            this._manifestResourceNames = this._assembly.GetManifestResourceNames ();
            this._normalizedResourceNames = this._manifestResourceNames.ToDictionary (s => s, StringComparer.OrdinalIgnoreCase);
        }
        
        /// <summary>
        /// Gets the resource assembly
        /// </summary>
        public Assembly ResourceAssembly => _assembly;

        /// <summary>
        /// Gets the available resources.
        /// </summary>
        /// <param name="childNamespace">The child namespace.</param>
        /// <returns>The available resources under the specified <paramref name="childNamespace"/></returns>
        public IEnumerable<string> GetAvailableResources (string childNamespace) {
            string actualNameSpace = string.Format (
                CultureInfo.InvariantCulture,
                "{0}.{1}",
                this._rootNamespace,
                childNamespace);
            return this._manifestResourceNames.Where (s => s.StartsWith (actualNameSpace, StringComparison.OrdinalIgnoreCase)).Select (s => s.Substring (this._rootNamespace.Length + 1));
        }

        /// <summary>
        /// Determines if a resource exists
        /// </summary>
        /// <param name="location">The location.</param>
        /// <returns><c>true</c> if it exists; otherwise <c>false</c>. </returns>
        public bool Exists (string location) {
            var fullPath = string.Format (CultureInfo.InvariantCulture, "{0}.{1}", this._rootNamespace, location);
            var folderPath = fullPath + ".";
            return this._normalizedResourceNames.ContainsKey (fullPath) || this._manifestResourceNames.Any (x => x.StartsWith (folderPath, StringComparison.OrdinalIgnoreCase));
        }

        /// <summary>
        /// Gets the data location.
        /// </summary>
        /// <param name="location">The location.</param>
        /// <returns>The path pointing to the resource.</returns>
        public string GetDataLocation (string location) {
            var fullPath = string.Format (CultureInfo.InvariantCulture, "{0}.{1}", this._rootNamespace, location);
            return GetDataLocationFromFullPath (fullPath);
        }

        private string GetDataLocationFromFullPath (string fullPath) {
            if (this._normalizedResourceNames.TryGetValue(fullPath, out string normPath))
            {
                return normPath;
            }

            throw new FileNotFoundException(fullPath);
        }

        public IEnumerable<string> GetDataLocations (string resourcePath) {
            var fullPath = string.Format (CultureInfo.InvariantCulture, "{0}.{1}.", this._rootNamespace, resourcePath);
            foreach (var resource in _manifestResourceNames.Where (x => x.StartsWith (fullPath, StringComparison.OrdinalIgnoreCase)).OrderBy (s => s)) {
                yield return GetDataLocationFromFullPath (resource);
            }
        }

    }
}