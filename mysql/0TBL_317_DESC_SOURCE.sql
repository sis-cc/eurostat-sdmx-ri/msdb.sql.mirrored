
-- DESC_SOURCE
CREATE TABLE DESC_SOURCE
(
	DESC_SOURCE_ID BIGINT NOT NULL AUTO_INCREMENT,
	DESC_TABLE VARCHAR(255) NOT NULL,
	RELATED_FIELD VARCHAR(255) NOT NULL,
	DESC_FIELD VARCHAR(255) NOT NULL,
	COL_ID BIGINT NOT NULL,
	PRIMARY KEY (DESC_SOURCE_ID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 
;
