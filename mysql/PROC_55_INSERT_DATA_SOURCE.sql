
DROP PROCEDURE IF EXISTS INSERT_DATA_SOURCE
;
CREATE PROCEDURE INSERT_DATA_SOURCE (
	IN p_data_url nvarchar(500),
    IN p_wsdl_url nvarchar(500),
    IN p_wadl_url nvarchar(500),
    IN p_is_simple bool,
    IN p_is_rest bool,
    IN p_is_ws bool,
	OUT p_pk bigint)
BEGIN
	insert into DATA_SOURCE (DATA_URL, WSDL_URL, WADL_URL, IS_SIMPLE, IS_REST, IS_WS) 
    values (p_data_url, p_wsdl_url, p_wadl_url, p_is_simple, p_is_rest, p_is_ws);
	set p_pk = LAST_INSERT_ID();
END
;
