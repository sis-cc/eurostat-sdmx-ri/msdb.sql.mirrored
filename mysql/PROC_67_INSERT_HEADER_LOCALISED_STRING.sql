
DROP PROCEDURE IF EXISTS INSERT_HEADER_LOCALISED_STRING
;
CREATE PROCEDURE INSERT_HEADER_LOCALISED_STRING(
IN p_type varchar(50),
IN p_header_id bigint,
IN p_party_id bigint,
IN p_contact_id bigint,
IN p_language varchar(50),
IN p_text varchar(255),
OUT p_pk bigint)
BEGIN
		INSERT INTO HEADER_LOCALISED_STRING (TYPE,HEADER_ID,PARTY_ID,CONTACT_ID,LANGUAGE,TEXT) VALUES (p_type, p_header_id, p_party_id, p_contact_id, p_language, p_text);
		set p_pk = LAST_INSERT_ID();
END;

;
