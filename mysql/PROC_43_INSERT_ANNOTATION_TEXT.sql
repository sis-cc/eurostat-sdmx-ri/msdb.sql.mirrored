DROP PROCEDURE IF EXISTS INSERT_ANNOTATION_TEXT
;
CREATE PROCEDURE INSERT_ANNOTATION_TEXT 
(
	IN p_ann_id bigint,
	IN p_language varchar(50),
	IN p_text nvarchar(4000),
	OUT p_pk bigint) 
BEGIN
	insert into ANNOTATION_TEXT (ANN_ID, LANGUAGE, TEXT) VALUES (p_ann_id, p_language, p_text);
	set p_pk = LAST_INSERT_ID();
END
;
