ALTER TABLE HLEVEL 
    ADD CONSTRAINT LEVEL_ARTEFACT_FK FOREIGN KEY 
    ( 
     LEVEL_ID
    ) 
    REFERENCES ARTEFACT 
    ( 
     ART_ID
    ) 
;
ALTER TABLE HLEVEL 
    ADD CONSTRAINT LEVEL_HIERARCHY_FK FOREIGN KEY 
    ( 
     H_ID
    ) 
    REFERENCES HIERARCHY 
    ( 
     H_ID
    ) 
    ON DELETE CASCADE 
;
ALTER TABLE HLEVEL 
    ADD CONSTRAINT LEVEL_LEVEL_FK FOREIGN KEY 
    ( 
     PARENT_LEVEL_ID
    ) 
    REFERENCES HLEVEL 
    ( 
     LEVEL_ID
    ) 
    ON DELETE SET NULL
;
