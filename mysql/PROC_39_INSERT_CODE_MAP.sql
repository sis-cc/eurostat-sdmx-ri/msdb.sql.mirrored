DROP PROCEDURE IF EXISTS INSERT_CODE_MAP
;
CREATE PROCEDURE INSERT_CODE_MAP(
	IN p_clm_id bigint,
	IN p_source_lcd_id bigint,
	IN p_target_lcd_id bigint,
	OUT p_pk bigint) 
BEGIN
		insert into CODE_MAP (CLM_ID, SOURCE_LCD_ID, TARGET_LCD_ID) VALUES (p_clm_id, p_source_lcd_id, p_target_lcd_id);
		set p_pk = LAST_INSERT_ID();
END
;
