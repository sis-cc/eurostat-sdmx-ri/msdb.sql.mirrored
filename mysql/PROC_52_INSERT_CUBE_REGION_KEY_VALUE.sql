DROP PROCEDURE IF EXISTS INSERT_CUBE_REGION_KEY_VALUE
;
CREATE PROCEDURE INSERT_CUBE_REGION_KEY_VALUE(
	IN p_cube_region_id  bigint,
	IN p_member_id  varchar(255),
	IN p_component_type  varchar(50),
	IN p_include  bigint,
	IN p_start_period  varchar(50),
	IN p_end_period  varchar(50),
	IN p_start_inclusive  bigint,
	IN p_end_inclusive  bigint,
	OUT p_pk bigint)
BEGIN
	INSERT INTO CUBE_REGION_KEY_VALUE
           (CUBE_REGION_ID, MEMBER_ID, COMPONENT_TYPE, INCLUDE,START_PERIOD,END_PERIOD,START_INCLUSIVE,END_INCLUSIVE)
     VALUES
           (p_cube_region_id, p_member_id, p_component_type, p_include,p_start_period,p_end_period,p_start_inclusive,p_end_inclusive);
		set p_pk = LAST_INSERT_ID();
END
;
