DROP PROCEDURE IF EXISTS INSERT_LOCAL_CODE
;
CREATE PROCEDURE INSERT_LOCAL_CODE(
	IN p_id varchar(255),
	IN p_column_id bigint,
	OUT p_pk bigint) 
BEGIN
		insert into ITEM (ID) VALUES (p_id);
		set p_pk = LAST_INSERT_ID();
		insert into LOCAL_CODE (LCD_ID, COLUMN_ID) VALUES (p_pk, p_column_id);
END
;
