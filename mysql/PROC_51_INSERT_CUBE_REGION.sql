
DROP PROCEDURE IF EXISTS INSERT_CUBE_REGION
;
CREATE PROCEDURE INSERT_CUBE_REGION(
      IN p_cont_cons_id  bigint,
      IN p_include  bigint,
      OUT p_pk  bigint)
BEGIN
       INSERT INTO CUBE_REGION
           (CONT_CONS_ID, INCLUDE)
     VALUES
           (p_cont_cons_id, p_include);
      	set p_pk = LAST_INSERT_ID();
END
;
