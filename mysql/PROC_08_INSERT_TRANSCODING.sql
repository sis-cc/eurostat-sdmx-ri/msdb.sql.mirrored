
DROP PROCEDURE IF EXISTS INSERT_TRANSCODING
;
CREATE PROCEDURE INSERT_TRANSCODING(
	IN p_map_id bigint,
	IN p_expression varchar(150),
	OUT p_pk bigint)
BEGIN
	insert into TRANSCODING (MAP_ID, EXPRESSION) values (p_map_id,p_expression);
	set p_pk = LAST_INSERT_ID();
END
;
