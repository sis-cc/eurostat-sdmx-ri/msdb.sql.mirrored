

CREATE TABLE DB_VERSION
(
	SYS_ID BIGINT NOT NULL AUTO_INCREMENT,
	VERSION NVARCHAR(250) NOT NULL,
	PRIMARY KEY (SYS_ID, VERSION)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
;
