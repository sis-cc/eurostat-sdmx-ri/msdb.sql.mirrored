
DROP PROCEDURE IF EXISTS INSERT_TEMPLATE_MAPPING
;
CREATE PROCEDURE INSERT_TEMPLATE_MAPPING(
IN p_column_name varchar(50),
IN p_col_desc nvarchar(255),
IN p_component_id varchar(50),
IN p_component_type varchar(50),
IN p_con_id bigint,
IN p_item_scheme_id bigint,
OUT p_pk bigint)
BEGIN
		call INSERT_COMPONENT_COMMON(p_pk);
		INSERT INTO TEMPLATE_MAPPING (TEMPLATE_ID,COLUMN_NAME,COL_DESC,COMPONENT_ID,COMPONENT_TYPE,CON_ID,ITEM_SCHEME_ID) VALUES (p_pk, p_column_name, p_col_desc, p_component_id, p_component_type, p_con_id, p_item_scheme_id);
END;
;
