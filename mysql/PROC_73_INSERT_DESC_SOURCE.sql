
DROP PROCEDURE IF EXISTS INSERT_DESC_SOURCE
;
CREATE PROCEDURE INSERT_DESC_SOURCE(
IN p_desc_table varchar(255),
IN p_related_field varchar(255),
IN p_desc_field varchar(255),
IN p_col_id bigint,
OUT p_pk bigint)
BEGIN
		INSERT INTO DESC_SOURCE (DESC_TABLE,RELATED_FIELD,DESC_FIELD,COL_ID) VALUES (p_desc_table, p_related_field, p_desc_field, p_col_id);
		set p_pk = LAST_INSERT_ID();
END;

;
