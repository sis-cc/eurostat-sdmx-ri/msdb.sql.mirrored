
DROP PROCEDURE IF EXISTS INSERT_PROXY_SETTINGS
;
CREATE PROCEDURE INSERT_PROXY_SETTINGS(
	IN p_enable_proxy bool,
	IN p_url varchar(250),
	IN p_authentication bool,
	IN p_username varchar(50),
	IN p_password varchar(250),
	OUT p_pk bigint) 
BEGIN
	insert into PROXY_SETTINGS (ENABLE_PROXY, URL, AUTHENTICATION, USERNAME, PASSWORD) 
    values (p_enable_proxy, p_url, p_authentication, p_username, p_password);
	set p_pk = LAST_INSERT_ID();
END
;
