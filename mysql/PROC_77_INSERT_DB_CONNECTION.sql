
DROP PROCEDURE IF EXISTS INSERT_DB_CONNECTION
;
CREATE PROCEDURE INSERT_DB_CONNECTION(
IN p_db_name varchar(1000),
IN p_db_type varchar(50),
IN p_name varchar(255),
IN p_db_password varchar(50),
IN p_db_port int,
IN p_db_server varchar(100),
IN p_db_user varchar(50),
IN p_ado_connection_string varchar(2000),
IN p_jdbc_connection_string varchar(2000),
IN p_properties VARCHAR (1000),
IN p_user_id bigint,
OUT p_pk bigint)
BEGIN
		INSERT INTO DB_CONNECTION (DB_NAME,DB_TYPE,NAME,DB_PASSWORD,DB_PORT,DB_SERVER,DB_USER,ADO_CONNECTION_STRING,JDBC_CONNECTION_STRING,PROPERTIES,USER_ID) VALUES (p_db_name, p_db_type, p_name, p_db_password, p_db_port, p_db_server, p_db_user, p_ado_connection_string, p_jdbc_connection_string,p_properties, p_user_id);
		set p_pk = LAST_INSERT_ID();
END;

;
