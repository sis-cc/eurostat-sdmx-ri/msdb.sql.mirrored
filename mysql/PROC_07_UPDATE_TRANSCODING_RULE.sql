
DROP PROCEDURE IF EXISTS UPDATE_TRANSCODING_RULE
;
CREATE PROCEDURE UPDATE_TRANSCODING_RULE(
	IN p_tr_id bigint,
	IN p_tr_rule_id bigint,
	IN p_lcd_id bigint,
	IN p_cd_id bigint,
	IN p_uncoded_value nvarchar(255),
	OUT p_pk bigint) 
BEGIN
		if p_tr_rule_id is null then
			INSERT INTO TRANSCODING_RULE (TR_ID,UNCODED_VALUE) VALUES (p_tr_id, p_uncoded_value);
			set p_pk = LAST_INSERT_ID();
		else
			-- clean up
			delete from TRANSCODING_RULE_DSD_CODE where TR_RULE_ID = p_tr_rule_id;
			delete from TRANSCODING_RULE_LOCAL_CODE where TR_RULE_ID = p_tr_rule_id;
			update TRANSCODING_RULE SET UNCODED_VALUE=p_uncoded_value where TR_RULE_ID = p_tr_rule_id;
			set p_pk = p_tr_rule_id;
		end if;

		-- p_cd_id could be null if p_uncoded_value is used
		if p_cd_id is not null then	
			insert into TRANSCODING_RULE_DSD_CODE (TR_RULE_ID, CD_ID) VALUES (p_pk, p_cd_id);
		end if;
		insert into TRANSCODING_RULE_LOCAL_CODE (TR_RULE_ID, LCD_ID) VALUES (p_pk, p_lcd_id);
END
;
