DROP PROCEDURE IF EXISTS PROCESS_CONSTRAINTS
;
CREATE PROCEDURE PROCESS_CONSTRAINTS(
	p_art_agency varchar(50),
	p_art_id varchar(255),
	p_art_version varchar(50),
	p_art_type varchar(30),
	p_cl_sys_id bigint,
	p_const_type varchar(30),
	out p_is_cl_partial bit)
BEGIN
	DECLARE pra_sys_id BIGINT;
	DECLARE df_sys_id BIGINT;
    DECLARE dsd_sys_id BIGINT; 
    DECLARE comp_sys_id BIGINT;
	DECLARE comp_id VARCHAR(255);
    DECLARE done BIT;
    
    DECLARE constraints_exist INT;
    DECLARE comp_count INT;
	IF (p_art_type = 'ProvisionAgreement') THEN
		BEGIN
        DECLARE art_cursor CURSOR FOR 
        SELECT PR.PA_ID, DF.DF_ID, DF.DSD_ID
		FROM PROVISION_AGREEMENT PR INNER JOIN ARTEFACT_VIEW PRA ON PR.PA_ID = PRA.ART_ID
		INNER JOIN DATAFLOW DF ON DF.DF_ID = PR.SU_ID
		WHERE PRA.AGENCY = p_art_agency AND PRA.ID = p_art_id AND PRA.VERSION = p_art_version;
		DECLARE CONTINUE HANDLER FOR NOT FOUND BEGIN END;
		OPEN art_cursor;
        FETCH art_cursor INTO pra_sys_id, df_sys_id, dsd_sys_id;
        CLOSE art_cursor;
        END;
	ELSEIF (p_art_type = 'Dataflow') THEN
		BEGIN
        DECLARE art_cursor CURSOR FOR 
        SELECT DF.DF_ID, DF.DSD_ID
		FROM DATAFLOW DF INNER JOIN ARTEFACT_VIEW DFA ON DFA.ART_ID = DF.DF_ID
		WHERE DFA.AGENCY = p_art_agency AND DFA.ID = p_art_id AND DFA.VERSION = p_art_version;
        DECLARE CONTINUE HANDLER FOR NOT FOUND BEGIN END;
		OPEN art_cursor;
        FETCH art_cursor INTO df_sys_id, dsd_sys_id;
        CLOSE art_cursor;
        END;
	ELSEIF (p_art_type = 'DataStructure') THEN
		BEGIN
        DECLARE art_cursor CURSOR FOR 
        SELECT DSD.DSD_ID
		FROM DSD DSD INNER JOIN ARTEFACT_VIEW ART ON ART.ART_ID = DSD.DSD_ID
		WHERE ART.ID = p_art_id AND ART.AGENCY = p_art_agency AND ART.VERSION = p_art_version;
        DECLARE CONTINUE HANDLER FOR NOT FOUND BEGIN END;
		OPEN art_cursor;
        FETCH art_cursor INTO dsd_sys_id;
        CLOSE art_cursor;
        END;
	END IF;
    
    -- check it there are constraints for the given codelist and for all components that use the codelist.
    SELECT COUNT(COMP.ID) INTO comp_count FROM COMPONENT COMP 
    LEFT OUTER JOIN CONTENT_CONSTRAINT_V CC ON CC.MEMBER_ID = COMP.ID AND CC.ART_ID IN (pra_sys_id, df_sys_id, dsd_sys_id)
    WHERE COMP.DSD_ID = dsd_sys_id AND COMP.CL_ID = p_cl_sys_id AND CC.ART_ID IS NULL;
    
    -- check if the requested artefact has attached constraints.
    SELECT COUNT(1) INTO constraints_exist FROM CONTENT_CONSTRAINT_V CC 
    WHERE CC.ART_ID IN (pra_sys_id, df_sys_id, dsd_sys_id);
    
    IF (constraints_exist = 0 OR comp_count > 0) THEN
		SET p_is_cl_partial = 0;
	ELSE
		BEGIN
		DECLARE components_cursor CURSOR FOR
		SELECT COMP.COMP_ID, COMP.ID FROM COMPONENT COMP WHERE DSD_ID = dsd_sys_id AND COMP.CL_ID = p_cl_sys_id;
    	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = 1;
		SET done = 0;
		
		OPEN components_cursor;
		lp: LOOP
			FETCH components_cursor INTO comp_sys_id, comp_id;  
			IF done = 1 THEN 
				LEAVE lp; 
			END IF;
			
            -- copy the initial codelist
			INSERT INTO TEMP_PROCESS_CONSTRAINTS_TABLE(COMP_ID, CODE_SYS_ID, CODE_ID, PARENT_CODE_SYS_ID, INCLUDED)
			SELECT COMP.ID, CD.LCD_ID, IT.ID, CD.PARENT_CODE_ID, 1
			FROM COMPONENT COMP INNER JOIN CODELIST CL ON COMP.CL_ID = CL.CL_ID
			INNER JOIN DSD_CODE CD ON CL.CL_ID = CD.CL_ID INNER JOIN ITEM IT ON CD.LCD_ID = IT.ITEM_ID
			WHERE COMP.COMP_ID = comp_sys_id AND CL.CL_ID = p_cl_sys_id;
			
			-- start the processing of constraints
			IF (p_art_type = 'ProvisionAgreement') THEN
				-- process DSD constraints
				CALL PROCESS_CONSTRAINTS_INTERNAL (dsd_sys_id, comp_id);
				-- process Dataflow constraints
				CALL PROCESS_CONSTRAINTS_INTERNAL (df_sys_id, comp_id);
				-- process Provision Agreement constraints
				CALL PROCESS_CONSTRAINTS_INTERNAL (pra_sys_id, comp_id);
			ELSEIF (p_art_type = 'Dataflow') THEN
				-- process DSD constraints
				CALL PROCESS_CONSTRAINTS_INTERNAL (dsd_sys_id, comp_id);
				-- process Dataflow constraints
				CALL PROCESS_CONSTRAINTS_INTERNAL (df_sys_id, comp_id);
			ELSEIF (p_art_type = 'DataStructure') THEN
				-- process DSD constraints
				CALL PROCESS_CONSTRAINTS_INTERNAL (dsd_sys_id, comp_id);
			END IF;
			
		END LOOP lp;
        CLOSE components_cursor;
		END;
		
		SET p_is_cl_partial = 1;
    END IF;
END
;
