
DROP PROCEDURE IF EXISTS INSERT_HEADER
;
CREATE PROCEDURE INSERT_HEADER(
IN p_test bool,
IN p_dataset_agency varchar(255),
IN p_df_id bigint,
IN p_structure_type varchar(50),
IN p_dataset_id varchar(250),
IN p_dataset_action varchar(50),
OUT p_pk bigint)
BEGIN
		INSERT INTO HEADER (TEST,DATASET_AGENCY,DF_ID,STRUCTURE_TYPE,DATASET_ID,DATASET_ACTION) 
      VALUES (p_test, p_dataset_agency, p_df_id, p_structure_type, p_dataset_id, p_dataset_action);
		set p_pk = LAST_INSERT_ID();
END;

;
