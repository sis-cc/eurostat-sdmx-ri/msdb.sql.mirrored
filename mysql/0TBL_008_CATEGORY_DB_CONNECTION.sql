

CREATE TABLE CATEGORY_DB_CONNECTION
(
	CAT_ID BIGINT NOT NULL,
	CONNECTION_ID BIGINT NOT NULL,
	PRIMARY KEY (CAT_ID, CONNECTION_ID),
	KEY (CAT_ID),
	KEY (CONNECTION_ID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
;
