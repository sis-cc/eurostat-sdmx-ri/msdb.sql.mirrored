-- $$
-- DELIMITER ;


DROP PROCEDURE IF EXISTS INSERT_TEXT_FORMAT
;
CREATE PROCEDURE INSERT_TEXT_FORMAT(
	IN p_compid BIGINT, 
	IN p_facet_type_enum BIGINT, 
	IN p_facet_value VARCHAR(51),
	OUT p_pk BIGINT)
BEGIN
	insert into TEXT_FORMAT (COMP_ID, FACET_TYPE_ENUM, FACET_VALUE) values (p_compid, p_facet_type_enum, p_facet_value);
	set p_pk = LAST_INSERT_ID();
END
;
