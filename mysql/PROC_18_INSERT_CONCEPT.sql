DROP PROCEDURE IF EXISTS INSERT_CONCEPT
;
CREATE PROCEDURE INSERT_CONCEPT(
	IN p_id varchar(255),
	IN p_con_sch_id bigint,
	IN p_parent_con_id bigint,
	IN p_cl_id bigint,
	OUT p_pk bigint) 
BEGIN
		insert into ITEM (ID) VALUES (p_id);
		set p_pk = LAST_INSERT_ID();
		insert into CONCEPT (CON_ID, CON_SCH_ID,PARENT_CONCEPT_ID,CL_ID) VALUES (p_pk, p_con_sch_id,p_parent_con_id,p_cl_id);
	
END
;
