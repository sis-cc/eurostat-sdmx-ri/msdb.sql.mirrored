DROP PROCEDURE IF EXISTS INSERT_ORGANISATION_UNIT
;
CREATE PROCEDURE INSERT_ORGANISATION_UNIT(
	IN p_id varchar(255),
	IN p_org_unit_sch_id bigint,
	IN p_parent_org_unit_id bigint,
	OUT p_pk bigint) 
BEGIN
		insert into ITEM (ID) VALUES (p_id);
		set p_pk = LAST_INSERT_ID();
		insert into ORGANISATION_UNIT (ORG_UNIT_ID, ORG_UNIT_SCH_ID,PARENT_ORG_UNIT_ID) VALUES (p_pk, p_org_unit_sch_id,p_parent_org_unit_id);
	
END
;
