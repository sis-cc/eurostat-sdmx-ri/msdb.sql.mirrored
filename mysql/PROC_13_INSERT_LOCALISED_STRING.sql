
DROP PROCEDURE IF EXISTS INSERT_LOCALISED_STRING
;
CREATE PROCEDURE INSERT_LOCALISED_STRING(
	IN p_item_id bigint,
	IN p_art_id bigInt,
	IN p_text nvarchar(4000),
	IN p_type varchar(10),
	IN p_language varchar(50),
	OUT p_pk bigint) 
BEGIN
	insert into LOCALISED_STRING (ART_ID, ITEM_ID, TEXT, TYPE, LANGUAGE) values (p_art_id, p_item_id, p_text, p_type, p_language);
	set p_pk = LAST_INSERT_ID();
END
;
