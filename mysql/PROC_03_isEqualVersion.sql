
-- DELIMITER $$
create function `isEqualVersion`(x1 bigint, y1 bigint, z1 bigint, x2 bigint, y2 bigint, z2 bigint)
RETURNS BOOL DETERMINISTIC
BEGIN
	if x1 is null then
		set x1 = 0;
	end if;
	if x2 is null then
		set x2 = 0;
	end if;
	if y1 is null then
		set y1 = 0;
	end if;
	if y2 is null then
		set y2 = 0;
	end if;
	RETURN ( x1 = x2 ) and ( y1 = y2 ) and (((z1 is null) and (z2 is null)) or (((z1 is not null) and (z2 is not null)) and ( z1 = z2 )));
END;
;
