
DROP PROCEDURE IF EXISTS INSERT_CONTACT_DETAIL
;
CREATE PROCEDURE INSERT_CONTACT_DETAIL(
IN p_contact_id bigint,
IN p_type varchar(50),
IN p_value varchar(255),
OUT p_pk bigint)
BEGIN
		INSERT INTO CONTACT_DETAIL (CONTACT_ID,TYPE,VALUE) VALUES (p_contact_id, p_type, p_value);
		set p_pk = LAST_INSERT_ID();
END;

;
