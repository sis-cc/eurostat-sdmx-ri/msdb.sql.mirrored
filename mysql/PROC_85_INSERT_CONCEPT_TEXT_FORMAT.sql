DROP PROCEDURE IF EXISTS INSERT_CONCEPT_TEXT_FORMAT
;
CREATE PROCEDURE INSERT_CONCEPT_TEXT_FORMAT(
    IN p_conceptid BIGINT, 
	IN p_facet_type_enum BIGINT, 
	IN p_facet_value VARCHAR(51)
	)
BEGIN
	insert into CONCEPT_TEXT_FORMAT (CON_ID, FACET_TYPE_ENUM, FACET_VALUE) values (p_conceptid, p_facet_type_enum, p_facet_value);
END
;


