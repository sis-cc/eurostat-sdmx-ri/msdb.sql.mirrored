
DROP PROCEDURE IF EXISTS INSERT_REGISTRATION
;
CREATE PROCEDURE INSERT_REGISTRATION (
	IN p_id varchar(50),
	IN p_valid_from datetime,
	IN p_valid_to datetime,
	IN p_last_updated datetime,
    IN p_index_ts bool,
    IN p_index_ds bool,
    IN p_index_attributes bool,
    IN p_index_reporting_period bool,
    IN p_pa_id bigint,
    IN p_datasource_id bigint,
	OUT p_pk bigint)
BEGIN
	insert into REGISTRATION (ID, VALID_FROM, VALID_TO, LAST_UPDATED, INDEX_TIME_SERIES, INDEX_DATA_SET, INDEX_ATTRIBUTES, INDEX_REPORTING_PERIOD, PA_ID, DATA_SOURCE_ID) 
    values (p_id, p_valid_from, p_valid_to, p_last_updated, p_index_ts, p_index_ds, p_index_attributes, p_index_reporting_period, p_pa_id, p_datasource_id);
	set p_pk = LAST_INSERT_ID();
END
;
