ALTER TABLE REGISTRATION ADD CONSTRAINT FK_REGISTRATION_PROVISION FOREIGN KEY (PA_ID) 
REFERENCES PROVISION_AGREEMENT (PA_ID)
;
ALTER TABLE REGISTRATION ADD CONSTRAINT FK_REGISTRATION_DATASOURCE FOREIGN KEY (DATA_SOURCE_ID)
REFERENCES DATA_SOURCE (DATA_SOURCE_ID)
ON DELETE CASCADE
;
