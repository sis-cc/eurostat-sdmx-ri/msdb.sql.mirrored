
DROP PROCEDURE IF EXISTS INSERT_USER_ACTION
;
CREATE PROCEDURE INSERT_USER_ACTION(
IN p_action_when datetime,
IN p_operation_type varchar(50),
IN p_username nvarchar(50),
IN p_entity_type varchar(50),
IN p_entity_id bigint,
IN p_entity_name nvarchar(255),
OUT p_pk bigint)
BEGIN
		INSERT INTO USER_ACTION (ACTION_WHEN,OPERATION_TYPE,USERNAME,ENTITY_TYPE,ENTITY_ID,ENTITY_NAME) VALUES (p_action_when, p_operation_type, p_username, p_entity_type, p_entity_id, p_entity_name);
		set p_pk = LAST_INSERT_ID();
END;
;
