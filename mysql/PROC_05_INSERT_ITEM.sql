DROP PROCEDURE IF EXISTS INSERT_ITEM
;
CREATE PROCEDURE INSERT_ITEM(
	IN p_id varchar(255),
	OUT p_pk bigint)
BEGIN
	insert into ITEM (ID) values (p_id);
	set p_pk = LAST_INSERT_ID();
END
;
