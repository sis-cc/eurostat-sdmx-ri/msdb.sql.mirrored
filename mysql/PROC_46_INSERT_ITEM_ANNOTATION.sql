DROP PROCEDURE IF EXISTS INSERT_ITEM_ANNOTATION
;
CREATE PROCEDURE INSERT_ITEM_ANNOTATION 
(
	IN p_item_id bigint,
	IN p_id nvarchar(50),
	IN p_title nvarchar(4000),
	IN p_type nvarchar(50),
	IN p_url nvarchar(1000),
	OUT p_pk bigint) 
BEGIN
	CALL INSERT_ANNOTATION (p_id, p_title, p_type, p_url, p_pk);
	insert into ITEM_ANNOTATION (ANN_ID, ITEM_ID) VALUES (p_pk, p_item_id);
END
;
