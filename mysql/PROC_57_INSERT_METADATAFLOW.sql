DROP PROCEDURE IF EXISTS INSERT_METADATAFLOW
;
CREATE PROCEDURE INSERT_METADATAFLOW(
	IN p_id varchar(255),
    IN p_version  varchar(50),
    IN p_agency varchar(50),
    IN p_valid_from   datetime,
    IN p_valid_to   datetime,
    IN p_is_final  bigint,
    IN p_uri  nvarchar(255),
    IN p_last_modified  datetime,
	IN p_msd_id  bigint,
	OUT p_pk bigint)
BEGIN
     call INSERT_STRUCTURE_USAGE(p_id, p_version, p_agency, p_valid_from, p_valid_to, p_is_final, p_uri, p_last_modified, 0, null, null, 'Metadataflow', p_pk);
	 INSERT INTO METADATAFLOW
           (MSD_ID,MDF_ID)
     VALUES
           (p_msd_id,p_pk);
END
;
