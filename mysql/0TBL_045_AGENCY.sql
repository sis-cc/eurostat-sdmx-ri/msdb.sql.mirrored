

CREATE TABLE AGENCY
(
	AG_ID BIGINT NOT NULL,
	AG_SCH_ID BIGINT NOT NULL,
	PRIMARY KEY (AG_ID),
	KEY (AG_ID),
	KEY (AG_SCH_ID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
;
