DROP PROCEDURE IF EXISTS INSERT_DSD_CODE
;
CREATE PROCEDURE INSERT_DSD_CODE(
	IN p_id varchar(255),
	IN p_cl_id bigint,
	IN p_parent_code_id bigint,
	OUT p_pk bigint) 
BEGIN
		insert into ITEM (ID) VALUES (p_id);
		set p_pk = LAST_INSERT_ID();
		insert into DSD_CODE (LCD_ID, CL_ID, PARENT_CODE_ID) VALUES (p_pk, p_cl_id, p_parent_code_id);
	
END
;
