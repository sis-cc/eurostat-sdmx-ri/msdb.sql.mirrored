DROP PROCEDURE IF EXISTS INSERT_COMPONENT_MAP
;
CREATE PROCEDURE INSERT_COMPONENT_MAP(
	IN p_sm_id bigint,
	IN p_source_comp_id bigint,
	IN p_target_comp_id bigint,
	OUT p_pk bigint) 
BEGIN
		insert into COMPONENT_MAP (SM_ID, SOURCE_COMP_ID, TARGET_COMP_ID) VALUES (p_sm_id, p_source_comp_id, p_target_comp_id);
		set p_pk = LAST_INSERT_ID();
END
;
