
DROP PROCEDURE IF EXISTS INSERT_COMPONENT_MAPPING
;
CREATE PROCEDURE INSERT_COMPONENT_MAPPING(
IN p_map_set_id bigint,
IN p_type varchar(10),
IN p_constant nvarchar(255),
IN p_default_value nvarchar(255),
IN p_is_metadata bool,
OUT p_pk bigint)
BEGIN
		INSERT INTO COMPONENT_MAPPING (MAP_SET_ID,TYPE,CONSTANT,DEFAULT_VALUE, IS_METADATA) VALUES (p_map_set_id, p_type, p_constant, p_default_value, p_is_metadata);
		set p_pk = LAST_INSERT_ID();
END;

;
