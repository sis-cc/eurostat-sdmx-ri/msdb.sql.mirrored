DROP PROCEDURE IF EXISTS INSERT_AGENCY
;
CREATE PROCEDURE INSERT_AGENCY(
	IN p_id varchar(255),
	IN p_ag_sch_id bigint,
	OUT p_pk bigint) 
BEGIN
		insert into ITEM (ID) VALUES (p_id);
		set p_pk = LAST_INSERT_ID();
		insert into AGENCY (AG_ID, AG_SCH_ID) VALUES (p_pk, p_ag_sch_id);
	
END
;
