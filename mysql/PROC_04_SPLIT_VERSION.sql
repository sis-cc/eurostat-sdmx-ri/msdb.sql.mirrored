-- DELIMITER $$
DROP PROCEDURE IF EXISTS split_version
;
CREATE PROCEDURE `split_version`(IN p_version varchar(50), out p_version1 bigint, out p_version2 bigint, out p_version3 bigint)
BEGIN
    declare point1 int;
    declare point2 int;
    set p_version1 = getVersion(p_version, 1);
    set p_version2 = getVersion(p_version, 2);
    set p_version3 = getVersion(p_version, 3);
    if p_version1 is null then
        set p_version1 = 0;
    end if;
    if p_version2 is null then
        set p_version2 = 0;
    end if;
END
;
