
DROP PROCEDURE IF EXISTS INSERT_PARTY
;
CREATE PROCEDURE INSERT_PARTY(
IN p_id varchar(255),
IN p_header_id bigint,
IN p_type varchar(50),
OUT p_pk bigint)
BEGIN
		INSERT INTO PARTY (ID,HEADER_ID,TYPE) VALUES (p_id, p_header_id, p_type);
		set p_pk = LAST_INSERT_ID();
END;

;
