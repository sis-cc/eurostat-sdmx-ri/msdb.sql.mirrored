
DROP PROCEDURE IF EXISTS INSERT_DATASET_COLUMN
;
CREATE PROCEDURE INSERT_DATASET_COLUMN(
IN p_name varchar(255),
IN p_description varchar(1024),
IN p_ds_id bigint,
IN p_lastretrieval datetime,
OUT p_pk bigint)
BEGIN
		INSERT INTO DATASET_COLUMN (NAME,DESCRIPTION,DS_ID,LASTRETRIEVAL) VALUES (p_name, p_description, p_ds_id, p_lastretrieval);
		set p_pk = LAST_INSERT_ID();
END;

;
