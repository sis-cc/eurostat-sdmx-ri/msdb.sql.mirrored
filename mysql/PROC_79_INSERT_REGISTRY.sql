
DROP PROCEDURE IF EXISTS INSERT_REGISTRY
;
CREATE PROCEDURE INSERT_REGISTRY(
  IN p_url varchar(250),
  IN p_username varchar(250),
  IN p_password varchar(250),
  IN p_description varchar(250),
  IN p_technology varchar(250),
  IN p_name varchar(250),
  IN p_is_public bool,
  IN p_upgrades bool,
  IN p_proxy bool,
  OUT p_pk bigint)
BEGIN
     INSERT INTO REGISTRY (URL,USERNAME,PASSWORD,DESCRIPTION,TECHNOLOGY,NAME,IS_PUBLIC,UPGRADES,PROXY) VALUES (p_url,p_username, p_password,p_description,p_technology,p_name,p_is_public,p_upgrades,p_proxy);
     set p_pk = LAST_INSERT_ID();
END;
;
