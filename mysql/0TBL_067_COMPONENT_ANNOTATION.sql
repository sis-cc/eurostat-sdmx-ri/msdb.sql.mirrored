CREATE TABLE COMPONENT_ANNOTATION
(
	ANN_ID bigint NOT NULL, 
	COMP_ID bigint NOT NULL,
	PRIMARY KEY(ANN_ID, COMP_ID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
;
