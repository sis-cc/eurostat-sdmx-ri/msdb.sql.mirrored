DROP PROCEDURE IF EXISTS INSERT_CONTENT_CONSTRAINT
;
CREATE PROCEDURE INSERT_CONTENT_CONSTRAINT(
      IN p_id varchar(255),
      IN p_version  varchar(50),
      IN p_agency varchar(50),
      IN p_valid_from   datetime,
      IN p_valid_to   datetime,
      IN p_is_final  bigint,
      IN p_uri  nvarchar(255),
      IN p_last_modified  datetime,
      IN p_actual_data bigint,
      IN p_periodicity varchar(50),
      IN p_offset varchar(50),
      IN p_tolerance varchar(50),
	  IN p_attach_type varchar(20),
	  IN dp_id bigint,
	  IN set_id varchar(255),
	  IN simple_data_source nvarchar(1024),
	  IN p_start_time datetime,
      IN p_end_time datetime,
      OUT p_pk bigint )
BEGIN
	call INSERT_ARTEFACT(p_id, p_version, p_agency, p_valid_from, p_valid_to, p_is_final, p_uri, p_last_modified, 0, null, null, 'ContentConstraint', p_pk);

       INSERT INTO CONTENT_CONSTRAINT
           (CONT_CONS_ID, ACTUAL_DATA, PERIODICITY, `OFFSET`, TOLERANCE,ATTACH_TYPE,DP_ID,SET_ID,SIMPLE_DATA_SOURCE,START_TIME,END_TIME)
     VALUES
           (p_pk, p_actual_data, p_periodicity, p_offset, p_tolerance,p_attach_type,dp_id,set_id,simple_data_source,p_start_time,p_end_time);
END
;
