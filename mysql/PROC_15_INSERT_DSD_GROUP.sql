
DROP PROCEDURE IF EXISTS INSERT_DSD_GROUP
;
CREATE PROCEDURE INSERT_DSD_GROUP(
	IN p_id varchar(50) ,
	IN p_dsd_id bigint ,
	OUT p_pk bigint) 
BEGIN
	insert into DSD_GROUP (ID, DSD_ID) 
		values (p_id, p_dsd_id);
	set p_pk = LAST_INSERT_ID();
END
;
