create function `versionToString`(particle1 bigint, particle2 bigint, particle3 bigint) 
RETURNS varchar(50) DETERMINISTIC
BEGIN
	declare version varchar(50);
	if particle1 is null then
		set particle1 = 0;
	end if;
	if particle2 is null then
		set particle2 = 0;
	end if;
	
	if particle3 is null then
		set version = concat(particle1, '.', particle2);
	else
		set version = concat(particle1, '.', particle2, '.', particle3);
	end if;

	return version;
END
;
