ALTER TABLE METADATA_ATTRIBUTE  ADD  CONSTRAINT FK_METADATA_ATTRIBUTE_CODELIST FOREIGN KEY(CL_ID)
REFERENCES CODELIST (CL_ID)
;
ALTER TABLE METADATA_ATTRIBUTE  ADD  CONSTRAINT FK_MD_ATTR_COMP_COMMON FOREIGN KEY(MTD_ATTR_ID)
REFERENCES COMPONENT_COMMON (COMP_ID)
ON DELETE CASCADE
;
ALTER TABLE METADATA_ATTRIBUTE  ADD  CONSTRAINT FK_METADATA_ATTRIBUTE_CONCEPT FOREIGN KEY(CON_ID)
REFERENCES CONCEPT (CON_ID)
;
ALTER TABLE METADATA_ATTRIBUTE  ADD  CONSTRAINT FK_MD_ATTR_MD_ATTR FOREIGN KEY(PARENT)
REFERENCES METADATA_ATTRIBUTE (MTD_ATTR_ID)
 ON DELETE SET NULL
;
ALTER TABLE METADATA_ATTRIBUTE  ADD  CONSTRAINT FK_METADATA_ATTRIBUTE_REPORT_STRUCTURE FOREIGN KEY(RS_ID)
REFERENCES REPORT_STRUCTURE (RS_ID)
;
