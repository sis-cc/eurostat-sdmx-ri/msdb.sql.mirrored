
DROP PROCEDURE IF EXISTS INSERT_CUBE_REGION_VALUE
;
CREATE PROCEDURE INSERT_CUBE_REGION_VALUE(
	IN p_cube_region_key_value_id  bigint,
	IN p_member_value  varchar(150),
	IN p_include  bigint)
BEGIN
	INSERT INTO CUBE_REGION_VALUE
           (CUBE_REGION_KEY_VALUE_ID, MEMBER_VALUE, INCLUDE)
     VALUES
           (p_cube_region_key_value_id, p_member_value, p_include);
END
;
