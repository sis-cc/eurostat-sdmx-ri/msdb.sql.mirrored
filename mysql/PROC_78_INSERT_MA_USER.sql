
DROP PROCEDURE IF EXISTS INSERT_MA_USER
;
CREATE PROCEDURE INSERT_MA_USER(
  IN p_username varchar(250),
  IN p_password varchar(250),
  IN p_user_type smallint,
  IN p_salt varchar(250),
  IN p_algorithm varchar(50),
  OUT p_pk bigint)
BEGIN
     INSERT INTO MA_USER (USERNAME,PASSWORD,USER_TYPE,SALT,ALGORITHM) VALUES (p_username, p_password, p_user_type, p_salt, p_algorithm);
     set p_pk = LAST_INSERT_ID();
END;
;
