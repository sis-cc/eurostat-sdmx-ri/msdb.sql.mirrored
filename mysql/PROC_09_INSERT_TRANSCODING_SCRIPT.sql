
DROP PROCEDURE IF EXISTS INSERT_TRANSCODING_SCRIPT
;
CREATE PROCEDURE INSERT_TRANSCODING_SCRIPT(
	IN p_tr_id bigint,
	IN p_title varchar(128),
	IN p_content text,
	OUT p_pk bigint)
BEGIN
	insert into TRANSCODING_SCRIPT (TR_ID, SCRIPT_TITLE, SCRIPT_CONTENT) values (p_tr_id, p_title, p_content);
	set p_pk = LAST_INSERT_ID();
END
;
