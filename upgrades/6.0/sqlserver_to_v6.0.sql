-- updates for 6.0
ALTER TABLE MA_USER ADD SALT VARCHAR(250) NULL, ALGORITHM VARCHAR(50) NULL
;

UPDATE MA_USER SET ALGORITHM = 'MD5' WHERE SALT is null
;

IF EXISTS (SELECT * FROM dbo.SYSOBJECTS WHERE id = object_id('INSERT_MA_USER') AND  OBJECTPROPERTY(id, 'IsProcedure') = 1)
 DROP PROC INSERT_MA_USER
;

CREATE PROCEDURE INSERT_MA_USER
@p_username varchar(250) ,
@p_password varchar(250) ,
@p_user_type smallint ,
@p_salt varchar(250) ,
@p_algorithm varchar(50) ,
@p_pk bigint OUT
AS
BEGIN
  SET NOCOUNT ON;
  SET XACT_ABORT ON;
  DECLARE @starttrancount int;
  SET @starttrancount = @@TRANCOUNT;
  BEGIN TRY
    IF @starttrancount = 0 
      BEGIN TRANSACTION

    INSERT INTO MA_USER (USERNAME,PASSWORD,USER_TYPE,SALT,ALGORITHM) VALUES (@p_username, @p_password, @p_user_type, @p_salt, @p_algorithm);
    set @p_pk = SCOPE_IDENTITY();
    IF @starttrancount = 0
      COMMIT TRANSACTION
  END TRY
  BEGIN CATCH
      DECLARE @ErrorMessage NVARCHAR(4000);
      DECLARE @ErrorSeverity INT;
      DECLARE @ErrorState INT;

      SELECT 
        @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();
      IF XACT_STATE() <> 0 AND @starttrancount = 0 
        ROLLBACK TRANSACTION
      RAISERROR (@ErrorMessage, -- Message text.
          @ErrorSeverity, -- Severity.
          @ErrorState -- State.
        );
    END CATCH
END

;

GO

;
UPDATE MA_USER SET USER_TYPE = 127 WHERE USER_TYPE = 1
;
UPDATE MA_USER SET USER_TYPE = 15 WHERE USER_TYPE = 2
;

CREATE TABLE REGISTRY (
    ID bigint IDENTITY(1,1) NOT NULL,
    URL nvarchar(250) NOT NULL,
    USERNAME nvarchar(50),
    PASSWORD nvarchar(250),
	DESCRIPTION nvarchar(250),
	TECHNOLOGY nvarchar(250),
    PRIMARY KEY (ID)
);

IF EXISTS (SELECT * FROM dbo.SYSOBJECTS WHERE id = object_id('INSERT_REGISTRY') AND  OBJECTPROPERTY(id, 'IsProcedure') = 1)
 DROP PROC INSERT_REGISTRY
;


CREATE PROCEDURE INSERT_REGISTRY
@p_url varchar(250),
@p_username varchar(250),
@p_password varchar(250),
@p_description varchar(250),
@p_technology varchar(250),
@p_pk bigint out
AS
BEGIN
  SET NOCOUNT ON;
  SET XACT_ABORT ON;
  DECLARE @starttrancount int;
  SET @starttrancount = @@TRANCOUNT;
  BEGIN TRY
    IF @starttrancount = 0 
      BEGIN TRANSACTION

    INSERT INTO REGISTRY (URL,USERNAME,PASSWORD,DESCRIPTION,TECHNOLOGY) VALUES (@p_url,@p_username, @p_password,@p_description,@p_technology);
    set @p_pk = SCOPE_IDENTITY();
    IF @starttrancount = 0
      COMMIT TRANSACTION
  END TRY
  BEGIN CATCH
      DECLARE @ErrorMessage NVARCHAR(4000);
      DECLARE @ErrorSeverity INT;
      DECLARE @ErrorState INT;

      SELECT 
        @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();
      IF XACT_STATE() <> 0 AND @starttrancount = 0 
        ROLLBACK TRANSACTION
      RAISERROR (@ErrorMessage, -- Message text.
          @ErrorSeverity, -- Severity.
          @ErrorState -- State.
        );
    END CATCH
END

;
-- always last
update DB_VERSION SET VERSION = '6.0'
;

