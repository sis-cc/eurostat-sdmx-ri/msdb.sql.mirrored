-- updates for 6.0

ALTER TABLE MA_USER ADD ( SALT VARCHAR(250) NULL, ALGORITHM VARCHAR(50) NULL )
;

UPDATE MA_USER SET ALGORITHM = 'MD5' WHERE SALT is null
;

DROP PROCEDURE IF EXISTS INSERT_MA_USER
;

CREATE PROCEDURE INSERT_MA_USER(
  IN p_username varchar(250),
  IN p_password varchar(250),
  IN p_user_type smallint,
  IN p_salt varchar(250),
  IN p_algorithm varchar(50),
  OUT p_pk bigint)
BEGIN
     INSERT INTO MA_USER (USERNAME,PASSWORD,USER_TYPE,SALT,ALGORITHM) VALUES (p_username, p_password, p_user_type, p_salt, p_algorithm);
     set p_pk = LAST_INSERT_ID();
END;
;

UPDATE MA_USER SET USER_TYPE = 127 WHERE USER_TYPE = 1
;
UPDATE MA_USER SET USER_TYPE = 15 WHERE USER_TYPE = 2
;

CREATE TABLE REGISTRY
(
	ID BIGINT NOT NULL AUTO_INCREMENT,
	URL NVARCHAR(250),
	USERNAME NVARCHAR(250),
	PASSWORD NVARCHAR(250),
	DESCRIPTION NVARCHAR(250),
	TECHNOLOGY NVARCHAR(250),
	PRIMARY KEY (ID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
;

DROP PROCEDURE IF EXISTS INSERT_REGISTRY
;

CREATE PROCEDURE INSERT_REGISTRY(
  IN p_url varchar(250),
  IN p_username varchar(250),
  IN p_password varchar(250),
  IN p_description varchar(250),
  IN p_technology varchar(250),
  OUT p_pk bigint)
BEGIN
     INSERT INTO REGISTRY (URL,USERNAME,PASSWORD,DESCRIPTION,TECHNOLOGY) VALUES (p_url,p_username, p_password,p_description,p_technology);
     set p_pk = LAST_INSERT_ID();
END;
;



-- always last
update DB_VERSION SET VERSION = '6.0'
;

