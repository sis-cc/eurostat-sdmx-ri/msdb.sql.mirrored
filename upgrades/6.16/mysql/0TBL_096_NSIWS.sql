
CREATE TABLE NSIWS (
	ID BIGINT NOT NULL AUTO_INCREMENT,
	URL NVARCHAR(250),
	USERNAME NVARCHAR(250),
	PASSWORD NVARCHAR(250),
	DESCRIPTION NVARCHAR(250),
	TECHNOLOGY NVARCHAR(250),
	NAME NVARCHAR(250) NULL,
	PROXY BOOL NOT NULL DEFAULT 0,
PRIMARY KEY(ID),
CONSTRAINT CHK_TECH CHECK (TECHNOLOGY IN ('SoapV20', 'SoapV21', 'REST'))
) ENGINE=InnoDB DEFAULT CHARSET=utf8
;
