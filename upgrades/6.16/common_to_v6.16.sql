-- old MA for some reason updated DATAFLOW.MAP_SET_ID instead of MAPPING_SET.DF_ID
update MAPPING_SET
  set DF_ID = (SELECT DF_ID FROM DATAFLOW WHERE DATAFLOW.MAP_SET_ID = MAPPING_SET.MAP_SET_ID)
where DF_ID is null
;
