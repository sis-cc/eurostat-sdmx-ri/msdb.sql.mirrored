
-- Normally not needed, but duplicated entries may have been allowed by bugs
-- Sub queries needed for MariaDB 10.2.x
UPDATE DATASET
SET NAME =  CONCAT(SUBSTR(NAME,0,206), CONCAT(' Duplicate entry ', SYS_GUID()))
WHERE DS_ID NOT IN (
	SELECT MIN(c.DS_ID) FROM  DATASET c
	GROUP BY c.NAME
	HAVING COUNT(*) > 1
) AND DS_ID IN (
	SELECT G1.DS_ID FROM DATASET G1
	INNER JOIN  DATASET G2 ON G1.NAME = G2.NAME AND G1.DS_ID <> G2.DS_ID
)
;

ALTER TABLE DATASET ADD CONSTRAINT UQ_DATASET UNIQUE(NAME)
;
