-- Increase NAME size to 255

ALTER TABLE DB_CONNECTION MODIFY NAME VARCHAR(255) NOT NULL
;

-- Normally not needed, but duplicated entries may have been allowed by bugs
-- Sub queries needed for MariaDB 10.2.x
-- we don't need substring because NAME was varchar(50) and now it is 255
-- Sub queries needed for MariaDB 10.2.x
-- Append to duplicate names 50+ characters to avoid generating duplicates
UPDATE DB_CONNECTION
SET NAME =  CONCAT(NAME, CONCAT(' Duplicate entry ', UUID()))
WHERE CONNECTION_ID NOT IN (
	SELECT MIN(c.CONNECTION_ID) FROM (select * FROM DB_CONNECTION) as c
	GROUP BY c.NAME
	HAVING COUNT(*) > 1
) AND CONNECTION_ID IN (
	SELECT G1.CONNECTION_ID FROM (select * FROM DB_CONNECTION) as G1
	INNER JOIN (select * FROM DB_CONNECTION) as G2 ON G1.NAME = G2.NAME AND G1.CONNECTION_ID <> G2.CONNECTION_ID
)
;

ALTER TABLE DB_CONNECTION ADD CONSTRAINT UQ_DB_CONNECTION UNIQUE(NAME)
;
