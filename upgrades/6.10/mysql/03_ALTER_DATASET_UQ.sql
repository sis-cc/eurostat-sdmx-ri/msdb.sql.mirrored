
-- Normally not needed, but duplicated entries may have been allowed by bugs
-- Sub queries needed for MariaDB 10.2.x
UPDATE DATASET
SET NAME =  CONCAT(SUBSTRING(NAME,0,202), CONCAT(' Duplicate entry ', UUID()))
WHERE DS_ID NOT IN (
	SELECT MIN(c.DS_ID) FROM (select * FROM DATASET) as c
	GROUP BY c.NAME
	HAVING COUNT(*) > 1
) AND DS_ID IN (
	SELECT G1.DS_ID FROM (select * FROM DATASET) as G1
	INNER JOIN (select * FROM DATASET) as G2 ON G1.NAME = G2.NAME AND G1.DS_ID <> G2.DS_ID
)
;

ALTER TABLE DATASET ADD CONSTRAINT UQ_DATASET UNIQUE(NAME)
;
