INSERT INTO REGISTRY (URL, TECHNOLOGY, NAME, IS_PUBLIC, UPGRADES, PROXY) VALUES ('https://registry.sdmxcloud.org/ws/public/sdmxapi/rest', 'REST', 'Fusion Cloud Registry', 1, 1, 0)
;

INSERT INTO REGISTRY (URL, TECHNOLOGY, NAME, IS_PUBLIC, UPGRADES, PROXY) VALUES ('https://registry.sdmx.org/ws/public/sdmxapi/rest/', 'REST', 'SDMX Global Registry', 1, 1, 0)
;

INSERT INTO REGISTRY (URL, TECHNOLOGY, NAME, IS_PUBLIC, UPGRADES, PROXY) VALUES ('https://ec.europa.eu/tools/cspa_services_global/sdmxregistry/v2.1/SdmxRegistryServicePS?wsdl', 'SoapV21', 'Euro Registry (2.1)', 1, 1, 0)
;

INSERT INTO REGISTRY (URL, TECHNOLOGY, NAME, IS_PUBLIC, UPGRADES, PROXY) VALUES ('https://ec.europa.eu/tools/cspa_services_global/sdmxregistry/v2.0/SdmxRegistryServicePS?wsdl', 'SoapV20', 'Euro Registry (2.0 extended)', 1, 1, 0)
;

INSERT INTO REGISTRY (URL, TECHNOLOGY, NAME, IS_PUBLIC, UPGRADES, PROXY) VALUES ('https://ec.europa.eu/tools/cspa_services_global/sdmxregistry/v2.0std/SdmxRegistryServicePS?wsdl', 'SoapV20', 'Euro Registry (2.0 standard)', 1, 1, 0)
;

INSERT INTO REGISTRY (URL, TECHNOLOGY, NAME, IS_PUBLIC, UPGRADES, PROXY) VALUES ('https://sdmxcentral.imf.org/ws/public/sdmxapi/rest', 'REST', 'IMF Registry', 1, 1, 0)
;