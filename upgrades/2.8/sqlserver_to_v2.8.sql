--- change to nvarchar
alter table COMPONENT_MAPPING alter column CONSTANT nvarchar(255) null
;

CREATE TABLE DESC_SOURCE
(
	DESC_SOURCE_ID BIGINT IDENTITY (1,1) NOT NULL,
	DESC_TABLE varchar(255) NOT NULL,
	RELATED_FIELD varchar(255) NOT NULL,
	DESC_FIELD varchar(255) NOT NULL,
	COL_ID bigint NOT NULL
) 
;

ALTER TABLE DESC_SOURCE ADD CONSTRAINT PK_DESC_SOURCE
	PRIMARY KEY CLUSTERED (DESC_SOURCE_ID)
;

ALTER TABLE DESC_SOURCE ADD CONSTRAINT FK_DATASET_COLUMN_DESC_SOURCE FOREIGN KEY(COL_ID)
REFERENCES DATASET_COLUMN (COL_ID)
ON DELETE CASCADE
;

ALTER TABLE DATAFLOW ADD PRODUCTION INT NOT NULL DEFAULT 0
;

UPDATE DATAFLOW SET PRODUCTION = 0
;

update dataflow set production = 1 where df_id in ( 
SELECT
  T.DF_ID AS SYSID
FROM
  DATAFLOW             AS T
INNER JOIN MAPPING_SET AS MSET
ON
  MSET.MAP_SET_ID = T.MAP_SET_ID
INNER JOIN ARTEFACT AS A
ON
  T.DF_ID = A.ART_ID
WHERE
  (
    MSET.MAP_SET_ID NOT IN
    (
      SELECT
        MAP_SET_ID
      FROM
        (
          SELECT
            cm.MAP_SET_ID,
            COUNT(*) AS c
          FROM
            COM_COL_MAPPING_COLUMN     AS l
          INNER JOIN COMPONENT_MAPPING AS cm
          ON
            l.MAP_ID = cm.MAP_ID
          INNER JOIN COM_COL_MAPPING_COMPONENT AS s
          ON
            s.MAP_ID = cm.MAP_ID
          WHERE
            (
              l.MAP_ID NOT IN
              (
                SELECT
                  MAP_ID
                FROM
                  TRANSCODING
              )
            )
          GROUP BY
            cm.MAP_SET_ID,
            cm.MAP_ID
        ) AS t_2
      WHERE
        (
          c > 1
        )
    )
  )
AND
  (
    (
      SELECT
        COUNT(c.COMP_ID) AS error
      FROM
        COMPONENT         AS c
      INNER JOIN DATAFLOW AS d
      ON
        c.DSD_ID = d.DSD_ID
      WHERE
        (
          c.TYPE LIKE '%Dimension'
        )
      AND
        (
          c.IS_MEASURE_DIM IS NULL
        )
      AND
        (
          d.DF_ID = T.DF_ID
        )
      AND
        (
          c.COMP_ID NOT IN
          (
            SELECT
              l.COMP_ID
            FROM
              COMPONENT_MAPPING                  AS cm
            INNER JOIN COM_COL_MAPPING_COMPONENT AS l
            ON
              cm.MAP_ID = l.MAP_ID
            INNER JOIN DATAFLOW AS d
            ON
              cm.MAP_SET_ID = d.MAP_SET_ID
            WHERE
              (
                d.DF_ID = T.DF_ID
              )
          )
        )
      OR
        (
          c.TYPE = 'Attribute'
        )
      AND
        (
          d.DF_ID = T.DF_ID
        )
      AND
        (
          c.COMP_ID NOT IN
          (
            SELECT
              l.COMP_ID
            FROM
              COMPONENT_MAPPING                  AS cm
            INNER JOIN COM_COL_MAPPING_COMPONENT AS l
            ON
              cm.MAP_ID = l.MAP_ID
            INNER JOIN DATAFLOW AS d
            ON
              cm.MAP_SET_ID = d.MAP_SET_ID
            WHERE
              (
                d.DF_ID = T.DF_ID
              )
          )
        )
      AND
        (
          c.ATT_STATUS = 'Mandatory'
        )
    )
    = 0
  )
AND
  (
    (
      SELECT
        COUNT(c.COMP_ID) AS error
      FROM
        COMPONENT         AS c
      INNER JOIN DATAFLOW AS d
      ON
        c.DSD_ID = d.DSD_ID
      WHERE
        (
          c.TYPE LIKE '%Dimension'
        )
      AND
        (
          c.IS_MEASURE_DIM = 1
        )
      AND
        (
          d.DF_ID = T.DF_ID
        )
      AND
        (
          c.COMP_ID NOT IN
          (
            SELECT
              l.COMP_ID
            FROM
              COMPONENT_MAPPING                  AS cm
            INNER JOIN COM_COL_MAPPING_COMPONENT AS l
            ON
              cm.MAP_ID = l.MAP_ID
            INNER JOIN DATAFLOW AS d
            ON
              cm.MAP_SET_ID = d.MAP_SET_ID
            WHERE
              (
                d.DF_ID = T.DF_ID
              )
          )
        )
      OR
        (
          c.TYPE = 'PrimaryMeasure'
        )
      AND
        (
          d.DF_ID = T.DF_ID
        )
      AND
        (
          c.COMP_ID NOT IN
          (
            SELECT
              l.COMP_ID
            FROM
              COMPONENT_MAPPING                  AS cm
            INNER JOIN COM_COL_MAPPING_COMPONENT AS l
            ON
              cm.MAP_ID = l.MAP_ID
            INNER JOIN DATAFLOW AS d
            ON
              cm.MAP_SET_ID = d.MAP_SET_ID
            WHERE
              (
                d.DF_ID = T.DF_ID
              )
          )
        )
    )
    = 0
  )
OR
  (
    MSET.MAP_SET_ID NOT IN
    (
      SELECT
        MAP_SET_ID
      FROM
        (
          SELECT
            cm.MAP_SET_ID,
            COUNT(*) AS c
          FROM
            COM_COL_MAPPING_COLUMN     AS l
          INNER JOIN COMPONENT_MAPPING AS cm
          ON
            l.MAP_ID = cm.MAP_ID
          INNER JOIN COM_COL_MAPPING_COMPONENT AS s
          ON
            s.MAP_ID = cm.MAP_ID
          WHERE
            (
              l.MAP_ID NOT IN
              (
                SELECT
                  MAP_ID
                FROM
                  TRANSCODING AS TRANSCODING_1
              )
            )
          GROUP BY
            cm.MAP_SET_ID,
            cm.MAP_ID
        ) AS t_1
      WHERE
        (
          c > 1
        )
    )
  )
AND
  (
    (
      SELECT
        COUNT(c.COMP_ID) AS error
      FROM
        COMPONENT         AS c
      INNER JOIN DATAFLOW AS d
      ON
        c.DSD_ID = d.DSD_ID
      WHERE
        (
          c.TYPE LIKE '%Dimension'
        )
      AND
        (
          c.IS_MEASURE_DIM IS NULL
        )
      AND
        (
          d.DF_ID = T.DF_ID
        )
      AND
        (
          c.COMP_ID NOT IN
          (
            SELECT
              l.COMP_ID
            FROM
              COMPONENT_MAPPING                  AS cm
            INNER JOIN COM_COL_MAPPING_COMPONENT AS l
            ON
              cm.MAP_ID = l.MAP_ID
            INNER JOIN DATAFLOW AS d
            ON
              cm.MAP_SET_ID = d.MAP_SET_ID
            WHERE
              (
                d.DF_ID = T.DF_ID
              )
          )
        )
      OR
        (
          c.TYPE = 'Attribute'
        )
      AND
        (
          d.DF_ID = T.DF_ID
        )
      AND
        (
          c.COMP_ID NOT IN
          (
            SELECT
              l.COMP_ID
            FROM
              COMPONENT_MAPPING                  AS cm
            INNER JOIN COM_COL_MAPPING_COMPONENT AS l
            ON
              cm.MAP_ID = l.MAP_ID
            INNER JOIN DATAFLOW AS d
            ON
              cm.MAP_SET_ID = d.MAP_SET_ID
            WHERE
              (
                d.DF_ID = T.DF_ID
              )
          )
        )
      AND
        (
          c.ATT_STATUS = 'Mandatory'
        )
    )
    = 0
  )
AND
  (
    (
      SELECT
        COUNT(c.COMP_ID) AS error
      FROM
        COMPONENT         AS c
      INNER JOIN DATAFLOW AS d
      ON
        c.DSD_ID = d.DSD_ID
      WHERE
        (
          c.TYPE = 'CrossSectionalMeasure'
        )
      AND
        (
          d.DF_ID = T.DF_ID
        )
      AND
        (
          c.COMP_ID NOT IN
          (
            SELECT
              l.COMP_ID
            FROM
              COMPONENT_MAPPING                  AS cm
            INNER JOIN COM_COL_MAPPING_COMPONENT AS l
            ON
              cm.MAP_ID = l.MAP_ID
            INNER JOIN DATAFLOW AS d
            ON
              cm.MAP_SET_ID = d.MAP_SET_ID
            WHERE
              (
                d.DF_ID = T.DF_ID
              )
          )
        )
    )
    = 0
  )
AND
  (
    (
      SELECT
        COUNT(c.COMP_ID) AS hasXS
      FROM
        COMPONENT         AS c
      INNER JOIN DATAFLOW AS d
      ON
        c.DSD_ID = d.DSD_ID
      WHERE
        (
          c.TYPE = 'CrossSectionalMeasure'
        )
      AND
        (
          d.DF_ID = T.DF_ID
        )
    )
    > 0
  )
)
;

-- always last
update DB_VERSION SET VERSION = '2.8'
;

