--- change to nvarchar
alter table COMPONENT_MAPPING modify CONSTANT nvarchar2(255) default null
;

CREATE TABLE DESC_SOURCE
	(
	DESC_SOURCE_ID NUMBER(18) NOT NULL, 
	DESC_TABLE VARCHAR(255) NOT NULL,
	RELATED_FIELD VARCHAR(255) NOT NULL,
	DESC_FIELD VARCHAR(255) NULL,
	COL_ID NUMBER (18) NOT NULL
	) 
;

ALTER TABLE DESC_SOURCE ADD CONSTRAINT PK_DESC_SOURCE
	PRIMARY KEY (DESC_SOURCE_ID)
;

ALTER TABLE DESC_SOURCE ADD CONSTRAINT FK_DATASET_COLUMN_DESC_SOURCE FOREIGN KEY(COL_ID)
REFERENCES DATASET_COLUMN (COL_ID)
ON DELETE CASCADE
;

ALTER TABLE DATAFLOW ADD (PRODUCTION NUMBER(3) default 0 NOT NULL)
;

UPDATE DATAFLOW SET PRODUCTION = 0
;

update dataflow set production = 1 
where df_id in (
SELECT
  T.DF_ID
FROM
  DATAFLOW T
INNER JOIN MAPPING_SET  MSET
ON
  MSET.MAP_SET_ID = T.MAP_SET_ID
INNER JOIN ARTEFACT  A
ON
  T.DF_ID = A.ART_ID
WHERE
  (
    MSET.MAP_SET_ID NOT IN
    (
      SELECT
        MAP_SET_ID
      FROM
        (
          SELECT
            cm.MAP_SET_ID,
            COUNT(*) AS c
          FROM
            COM_COL_MAPPING_COLUMN      l
          INNER JOIN COMPONENT_MAPPING  cm
          ON
            l.MAP_ID = cm.MAP_ID
          INNER JOIN COM_COL_MAPPING_COMPONENT  s
          ON
            s.MAP_ID = cm.MAP_ID
          WHERE
            (
              l.MAP_ID NOT IN
              (
                SELECT
                  MAP_ID
                FROM
                  TRANSCODING
              )
            )
          GROUP BY
            cm.MAP_SET_ID,
            cm.MAP_ID
        ) t_2
      WHERE
        (
          c > 1
        )
    )
  )
AND
  (
    (
      SELECT
        COUNT(c.COMP_ID) AS error
      FROM
        COMPONENT         c
      INNER JOIN DATAFLOW d
      ON
        c.DSD_ID = d.DSD_ID
      WHERE
        (
          c.TYPE LIKE '%Dimension'
        )
      AND
        (
          c.IS_MEASURE_DIM IS NULL
        )
      AND
        (
          d.DF_ID = T.DF_ID
        )
      AND
        (
          c.COMP_ID NOT IN
          (
            SELECT
              l.COMP_ID
            FROM
              COMPONENT_MAPPING                  cm
            INNER JOIN COM_COL_MAPPING_COMPONENT l
            ON
              cm.MAP_ID = l.MAP_ID
            INNER JOIN DATAFLOW d
            ON
              cm.MAP_SET_ID = d.MAP_SET_ID
            WHERE
              (
                d.DF_ID = T.DF_ID
              )
          )
        )
      OR
        (
          c.TYPE = 'Attribute'
        )
      AND
        (
          d.DF_ID = T.DF_ID
        )
      AND
        (
          c.COMP_ID NOT IN
          (
            SELECT
              l.COMP_ID
            FROM
              COMPONENT_MAPPING                  cm
            INNER JOIN COM_COL_MAPPING_COMPONENT l
            ON
              cm.MAP_ID = l.MAP_ID
            INNER JOIN DATAFLOW d
            ON
              cm.MAP_SET_ID = d.MAP_SET_ID
            WHERE
              (
                d.DF_ID = T.DF_ID
              )
          )
        )
      AND
        (
          c.ATT_STATUS = 'Mandatory'
        )
    )
    = 0
  )
AND
  (
    (
      SELECT
        COUNT(c.COMP_ID) AS error
      FROM
        COMPONENT         c
      INNER JOIN DATAFLOW d
      ON
        c.DSD_ID = d.DSD_ID
      WHERE
        (
          c.TYPE LIKE '%Dimension'
        )
      AND
        (
          c.IS_MEASURE_DIM = 1
        )
      AND
        (
          d.DF_ID = T.DF_ID
        )
      AND
        (
          c.COMP_ID NOT IN
          (
            SELECT
              l.COMP_ID
            FROM
              COMPONENT_MAPPING                  cm
            INNER JOIN COM_COL_MAPPING_COMPONENT l
            ON
              cm.MAP_ID = l.MAP_ID
            INNER JOIN DATAFLOW d
            ON
              cm.MAP_SET_ID = d.MAP_SET_ID
            WHERE
              (
                d.DF_ID = T.DF_ID
              )
          )
        )
      OR
        (
          c.TYPE = 'PrimaryMeasure'
        )
      AND
        (
          d.DF_ID = T.DF_ID
        )
      AND
        (
          c.COMP_ID NOT IN
          (
            SELECT
              l.COMP_ID
            FROM
              COMPONENT_MAPPING                  cm
            INNER JOIN COM_COL_MAPPING_COMPONENT l
            ON
              cm.MAP_ID = l.MAP_ID
            INNER JOIN DATAFLOW d
            ON
              cm.MAP_SET_ID = d.MAP_SET_ID
            WHERE
              (
                d.DF_ID = T.DF_ID
              )
          )
        )
    )
    = 0
  )
OR
  (
    MSET.MAP_SET_ID NOT IN
    (
      SELECT
        MAP_SET_ID
      FROM
        (
          SELECT
            cm.MAP_SET_ID,
            COUNT(*) AS c
          FROM
            COM_COL_MAPPING_COLUMN     l
          INNER JOIN COMPONENT_MAPPING cm
          ON
            l.MAP_ID = cm.MAP_ID
          INNER JOIN COM_COL_MAPPING_COMPONENT s
          ON
            s.MAP_ID = cm.MAP_ID
          WHERE
            (
              l.MAP_ID NOT IN
              (
                SELECT
                  MAP_ID
                FROM
                  TRANSCODING TRANSCODING_1
              )
            )
          GROUP BY
            cm.MAP_SET_ID,
            cm.MAP_ID
        ) t_1
      WHERE
        (
          c > 1
        )
    )
  )
AND
  (
    (
      SELECT
        COUNT(c.COMP_ID) AS error
      FROM
        COMPONENT         c
      INNER JOIN DATAFLOW d
      ON
        c.DSD_ID = d.DSD_ID
      WHERE
        (
          c.TYPE LIKE '%Dimension'
        )
      AND
        (
          c.IS_MEASURE_DIM IS NULL
        )
      AND
        (
          d.DF_ID = T.DF_ID
        )
      AND
        (
          c.COMP_ID NOT IN
          (
            SELECT
              l.COMP_ID
            FROM
              COMPONENT_MAPPING                  cm
            INNER JOIN COM_COL_MAPPING_COMPONENT l
            ON
              cm.MAP_ID = l.MAP_ID
            INNER JOIN DATAFLOW d
            ON
              cm.MAP_SET_ID = d.MAP_SET_ID
            WHERE
              (
                d.DF_ID = T.DF_ID
              )
          )
        )
      OR
        (
          c.TYPE = 'Attribute'
        )
      AND
        (
          d.DF_ID = T.DF_ID
        )
      AND
        (
          c.COMP_ID NOT IN
          (
            SELECT
              l.COMP_ID
            FROM
              COMPONENT_MAPPING                  cm
            INNER JOIN COM_COL_MAPPING_COMPONENT l
            ON
              cm.MAP_ID = l.MAP_ID
            INNER JOIN DATAFLOW d
            ON
              cm.MAP_SET_ID = d.MAP_SET_ID
            WHERE
              (
                d.DF_ID = T.DF_ID
              )
          )
        )
      AND
        (
          c.ATT_STATUS = 'Mandatory'
        )
    )
    = 0
  )
AND
  (
    (
      SELECT
        COUNT(c.COMP_ID) AS error
      FROM
        COMPONENT         c
      INNER JOIN DATAFLOW d
      ON
        c.DSD_ID = d.DSD_ID
      WHERE
        (
          c.TYPE = 'CrossSectionalMeasure'
        )
      AND
        (
          d.DF_ID = T.DF_ID
        )
      AND
        (
          c.COMP_ID NOT IN
          (
            SELECT
              l.COMP_ID
            FROM
              COMPONENT_MAPPING                  cm
            INNER JOIN COM_COL_MAPPING_COMPONENT l
            ON
              cm.MAP_ID = l.MAP_ID
            INNER JOIN DATAFLOW d
            ON
              cm.MAP_SET_ID = d.MAP_SET_ID
            WHERE
              (
                d.DF_ID = T.DF_ID
              )
          )
        )
    )
    = 0
  )
AND
  (
    (
      SELECT
        COUNT(c.COMP_ID) AS hasXS
      FROM
        COMPONENT         c
      INNER JOIN DATAFLOW d
      ON
        c.DSD_ID = d.DSD_ID
      WHERE
        (
          c.TYPE = 'CrossSectionalMeasure'
        )
      AND
        (
          d.DF_ID = T.DF_ID
        )
    )
    > 0
  )
)
;

update DB_VERSION SET VERSION = '2.8'
;
