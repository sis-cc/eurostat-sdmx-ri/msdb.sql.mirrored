﻿-- Upgrade script to 4.0 from 3.3

-- increase size of DB_NAME and ADO/JDBC connection strings for SDMXRI-50.

ALTER TABLE DB_CONNECTION MODIFY DB_NAME VARCHAR(1000)
;
ALTER TABLE DB_CONNECTION MODIFY ADO_CONNECTION_STRING VARCHAR(2000)
;
ALTER TABLE DB_CONNECTION MODIFY JDBC_CONNECTION_STRING VARCHAR(2000)
;


-- attribute attachment measure table
CREATE TABLE ATT_MEASURE
(
	MEASURE_COMP_ID  NUMBER(18) NOT NULL,
	ATT_COMP_ID      NUMBER(18) NOT NULL
)
;

ALTER TABLE ATT_MEASURE ADD CONSTRAINT PK_ATT_MEASURE 
	PRIMARY KEY (MEASURE_COMP_ID, ATT_COMP_ID)
;

ALTER TABLE ATT_MEASURE ADD CONSTRAINT FK_ATT_MEASURE_ATT 
	FOREIGN KEY (ATT_COMP_ID) REFERENCES COMPONENT (COMP_ID)
;

ALTER TABLE ATT_MEASURE ADD CONSTRAINT FK_ATT_MEASURE_DIM 
	FOREIGN KEY (MEASURE_COMP_ID) REFERENCES COMPONENT (COMP_ID) ON DELETE CASCADE
;

-- move data from CATEGORISATION.DC_ORDER to ANNOTATION
declare 
sysid number; 
corder number;
ann_id number;
lpk number;
cursor cat_cursor is
select CATN_ID as SYSID, DC_ORDER as CORDER FROM CATEGORISATION WHERE DC_ORDER IS NOT NULL AND CATN_ID NOT IN (SELECT AA.ART_ID FROM ARTEFACT_ANNOTATION AA INNER JOIN ANNOTATION A ON A.ANN_ID = AA.ANN_ID WHERE A.TYPE='CategoryScheme_node_order') ;
begin
open cat_cursor;

FETCH cat_cursor into sysid, corder;
while cat_cursor%FOUND
loop
  INSERT_ARTEFACT_ANNOTATION(p_art_id => sysid, p_id => null, p_title => null, p_type => 'CategoryScheme_node_order', p_url => null, p_pk => ann_id);
	INSERT_ANNOTATION_TEXT(p_ann_id => ann_id, p_language => 'en', p_text => corder, p_pk => lpk);
  FETCH cat_cursor into sysid, corder;
end loop;

close cat_cursor;
end;
/

-- move data from CATEGORY_SCHEME.CS_ORDER to ANNOTATION
declare 
sysid number; 
corder number;
ann_id number;
lpk number;
cursor cat_cursor is
select CAT_SCH_ID as SYSID, CS_ORDER as CORDER FROM CATEGORY_SCHEME WHERE CS_ORDER IS NOT NULL AND CAT_SCH_ID NOT IN (SELECT AA.ART_ID FROM ARTEFACT_ANNOTATION AA INNER JOIN ANNOTATION A ON A.ANN_ID = AA.ANN_ID WHERE A.TYPE='CategoryScheme_node_order') ;
begin
open cat_cursor;

FETCH cat_cursor into sysid, corder;
while cat_cursor%FOUND
loop
  INSERT_ARTEFACT_ANNOTATION(p_art_id => sysid, p_id => null, p_title => null, p_type => 'CategoryScheme_node_order', p_url => null, p_pk => ann_id);
	INSERT_ANNOTATION_TEXT(p_ann_id => ann_id, p_language => 'en', p_text => corder, p_pk => lpk);
  FETCH cat_cursor into sysid, corder;
end loop;

close cat_cursor;
end;
/

-- move data from CATEGORY.CORDER to ANNOTATION
declare 
sysid number; 
corder number;
ann_id number;
lpk number;
cursor cat_cursor is
select CAT_ID as SYSID, CORDER FROM CATEGORY WHERE CORDER IS NOT NULL AND CAT_ID NOT IN (SELECT AA.ITEM_ID FROM ITEM_ANNOTATION AA INNER JOIN ANNOTATION A ON A.ANN_ID = AA.ANN_ID WHERE A.TYPE='CategoryScheme_node_order') ;
begin
open cat_cursor;

FETCH cat_cursor into sysid, corder;
while cat_cursor%FOUND
loop
  INSERT_ITEM_ANNOTATION(p_item_id => sysid, p_id => null, p_title => null, p_type => 'CategoryScheme_node_order', p_url => null, p_pk => ann_id);
	INSERT_ANNOTATION_TEXT(p_ann_id => ann_id, p_language => 'en', p_text => corder, p_pk => lpk);
  FETCH cat_cursor into sysid, corder;
end loop;

close cat_cursor;
end;
/

-- update stored procedures
CREATE OR REPLACE PROCEDURE INSERT_CATEGORISATION(
	p_id IN varchar2,
	p_version IN varchar2,
	p_agency IN varchar2,
	p_valid_from IN  timestamp DEFAULT NULL,
	p_valid_to IN  timestamp DEFAULT NULL,
	p_is_final IN number DEFAULT NULL,
	p_uri IN nvarchar2,
	p_last_modified IN timestamp DEFAULT NULL,
	p_art_id IN number,
	p_cat_id IN number,
	p_pk OUT NUMBER)
AS
BEGIN

	INSERT_ARTEFACT(p_id => p_id,p_version => p_version,p_agency => p_agency,p_valid_from => p_valid_from,p_valid_to => p_valid_to,p_is_final => p_is_final,p_uri =>p_uri,p_last_modified => p_last_modified,p_pk => p_pk);
	
INSERT INTO CATEGORISATION
           (CATN_ID
           ,ART_ID
           ,CAT_ID)
     VALUES
           (p_pk
           ,p_art_id
           ,p_cat_id);
END;
/


CREATE OR REPLACE PROCEDURE INSERT_CATEGORY_SCHEME(
	p_id IN varchar2,
	p_version IN varchar2,
	p_agency IN varchar2,
	p_valid_from IN  timestamp DEFAULT NULL,
	p_valid_to IN  timestamp DEFAULT NULL,
	p_is_final IN number DEFAULT NULL,
	p_uri IN nvarchar2,
	p_last_modified IN timestamp DEFAULT NULL,
    p_is_partial IN number default 0,
	p_pk OUT NUMBER)
AS
BEGIN

	INSERT_ARTEFACT(p_id => p_id,p_version => p_version,p_agency => p_agency,p_valid_from => p_valid_from,p_valid_to => p_valid_to,p_is_final => p_is_final,p_uri => p_uri,p_last_modified => p_last_modified,p_pk => p_pk);
	
	INSERT INTO CATEGORY_SCHEME
           (CAT_SCH_ID, IS_PARTIAL)
     VALUES
           (p_pk, p_is_partial);
END;
/
CREATE OR REPLACE PROCEDURE INSERT_CATEGORY(
	p_id IN item.id%type,
	p_cat_sch_id IN number,
	p_parent_cat_id IN CATEGORY.PARENT_CAT_ID%type DEFAULT NULL,
	p_pk OUT item.item_id%type)
AS
BEGIN

		insert into ITEM (ITEM_ID, ID) VALUES (ITEM_SEQ.nextval, p_id);
		select ITEM_SEQ.currval into p_pk from dual;
		insert into CATEGORY (CAT_ID, CAT_SCH_ID, PARENT_CAT_ID) VALUES (p_pk, p_cat_sch_id, p_parent_cat_id);
END;
/
-- drop ORDER columns

ALTER TABLE CATEGORISATION DROP COLUMN DC_ORDER
;

ALTER TABLE CATEGORY DROP COLUMN CORDER
;

ALTER TABLE CATEGORY_SCHEME  DROP COLUMN CS_ORDER
;


-- always last
update DB_VERSION SET VERSION = '4.0'
;

