﻿-- Upgrade script to 4.0 from 3.3

-- increase size of DB_NAME and ADO/JDBC connection strings for SDMXRI-50.

ALTER TABLE DB_CONNECTION MODIFY DB_NAME VARCHAR(1000) NOT NULL
;
ALTER TABLE DB_CONNECTION MODIFY ADO_CONNECTION_STRING VARCHAR(2000)
;
ALTER TABLE DB_CONNECTION MODIFY JDBC_CONNECTION_STRING VARCHAR(2000)
;

-- attribute attachment measure table
CREATE TABLE ATT_MEASURE
(
	MEASURE_COMP_ID BIGINT NOT NULL,
	ATT_COMP_ID BIGINT NOT NULL,
	PRIMARY KEY (MEASURE_COMP_ID, ATT_COMP_ID),
	KEY (ATT_COMP_ID),
	KEY (MEASURE_COMP_ID)

) 
;

ALTER TABLE ATT_MEASURE ADD CONSTRAINT FK_ATT_MEASURE_ATT 
	FOREIGN KEY (ATT_COMP_ID) REFERENCES COMPONENT (COMP_ID)
;

ALTER TABLE ATT_MEASURE ADD CONSTRAINT FK_ATT_MEASURE_DIM 
	FOREIGN KEY (MEASURE_COMP_ID) REFERENCES COMPONENT (COMP_ID)
	ON DELETE CASCADE
;

-- move data from CATEGORISATION.DC_ORDER to ANNOTATION
DROP PROCEDURE IF EXISTS TMP_PROC
;

create procedure TMP_PROC()
begin
	DECLARE done INT DEFAULT FALSE;
declare sysid bigint; 
declare corder bigint;
declare pk bigint;
declare lpk bigint;
declare cat_cursor cursor for
select CATN_ID as SYSID, DC_ORDER as CORDER FROM CATEGORISATION WHERE DC_ORDER IS NOT NULL AND CATN_ID NOT IN (SELECT AA.ART_ID FROM ARTEFACT_ANNOTATION AA INNER JOIN ANNOTATION A ON A.ANN_ID = AA.ANN_ID WHERE A.TYPE='CategoryScheme_node_order');
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

open cat_cursor;

loopLabel : LOOP
    FETCH cat_cursor into sysid, corder;
    if done then
        close cat_cursor;
        leave loopLabel;
    end if;
    call INSERT_ARTEFACT_ANNOTATION (sysid, null, null, 'CategoryScheme_node_order', null, pk);
    call INSERT_ANNOTATION_TEXT (pk, 'en', corder, lpk);
end loop;
end;
;
call TMP_PROC()
;

DROP PROCEDURE IF EXISTS TMP_PROC
;

-- move data from CATEGORY_SCHEME.CS_ORDER to ANNOTATION
DROP PROCEDURE IF EXISTS TMP_PROC
;

create procedure TMP_PROC()
begin
	DECLARE done INT DEFAULT FALSE;
declare sysid bigint; 
declare corder bigint;
declare pk bigint;
declare lpk bigint;
declare cat_cursor cursor for
select CAT_SCH_ID as SYSID, CS_ORDER as CORDER FROM CATEGORY_SCHEME WHERE CS_ORDER IS NOT NULL AND CAT_SCH_ID NOT IN (SELECT AA.ART_ID FROM ARTEFACT_ANNOTATION AA INNER JOIN ANNOTATION A ON A.ANN_ID = AA.ANN_ID WHERE A.TYPE='CategoryScheme_node_order');
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

open cat_cursor;

loopLabel : LOOP
    FETCH cat_cursor into sysid, corder;
    if done then
        close cat_cursor;
        leave loopLabel;
    end if;
    call INSERT_ARTEFACT_ANNOTATION (sysid, null, null, 'CategoryScheme_node_order', null, pk);
    call INSERT_ANNOTATION_TEXT (pk, 'en', corder, lpk);
end loop;
end;
;
call TMP_PROC()
;

DROP PROCEDURE IF EXISTS TMP_PROC
;

-- move data from CATEGORY.CORDER to ANNOTATION
DROP PROCEDURE IF EXISTS TMP_PROC
;

create procedure TMP_PROC()
begin
DECLARE done INT DEFAULT FALSE;
declare sysid bigint;
declare m_corder bigint;
declare pk bigint;
declare lpk bigint;
declare cat_cursor cursor for
select CAT_ID as SYSID,  CORDER FROM CATEGORY WHERE CORDER IS NOT NULL AND CAT_ID NOT IN (SELECT AA.ITEM_ID FROM ITEM_ANNOTATION AA INNER JOIN ANNOTATION A ON A.ANN_ID = AA.ANN_ID WHERE A.TYPE='CategoryScheme_node_order');
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

open cat_cursor;
loopLabel : LOOP
    FETCH cat_cursor into sysid, m_corder;
    if done then
        close cat_cursor;
        leave loopLabel;
    end if;
    call INSERT_ITEM_ANNOTATION (sysid, null, null, 'CategoryScheme_node_order', null, pk);
    call INSERT_ANNOTATION_TEXT (pk, 'en', m_corder, lpk);
end loop;
end;
;
call TMP_PROC()
;

DROP PROCEDURE IF EXISTS TMP_PROC
;

-- update stored procedures
DROP PROCEDURE IF EXISTS INSERT_CATEGORISATION
;
CREATE PROCEDURE INSERT_CATEGORISATION(
	IN p_id varchar(50),
	IN p_version varchar(50),
	IN p_agency varchar(50),
	IN p_valid_from datetime,
	IN p_valid_to datetime,
	IN p_is_final int,
	IN p_uri nvarchar(255),
	IN p_last_modified datetime,
	IN p_art_id bigint,
	IN p_cat_id bigint,
	OUT p_pk bigint)
BEGIN
	call INSERT_ARTEFACT (p_id,p_version,p_agency,p_valid_from,p_valid_to,p_is_final,p_uri,p_last_modified, p_pk);
	
INSERT INTO CATEGORISATION
           (CATN_ID
           ,ART_ID
           ,CAT_ID
           )
     VALUES
           (p_pk
           ,p_art_id
           ,p_cat_id
           );
END
;
DROP PROCEDURE IF EXISTS INSERT_CATEGORY
;
CREATE PROCEDURE INSERT_CATEGORY(
	IN p_id varchar(50),
	IN p_cat_sch_id bigint,
	IN p_parent_cat_id bigint,
	OUT p_pk bigint) 
BEGIN
		insert into ITEM (ID) VALUES (p_id);
		set p_pk = LAST_INSERT_ID();
		insert into CATEGORY (CAT_ID, CAT_SCH_ID, PARENT_CAT_ID) VALUES (p_pk, p_cat_sch_id, p_parent_cat_id);
END
;

DROP PROCEDURE IF EXISTS INSERT_CATEGORY_SCHEME
;
CREATE PROCEDURE INSERT_CATEGORY_SCHEME(
	IN p_id varchar(50),
	IN p_version varchar(50),
	IN p_agency varchar(50),
	IN p_valid_from datetime,
	IN p_valid_to datetime,
	IN p_is_final int,
	IN p_uri nvarchar(255),
	IN p_last_modified datetime,
    IN p_is_partial int,
	OUT p_pk bigint)
BEGIN
	call INSERT_ARTEFACT (p_id,p_version,p_agency,p_valid_from,p_valid_to,p_is_final,p_uri,p_last_modified, p_pk);
	
	INSERT INTO CATEGORY_SCHEME
           (CAT_SCH_ID, IS_PARTIAL)
     VALUES
           (p_pk, p_is_partial);
END
;
-- drop ORDER columns

ALTER TABLE CATEGORISATION DROP COLUMN DC_ORDER
;

ALTER TABLE CATEGORY DROP COLUMN CORDER
;

ALTER TABLE CATEGORY_SCHEME  DROP COLUMN CS_ORDER
;


-- always last
update DB_VERSION SET VERSION = '4.0'
;
