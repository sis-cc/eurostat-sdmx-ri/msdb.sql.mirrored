
-- create sequences for ARTEFACT, ITEM, COMPONENT, DSD_GROUP, LOCALISED_STRING
declare
     id  integer;
begin
    -- artefact
    select max(art_id)+1 into id from artefact;
    if id is null 
    then
		id := 1;
    end if;
      
    execute immediate 'create sequence ARTEFACT_SEQ INCREMENT BY 1 start with '||TO_CHAR(id) ||' NOMAXVALUE MINVALUE 1 NOCYCLE NOCACHE NOORDER';

    -- item
    select max(item_id)+1 into id from item;
    if id is null 
    then
		id := 1;
    end if;
      
    execute immediate 'create sequence ITEM_SEQ INCREMENT BY 1 start with '||TO_CHAR(id) ||' NOMAXVALUE MINVALUE 1 NOCYCLE NOCACHE NOORDER';

    -- component
    select max(comp_id)+1 into id from component;
    if id is null 
    then
		id := 1;
    end if;
      
    execute immediate 'create sequence COMPONENT_SEQ INCREMENT BY 1 start with '||TO_CHAR(id) ||' NOMAXVALUE MINVALUE 1 NOCYCLE NOCACHE NOORDER';
      
    -- dsd_group
    select max(gr_id)+1 into id from dsd_group;
    if id is null 
    then
		id := 1;
    end if;
      
    execute immediate 'create sequence DSD_GROUP_SEQ INCREMENT BY 1 start with '||TO_CHAR(id) ||' NOMAXVALUE MINVALUE 1 NOCYCLE NOCACHE NOORDER';
      
    -- localised string
    select max(ls_id)+1 into id from localised_string;
    if id is null 
    then
		id := 1;
    end if;
      
    execute immediate 'create sequence LOCALISED_STRING_SEQ INCREMENT BY 1 start with '||TO_CHAR(id) ||' NOMAXVALUE MINVALUE 1 NOCYCLE NOCACHE NOORDER';
end;
/


CREATE OR REPLACE TRIGGER SET_LS_ID
BEFORE INSERT
ON LOCALISED_STRING
FOR EACH ROW
BEGIN
  select LOCALISED_STRING_SEQ.NEXTVAL into :NEW.LS_ID FROM DUAL;
END;
/

-- add new column to dataset_column
alter table dataset_column add LASTRETRIEVAL timestamp NULL
;

-- fix constrain order to improve performance
alter table TRANSCODING_RULE_LOCAL_CODE drop constraint PK_TRANSCODING_RULE_LOCAL_CODE
;

ALTER TABLE TRANSCODING_RULE_LOCAL_CODE ADD CONSTRAINT PK_TRANSCODING_RULE_LOCAL_CODE 
	PRIMARY KEY (TR_RULE_ID, LCD_ID)
;

-- drop sequence table
drop table SEQUENCE_ID  CASCADE CONSTRAINTS
;


-- stored procedures

CREATE OR REPLACE PROCEDURE INSERT_LOCAL_CODE(
	p_id IN varchar2, 
	p_column_id IN number,
	p_pk OUT NUMBER) 
AS
BEGIN
		insert into ITEM (ITEM_ID, ID) VALUES (ITEM_SEQ.nextval, p_id);
		select ITEM_SEQ.currval into p_pk from dual;
		insert into LOCAL_CODE (LCD_ID, COLUMN_ID) VALUES (p_pk, p_column_id);
END;
/


CREATE OR REPLACE PROCEDURE UPDATE_TRANSCODING_RULE(
	-- Add the parameters for the stored procedure here 
	p_tr_id IN number,
	p_tr_rule_id IN number,
	p_lcd_id IN number,
	p_cd_id IN number,
	p_pk OUT NUMBER) 
AS
BEGIN
		-- Insert statements for procedure here
		if (p_tr_rule_id) is null
		then
			insert into TRANSCODING_RULE (TR_ID) values (p_tr_id);
			select TRANSCODING_RUL_TR_RULE_ID_SEQ.currval into p_pk from dual;
		else
			-- clean up
			delete from TRANSCODING_RULE_DSD_CODE where TR_RULE_ID = p_tr_rule_id;
			delete from TRANSCODING_RULE_LOCAL_CODE where TR_RULE_ID = p_tr_rule_id;
			p_pk := p_tr_rule_id;
		end if;
		
		insert into TRANSCODING_RULE_DSD_CODE (TR_RULE_ID, CD_ID) VALUES (p_pk, p_cd_id);
		insert into TRANSCODING_RULE_LOCAL_CODE (TR_RULE_ID, LCD_ID) VALUES (p_pk, p_lcd_id);		
END;
/

CREATE OR REPLACE PROCEDURE INSERT_TRANSCODING(
	p_map_id IN number, 
	p_expression IN varchar2 DEFAULT NULL,
	p_pk OUT NUMBER)
AS
BEGIN
	insert into TRANSCODING (MAP_ID, EXPRESSION) values (p_map_id,p_expression);
	select TRANSCODING_TR_ID_SEQ.currval into p_pk from dual;
END;
/


CREATE OR REPLACE PROCEDURE INSERT_TRANSCODING_SCRIPT(
	p_tr_id IN number, 
	p_title IN varchar2,
	p_content TRANSCODING_SCRIPT.SCRIPT_CONTENT%Type,
	p_pk OUT NUMBER)
AS
BEGIN
	insert into TRANSCODING_SCRIPT (TR_ID, SCRIPT_TITLE, SCRIPT_CONTENT) values (p_tr_id, p_title, p_content);
	select TR_SCRIPT_TR_SCRIPT_ID_SEQ.currval into p_pk from dual;
END;
/


CREATE OR REPLACE PROCEDURE INSERT_ITEM(
	p_id IN varchar2,
	p_pk OUT NUMBER)
AS
BEGIN
	insert into ITEM (ID) values (p_id);
	select ITEM_SEQ.currval into p_pk from dual;
END;
/


CREATE OR REPLACE PROCEDURE INSERT_ARTEFACT(
	p_id IN varchar2,
	p_version IN varchar2,
	p_agency IN varchar2,
	p_valid_from IN varchar2 DEFAULT NULL,
	p_valid_to IN varchar2 DEFAULT NULL,
	p_is_final IN number DEFAULT NULL,
	p_pk OUT NUMBER)
AS
BEGIN
	insert into ARTEFACT (ART_ID, ID,VERSION, AGENCY, VALID_FROM, VALID_TO, IS_FINAL) values (ARTEFACT_SEQ.nextval, p_id,p_version,p_agency,p_valid_from,p_valid_to,p_is_final);
	select ARTEFACT_SEQ.currval into p_pk from dual;
END;
/


CREATE OR REPLACE PROCEDURE INSERT_LOCALISED_STRING(
	p_item_id IN number DEFAULT NULL,
	p_art_id IN number DEFAULT NULL,
	p_text IN nvarchar2,
	p_type IN varchar2,
	p_language IN varchar2,
	p_pk OUT NUMBER) 
AS
BEGIN
	insert into LOCALISED_STRING (ART_ID, ITEM_ID, TEXT, TYPE, LANGUAGE) values (p_art_id, p_item_id, p_text, p_type, p_language);
	select LOCALISED_STRING_SEQ.currval into p_pk from dual;
END;
/


CREATE OR REPLACE PROCEDURE INSERT_COMPONENT(
	p_type IN varchar2,
	p_dsd_id IN number,
	p_con_id IN number,
	p_cl_id IN number DEFAULT NULL,
	p_is_freq_dim IN number DEFAULT NULL,
	p_is_measure_dim IN number DEFAULT NULL,
	p_att_ass_level IN varchar2 DEFAULT NULL,
	p_att_status IN varchar2 DEFAULT NULL,
	p_att_is_time_format IN number DEFAULT NULL,
	p_xs_attlevel_ds IN number DEFAULT NULL,
	p_xs_attlevel_group IN number DEFAULT NULL,
	p_xs_attlevel_section IN number DEFAULT NULL,
	p_xs_attlevel_obs IN number DEFAULT NULL,
	p_xs_measure_code IN varchar2 DEFAULT NULL,

	p_pk OUT NUMBER) 
AS
BEGIN
	insert into COMPONENT (COMP_ID, TYPE, DSD_ID, CON_ID, CL_ID, IS_FREQ_DIM, IS_MEASURE_DIM, ATT_ASS_LEVEL, ATT_STATUS, ATT_IS_TIME_FORMAT, XS_ATTLEVEL_DS, XS_ATTLEVEL_GROUP, XS_ATTLEVEL_SECTION, XS_ATTLEVEL_OBS, XS_MEASURE_CODE) 
		values (COMPONENT_SEQ.nextval, p_type, p_dsd_id, p_con_id, p_cl_id, p_is_freq_dim, p_is_measure_dim, p_att_ass_level, p_att_status, p_att_is_time_format, p_xs_attlevel_ds, p_xs_attlevel_group, p_xs_attlevel_section, p_xs_attlevel_obs, p_xs_measure_code);
	select COMPONENT_SEQ.currval into p_pk from dual;
END;
/


CREATE OR REPLACE PROCEDURE INSERT_DSD_GROUP(
	p_id IN varchar2,
	p_dsd_id IN number ,
	p_pk OUT NUMBER) 
AS
BEGIN
	insert into DSD_GROUP (GR_ID, ID, DSD_ID) 
		values (DSD_GROUP_SEQ.nextval, p_id, p_dsd_id);
	select DSD_GROUP_SEQ.currval into p_pk from dual;
END;
/


CREATE OR REPLACE PROCEDURE INSERT_DSD_CODE(
	p_id IN item.id%type,
	p_cl_id IN number,
	p_parent_code_id IN number DEFAULT NULL,
	p_pk OUT item.item_id%type)
AS
BEGIN

		insert into ITEM (ITEM_ID, ID) VALUES (ITEM_SEQ.nextval, p_id);
		select ITEM_SEQ.currval into p_pk from dual;
		insert into DSD_CODE (LCD_ID, CL_ID, PARENT_CODE_ID) VALUES (p_pk, p_cl_id, p_parent_code_id);
END;
/

CREATE OR REPLACE PROCEDURE INSERT_CATEGORY(
	p_id IN item.id%type,
	p_cat_sch_id IN number,
	p_parent_cat_id IN CATEGORY.PARENT_CAT_ID%type DEFAULT NULL,
	p_corder IN number default 0,
	p_pk OUT item.item_id%type)
AS
BEGIN

		insert into ITEM (ITEM_ID, ID) VALUES (ITEM_SEQ.nextval, p_id);
		select ITEM_SEQ.currval into p_pk from dual;
		insert into CATEGORY (CAT_ID, CAT_SCH_ID, PARENT_CAT_ID, CORDER) VALUES (p_pk, p_cat_sch_id, p_parent_cat_id, p_corder);
END;
/

CREATE OR REPLACE PROCEDURE INSERT_CONCEPT(
	p_id IN item.id%type,
	p_con_sch_id IN CONCEPT.CON_SCH_ID%type,
	p_pk OUT item.item_id%type)
AS
BEGIN

		insert into ITEM (ITEM_ID, ID) VALUES (ITEM_SEQ.nextval, p_id);
		select ITEM_SEQ.currval into p_pk from dual;
		insert into CONCEPT (CON_ID, CON_SCH_ID) VALUES (p_pk, p_con_sch_id);
END;
/

CREATE OR REPLACE PROCEDURE INSERT_HCL_CODE(
	p_id IN varchar2,
	p_version IN varchar2,
	p_agency IN varchar2,
	p_valid_from IN varchar2 DEFAULT NULL,
	p_valid_to IN varchar2 DEFAULT NULL,
	p_is_final IN number DEFAULT NULL,
	p_parent_hcode_id IN number DEFAULT NULL,
	p_lcd_id IN number,
	p_h_id IN number,
	p_level_id IN number DEFAULT NULL,
	p_pk OUT NUMBER)
AS
BEGIN

	insert into ARTEFACT (ART_ID, ID,VERSION, AGENCY, VALID_FROM, VALID_TO, IS_FINAL) values (ARTEFACT_SEQ.nextval, p_id,p_version,p_agency,p_valid_from,p_valid_to,p_is_final);
	select ARTEFACT_SEQ.currval into p_pk from dual;
	
	INSERT INTO HCL_CODE
           (HCODE_ID
           ,PARENT_HCODE_ID
           ,LCD_ID
           ,H_ID
           ,LEVEL_ID)
	VALUES
		(p_pk
        ,p_parent_hcode_id
        ,p_lcd_id
        ,p_h_id
         ,p_level_id);
END;
/


CREATE OR REPLACE PROCEDURE INSERT_HCL(
	p_id IN varchar2,
	p_version IN varchar2,
	p_agency IN varchar2,
	p_valid_from IN varchar2 DEFAULT NULL,
	p_valid_to IN varchar2 DEFAULT NULL,
	p_is_final IN number DEFAULT NULL,
	p_pk OUT NUMBER)
AS
BEGIN

	insert into ARTEFACT (ART_ID, ID,VERSION, AGENCY, VALID_FROM, VALID_TO, IS_FINAL) values (ARTEFACT_SEQ.nextval, p_id,p_version,p_agency,p_valid_from,p_valid_to,p_is_final);
	select ARTEFACT_SEQ.currval into p_pk from dual;
	insert into HCL (HCL_ID) values(p_pk);
END;
/


CREATE OR REPLACE PROCEDURE INSERT_HIERACHY(
	p_id IN varchar2,
	p_version IN varchar2,
	p_agency IN varchar2,
	p_valid_from IN varchar2 DEFAULT NULL,
	p_valid_to IN varchar2 DEFAULT NULL,
	p_is_final IN number DEFAULT NULL,
	p_hcl_id IN number,
	p_pk OUT NUMBER)
AS
BEGIN

	insert into ARTEFACT (ART_ID, ID,VERSION, AGENCY, VALID_FROM, VALID_TO, IS_FINAL) values (ARTEFACT_SEQ.nextval, p_id,p_version,p_agency,p_valid_from,p_valid_to,p_is_final);
	select ARTEFACT_SEQ.currval into p_pk from dual;
	
	INSERT INTO HIERARCHY
           (H_ID
           ,HCL_ID)
     VALUES
           (p_pk
           ,p_hcl_id);
END;
/


CREATE OR REPLACE PROCEDURE INSERT_HLEVEL(
	p_id IN varchar2,
	p_version IN varchar2,
	p_agency IN varchar2,
	p_valid_from IN varchar2 DEFAULT NULL,
	p_valid_to IN varchar2 DEFAULT NULL,
	p_is_final IN number DEFAULT NULL,
	p_h_id IN number,
	p_parent_level_id IN number DEFAULT NULL,
	p_pk OUT NUMBER)
AS
BEGIN

	insert into ARTEFACT (ART_ID, ID,VERSION, AGENCY, VALID_FROM, VALID_TO, IS_FINAL) values (ARTEFACT_SEQ.nextval, p_id,p_version,p_agency,p_valid_from,p_valid_to,p_is_final);
	select ARTEFACT_SEQ.currval into p_pk from dual;
	
	INSERT INTO HLEVEL
           (LEVEL_ID
           ,H_ID
           ,PARENT_LEVEL_ID)
     VALUES
           (p_pk
           ,p_h_id
           ,p_parent_level_id);
END;
/

CREATE OR REPLACE PROCEDURE INSERT_DSD(
	p_id IN varchar2,
	p_version IN varchar2,
	p_agency IN varchar2,
	p_valid_from IN varchar2 DEFAULT NULL,
	p_valid_to IN varchar2 DEFAULT NULL,
	p_is_final IN number DEFAULT NULL,
	p_pk OUT NUMBER)
AS
BEGIN

	insert into ARTEFACT (ART_ID, ID,VERSION, AGENCY, VALID_FROM, VALID_TO, IS_FINAL) values (ARTEFACT_SEQ.nextval, p_id,p_version,p_agency,p_valid_from,p_valid_to,p_is_final);
	select ARTEFACT_SEQ.currval into p_pk from dual;
	
	INSERT INTO DSD
           (DSD_ID)
     VALUES
           (p_pk);
END;
/


CREATE OR REPLACE PROCEDURE INSERT_DATAFLOW(
	p_id IN varchar2,
	p_version IN varchar2,
	p_agency IN varchar2,
	p_valid_from IN varchar2 DEFAULT NULL,
	p_valid_to IN varchar2 DEFAULT NULL,
	p_is_final IN number DEFAULT NULL,
	p_dsd_id IN number,
	p_map_set_id IN number DEFAULT NULL,
	p_pk OUT NUMBER)
AS
BEGIN

	insert into ARTEFACT (ART_ID, ID,VERSION, AGENCY, VALID_FROM, VALID_TO, IS_FINAL) values (ARTEFACT_SEQ.nextval, p_id,p_version,p_agency,p_valid_from,p_valid_to,p_is_final);
	select ARTEFACT_SEQ.currval into p_pk from dual;
	
INSERT INTO DATAFLOW
           (DF_ID
           ,DSD_ID
           ,MAP_SET_ID)
     VALUES
           (p_pk
           ,p_dsd_id
           ,p_map_set_id);
END;
/


CREATE OR REPLACE PROCEDURE INSERT_CATEGORY_SCHEME(
	p_id IN varchar2,
	p_version IN varchar2,
	p_agency IN varchar2,
	p_valid_from IN varchar2 DEFAULT NULL,
	p_valid_to IN varchar2 DEFAULT NULL,
	p_is_final IN number DEFAULT NULL,
	p_cs_order IN number default 0,
	p_pk OUT NUMBER)
AS
BEGIN

	insert into ARTEFACT (ART_ID, ID,VERSION, AGENCY, VALID_FROM, VALID_TO, IS_FINAL) values (ARTEFACT_SEQ.nextval, p_id,p_version,p_agency,p_valid_from,p_valid_to,p_is_final);
	select ARTEFACT_SEQ.currval into p_pk from dual;
	
	INSERT INTO CATEGORY_SCHEME
           (CAT_SCH_ID, CS_ORDER)
     VALUES
           (p_pk, p_cs_order);
END;
/


CREATE OR REPLACE PROCEDURE INSERT_CODELIST(
	p_id IN varchar2,
	p_version IN varchar2,
	p_agency IN varchar2,
	p_valid_from IN varchar2 DEFAULT NULL,
	p_valid_to IN varchar2 DEFAULT NULL,
	p_is_final IN number DEFAULT NULL,
	p_pk OUT NUMBER)
AS
BEGIN

	insert into ARTEFACT (ART_ID, ID,VERSION, AGENCY, VALID_FROM, VALID_TO, IS_FINAL) values (ARTEFACT_SEQ.nextval, p_id,p_version,p_agency,p_valid_from,p_valid_to,p_is_final);
	select ARTEFACT_SEQ.currval into p_pk from dual;
	
	INSERT INTO CODELIST
           (CL_ID)
     VALUES
           (p_pk);
END;
/


CREATE OR REPLACE PROCEDURE INSERT_CONCEPT_SCHEME(
	p_id IN varchar2,
	p_version IN varchar2,
	p_agency IN varchar2,
	p_valid_from IN varchar2 DEFAULT NULL,
	p_valid_to IN varchar2 DEFAULT NULL,
	p_is_final IN number DEFAULT NULL,
	p_pk OUT NUMBER)
AS
BEGIN
	insert into ARTEFACT (ART_ID, ID,VERSION, AGENCY, VALID_FROM, VALID_TO, IS_FINAL) values (ARTEFACT_SEQ.nextval, p_id,p_version,p_agency,p_valid_from,p_valid_to,p_is_final);
	select ARTEFACT_SEQ.currval into p_pk from dual;
	
	INSERT INTO CONCEPT_SCHEME
           (CON_SCH_ID)
     VALUES
           (p_pk);
END;
/



-- ENUMERATIONS
CREATE TABLE ENUMERATIONS
	(
	ENUM_ID NUMBER(18) NOT NULL,
	ENUM_NAME VARCHAR2(30) NULL,
	ENUM_VALUE VARCHAR2(50) NULL,
	ENUM_VALUE_DATATYPE VARCHAR2(20) NULL
	) 
;

ALTER TABLE ENUMERATIONS ADD CONSTRAINT
	PK_ENUMERATIONS PRIMARY KEY (ENUM_ID) 
;


-- TEXT_FORMAT
CREATE TABLE TEXT_FORMAT
	(
	FAC_ID NUMBER(18) NOT NULL, 
	COMP_ID NUMBER(18) NOT NULL,
	FACET_TYPE_ENUM NUMBER(18) NOT NULL,
	FACET_VALUE NVARCHAR2(51) NULL
	) 
;
ALTER TABLE TEXT_FORMAT ADD CONSTRAINT
	PK_TEXT_FORMAT PRIMARY KEY (FAC_ID)
;

ALTER TABLE TEXT_FORMAT ADD CONSTRAINT
	FK_TEXT_FORMAT_COMPONENT FOREIGN KEY
	(COMP_ID) REFERENCES COMPONENT (COMP_ID) ON DELETE CASCADE
;

ALTER TABLE TEXT_FORMAT ADD CONSTRAINT
	FK_TEXT_FORMAT_ENUMERATIONS FOREIGN KEY 
	(FACET_TYPE_ENUM) REFERENCES ENUMERATIONS (ENUM_ID) ON DELETE CASCADE 
;


CREATE SEQUENCE TEXT_FORMAT_SEQ
INCREMENT BY 1
START WITH 1
NOMAXVALUE
MINVALUE 1
NOCYCLE
NOCACHE
NOORDER
;


CREATE OR REPLACE PROCEDURE INSERT_TEXT_FORMAT(
	p_compid IN NUMBER, 
	p_facet_type_enum IN NUMBER, 
	p_facet_value IN NVARCHAR2 DEFAULT NULL,
	p_pk OUT NUMBER)
AS
BEGIN
	insert into TEXT_FORMAT (FAC_ID, COMP_ID, FACET_TYPE_ENUM, FACET_VALUE) values (TEXT_FORMAT_SEQ.nextval, p_compid, p_facet_type_enum, p_facet_value);
	select TEXT_FORMAT_SEQ.currval into p_pk from dual;
END;
/

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (1,'DataType','String','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (2,'DataType','BigInteger','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (3,'DataType','Integer','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (4,'DataType','Long','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (5,'DataType','Short','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (6,'DataType','Decimal','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (7,'DataType','Float','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (8,'DataType','Double','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (9,'DataType','Boolean','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (10,'DataType','DateTime','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (11,'DataType','Time','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (12,'DataType','Date','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (13,'DataType','Year','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (14,'DataType','Month','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (15,'DataType','Day','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (16,'DataType','MonthDay','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (17,'DataType','YearMonth','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (18,'DataType','Duration','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (19,'DataType','Timespan','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (20,'DataType','URI','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (21,'DataType','Count','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (22,'DataType','InclusiveValueRange','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (23,'DataType','ExclusiveValueRange','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (24,'DataType','Incremental','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (25,'DataType','ObservationalTimePeriod','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (26,'DataType','Base64Binary','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (27,'DataType','Alpha','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (28,'DataType','AlphaNumeric','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (29,'DataType','Numeric','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (30,'DataType','StandardTimePeriod','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (31,'DataType','BasicTimePeriod','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (32,'DataType','GregorianTimePeriod','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (33,'DataType','GregorianYear','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (34,'DataType','GregorianYearMonth','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (35,'DataType','GregorianDay','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (36,'DataType','ReportingTimePeriod','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (37,'DataType','ReportingYear','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (38,'DataType','ReportingSemester','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (39,'DataType','ReportingTrimester','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (40,'DataType','ReportingQuarter','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (41,'DataType','ReportingMonth','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (42,'DataType','ReportingWeek','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (43,'DataType','ReportingDay','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (44,'DataType','TimeRange','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (45,'DataType','XHTML','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (46,'DataType','KeyValues','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (47,'DataType','IdentifiableReference','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (48,'DataType','DataSetReference','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (49,'DataType','AttachmentConstraintReference','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (50,'FacetType','isSequence','Boolean')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (51,'FacetType','isInclusive','Boolean')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (52,'FacetType','minLength','Integer')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (53,'FacetType','maxLength','Integer')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (54,'FacetType','minValue','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (55,'FacetType','maxValue','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (56,'FacetType','startValue','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (57,'FacetType','endValue','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (58,'FacetType','decimals','Integer')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (59,'FacetType','interval','Double')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (60,'FacetType','timeInterval','Duration')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (61,'FacetType','pattern','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (62,'FacetType','enumeration','ItemScheme')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (63,'FacetType','startTime','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (64,'FacetType','endTime','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (65,'FacetType','isMultiLingual','Boolean')
;


update DB_VERSION SET VERSION = '2.7'
;
