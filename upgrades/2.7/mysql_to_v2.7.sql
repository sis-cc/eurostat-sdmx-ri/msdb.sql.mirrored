-- create sequences for ARTEFACT, ITEM, COMPONENT, DSD_GROUP, LOCALISED_STRING
alter table artefact modify column art_id BIGINT NOT NULL AUTO_INCREMENT
;

alter table item modify column item_id BIGINT NOT NULL AUTO_INCREMENT
;

alter table component modify column comp_id BIGINT NOT NULL AUTO_INCREMENT
;

alter table dsd_group modify column gr_id BIGINT NOT NULL AUTO_INCREMENT
;

alter table localised_string modify column ls_id BIGINT NOT NULL AUTO_INCREMENT
;

-- add new column to dataset_column
alter table dataset_column add LASTRETRIEVAL datetime
;

-- stored procedures
CREATE PROCEDURE INSERT_LOCAL_CODE(
	IN p_id varchar(50),
	IN p_column_id bigint,
	OUT p_pk bigint) 
BEGIN
		insert into ITEM (ID) VALUES (p_id);
		set p_pk = LAST_INSERT_ID();
		insert into LOCAL_CODE (LCD_ID, COLUMN_ID) VALUES (p_pk, p_column_id);
END
;

CREATE PROCEDURE UPDATE_TRANSCODING_RULE(
	IN p_tr_id bigint,
	IN p_tr_rule_id bigint,
	IN p_lcd_id bigint,
	IN p_cd_id bigint,
	OUT p_pk bigint) 
BEGIN
	    if p_tr_rule_id is null then
			insert into TRANSCODING_RULE (TR_ID) values (p_tr_id);
			set p_pk = LAST_INSERT_ID();
		else
			-- clean up
			delete from TRANSCODING_RULE_DSD_CODE where TR_RULE_ID = p_tr_rule_id;
			delete from TRANSCODING_RULE_LOCAL_CODE where TR_RULE_ID = p_tr_rule_id;
			set p_pk = p_tr_rule_id;
		end if;
			
		insert into TRANSCODING_RULE_DSD_CODE (TR_RULE_ID, CD_ID) VALUES (p_pk, p_cd_id);
		insert into TRANSCODING_RULE_LOCAL_CODE (TR_RULE_ID, LCD_ID) VALUES (p_pk, p_lcd_id);
END
;

CREATE PROCEDURE INSERT_TRANSCODING(
	IN p_map_id bigint,
	IN p_expression varchar(150),
	OUT p_pk bigint)
BEGIN
	insert into TRANSCODING (MAP_ID, EXPRESSION) values (p_map_id,p_expression);
	set p_pk = LAST_INSERT_ID();
END
;

CREATE PROCEDURE INSERT_TRANSCODING_SCRIPT(
	IN p_tr_id bigint,
	IN p_title varchar(128),
	IN p_content text,
	OUT p_pk bigint)
BEGIN
	insert into TRANSCODING_SCRIPT (TR_ID, SCRIPT_TITLE, SCRIPT_CONTENT) values (p_tr_id, p_title, p_content);
	set p_pk = LAST_INSERT_ID();
END
;

CREATE PROCEDURE INSERT_ITEM(
	IN p_id varchar(50),
	OUT p_pk bigint)
BEGIN
	insert into ITEM (ID) values (p_id);
	set p_pk = LAST_INSERT_ID();
END
;

CREATE PROCEDURE INSERT_ARTEFACT(
	IN p_id varchar(50),
	IN p_version varchar(50),
	IN p_agency varchar(50),
	IN p_valid_from varchar(50),
	IN p_valid_to varchar(50),
	IN p_is_final int,
	OUT p_pk bigint)
BEGIN
	insert into ARTEFACT (ID,VERSION, AGENCY, VALID_FROM, VALID_TO, IS_FINAL) values (p_id,p_version,p_agency,p_valid_from,p_valid_to,p_is_final);
	set p_pk = LAST_INSERT_ID();
END
;

CREATE PROCEDURE INSERT_LOCALISED_STRING(
	IN p_item_id bigint,
	IN p_art_id bigInt,
	IN p_text nvarchar(4000),
	IN p_type varchar(10),
	IN p_language varchar(50),
	OUT p_pk bigint) 
BEGIN
	insert into LOCALISED_STRING (ART_ID, ITEM_ID, TEXT, TYPE, LANGUAGE) values (p_art_id, p_item_id, p_text, p_type, p_language);
	set p_pk = LAST_INSERT_ID();
END
;

CREATE PROCEDURE INSERT_COMPONENT(
	IN p_type varchar(50) ,
	IN p_dsd_id bigint ,
	IN p_con_id bigint ,
	IN p_cl_id bigint,
	IN p_is_freq_dim int,
	IN p_is_measure_dim int,
	IN p_att_ass_level varchar(11),
	IN p_att_status varchar(11),
	IN p_att_is_time_format int,
	IN p_xs_attlevel_ds int,
	IN p_xs_attlevel_group int,
	IN p_xs_attlevel_section int,
	IN p_xs_attlevel_obs int,
	IN p_xs_measure_code varchar(50),
	OUT p_pk bigint) 
BEGIN
	insert into COMPONENT (TYPE, DSD_ID, CON_ID, CL_ID, IS_FREQ_DIM, IS_MEASURE_DIM, ATT_ASS_LEVEL, ATT_STATUS, ATT_IS_TIME_FORMAT, XS_ATTLEVEL_DS, XS_ATTLEVEL_GROUP, XS_ATTLEVEL_SECTION, XS_ATTLEVEL_OBS, XS_MEASURE_CODE) 
		values (p_type, p_dsd_id, p_con_id, p_cl_id, p_is_freq_dim, p_is_measure_dim, p_att_ass_level, p_att_status, p_att_is_time_format, p_xs_attlevel_ds, p_xs_attlevel_group, p_xs_attlevel_section, p_xs_attlevel_obs, p_xs_measure_code);
	set p_pk = LAST_INSERT_ID();
END
;

CREATE PROCEDURE INSERT_DSD_GROUP(
	IN p_id varchar(50) ,
	IN p_dsd_id bigint ,
	OUT p_pk bigint) 
BEGIN
	insert into DSD_GROUP (ID, DSD_ID) 
		values (p_id, p_dsd_id);
	set p_pk = LAST_INSERT_ID();
END
;

CREATE PROCEDURE INSERT_DSD_CODE(
	IN p_id varchar(50),
	IN p_cl_id bigint,
	IN p_parent_code_id bigint,
	OUT p_pk bigint) 
BEGIN
		insert into ITEM (ID) VALUES (p_id);
		set p_pk = LAST_INSERT_ID();
		insert into DSD_CODE (LCD_ID, CL_ID, PARENT_CODE_ID) VALUES (p_pk, p_cl_id, p_parent_code_id);
	
END
;

CREATE PROCEDURE INSERT_CATEGORY(
	IN p_id varchar(50),
	IN p_cat_sch_id bigint,
	IN p_parent_cat_id bigint,
	IN p_corder bigint,
	OUT p_pk bigint) 
BEGIN
	if p_corder is null then
		set p_corder = 0;
	end if;
		insert into ITEM (ID) VALUES (p_id);
		set p_pk = LAST_INSERT_ID();
		insert into CATEGORY (CAT_ID, CAT_SCH_ID, PARENT_CAT_ID, CORDER) VALUES (p_pk, p_cat_sch_id, p_parent_cat_id, p_corder);
	
END
;

CREATE PROCEDURE INSERT_CONCEPT(
	IN p_id varchar(50),
	IN p_con_sch_id bigint,
	OUT p_pk bigint) 
BEGIN
		insert into ITEM (ID) VALUES (p_id);
		set p_pk = LAST_INSERT_ID();
		insert into CONCEPT (CON_ID, CON_SCH_ID) VALUES (p_pk, p_con_sch_id);
	
END
;

CREATE PROCEDURE INSERT_HCL_CODE(
	IN p_id varchar(50),
	IN p_version varchar(50),
	IN p_agency varchar(50),
	IN p_valid_from varchar(50),
	IN p_valid_to varchar(50),
	IN p_is_final int,
	IN p_parent_hcode_id bigint,
	IN p_lcd_id bigint,
	IN p_h_id bigint,
	IN p_level_id bigint,
	OUT p_pk bigint)
BEGIN
	insert into ARTEFACT (ID,VERSION, AGENCY, VALID_FROM, VALID_TO, IS_FINAL) values (p_id,p_version,p_agency,p_valid_from,p_valid_to,p_is_final);
	set p_pk = LAST_INSERT_ID();
	
	INSERT INTO HCL_CODE
           (HCODE_ID
           ,PARENT_HCODE_ID
           ,LCD_ID
           ,H_ID
           ,LEVEL_ID)
	VALUES
		(p_pk
        ,p_parent_hcode_id
        ,p_lcd_id
        ,p_h_id
         ,p_level_id);
END
;

CREATE PROCEDURE INSERT_HCL(
	IN p_id varchar(50),
	IN p_version varchar(50),
	IN p_agency varchar(50),
	IN p_valid_from varchar(50),
	IN p_valid_to varchar(50),
	IN p_is_final int,
	OUT p_pk bigint)
BEGIN
	insert into ARTEFACT (ID,VERSION, AGENCY, VALID_FROM, VALID_TO, IS_FINAL) values (p_id,p_version,p_agency,p_valid_from,p_valid_to,p_is_final);
	set p_pk = LAST_INSERT_ID();
	insert into HCL (HCL_ID) values(p_pk);
END
;

CREATE PROCEDURE INSERT_HIERACHY(
	IN p_id varchar(50),
	IN p_version varchar(50),
	IN p_agency varchar(50),
	IN p_valid_from varchar(50),
	IN p_valid_to varchar(50),
	IN p_is_final int,
	IN p_hcl_id bigint,
	OUT p_pk bigint)
BEGIN
	insert into ARTEFACT (ID,VERSION, AGENCY, VALID_FROM, VALID_TO, IS_FINAL) values (p_id,p_version,p_agency,p_valid_from,p_valid_to,p_is_final);
	set p_pk = LAST_INSERT_ID();
	
	INSERT INTO HIERARCHY
           (H_ID
           ,HCL_ID)
     VALUES
           (p_pk
           ,p_hcl_id);
END
;

CREATE PROCEDURE INSERT_HLEVEL(
	IN p_id varchar(50),
	IN p_version varchar(50),
	IN p_agency varchar(50),
	IN p_valid_from varchar(50),
	IN p_valid_to varchar(50),
	IN p_is_final int,
	IN p_h_id bigint,
	IN p_parent_level_id bigint,
	OUT p_pk bigint)
BEGIN
	insert into ARTEFACT (ID,VERSION, AGENCY, VALID_FROM, VALID_TO, IS_FINAL) values (p_id,p_version,p_agency,p_valid_from,p_valid_to,p_is_final);
	set p_pk = LAST_INSERT_ID();
	
	INSERT INTO HLEVEL
           (LEVEL_ID
           ,H_ID
           ,PARENT_LEVEL_ID)
     VALUES
           (p_pk
           ,p_h_id
           ,p_parent_level_id);
END
;

CREATE PROCEDURE INSERT_DSD(
	IN p_id varchar(50),
	IN p_version varchar(50),
	IN p_agency varchar(50),
	IN p_valid_from varchar(50),
	IN p_valid_to varchar(50),
	IN p_is_final int,
	OUT p_pk bigint)
BEGIN
	insert into ARTEFACT (ID,VERSION, AGENCY, VALID_FROM, VALID_TO, IS_FINAL) values (p_id,p_version,p_agency,p_valid_from,p_valid_to,p_is_final);
	set p_pk = LAST_INSERT_ID();
	
	INSERT INTO DSD
           (DSD_ID)
     VALUES
           (p_pk);
END
;

CREATE PROCEDURE INSERT_DATAFLOW(
	IN p_id varchar(50),
	IN p_version varchar(50),
	IN p_agency varchar(50),
	IN p_valid_from varchar(50),
	IN p_valid_to varchar(50),
	IN p_is_final int,
	IN p_dsd_id bigint,
	IN p_map_set_id bigint,
	OUT p_pk bigint)
BEGIN
	insert into ARTEFACT (ID,VERSION, AGENCY, VALID_FROM, VALID_TO, IS_FINAL) values (p_id,p_version,p_agency,p_valid_from,p_valid_to,p_is_final);
	set p_pk = LAST_INSERT_ID();
	
INSERT INTO DATAFLOW
           (DF_ID
           ,DSD_ID
           ,MAP_SET_ID)
     VALUES
           (p_pk
           ,p_dsd_id
           ,p_map_set_id);
END
;

CREATE PROCEDURE INSERT_CATEGORY_SCHEME(
	IN p_id varchar(50),
	IN p_version varchar(50),
	IN p_agency varchar(50),
	IN p_valid_from varchar(50),
	IN p_valid_to varchar(50),
	IN p_is_final int,
	IN p_cs_order bigint,
	OUT p_pk bigint)
BEGIN
	if p_cs_order is null then
		set p_cs_order = 0;
	end if;
	insert into ARTEFACT (ID,VERSION, AGENCY, VALID_FROM, VALID_TO, IS_FINAL) values (p_id,p_version,p_agency,p_valid_from,p_valid_to,p_is_final);
	set p_pk = LAST_INSERT_ID();
	
	INSERT INTO CATEGORY_SCHEME
           (CAT_SCH_ID, CS_ORDER)
     VALUES
           (p_pk, p_cs_order);
END
;

CREATE PROCEDURE INSERT_CODELIST(
	IN p_id varchar(50),
	IN p_version varchar(50),
	IN p_agency varchar(50),
	IN p_valid_from varchar(50),
	IN p_valid_to varchar(50),
	IN p_is_final int,
	OUT p_pk bigint)
BEGIN
	insert into ARTEFACT (ID,VERSION, AGENCY, VALID_FROM, VALID_TO, IS_FINAL) values (p_id,p_version,p_agency,p_valid_from,p_valid_to,p_is_final);
	set p_pk = LAST_INSERT_ID();
	
	INSERT INTO CODELIST
           (CL_ID)
     VALUES
           (p_pk);
END
;

CREATE PROCEDURE INSERT_CONCEPT_SCHEME(
	IN p_id varchar(50),
	IN p_version varchar(50),
	IN p_agency varchar(50),
	IN p_valid_from varchar(50),
	IN p_valid_to varchar(50),
	IN p_is_final int,
	OUT p_pk bigint)
BEGIN
	insert into ARTEFACT (ID,VERSION, AGENCY, VALID_FROM, VALID_TO, IS_FINAL) values (p_id,p_version,p_agency,p_valid_from,p_valid_to,p_is_final);
	set p_pk = LAST_INSERT_ID();
	
	INSERT INTO CONCEPT_SCHEME
           (CON_SCH_ID)
     VALUES
           (p_pk);
END
;

-- ENUMERATIONS
CREATE TABLE ENUMERATIONS
	(
	ENUM_ID BIGINT NOT NULL,
	ENUM_NAME VARCHAR(30) NULL,
	ENUM_VALUE VARCHAR(50) NULL,
	ENUM_VALUE_DATATYPE VARCHAR(20) NULL
	) ENGINE=InnoDB DEFAULT CHARSET=utf8
;
ALTER TABLE ENUMERATIONS ADD CONSTRAINT
	PK_ENUMERATIONS PRIMARY KEY (ENUM_ID) 
;


-- TEXT_FORMAT
CREATE TABLE TEXT_FORMAT
	(
	FAC_ID BIGINT NOT NULL AUTO_INCREMENT,
	COMP_ID BIGINT NOT NULL,
	FACET_TYPE_ENUM BIGINT NOT NULL,
	FACET_VALUE NVARCHAR(51) NULL,
	PRIMARY KEY (FAC_ID)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8
;
ALTER TABLE TEXT_FORMAT ADD CONSTRAINT
	FK_TEXT_FORMAT_COMPONENT FOREIGN KEY
	(COMP_ID) REFERENCES COMPONENT (COMP_ID) ON DELETE CASCADE
;
ALTER TABLE TEXT_FORMAT ADD CONSTRAINT
	FK_TEXT_FORMAT_ENUMERATIONS FOREIGN KEY 
	(FACET_TYPE_ENUM) REFERENCES ENUMERATIONS (ENUM_ID) ON DELETE CASCADE 
;


CREATE PROCEDURE INSERT_TEXT_FORMAT(
	IN p_compid BIGINT, 
	IN p_facet_type_enum BIGINT, 
	IN p_facet_value VARCHAR(51),
	OUT p_pk BIGINT)
BEGIN
	insert into TEXT_FORMAT (COMP_ID, FACET_TYPE_ENUM, FACET_VALUE) values (p_compid, p_facet_type_enum, p_facet_value);
	set p_pk = LAST_INSERT_ID();
END
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (1,'DataType','String','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (2,'DataType','BigInteger','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (3,'DataType','Integer','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (4,'DataType','Long','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (5,'DataType','Short','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (6,'DataType','Decimal','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (7,'DataType','Float','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (8,'DataType','Double','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (9,'DataType','Boolean','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (10,'DataType','DateTime','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (11,'DataType','Time','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (12,'DataType','Date','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (13,'DataType','Year','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (14,'DataType','Month','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (15,'DataType','Day','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (16,'DataType','MonthDay','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (17,'DataType','YearMonth','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (18,'DataType','Duration','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (19,'DataType','Timespan','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (20,'DataType','URI','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (21,'DataType','Count','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (22,'DataType','InclusiveValueRange','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (23,'DataType','ExclusiveValueRange','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (24,'DataType','Incremental','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (25,'DataType','ObservationalTimePeriod','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (26,'DataType','Base64Binary','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (27,'DataType','Alpha','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (28,'DataType','AlphaNumeric','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (29,'DataType','Numeric','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (30,'DataType','StandardTimePeriod','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (31,'DataType','BasicTimePeriod','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (32,'DataType','GregorianTimePeriod','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (33,'DataType','GregorianYear','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (34,'DataType','GregorianYearMonth','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (35,'DataType','GregorianDay','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (36,'DataType','ReportingTimePeriod','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (37,'DataType','ReportingYear','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (38,'DataType','ReportingSemester','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (39,'DataType','ReportingTrimester','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (40,'DataType','ReportingQuarter','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (41,'DataType','ReportingMonth','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (42,'DataType','ReportingWeek','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (43,'DataType','ReportingDay','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (44,'DataType','TimeRange','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (45,'DataType','XHTML','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (46,'DataType','KeyValues','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (47,'DataType','IdentifiableReference','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (48,'DataType','DataSetReference','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (49,'DataType','AttachmentConstraintReference','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (50,'FacetType','isSequence','Boolean')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (51,'FacetType','isInclusive','Boolean')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (52,'FacetType','minLength','Integer')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (53,'FacetType','maxLength','Integer')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (54,'FacetType','minValue','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (55,'FacetType','maxValue','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (56,'FacetType','startValue','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (57,'FacetType','endValue','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (58,'FacetType','decimals','Integer')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (59,'FacetType','interval','Double')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (60,'FacetType','timeInterval','Duration')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (61,'FacetType','pattern','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (62,'FacetType','enumeration','ItemScheme')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (63,'FacetType','startTime','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (64,'FacetType','endTime','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (65,'FacetType','isMultiLingual','Boolean')
;

update DB_VERSION SET VERSION = '2.7'
;

