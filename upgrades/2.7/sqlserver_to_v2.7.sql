
-- SET ITEM.ITEM_ID as IDENTITY

BEGIN
CREATE TABLE Tmp_ITEM
	(
	ITEM_ID bigint NOT NULL IDENTITY (1, 1),
	ID varchar(50) NULL
	);
SET IDENTITY_INSERT Tmp_ITEM ON;
IF EXISTS(SELECT * FROM ITEM)
	 EXEC('INSERT INTO Tmp_ITEM (ITEM_ID, ID)
		SELECT ITEM_ID, ID FROM ITEM WITH (HOLDLOCK TABLOCKX)');
SET IDENTITY_INSERT Tmp_ITEM OFF;
ALTER TABLE CATEGORY
	DROP CONSTRAINT CAT_ID;
ALTER TABLE DSD_CODE
	DROP CONSTRAINT DC_ID;
ALTER TABLE CONCEPT
	DROP CONSTRAINT CON_ID;
ALTER TABLE LOCALISED_STRING
	DROP CONSTRAINT FK_LOCALISED_STRING_ITEM;
ALTER TABLE LOCAL_CODE
	DROP CONSTRAINT LC_ID;
DROP TABLE ITEM;
EXECUTE sp_rename N'Tmp_ITEM', N'ITEM', 'OBJECT' ;
ALTER TABLE ITEM ADD CONSTRAINT	PK_ITEM PRIMARY KEY CLUSTERED 
	(
	ITEM_ID
	);
END
;

ALTER TABLE LOCAL_CODE ADD CONSTRAINT
	LC_ID FOREIGN KEY
	(
	LCD_ID
	) REFERENCES ITEM
	(
	ITEM_ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
;

ALTER TABLE LOCALISED_STRING ADD CONSTRAINT
	FK_LOCALISED_STRING_ITEM FOREIGN KEY
	(
	ITEM_ID
	) REFERENCES ITEM
	(
	ITEM_ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
;

ALTER TABLE CONCEPT ADD CONSTRAINT
	CON_ID FOREIGN KEY
	(
	CON_ID
	) REFERENCES ITEM
	(
	ITEM_ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
;

ALTER TABLE DSD_CODE ADD CONSTRAINT
	DC_ID FOREIGN KEY
	(
	LCD_ID
	) REFERENCES ITEM
	(
	ITEM_ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
;

ALTER TABLE CATEGORY ADD CONSTRAINT
	CAT_ID FOREIGN KEY
	(
	CAT_ID
	) REFERENCES ITEM
	(
	ITEM_ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
;



-- SET ARTEFACT.ART_ID as IDENTITY
BEGIN
CREATE TABLE Tmp_ARTEFACT
	(
	ART_ID bigint NOT NULL IDENTITY (1, 1),
	ID varchar(50) NULL,
	VERSION varchar(50) NOT NULL,
	AGENCY varchar(50) NULL,
	VALID_FROM varchar(50) NULL,
	VALID_TO varchar(50) NULL,
	IS_FINAL int NULL
	); 
SET IDENTITY_INSERT Tmp_ARTEFACT ON;
IF EXISTS(SELECT * FROM ARTEFACT)
	 EXEC('INSERT INTO Tmp_ARTEFACT (ART_ID, ID, VERSION, AGENCY, VALID_FROM, VALID_TO, IS_FINAL)
		SELECT ART_ID, ID, VERSION, AGENCY, VALID_FROM, VALID_TO, IS_FINAL FROM ARTEFACT WITH (HOLDLOCK TABLOCKX)');
SET IDENTITY_INSERT Tmp_ARTEFACT OFF;
ALTER TABLE HCL
	DROP CONSTRAINT HCL_ARTEFACT_FK;
ALTER TABLE HCL_CODE
	DROP CONSTRAINT HCL_CODE_ARTEFACT_FK;
ALTER TABLE HIERARCHY
	DROP CONSTRAINT HIERARCHY_ARTEFACT_FK;
ALTER TABLE HLEVEL
	DROP CONSTRAINT LEVEL_ARTEFACT_FK;
ALTER TABLE DSD
	DROP CONSTRAINT DSD_ART_ID;
ALTER TABLE DATAFLOW
	DROP CONSTRAINT FK_DATAFLOW_ARTEFACT;
ALTER TABLE CATEGORY_SCHEME
	DROP CONSTRAINT CAT_SCH_ART_ID;
ALTER TABLE CODELIST
	DROP CONSTRAINT CL_ID;
ALTER TABLE CONCEPT_SCHEME
	DROP CONSTRAINT CON_SCH_ART_ID;
ALTER TABLE LOCALISED_STRING
	DROP CONSTRAINT FK_LOCALISED_STRING_ARTEFACT;
DROP TABLE ARTEFACT;
EXECUTE sp_rename N'Tmp_ARTEFACT', N'ARTEFACT', 'OBJECT' ;
ALTER TABLE ARTEFACT ADD CONSTRAINT
	PK_ARTEFACT PRIMARY KEY CLUSTERED 
	(
	ART_ID
	);
END
;

ALTER TABLE LOCALISED_STRING ADD CONSTRAINT
	FK_LOCALISED_STRING_ARTEFACT FOREIGN KEY
	(
	ART_ID
	) REFERENCES ARTEFACT
	(
	ART_ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
;

ALTER TABLE CONCEPT_SCHEME ADD CONSTRAINT
	CON_SCH_ART_ID FOREIGN KEY
	(
	CON_SCH_ID
	) REFERENCES ARTEFACT
	(
	ART_ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
;

ALTER TABLE CODELIST ADD CONSTRAINT
	CL_ID FOREIGN KEY
	(
	CL_ID
	) REFERENCES ARTEFACT
	(
	ART_ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
;

ALTER TABLE CATEGORY_SCHEME ADD CONSTRAINT
	CAT_SCH_ART_ID FOREIGN KEY
	(
	CAT_SCH_ID
	) REFERENCES ARTEFACT
	(
	ART_ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
;

ALTER TABLE DATAFLOW ADD CONSTRAINT
	FK_DATAFLOW_ARTEFACT FOREIGN KEY
	(
	DF_ID
	) REFERENCES ARTEFACT
	(
	ART_ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
;

ALTER TABLE DSD ADD CONSTRAINT
	DSD_ART_ID FOREIGN KEY
	(
	DSD_ID
	) REFERENCES ARTEFACT
	(
	ART_ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
;

ALTER TABLE HLEVEL ADD CONSTRAINT
	LEVEL_ARTEFACT_FK FOREIGN KEY
	(
	LEVEL_ID
	) REFERENCES ARTEFACT
	(
	ART_ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
;

ALTER TABLE HIERARCHY ADD CONSTRAINT
	HIERARCHY_ARTEFACT_FK FOREIGN KEY
	(
	H_ID
	) REFERENCES ARTEFACT
	(
	ART_ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
;

ALTER TABLE HCL_CODE ADD CONSTRAINT
	HCL_CODE_ARTEFACT_FK FOREIGN KEY
	(
	HCODE_ID
	) REFERENCES ARTEFACT
	(
	ART_ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
;

ALTER TABLE HCL ADD CONSTRAINT
	HCL_ARTEFACT_FK FOREIGN KEY
	(
	HCL_ID
	) REFERENCES ARTEFACT
	(
	ART_ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
;

-- SET COMPONENT.COMP_ID as IDENTITY
ALTER TABLE COMPONENT
	DROP CONSTRAINT FK_COMPONENT_DSD
;

ALTER TABLE COMPONENT
	DROP CONSTRAINT FK_COMPONENT_CONCEPT
;

ALTER TABLE COMPONENT
	DROP CONSTRAINT FK_COMPONENT_CODELIST
;

BEGIN
CREATE TABLE Tmp_COMPONENT
	(
	COMP_ID bigint NOT NULL IDENTITY (1, 1),
	TYPE varchar(50) NOT NULL,
	DSD_ID bigint NOT NULL,
	CON_ID bigint NOT NULL,
	CL_ID bigint NULL,
	IS_FREQ_DIM int NULL,
	IS_MEASURE_DIM int NULL,
	ATT_ASS_LEVEL varchar(11) NULL,
	ATT_STATUS varchar(11) NULL,
	ATT_IS_TIME_FORMAT int NULL,
	XS_ATTLEVEL_DS int NULL,
	XS_ATTLEVEL_GROUP int NULL,
	XS_ATTLEVEL_SECTION int NULL,
	XS_ATTLEVEL_OBS int NULL,
	XS_MEASURE_CODE varchar(50) NULL
	);
	
SET IDENTITY_INSERT Tmp_COMPONENT ON;
IF EXISTS(SELECT * FROM COMPONENT)
	 EXEC('INSERT INTO Tmp_COMPONENT (COMP_ID, TYPE, DSD_ID, CON_ID, CL_ID, IS_FREQ_DIM, IS_MEASURE_DIM, ATT_ASS_LEVEL, ATT_STATUS, ATT_IS_TIME_FORMAT, XS_ATTLEVEL_DS, XS_ATTLEVEL_GROUP, XS_ATTLEVEL_SECTION, XS_ATTLEVEL_OBS, XS_MEASURE_CODE)
		SELECT COMP_ID, TYPE, DSD_ID, CON_ID, CL_ID, IS_FREQ_DIM, IS_MEASURE_DIM, ATT_ASS_LEVEL, ATT_STATUS, ATT_IS_TIME_FORMAT, XS_ATTLEVEL_DS, XS_ATTLEVEL_GROUP, XS_ATTLEVEL_SECTION, XS_ATTLEVEL_OBS, XS_MEASURE_CODE FROM COMPONENT WITH (HOLDLOCK TABLOCKX)');
SET IDENTITY_INSERT Tmp_COMPONENT OFF;
ALTER TABLE ATT_GROUP
	DROP CONSTRAINT FK_ATT_GROUP_COMPONENT;
ALTER TABLE DIM_GROUP
	DROP CONSTRAINT FK_DIM_GROUP_COMPONENT;
ALTER TABLE COM_COL_MAPPING_COMPONENT
	DROP CONSTRAINT FK_COM_COL_MAPP_COMP_COMP;
DROP TABLE COMPONENT;
EXECUTE sp_rename N'Tmp_COMPONENT', N'COMPONENT', 'OBJECT' ;
ALTER TABLE COMPONENT ADD CONSTRAINT
	PK_COMPONENT PRIMARY KEY CLUSTERED 
	(
	COMP_ID
	);

ALTER TABLE COMPONENT ADD CONSTRAINT
	FK_COMPONENT_CODELIST FOREIGN KEY
	(
	CL_ID
	) REFERENCES CODELIST
	(
	CL_ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION ;
	
ALTER TABLE COMPONENT ADD CONSTRAINT
	FK_COMPONENT_CONCEPT FOREIGN KEY
	(
	CON_ID
	) REFERENCES CONCEPT
	(
	CON_ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION ;
	
ALTER TABLE COMPONENT ADD CONSTRAINT
	FK_COMPONENT_DSD FOREIGN KEY
	(
	DSD_ID
	) REFERENCES DSD
	(
	DSD_ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE ;
	
END
;

ALTER TABLE COM_COL_MAPPING_COMPONENT ADD CONSTRAINT
	FK_COM_COL_MAPP_COMP_COMP FOREIGN KEY
	(
	COMP_ID
	) REFERENCES COMPONENT
	(
	COMP_ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
;

ALTER TABLE DIM_GROUP ADD CONSTRAINT
	FK_DIM_GROUP_COMPONENT FOREIGN KEY
	(
	COMP_ID
	) REFERENCES COMPONENT
	(
	COMP_ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
;

ALTER TABLE ATT_GROUP ADD CONSTRAINT
	FK_ATT_GROUP_COMPONENT FOREIGN KEY
	(
	COMP_ID
	) REFERENCES COMPONENT
	(
	COMP_ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE 
;

-- SET DSD_GROUP.GR_ID as IDENTITY
ALTER TABLE DSD_GROUP
	DROP CONSTRAINT FK_DSD_GROUP_DSD
;

BEGIN
CREATE TABLE Tmp_DSD_GROUP
	(
	GR_ID bigint NOT NULL IDENTITY (1, 1),
	ID varchar(50) NOT NULL,
	DSD_ID bigint NOT NULL
	);

SET IDENTITY_INSERT Tmp_DSD_GROUP ON;

IF EXISTS(SELECT * FROM DSD_GROUP)
	 EXEC('INSERT INTO Tmp_DSD_GROUP (GR_ID, ID, DSD_ID)
		SELECT GR_ID, ID, DSD_ID FROM DSD_GROUP WITH (HOLDLOCK TABLOCKX)');

SET IDENTITY_INSERT Tmp_DSD_GROUP OFF;
ALTER TABLE ATT_GROUP
	DROP CONSTRAINT FK_ATT_GROUP_DSD_GROUP;

ALTER TABLE DIM_GROUP
	DROP CONSTRAINT FK_DIM_GROUP_DSD_GROUP;

DROP TABLE DSD_GROUP;

EXECUTE sp_rename N'Tmp_DSD_GROUP', N'DSD_GROUP', 'OBJECT';

ALTER TABLE DSD_GROUP ADD CONSTRAINT
	PK_DSD_GROUP PRIMARY KEY CLUSTERED 
	(
	GR_ID
	);
end
;

ALTER TABLE DSD_GROUP ADD CONSTRAINT
	FK_DSD_GROUP_DSD FOREIGN KEY
	(
	DSD_ID
	) REFERENCES DSD
	(
	DSD_ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE
;	 
	
ALTER TABLE DIM_GROUP ADD CONSTRAINT
	FK_DIM_GROUP_DSD_GROUP FOREIGN KEY
	(
	GR_ID
	) REFERENCES DSD_GROUP
	(
	GR_ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
;	

ALTER TABLE ATT_GROUP ADD CONSTRAINT
	FK_ATT_GROUP_DSD_GROUP FOREIGN KEY
	(
	GR_ID
	) REFERENCES DSD_GROUP
	(
	GR_ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
;	


-- SET LOCALISED_STRING.LS_ID as IDENTIT
ALTER TABLE LOCALISED_STRING
	DROP CONSTRAINT FK_LOCALISED_STRING_ITEM
;

ALTER TABLE LOCALISED_STRING
	DROP CONSTRAINT FK_LOCALISED_STRING_ARTEFACT
;

BEGIN
CREATE TABLE Tmp_LOCALISED_STRING
	(
	LS_ID bigint NOT NULL IDENTITY (1, 1),
	TYPE varchar(10) NULL,
	ITEM_ID bigint NULL,
	ART_ID bigint NULL,
	LANGUAGE varchar(50) NOT NULL,
	TEXT nvarchar(4000) NOT NULL
	);
SET IDENTITY_INSERT Tmp_LOCALISED_STRING ON;
IF EXISTS(SELECT * FROM LOCALISED_STRING)
	 EXEC('INSERT INTO Tmp_LOCALISED_STRING (LS_ID, TYPE, ITEM_ID, ART_ID, LANGUAGE, TEXT)
		SELECT LS_ID, TYPE, ITEM_ID, ART_ID, LANGUAGE, TEXT FROM LOCALISED_STRING WITH (HOLDLOCK TABLOCKX)');
SET IDENTITY_INSERT Tmp_LOCALISED_STRING OFF;
DROP TABLE LOCALISED_STRING;
EXECUTE sp_rename N'Tmp_LOCALISED_STRING', N'LOCALISED_STRING', 'OBJECT';
ALTER TABLE LOCALISED_STRING ADD CONSTRAINT
	PK_LOCALISED_STRING PRIMARY KEY CLUSTERED 
	(
	LS_ID
	);
ALTER TABLE LOCALISED_STRING ADD CONSTRAINT
	FK_LOCALISED_STRING_ARTEFACT FOREIGN KEY
	(
	ART_ID
	) REFERENCES ARTEFACT
	(
	ART_ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE;
	
ALTER TABLE LOCALISED_STRING ADD CONSTRAINT
	FK_LOCALISED_STRING_ITEM FOREIGN KEY
	(
	ITEM_ID
	) REFERENCES ITEM
	(
	ITEM_ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  CASCADE; 
	
END
;

-- add new column to dataset_column
alter table dataset_column add LASTRETRIEVAL datetime NULL
;

-- fix constrain order to improve performance
alter table TRANSCODING_RULE_LOCAL_CODE drop constraint PK_TRANSCODING_RULE_LOCAL_CODE
;

ALTER TABLE TRANSCODING_RULE_LOCAL_CODE ADD CONSTRAINT PK_TRANSCODING_RULE_LOCAL_CODE 
	PRIMARY KEY CLUSTERED (TR_RULE_ID, LCD_ID)
;

-- drop sequence table
drop table SEQUENCE_ID
;


-- stored procedures

CREATE PROCEDURE INSERT_LOCAL_CODE
	@p_id varchar(50), 
	@p_column_id bigint,
	@p_pk bigint OUT 
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

		insert into ITEM (ID) VALUES (@p_id);
		set @p_pk = SCOPE_IDENTITY();
		insert into LOCAL_CODE (LCD_ID, COLUMN_ID) VALUES (@p_pk, @p_column_id);
	
		IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;


CREATE PROCEDURE UPDATE_TRANSCODING_RULE
	-- Add the parameters for the stored procedure here 
	@p_tr_id bigint,
	@p_tr_rule_id bigint,
	@p_lcd_id bigint,
	@p_cd_id bigint,
	@p_pk bigint OUT 
AS
BEGIN
	
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

		-- Insert statements for procedure here
		if @p_tr_rule_id is null
		begin
			insert into TRANSCODING_RULE (TR_ID) values (@p_tr_id);
			set @p_pk = SCOPE_IDENTITY();
		end
		else
		begin
			-- clean up
			delete from TRANSCODING_RULE_DSD_CODE where TR_RULE_ID = @p_tr_rule_id;
			delete from TRANSCODING_RULE_LOCAL_CODE where TR_RULE_ID = @p_tr_rule_id;
			set @p_pk = @p_tr_rule_id
		end
		
		insert into TRANSCODING_RULE_DSD_CODE (TR_RULE_ID, CD_ID) VALUES (@p_pk, @p_cd_id);
		insert into TRANSCODING_RULE_LOCAL_CODE (TR_RULE_ID, LCD_ID) VALUES (@p_pk, @p_lcd_id);
		
		IF @starttrancount = 0 
			COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );	
	END CATCH
END
;


CREATE PROCEDURE INSERT_TRANSCODING
	@p_map_id bigint, 
	@p_expression varchar(150) = null,
	@p_pk bigint out
AS
BEGIN
	SET NOCOUNT ON;
	insert into TRANSCODING (MAP_ID, EXPRESSION) values (@p_map_id,@p_expression);
	set @p_pk = SCOPE_IDENTITY();
END
;


CREATE PROCEDURE INSERT_TRANSCODING_SCRIPT
	@p_tr_id bigint, 
	@p_title varchar(128),
	@p_content text,
	@p_pk bigint out
AS
BEGIN
	SET NOCOUNT ON;
	insert into TRANSCODING_SCRIPT (TR_ID, SCRIPT_TITLE, SCRIPT_CONTENT) values (@p_tr_id, @p_title, @p_content);
	set @p_pk=SCOPE_IDENTITY();
END
;


CREATE PROCEDURE INSERT_ITEM
	@p_id varchar(50),
	@p_pk bigint out
AS
BEGIN
	SET NOCOUNT ON;
	insert into ITEM (ID) values (@p_id);
	set @p_pk=SCOPE_IDENTITY();
END
;


CREATE PROCEDURE INSERT_ARTEFACT
	@p_id varchar(50),
	@p_version varchar(50),
	@p_agency varchar(50),
	@p_valid_from varchar(50) = NULL,
	@p_valid_to varchar(50)= NULL,
	@p_is_final int = NULL,
	@p_pk bigint out
AS
BEGIN
	SET NOCOUNT ON;
	insert into ARTEFACT (ID,VERSION, AGENCY, VALID_FROM, VALID_TO, IS_FINAL) values (@p_id,@p_version,@p_agency,@p_valid_from,@p_valid_to,@p_is_final);
	set @p_pk=SCOPE_IDENTITY();
END
;


CREATE PROCEDURE INSERT_LOCALISED_STRING
	@p_item_id bigint = null,
	@p_art_id bigInt = null,
	@p_text nvarchar(4000),
	@p_type varchar(10),
	@p_language varchar(50),
	@p_pk bigint OUT 
AS
BEGIN
	SET NOCOUNT ON;
	insert into LOCALISED_STRING (ART_ID, ITEM_ID, TEXT, TYPE, LANGUAGE) values (@p_art_id, @p_item_id, @p_text, @p_type, @p_language);
	set @p_pk = SCOPE_IDENTITY();
END
;


CREATE PROCEDURE INSERT_COMPONENT
	@p_type varchar(50) ,
	@p_dsd_id bigint ,
	@p_con_id bigint ,
	@p_cl_id bigint=NULL,
	@p_is_freq_dim int=NULL,
	@p_is_measure_dim int=NULL,
	@p_att_ass_level varchar(11)=NULL,
	@p_att_status varchar(11)=NULL,
	@p_att_is_time_format int=NULL,
	@p_xs_attlevel_ds int=NULL,
	@p_xs_attlevel_group int=NULL,
	@p_xs_attlevel_section int=NULL,
	@p_xs_attlevel_obs int=NULL,
	@p_xs_measure_code varchar(50)=NULL,

	@p_pk bigint OUT 
AS
BEGIN
	SET NOCOUNT ON;
	insert into COMPONENT (TYPE, DSD_ID, CON_ID, CL_ID, IS_FREQ_DIM, IS_MEASURE_DIM, ATT_ASS_LEVEL, ATT_STATUS, ATT_IS_TIME_FORMAT, XS_ATTLEVEL_DS, XS_ATTLEVEL_GROUP, XS_ATTLEVEL_SECTION, XS_ATTLEVEL_OBS, XS_MEASURE_CODE) 
		values (@p_type, @p_dsd_id, @p_con_id, @p_cl_id, @p_is_freq_dim, @p_is_measure_dim, @p_att_ass_level, @p_att_status, @p_att_is_time_format, @p_xs_attlevel_ds, @p_xs_attlevel_group, @p_xs_attlevel_section, @p_xs_attlevel_obs, @p_xs_measure_code);
	set @p_pk = SCOPE_IDENTITY();
END
;


CREATE PROCEDURE INSERT_DSD_GROUP
	@p_id varchar(50) ,
	@p_dsd_id bigint ,
	@p_pk bigint OUT 
AS
BEGIN
	SET NOCOUNT ON;
	insert into DSD_GROUP (ID, DSD_ID) 
		values (@p_id, @p_dsd_id);
	set @p_pk = SCOPE_IDENTITY();
END
;


CREATE PROCEDURE INSERT_DSD_CODE
	@p_id varchar(50), 
	@p_cl_id bigint,
	@p_parent_code_id bigint = null,
	@p_pk bigint OUT 
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

		insert into ITEM (ID) VALUES (@p_id);
		set @p_pk = SCOPE_IDENTITY();
		insert into DSD_CODE (LCD_ID, CL_ID, PARENT_CODE_ID) VALUES (@p_pk, @p_cl_id, @p_parent_code_id);
	
		IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;

CREATE PROCEDURE INSERT_CATEGORY
	@p_id varchar(50), 
	@p_cat_sch_id bigint,
	@p_parent_cat_id bigint = null,
	@p_corder bigint = 0,
	@p_pk bigint OUT 
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

		insert into ITEM (ID) VALUES (@p_id);
		set @p_pk = SCOPE_IDENTITY();
		insert into CATEGORY (CAT_ID, CAT_SCH_ID, PARENT_CAT_ID, CORDER) VALUES (@p_pk, @p_cat_sch_id, @p_parent_cat_id, @p_corder);
	
		IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
CREATE PROCEDURE INSERT_CONCEPT
	@p_id varchar(50), 
	@p_con_sch_id bigint,
	@p_pk bigint OUT 
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

		insert into ITEM (ID) VALUES (@p_id);
		set @p_pk = SCOPE_IDENTITY();
		insert into CONCEPT (CON_ID, CON_SCH_ID) VALUES (@p_pk, @p_con_sch_id);
	
		IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;


CREATE PROCEDURE INSERT_HCL_CODE
	@p_id varchar(50),
	@p_version varchar(50),
	@p_agency varchar(50),
	@p_valid_from varchar(50) = NULL,
	@p_valid_to varchar(50)= NULL,
	@p_is_final int = NULL,
	@p_parent_hcode_id bigint = null,
	@p_lcd_id bigint,
	@p_h_id bigint,
	@p_level_id bigint = null,
	@p_pk bigint out
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

	insert into ARTEFACT (ID,VERSION, AGENCY, VALID_FROM, VALID_TO, IS_FINAL) values (@p_id,@p_version,@p_agency,@p_valid_from,@p_valid_to,@p_is_final);
	set @p_pk=SCOPE_IDENTITY();
	
	INSERT INTO HCL_CODE
           (HCODE_ID
           ,PARENT_HCODE_ID
           ,LCD_ID
           ,H_ID
           ,LEVEL_ID)
	VALUES
		(@p_pk
        ,@p_parent_hcode_id
        ,@p_lcd_id
        ,@p_h_id
         ,@p_level_id);

	IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;


CREATE PROCEDURE INSERT_HCL
	@p_id varchar(50),
	@p_version varchar(50),
	@p_agency varchar(50),
	@p_valid_from varchar(50) = NULL,
	@p_valid_to varchar(50)= NULL,
	@p_is_final int = NULL,
	@p_pk bigint out
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

	insert into ARTEFACT (ID,VERSION, AGENCY, VALID_FROM, VALID_TO, IS_FINAL) values (@p_id,@p_version,@p_agency,@p_valid_from,@p_valid_to,@p_is_final);
	set @p_pk=SCOPE_IDENTITY();
	insert into HCL (HCL_ID) values(@p_pk);

	IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;


CREATE PROCEDURE INSERT_HIERACHY
	@p_id varchar(50),
	@p_version varchar(50),
	@p_agency varchar(50),
	@p_valid_from varchar(50) = NULL,
	@p_valid_to varchar(50)= NULL,
	@p_is_final int = NULL,
	@p_hcl_id bigint,
	@p_pk bigint out
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

	insert into ARTEFACT (ID,VERSION, AGENCY, VALID_FROM, VALID_TO, IS_FINAL) values (@p_id,@p_version,@p_agency,@p_valid_from,@p_valid_to,@p_is_final);
	set @p_pk=SCOPE_IDENTITY();
	
	INSERT INTO HIERARCHY
           (H_ID
           ,HCL_ID)
     VALUES
           (@p_pk
           ,@p_hcl_id);

	IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;


CREATE PROCEDURE INSERT_HLEVEL
	@p_id varchar(50),
	@p_version varchar(50),
	@p_agency varchar(50),
	@p_valid_from varchar(50) = NULL,
	@p_valid_to varchar(50)= NULL,
	@p_is_final int = NULL,
	@p_h_id bigint,
	@p_parent_level_id bigint = null,
	@p_pk bigint out
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

	insert into ARTEFACT (ID,VERSION, AGENCY, VALID_FROM, VALID_TO, IS_FINAL) values (@p_id,@p_version,@p_agency,@p_valid_from,@p_valid_to,@p_is_final);
	set @p_pk=SCOPE_IDENTITY();
	
	INSERT INTO HLEVEL
           (LEVEL_ID
           ,H_ID
           ,PARENT_LEVEL_ID)
     VALUES
           (@p_pk
           ,@p_h_id
           ,@p_parent_level_id);

	IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;


CREATE PROCEDURE INSERT_DSD
	@p_id varchar(50),
	@p_version varchar(50),
	@p_agency varchar(50),
	@p_valid_from varchar(50) = NULL,
	@p_valid_to varchar(50)= NULL,
	@p_is_final int = NULL,
	@p_pk bigint out
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

	insert into ARTEFACT (ID,VERSION, AGENCY, VALID_FROM, VALID_TO, IS_FINAL) values (@p_id,@p_version,@p_agency,@p_valid_from,@p_valid_to,@p_is_final);
	set @p_pk=SCOPE_IDENTITY();
	
	INSERT INTO DSD
           (DSD_ID)
     VALUES
           (@p_pk);


	IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;


CREATE PROCEDURE INSERT_DATAFLOW
	@p_id varchar(50),
	@p_version varchar(50),
	@p_agency varchar(50),
	@p_valid_from varchar(50) = NULL,
	@p_valid_to varchar(50)= NULL,
	@p_is_final int = NULL,
	@p_dsd_id bigint,
    @p_map_set_id bigint = null,
	@p_pk bigint out
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

	insert into ARTEFACT (ID,VERSION, AGENCY, VALID_FROM, VALID_TO, IS_FINAL) values (@p_id,@p_version,@p_agency,@p_valid_from,@p_valid_to,@p_is_final);
	set @p_pk=SCOPE_IDENTITY();
	
INSERT INTO DATAFLOW
           (DF_ID
           ,DSD_ID
           ,MAP_SET_ID)
     VALUES
           (@p_pk
           ,@p_dsd_id
           ,@p_map_set_id);

	IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;


CREATE PROCEDURE INSERT_CATEGORY_SCHEME
	@p_id varchar(50),
	@p_version varchar(50),
	@p_agency varchar(50),
	@p_valid_from varchar(50) = NULL,
	@p_valid_to varchar(50)= NULL,
	@p_is_final int = NULL,
	@p_cs_order bigint = 0,
	@p_pk bigint out
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

	insert into ARTEFACT (ID,VERSION, AGENCY, VALID_FROM, VALID_TO, IS_FINAL) values (@p_id,@p_version,@p_agency,@p_valid_from,@p_valid_to,@p_is_final);
	set @p_pk=SCOPE_IDENTITY();
	
	INSERT INTO CATEGORY_SCHEME
           (CAT_SCH_ID, CS_ORDER)
     VALUES
           (@p_pk, @p_cs_order);

	IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;


CREATE PROCEDURE INSERT_CODELIST
	@p_id varchar(50),
	@p_version varchar(50),
	@p_agency varchar(50),
	@p_valid_from varchar(50) = NULL,
	@p_valid_to varchar(50)= NULL,
	@p_is_final int = NULL,
	@p_pk bigint out
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

	insert into ARTEFACT (ID,VERSION, AGENCY, VALID_FROM, VALID_TO, IS_FINAL) values (@p_id,@p_version,@p_agency,@p_valid_from,@p_valid_to,@p_is_final);
	set @p_pk=SCOPE_IDENTITY();
	
	INSERT INTO CODELIST
           (CL_ID)
     VALUES
           (@p_pk);

	IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;


CREATE PROCEDURE INSERT_CONCEPT_SCHEME
	@p_id varchar(50),
	@p_version varchar(50),
	@p_agency varchar(50),
	@p_valid_from varchar(50) = NULL,
	@p_valid_to varchar(50)= NULL,
	@p_is_final int = NULL,
	@p_pk bigint out
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

	insert into ARTEFACT (ID,VERSION, AGENCY, VALID_FROM, VALID_TO, IS_FINAL) values (@p_id,@p_version,@p_agency,@p_valid_from,@p_valid_to,@p_is_final);
	set @p_pk=SCOPE_IDENTITY();
	
	INSERT INTO CONCEPT_SCHEME
           (CON_SCH_ID)
     VALUES
           (@p_pk);

	IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;

-- ENUMERATIONS
CREATE TABLE ENUMERATIONS
	(
	ENUM_ID bigint NOT NULL,
	ENUM_NAME VARCHAR(30) NULL,
	ENUM_VALUE VARCHAR(50) NULL,
	ENUM_VALUE_DATATYPE VARCHAR(20) NULL
	)  
;
ALTER TABLE ENUMERATIONS ADD CONSTRAINT
	PK_ENUMERATIONS PRIMARY KEY (ENUM_ID) 
;


-- TEXT_FORMAT
CREATE TABLE TEXT_FORMAT
	(
	FAC_ID bigint NOT NULL IDENTITY (1, 1),
	COMP_ID bigint NOT NULL,
	FACET_TYPE_ENUM bigint NOT NULL,
	FACET_VALUE NVARCHAR(51) NULL
	)
;

ALTER TABLE TEXT_FORMAT ADD CONSTRAINT
	PK_TEXT_FORMAT PRIMARY KEY (FAC_ID)
;

ALTER TABLE TEXT_FORMAT ADD CONSTRAINT
	FK_TEXT_FORMAT_COMPONENT FOREIGN KEY
	(COMP_ID) REFERENCES COMPONENT (COMP_ID) ON DELETE CASCADE
;

ALTER TABLE TEXT_FORMAT ADD CONSTRAINT
	FK_TEXT_FORMAT_ENUMERATIONS FOREIGN KEY 
	(FACET_TYPE_ENUM) REFERENCES ENUMERATIONS (ENUM_ID) ON DELETE CASCADE 
;

CREATE PROCEDURE INSERT_TEXT_FORMAT
	@p_compid bigint, 
	@p_facet_type_enum bigint, 
	@p_facet_value varchar(51) = null,
	@p_pk bigint out
AS
BEGIN
	SET NOCOUNT ON;
	insert into TEXT_FORMAT (COMP_ID, FACET_TYPE_ENUM, FACET_VALUE) values (@p_compid, @p_facet_type_enum, @p_facet_value);
	set @p_pk = SCOPE_IDENTITY();
END
;

-- populate ENUMERATIONS
insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (1,'DataType','String','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (2,'DataType','BigInteger','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (3,'DataType','Integer','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (4,'DataType','Long','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (5,'DataType','Short','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (6,'DataType','Decimal','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (7,'DataType','Float','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (8,'DataType','Double','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (9,'DataType','Boolean','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (10,'DataType','DateTime','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (11,'DataType','Time','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (12,'DataType','Date','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (13,'DataType','Year','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (14,'DataType','Month','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (15,'DataType','Day','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (16,'DataType','MonthDay','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (17,'DataType','YearMonth','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (18,'DataType','Duration','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (19,'DataType','Timespan','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (20,'DataType','URI','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (21,'DataType','Count','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (22,'DataType','InclusiveValueRange','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (23,'DataType','ExclusiveValueRange','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (24,'DataType','Incremental','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (25,'DataType','ObservationalTimePeriod','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (26,'DataType','Base64Binary','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (27,'DataType','Alpha','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (28,'DataType','AlphaNumeric','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (29,'DataType','Numeric','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (30,'DataType','StandardTimePeriod','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (31,'DataType','BasicTimePeriod','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (32,'DataType','GregorianTimePeriod','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (33,'DataType','GregorianYear','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (34,'DataType','GregorianYearMonth','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (35,'DataType','GregorianDay','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (36,'DataType','ReportingTimePeriod','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (37,'DataType','ReportingYear','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (38,'DataType','ReportingSemester','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (39,'DataType','ReportingTrimester','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (40,'DataType','ReportingQuarter','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (41,'DataType','ReportingMonth','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (42,'DataType','ReportingWeek','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (43,'DataType','ReportingDay','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (44,'DataType','TimeRange','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (45,'DataType','XHTML','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (46,'DataType','KeyValues','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (47,'DataType','IdentifiableReference','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (48,'DataType','DataSetReference','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (49,'DataType','AttachmentConstraintReference','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (50,'FacetType','isSequence','Boolean')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (51,'FacetType','isInclusive','Boolean')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (52,'FacetType','minLength','Integer')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (53,'FacetType','maxLength','Integer')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (54,'FacetType','minValue','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (55,'FacetType','maxValue','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (56,'FacetType','startValue','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (57,'FacetType','endValue','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (58,'FacetType','decimals','Integer')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (59,'FacetType','interval','Double')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (60,'FacetType','timeInterval','Duration')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (61,'FacetType','pattern','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (62,'FacetType','enumeration','ItemScheme')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (63,'FacetType','startTime','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (64,'FacetType','endTime','String')
;

insert into enumerations (enum_id, enum_name, enum_value, enum_value_datatype) values (65,'FacetType','isMultiLingual','Boolean')
;


-- pump version
update DB_VERSION SET VERSION = '2.7'
;

