IF EXISTS (SELECT * FROM dbo.SYSOBJECTS WHERE id = object_id('PROCESS_INCLUSIVE_KEY_VALUES') AND  OBJECTPROPERTY(id, 'IsProcedure') = 1)
DROP PROC PROCESS_INCLUSIVE_KEY_VALUES
;
GO
;
CREATE PROCEDURE PROCESS_INCLUSIVE_KEY_VALUES(
	@p_art_sys_id BIGINT,
	@p_comp_id VARCHAR(255))
AS
BEGIN
	DECLARE @member_value VARCHAR(255), @cascade_values BIT;
	DECLARE @counter INT = 0;
	
	DECLARE @key_values_cursor AS CURSOR;
	
	SET @key_values_cursor = CURSOR LOCAL FORWARD_ONLY READ_ONLY FOR
	SELECT cc.MEMBER_VALUE, cc.CASCADE_VALUES
	FROM CONTENT_CONSTRAINT_V cc
	WHERE cc.ART_ID = @p_art_sys_id AND cc.CUBE_INCLUDE = 1 AND cc.MEMBER_ID = @p_comp_id AND cc.ACTUAL_DATA = 0
	
	OPEN @key_values_cursor
	FETCH NEXT FROM @key_values_cursor INTO @member_value, @cascade_values
	
	WHILE @@FETCH_STATUS = 0
	BEGIN
		IF(@counter = 0) -- reset included flag
			UPDATE #TEMP_PROCESS_CONSTRAINTS_TABLE SET INCLUDED = 0
			
		PRINT  '------ Code=' + @member_value + ' (cascadeValues=' + CAST (@cascade_values AS VARCHAR(10)) + ')';
		IF (@cascade_values = 1)
			BEGIN
			WITH CHILD_CODES(CODE_SYS_ID, CODE_ID, PARENT_CODE_SYS_ID) AS (
				SELECT cd.CODE_SYS_ID, cd.CODE_ID, cd.PARENT_CODE_SYS_ID
				FROM #TEMP_PROCESS_CONSTRAINTS_TABLE cd
				WHERE cd.CODE_ID = @member_value AND cd.COMP_ID = @p_comp_id
				UNION ALL
				SELECT c.CODE_SYS_ID, c.CODE_ID, c.PARENT_CODE_SYS_ID
				FROM #TEMP_PROCESS_CONSTRAINTS_TABLE c
				INNER JOIN CHILD_CODES p ON c.PARENT_CODE_SYS_ID = p.CODE_SYS_ID AND c.COMP_ID = @p_comp_id
			)
			UPDATE #TEMP_PROCESS_CONSTRAINTS_TABLE SET INCLUDED = 1 WHERE CODE_SYS_ID IN
			(SELECT CODE_SYS_ID FROM CHILD_CODES) AND COMP_ID = @p_comp_id
			END
		ELSE
			UPDATE #TEMP_PROCESS_CONSTRAINTS_TABLE SET INCLUDED = 1 WHERE CODE_ID = @member_value AND COMP_ID = @p_comp_id
		
		SET @counter = @counter + 1;
		FETCH NEXT FROM @key_values_cursor INTO @member_value, @cascade_values
	END
	
	CLOSE @key_values_cursor;
	DEALLOCATE @key_values_cursor;
	
	DELETE FROM #TEMP_PROCESS_CONSTRAINTS_TABLE WHERE INCLUDED = 0 AND COMP_ID = @p_comp_id
END
;
GO
;
