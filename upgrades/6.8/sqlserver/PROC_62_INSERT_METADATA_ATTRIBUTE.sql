IF EXISTS (SELECT * FROM dbo.SYSOBJECTS WHERE id = object_id('INSERT_METADATA_ATTRIBUTE') AND  OBJECTPROPERTY(id, 'IsProcedure') = 1)
   DROP PROC INSERT_METADATA_ATTRIBUTE
;
CREATE PROCEDURE INSERT_METADATA_ATTRIBUTE(
 @p_id varchar(255),
 @p_MIN_OCCURS  int,
 @p_MAX_OCCURS  int,
 @p_IS_PRESENTATIONAL  Smallint,
 @p_PARENT  bigint,
 @p_CON_ID  bigint,
 @p_CL_ID  bigint,
 @p_RS_ID  bigint,
 @p_pk  bigint OUT)
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

	EXEC INSERT_COMPONENT_COMMON @p_pk = @p_pk OUTPUT;
	INSERT INTO METADATA_ATTRIBUTE
           (MTD_ATTR_ID,ID,MIN_OCCURS,MAX_OCCURS,IS_PRESENTATIONAL,PARENT,CON_ID,CL_ID,RS_ID)
     VALUES
           (@p_pk,@p_id,@p_MIN_OCCURS,@p_MAX_OCCURS,@p_IS_PRESENTATIONAL,@p_PARENT,@p_CON_ID,@p_CL_ID,@p_RS_ID);

	 IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
	
END
;
