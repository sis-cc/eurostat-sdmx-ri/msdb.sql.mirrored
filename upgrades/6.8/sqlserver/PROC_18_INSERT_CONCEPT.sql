IF EXISTS (SELECT * FROM dbo.SYSOBJECTS WHERE id = object_id('INSERT_CONCEPT') AND  OBJECTPROPERTY(id, 'IsProcedure') = 1)
   DROP PROC INSERT_CONCEPT
;
CREATE PROCEDURE INSERT_CONCEPT
	@p_id varchar(255), 
	@p_con_sch_id bigint,
	@p_parent_con_id bigint,
	@p_cl_id bigint,
	@p_pk bigint OUT 
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

		insert into ITEM (ID) VALUES (@p_id);
		set @p_pk = SCOPE_IDENTITY();
		insert into CONCEPT (CON_ID, CON_SCH_ID,PARENT_CONCEPT_ID,CL_ID) VALUES (@p_pk, @p_con_sch_id,@p_parent_con_id,@p_cl_id);
	
		IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
