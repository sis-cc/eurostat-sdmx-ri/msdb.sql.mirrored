IF EXISTS (SELECT * FROM dbo.SYSOBJECTS WHERE id = object_id('INSERT_ITEM') AND  OBJECTPROPERTY(id, 'IsProcedure') = 1)
   DROP PROC INSERT_ITEM
;
CREATE PROCEDURE INSERT_ITEM
	@p_id varchar(255),
	@p_pk bigint out
AS
BEGIN
	SET NOCOUNT ON;
	insert into ITEM (ID) values (@p_id);
	set @p_pk=SCOPE_IDENTITY();
END
;
