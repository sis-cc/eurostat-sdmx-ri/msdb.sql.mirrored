CREATE OR REPLACE PROCEDURE PROCESS_EXCLUSIVE_KEY_VALUES(
	p_art_sys_id IN NUMBER,
	p_comp_id IN VARCHAR2)
AS
    member_value VARCHAR2(255);
    cascade_values NUMBER(1);
    
    CURSOR key_values_cursor IS
	SELECT cc.MEMBER_VALUE, cc.CASCADE_VALUES
	FROM CONTENT_CONSTRAINT_V cc
	WHERE cc.ART_ID = p_art_sys_id AND cc.MEMBER_ID = p_comp_id AND cc.CUBE_INCLUDE = 0 AND cc.ACTUAL_DATA = 0;
BEGIN
	OPEN key_values_cursor;
    LOOP
        FETCH key_values_cursor INTO member_value, cascade_values;
        EXIT WHEN key_values_cursor%NOTFOUND;
        IF (cascade_values = 1) THEN
			DELETE FROM TEMP_PROCESS_CONSTRAINTS_TABLE WHERE CODE_SYS_ID IN (
                -- hierarchical query to get the children of the code.
				WITH CHILD_CODES(CODE_SYS_ID, CODE_ID, PARENT_CODE_SYS_ID) AS (
					SELECT cd.CODE_SYS_ID, cd.CODE_ID, cd.PARENT_CODE_SYS_ID
					FROM TEMP_PROCESS_CONSTRAINTS_TABLE cd
					WHERE cd.CODE_ID = member_value AND cd.COMP_ID = p_comp_id
					UNION ALL
					SELECT c.CODE_SYS_ID, c.CODE_ID, c.PARENT_CODE_SYS_ID
					FROM TEMP_PROCESS_CONSTRAINTS_TABLE c
					INNER JOIN CHILD_CODES p ON c.PARENT_CODE_SYS_ID = p.CODE_SYS_ID AND c.COMP_ID = p_comp_id
				)
                SELECT cc.CODE_SYS_ID FROM CHILD_CODES cc
            ) AND COMP_ID = p_comp_id;
		ELSE
			DELETE FROM TEMP_PROCESS_CONSTRAINTS_TABLE WHERE CODE_ID = member_value AND COMP_ID = p_comp_id;
        END IF;
    
    END LOOP;
    CLOSE key_values_cursor;
END;
/
;
