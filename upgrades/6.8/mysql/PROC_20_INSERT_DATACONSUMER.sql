DROP PROCEDURE IF EXISTS INSERT_DATACONSUMER
;
CREATE PROCEDURE INSERT_DATACONSUMER(
	IN p_id varchar(255),
	IN p_datacons_sch_id bigint,
	OUT p_pk bigint) 
BEGIN
		insert into ITEM (ID) VALUES (p_id);
		set p_pk = LAST_INSERT_ID();
		insert into DATACONSUMER (DC_ID, DC_SCH_ID) VALUES (p_pk, p_datacons_sch_id);
	
END
;
