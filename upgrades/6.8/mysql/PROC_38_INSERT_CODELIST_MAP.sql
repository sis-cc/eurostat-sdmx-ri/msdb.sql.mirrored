DROP PROCEDURE IF EXISTS INSERT_CODELIST_MAP
;
CREATE PROCEDURE INSERT_CODELIST_MAP(
	IN p_id varchar(255),
	IN p_ss_id bigint,
	IN p_source_cl_id bigint,
	IN p_target_cl_id bigint,
	OUT p_pk bigint) 
BEGIN
		insert into ITEM (ID) VALUES (p_id);
		set p_pk = LAST_INSERT_ID();
		insert into CODELIST_MAP (CLM_ID, SS_ID, SOURCE_CL_ID, TARGET_CL_ID) VALUES (p_pk, p_ss_id ,p_source_cl_id, p_target_cl_id);
END
;
