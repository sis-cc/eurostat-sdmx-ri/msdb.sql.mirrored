DROP PROCEDURE IF EXISTS INSERT_COMPONENT
;
CREATE PROCEDURE INSERT_COMPONENT(
	IN p_type varchar(50) ,
	IN p_id varchar(255) ,
	IN p_dsd_id bigint ,
	IN p_con_id bigint ,
	IN p_cl_id bigint,
	IN p_con_sch_id bigint,
	IN p_is_freq_dim int,
	IN p_is_measure_dim int,
	IN p_att_ass_level varchar(11),
	IN p_att_status varchar(11),
	IN p_att_is_time_format int,
	IN p_xs_attlevel_ds int,
	IN p_xs_attlevel_group int,
	IN p_xs_attlevel_section int,
	IN p_xs_attlevel_obs int,
	IN p_xs_measure_code varchar(50),
	OUT p_pk bigint) 
BEGIN
	call INSERT_COMPONENT_COMMON(p_pk);
	insert into COMPONENT (COMP_ID,ID, TYPE, DSD_ID, CON_ID, CL_ID, IS_FREQ_DIM, IS_MEASURE_DIM, ATT_ASS_LEVEL, ATT_STATUS, ATT_IS_TIME_FORMAT, XS_ATTLEVEL_DS, XS_ATTLEVEL_GROUP, XS_ATTLEVEL_SECTION, XS_ATTLEVEL_OBS, XS_MEASURE_CODE, CON_SCH_ID) 
		values (p_pk,p_id, p_type, p_dsd_id, p_con_id, p_cl_id, p_is_freq_dim, p_is_measure_dim, p_att_ass_level, p_att_status, p_att_is_time_format, p_xs_attlevel_ds, p_xs_attlevel_group, p_xs_attlevel_section, p_xs_attlevel_obs, p_xs_measure_code, p_con_sch_id);
END
;
