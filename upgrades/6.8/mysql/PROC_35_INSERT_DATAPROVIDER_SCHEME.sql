DROP PROCEDURE IF EXISTS INSERT_DATAPROVIDER_SCHEME
;
CREATE PROCEDURE INSERT_DATAPROVIDER_SCHEME(
	IN p_id varchar(255),
	IN p_version varchar(50),
	IN p_agency varchar(50),
	IN p_valid_from datetime,
	IN p_valid_to datetime,
	IN p_is_final int,
	IN p_uri nvarchar(255),
	IN p_last_modified datetime,
    IN p_is_partial int,
	OUT p_pk bigint)
BEGIN
	call INSERT_ITEM_SCHEME(p_id, p_version, p_agency, p_valid_from, p_valid_to, p_is_final, p_uri, p_last_modified, 0, null, null, 'DataProviderScheme', p_pk);

	INSERT INTO DATAPROVIDER_SCHEME
           (DP_SCH_ID, IS_PARTIAL)
     VALUES
           (p_pk, p_is_partial);
END
;
