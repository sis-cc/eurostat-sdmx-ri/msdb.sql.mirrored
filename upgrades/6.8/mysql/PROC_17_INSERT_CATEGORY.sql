DROP PROCEDURE IF EXISTS INSERT_CATEGORY
;
CREATE PROCEDURE INSERT_CATEGORY(
	IN p_id varchar(255),
	IN p_cat_sch_id bigint,
	IN p_parent_cat_id bigint,
	OUT p_pk bigint) 
BEGIN
		insert into ITEM (ID) VALUES (p_id);
		set p_pk = LAST_INSERT_ID();
		insert into CATEGORY (CAT_ID, CAT_SCH_ID, PARENT_CAT_ID) VALUES (p_pk, p_cat_sch_id, p_parent_cat_id);
	
END
;
