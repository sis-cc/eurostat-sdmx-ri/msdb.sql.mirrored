DROP PROCEDURE IF EXISTS INSERT_HCL_CODE
;
CREATE PROCEDURE INSERT_HCL_CODE(
	IN p_id varchar(255),
	IN p_version varchar(50),
	IN p_agency varchar(50),
	IN p_valid_from datetime,
	IN p_valid_to datetime,
	IN p_is_final int,
	IN p_uri nvarchar(255),
	IN p_last_modified datetime,
	IN p_parent_hcode_id bigint,
	IN p_lcd_id bigint,
	IN p_h_id bigint,
	IN p_level_id bigint,
	OUT p_pk bigint)
BEGIN
	call INSERT_ARTEFACT(p_id, p_version, p_agency, p_valid_from, p_valid_to, p_is_final, p_uri, p_last_modified, 0, null, null, null, p_pk);

	INSERT INTO HCL_CODE
           (HCODE_ID
           ,PARENT_HCODE_ID
           ,LCD_ID
           ,H_ID
           ,LEVEL_ID)
	VALUES
		(p_pk
        ,p_parent_hcode_id
        ,p_lcd_id
        ,p_h_id
         ,p_level_id);
END
;
