DROP PROCEDURE IF EXISTS INSERT_ITEM_SCHEME
;
CREATE PROCEDURE INSERT_ITEM_SCHEME(
	IN p_id varchar(255),
	IN p_version varchar(50),
	IN p_agency varchar(50),
	IN p_valid_from datetime,
	IN p_valid_to datetime,
	IN p_is_final int,
	IN p_uri nvarchar(255),
	IN p_last_modified datetime,
	IN p_is_stub int,
	IN p_service_url nvarchar(255),
	IN p_structure_url nvarchar(255),
	IN p_artefact_type varchar(50),
	OUT p_pk bigint)
BEGIN
	call INSERT_ARTEFACT(p_id, p_version, p_agency, p_valid_from, p_valid_to, p_is_final, p_uri, p_last_modified, p_is_stub, p_service_url, p_structure_url, p_artefact_type, p_pk);
	insert into ITEM_SCHEME (ITEM_SCHEME_ID) VALUES (p_pk);
END
;
