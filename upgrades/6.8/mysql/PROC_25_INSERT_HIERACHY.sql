DROP PROCEDURE IF EXISTS INSERT_HIERACHY
;
CREATE PROCEDURE INSERT_HIERACHY(
	IN p_id varchar(255),
	IN p_version varchar(50),
	IN p_agency varchar(50),
	IN p_valid_from datetime,
	IN p_valid_to datetime,
	IN p_is_final int,
	IN p_uri nvarchar(255),
	IN p_last_modified datetime,
	IN p_hcl_id bigint,
	OUT p_pk bigint)
BEGIN
	call INSERT_ARTEFACT(p_id, p_version, p_agency, p_valid_from, p_valid_to, p_is_final, p_uri, p_last_modified, 0, null, null, null, p_pk);

	INSERT INTO HIERARCHY
           (H_ID
           ,HCL_ID)
     VALUES
           (p_pk
           ,p_hcl_id);
END
;
