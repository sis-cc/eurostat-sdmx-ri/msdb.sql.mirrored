DROP PROCEDURE IF EXISTS INSERT_ARTEFACT
;
CREATE PROCEDURE INSERT_ARTEFACT(
	IN p_id varchar(255),
	IN p_version varchar(50),
	IN p_agency varchar(50),
	IN p_valid_from datetime,
	IN p_valid_to datetime,
	IN p_is_final int,
	IN p_uri nvarchar(255),
	IN p_last_modified datetime,
	IN p_is_stub int,
	IN p_service_url nvarchar(255),
	IN p_structure_url nvarchar(255),
	IN p_artefact_type varchar(50),
	OUT p_pk bigint)
BEGIN
    declare ver1 bigint;
    declare ver2 bigint;
    declare ver3 bigint;
    CALL SPLIT_VERSION(p_version, ver1, ver2, ver3);
	INSERT INTO ARTEFACT (ID,VERSION1, VERSION2, VERSION3, AGENCY, VALID_FROM, VALID_TO, IS_FINAL, URI, LAST_MODIFIED, IS_STUB, SERVICE_URL, STRUCTURE_URL, ARTEFACT_TYPE) VALUES (p_id, ver1, ver2, ver3, p_agency,  p_valid_from,  p_valid_to,  p_is_final,  p_uri,  p_last_modified,  p_is_stub,  p_service_url,  p_structure_url,  p_artefact_type);
	set p_pk = LAST_INSERT_ID();
END
;
