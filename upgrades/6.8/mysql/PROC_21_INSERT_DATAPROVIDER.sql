DROP PROCEDURE IF EXISTS INSERT_DATAPROVIDER
;
CREATE PROCEDURE INSERT_DATAPROVIDER(
	IN p_id varchar(255),
	IN p_dataprov_sch_id bigint,
	OUT p_pk bigint) 
BEGIN
		insert into ITEM (ID) VALUES (p_id);
		set p_pk = LAST_INSERT_ID();
		insert into DATAPROVIDER (DP_ID, DP_SCH_ID) VALUES (p_pk, p_dataprov_sch_id);
	
END
;
