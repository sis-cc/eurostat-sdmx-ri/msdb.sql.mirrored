DROP PROCEDURE IF EXISTS INSERT_STRUCTURE_MAP
;
CREATE PROCEDURE INSERT_STRUCTURE_MAP(
	IN p_id varchar(255),
	IN p_ss_id bigint,
	IN p_source_str_id bigint,
	IN p_target_str_id bigint,
	OUT p_pk bigint) 
BEGIN
		insert into ITEM (ID) VALUES (p_id);
		set p_pk = LAST_INSERT_ID();
		insert into STRUCTURE_MAP (SM_ID, SS_ID, SOURCE_STR_ID, TARGET_STR_ID) VALUES (p_pk, p_ss_id ,p_source_str_id, p_target_str_id);
END
;
