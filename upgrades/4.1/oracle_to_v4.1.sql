﻿-- Upgrade script to 4.1 from 4.0


ALTER TABLE DATAFLOW ADD ( EXTERNAL_USAGE NUMBER(18) DEFAULT 0 NOT NULL)
;

-- always last
update DB_VERSION SET VERSION = '4.1'
;

