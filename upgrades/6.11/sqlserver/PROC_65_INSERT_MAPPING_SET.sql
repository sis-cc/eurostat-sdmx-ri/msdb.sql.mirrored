IF EXISTS (SELECT * FROM dbo.SYSOBJECTS WHERE id = object_id('INSERT_MAPPING_SET') AND  OBJECTPROPERTY(id, 'IsProcedure') = 1)
DROP PROC INSERT_MAPPING_SET
;

CREATE PROCEDURE INSERT_MAPPING_SET
@p_id varchar(50) ,
@p_description varchar(1024) ,
@p_ds_id bigint ,
@p_df_id bigint ,
@p_user_id bigint ,
@p_valid_from datetime ,
@p_valid_to datetime ,
@p_pk bigint OUT
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

		INSERT INTO MAPPING_SET (ID,DESCRIPTION,DS_ID,DF_ID,USER_ID,VALID_FROM,VALID_TO) VALUES (@p_id, @p_description, @p_ds_id, @p_df_id, @p_user_id, @p_valid_from, @p_valid_to);
		set @p_pk = SCOPE_IDENTITY();
	IF @starttrancount = 0
	COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	RAISERROR (@ErrorMessage, -- Message text.
				@ErrorSeverity, -- Severity.
				@ErrorState -- State.
				);
	END CATCH
END
;
