
DROP PROCEDURE IF EXISTS INSERT_MAPPING_SET
;
CREATE PROCEDURE INSERT_MAPPING_SET(
IN p_id varchar(50),
IN p_description varchar(1024),
IN p_ds_id bigint,
IN p_df_id bigint,
IN p_user_id bigint,
IN p_valid_from datetime,
IN p_valid_to datetime,
OUT p_pk bigint)
BEGIN
		INSERT INTO MAPPING_SET (ID,DESCRIPTION,DS_ID,DF_ID,USER_ID,VALID_FROM,VALID_TO) VALUES (p_id, p_description, p_ds_id, p_df_id, p_user_id, p_valid_from, p_valid_to);
		set p_pk = LAST_INSERT_ID();
END;

;
