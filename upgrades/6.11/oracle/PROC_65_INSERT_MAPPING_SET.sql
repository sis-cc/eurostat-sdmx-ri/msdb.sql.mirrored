

CREATE OR REPLACE PROCEDURE INSERT_MAPPING_SET(
p_id IN varchar2,
p_description IN varchar2,
p_ds_id IN number,
p_df_id IN number,
p_user_id IN number,
p_valid_from IN timestamp,
p_valid_to IN timestamp,
p_pk OUT number)
AS
BEGIN
		INSERT INTO MAPPING_SET (ID,DESCRIPTION,DS_ID,DF_ID,USER_ID,VALID_FROM,VALID_TO) VALUES (p_id, p_description, p_ds_id, p_df_id, p_user_id, p_valid_from, p_valid_to) RETURNING MAP_SET_ID INTO p_pk;
END;
/
;
