-- Upgrade script to 5.1 from 5.0.

DROP FUNCTION IF EXISTS `getVersion`
;
-- DELIMITER $$
create function `getVersion`(p_version varchar(50), particle int) 
RETURNS bigint DETERMINISTIC
BEGIN
	declare point int;
	declare currentParticle int;
	declare returnValue bigint;
	set point = locate('.', p_version);
	set currentParticle = 0;
	set returnValue = null;

	WHILE returnValue is null and p_version is not null and currentParticle < particle DO
		SET currentParticle = currentParticle + 1;
		if currentParticle = particle then
			if point > 0  then
				set returnValue = substring(p_version,1,point - 1);
			else
				set returnValue = substring(p_version,1);
			end if;
		elseif point > 0 then
			set p_version = substring(p_version, point + 1);
			set point = locate('.', p_version);
		else 
			set p_version = null;
		end if;
	END WHILE;
	return returnValue;
END
;
-- always last
update DB_VERSION SET VERSION = '5.1'
;

