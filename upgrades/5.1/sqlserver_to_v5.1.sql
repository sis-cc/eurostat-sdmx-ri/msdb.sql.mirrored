-- Upgrade script to 5.1 from 5.0.
drop function dbo.getVersion
;
GO
;
create function [dbo].[getVersion] (@p_version varchar(50), @particle int)
RETURNS bigint
begin
declare @point int;
declare @currentParticle int;
declare @returnValue varchar(50);
set @point = charindex('.', @p_version);
set @currentParticle = 0;
set @returnValue = null;

while @returnValue is null and @p_version is not null and @currentParticle < @particle
begin
    SET @currentParticle = @currentParticle + 1;
	if @currentParticle = @particle
		if @point > 0
			set @returnValue = substring(@p_version,1,@point-1);
		else
			set @returnValue = @p_version;
	else if @point > 0
    begin		
		set @p_version = substring(@p_version, @point + 1,100);
		set @point = charindex('.', @p_version);
	end;
	else
		set @p_version = null;
end;

return cast(@returnValue as bigint);
end
;

-- always last
update DB_VERSION SET VERSION = '5.1'
;
