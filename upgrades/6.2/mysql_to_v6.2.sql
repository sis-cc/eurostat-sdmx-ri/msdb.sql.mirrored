-- updates for 6.2

-- user action changes

CREATE TABLE USER_ACTION(
UA_ID bigint NOT NULL  AUTO_INCREMENT ,
ACTION_WHEN datetime NOT NULL ,
OPERATION_TYPE varchar(50) NOT NULL ,
USERNAME nvarchar(50) NOT NULL ,
ENTITY_TYPE varchar(50) NOT NULL ,
ENTITY_ID bigint NOT NULL ,
ENTITY_NAME nvarchar(255) NOT NULL ,
PRIMARY KEY (UA_ID))
;
DROP PROCEDURE IF EXISTS INSERT_USER_ACTION
;

CREATE PROCEDURE INSERT_USER_ACTION(
IN p_action_when datetime,
IN p_operation_type varchar(50),
IN p_username nvarchar(50),
IN p_entity_type varchar(50),
IN p_entity_id bigint,
IN p_entity_name nvarchar(255),
OUT p_pk bigint)
BEGIN
		INSERT INTO USER_ACTION (ACTION_WHEN,OPERATION_TYPE,USERNAME,ENTITY_TYPE,ENTITY_ID,ENTITY_NAME) VALUES (p_action_when, p_operation_type, p_username, p_entity_type, p_entity_id, p_entity_name);
		set p_pk = LAST_INSERT_ID();
END;
;

-- always last
update DB_VERSION SET VERSION = '6.2'
;

