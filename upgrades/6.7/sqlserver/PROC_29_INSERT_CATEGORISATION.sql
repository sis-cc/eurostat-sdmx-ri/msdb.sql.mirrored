IF EXISTS (SELECT * FROM dbo.SYSOBJECTS WHERE id = object_id('INSERT_CATEGORISATION') AND  OBJECTPROPERTY(id, 'IsProcedure') = 1)
   DROP PROC INSERT_CATEGORISATION
;
CREATE PROCEDURE INSERT_CATEGORISATION
	@p_id varchar(50),
	@p_version varchar(50),
	@p_agency varchar(50),
	@p_valid_from datetime = NULL,
	@p_valid_to datetime= NULL,
	@p_is_final int = NULL,
	@p_uri nvarchar(255),
	@p_last_modified datetime = NULL,
	@p_art_id bigint,
    @p_cat_id bigint,
	@p_pk bigint out
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

	EXEC INSERT_ARTEFACT @p_id = @p_id, @p_version = @p_version, @p_agency = @p_agency, @p_valid_from = @p_valid_from, @p_valid_to = @p_valid_to, @p_is_final = @p_is_final, @p_uri = @p_uri, @p_last_modified = @p_last_modified, @p_is_stub = 0, @p_service_url = null, @p_structure_url = null, @p_artefact_type = 'Categorisation', @p_pk = @p_pk OUTPUT;
	
INSERT INTO CATEGORISATION
           (CATN_ID
           ,ART_ID
           ,CAT_ID)
     VALUES
           (@p_pk
           ,@p_art_id
           ,@p_cat_id);

	IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
