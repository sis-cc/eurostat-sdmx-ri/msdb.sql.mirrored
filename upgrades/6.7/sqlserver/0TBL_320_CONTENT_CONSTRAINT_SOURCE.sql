-- An internal release of SDMX RI .NET used an old commit of v6.6
IF (NOT EXISTS (SELECT *
                 FROM INFORMATION_SCHEMA.TABLES
                 WHERE TABLE_NAME = 'CONTENT_CONSTRAINT_SOURCE'))
BEGIN
	CREATE TABLE CONTENT_CONSTRAINT_SOURCE(
		CONT_CONS_ID bigint NOT NULL ,
		DATA_SOURCE_ID bigint NOT NULL ,
	PRIMARY KEY (CONT_CONS_ID,DATA_SOURCE_ID));
	ALTER TABLE CONTENT_CONSTRAINT_SOURCE ADD CONSTRAINT FK_CONT_CONS_SRC_CONT_CONS FOREIGN KEY (CONT_CONS_ID) REFERENCES CONTENT_CONSTRAINT (CONT_CONS_ID)
	ON DELETE CASCADE;
	ALTER TABLE CONTENT_CONSTRAINT_SOURCE ADD CONSTRAINT FK_CONT_CONS_SRC_DS FOREIGN KEY (DATA_SOURCE_ID) REFERENCES DATA_SOURCE (DATA_SOURCE_ID)
	ON DELETE CASCADE;
END
;

