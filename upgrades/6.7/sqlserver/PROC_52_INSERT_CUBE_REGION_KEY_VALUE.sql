IF EXISTS (SELECT * FROM dbo.SYSOBJECTS WHERE id = object_id('INSERT_CUBE_REGION_KEY_VALUE') AND  OBJECTPROPERTY(id, 'IsProcedure') = 1)
   DROP PROC INSERT_CUBE_REGION_KEY_VALUE
;
CREATE PROCEDURE INSERT_CUBE_REGION_KEY_VALUE(
	@p_cube_region_id bigint,
	@p_member_id varchar(50),
	@p_component_type varchar(50),
	@p_include bigint,
	@p_start_period varchar(50),
	@p_end_period varchar(50),
	@p_start_inclusive bigint,
	@p_end_inclusive bigint,
	@p_pk bigint OUT )
AS
BEGIN
	INSERT INTO CUBE_REGION_KEY_VALUE
           (CUBE_REGION_ID, MEMBER_ID, COMPONENT_TYPE, INCLUDE, START_PERIOD, END_PERIOD, START_INCLUSIVE, END_INCLUSIVE)
     VALUES
           (@p_cube_region_id, @p_member_id, @p_component_type, @p_include, @p_start_period, @p_end_period, @p_start_inclusive, @p_end_inclusive);
	       set @p_pk = SCOPE_IDENTITY();
END
;
