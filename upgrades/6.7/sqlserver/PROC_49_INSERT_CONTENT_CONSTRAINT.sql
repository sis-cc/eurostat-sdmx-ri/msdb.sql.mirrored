IF EXISTS (SELECT * FROM dbo.SYSOBJECTS WHERE id = object_id('INSERT_CONTENT_CONSTRAINT') AND  OBJECTPROPERTY(id, 'IsProcedure') = 1)
   DROP PROC INSERT_CONTENT_CONSTRAINT
;
CREATE PROCEDURE INSERT_CONTENT_CONSTRAINT
       @p_id varchar(50),
       @p_version varchar(50),
       @p_agency varchar(50),
       @p_valid_from datetime = NULL,
       @p_valid_to datetime = NULL,
       @p_is_final bigint = NULL,
       @p_uri nvarchar(255),
       @p_last_modified  datetime = NULL,
       @p_actual_data bigint = 1,
       @p_periodicity varchar(50) = NULL,
       @p_offset varchar(50) = NULL,
       @p_tolerance varchar(50) = NULL,
       @p_attach_type VARCHAR(20) = 'Dataflow',
	   @dp_id bigint = null,
	   @set_id varchar(255) = null,
	   @simple_data_source nvarchar(1024) = null,
	   @p_start_time datetime = NULL,
	   @p_end_time datetime = NULL,	   
	   @p_pk bigint OUT
	   
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION
	
		 EXEC INSERT_ARTEFACT @p_id = @p_id, @p_version = @p_version, @p_agency = @p_agency, @p_valid_from = @p_valid_from, @p_valid_to = @p_valid_to, @p_is_final = @p_is_final, @p_uri = @p_uri, @p_last_modified = @p_last_modified, @p_is_stub = 0, @p_service_url = null, @p_structure_url = null, @p_artefact_type = 'ContentConstraint', @p_pk = @p_pk OUTPUT;

       INSERT INTO CONTENT_CONSTRAINT
           (CONT_CONS_ID, ACTUAL_DATA, PERIODICITY, OFFSET, TOLERANCE,ATTACH_TYPE,DP_ID,SET_ID,SIMPLE_DATA_SOURCE,START_TIME,END_TIME)
     VALUES
           (@p_pk, @p_actual_data, @p_periodicity, @p_offset, @p_tolerance,@p_attach_type,@dp_id,@set_id,@simple_data_source,@p_start_time,@p_end_time);
IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
