IF EXISTS (SELECT * FROM dbo.SYSOBJECTS WHERE id = object_id('INSERT_METADATAFLOW') AND  OBJECTPROPERTY(id, 'IsProcedure') = 1)
   DROP PROC INSERT_METADATAFLOW
;
CREATE PROCEDURE INSERT_METADATAFLOW(
    @p_id varchar(50),
    @p_version  varchar(50),
    @p_agency varchar(50),
    @p_valid_from datetime = NULL,
    @p_valid_to datetime = NULL,
    @p_is_final bigint = NULL,
    @p_uri  nvarchar(255),
    @p_last_modified datetime = NULL,
 @p_msd_id  bigint,
 @p_pk bigint OUT)
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

	EXEC INSERT_STRUCTURE_USAGE @p_id = @p_id , @p_version = @p_version , @p_agency = @p_agency , @p_valid_from = @p_valid_from , @p_valid_to = @p_valid_to , @p_is_final = @p_is_final , @p_uri = @p_uri , @p_last_modified = @p_last_modified , @p_is_stub = 0 , @p_service_url = null , @p_structure_url = null , @p_artefact_type = 'Metadataflow' , @p_pk = @p_pk OUTPUT;
	 INSERT INTO METADATAFLOW
           (MSD_ID,MDF_ID)
     VALUES
           (@p_msd_id,@p_pk);
	IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
