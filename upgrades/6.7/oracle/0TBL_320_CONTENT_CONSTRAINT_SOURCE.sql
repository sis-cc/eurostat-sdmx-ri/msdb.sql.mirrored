
-- An internal release of SDMX RI .NET used an old commit of v6.6
declare
nCount NUMBER;
v_sql LONG;
begin
SELECT count(*) into nCount FROM user_tables where table_name = 'CONTENT_CONSTRAINT_SOURCE';
IF(nCount <= 0)
THEN
v_sql:='
CREATE TABLE CONTENT_CONSTRAINT_SOURCE(
CONT_CONS_ID number(19) NOT NULL,
DATA_SOURCE_ID number(19) NOT NULL,
PRIMARY KEY (CONT_CONS_ID,DATA_SOURCE_ID))';
execute immediate v_sql;
v_sql:='ALTER TABLE CONTENT_CONSTRAINT_SOURCE ADD CONSTRAINT FK_CONT_CONS_SRC_CONT_CONS FOREIGN KEY (CONT_CONS_ID) REFERENCES CONTENT_CONSTRAINT (CONT_CONS_ID)
ON DELETE CASCADE';
execute immediate v_sql;
v_sql:='ALTER TABLE CONTENT_CONSTRAINT_SOURCE ADD CONSTRAINT FK_CONT_CONS_SRC_DS FOREIGN KEY (DATA_SOURCE_ID) REFERENCES DATA_SOURCE (DATA_SOURCE_ID)
ON DELETE CASCADE';
execute immediate v_sql;
END IF;
EXCEPTION
    WHEN OTHERS THEN
      IF SQLCODE = -955 THEN
        NULL; -- suppresses ORA-00955 exception
      ELSE
         RAISE;
      END IF;
end;
/

