
declare
  DoesNotExists  exception;
  pragma exception_init( DoesNotExists, -1418 );
begin
	execute immediate 'DROP INDEX INDEX_ANNOTATION_TEXT';
  dbms_output.put_line( 'INDEX_ANNOTATION_TEXT deleted' );
exception
  when DoesNotExists then
  execute immediate 'create index INDEX_ANNOTATION_TEXT on ANNOTATION_TEXT (ANN_ID, LANGUAGE,TEXT)';
  dbms_output.put_line( 'INDEX_ANNOTATION_TEXT created' );
  end;
/
;
