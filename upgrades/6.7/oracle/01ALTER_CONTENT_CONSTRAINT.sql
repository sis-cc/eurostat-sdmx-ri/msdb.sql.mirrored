-- Added columns to CONTENT_CONSTRAINT.
Alter table CONTENT_CONSTRAINT
ADD (START_TIME timestamp NULL,
	END_TIME timestamp NULL )
;
