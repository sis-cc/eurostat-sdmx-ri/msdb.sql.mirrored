
declare
  DoesNotExists  exception;
  pragma exception_init( DoesNotExists, -1418 );
begin
	execute immediate 'DROP INDEX INDEX_ITEM_ANNOTATION';
  dbms_output.put_line( 'INDEX_ITEM_ANNOTATION deleted' );
exception
  when DoesNotExists then
  execute immediate 'create index INDEX_ITEM_ANNOTATION on ITEM_ANNOTATION (ITEM_ID)';
  dbms_output.put_line( 'INDEX_ITEM_ANNOTATION created' );
  end;
/
;
