
-- An internal release of SDMX RI .NET used an old commit of v6.6
ALTER TABLE CONTENT_CONSTRAINT_SOURCE ADD FOREIGN KEY IF NOT EXISTS FK_CONT_CONS_SRC_CONT_CONS (CONT_CONS_ID) REFERENCES CONTENT_CONSTRAINT (CONT_CONS_ID)
ON DELETE CASCADE
;
ALTER TABLE CONTENT_CONSTRAINT_SOURCE ADD FOREIGN KEY IF NOT EXISTS FK_CONT_CONS_SRC_DS (DATA_SOURCE_ID) REFERENCES DATA_SOURCE (DATA_SOURCE_ID)
ON DELETE CASCADE
;
