
DROP PROCEDURE IF EXISTS INSERT_ANNOTATION
;
CREATE PROCEDURE INSERT_ANNOTATION
(
	IN p_id nvarchar(50),
	IN p_title nvarchar(4000),
	IN p_type nvarchar(50),
	IN p_url nvarchar(1000),
	OUT p_pk bigint)
BEGIN
	insert into ANNOTATION (ID, TITLE, TYPE, URL) VALUES (p_id, p_title, p_type, p_url);
	set p_pk = LAST_INSERT_ID();
END
;
