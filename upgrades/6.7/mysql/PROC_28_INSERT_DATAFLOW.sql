
DROP PROCEDURE IF EXISTS INSERT_DATAFLOW
;
CREATE PROCEDURE INSERT_DATAFLOW(
	IN p_id varchar(50),
	IN p_version varchar(50),
	IN p_agency varchar(50),
	IN p_valid_from datetime,
	IN p_valid_to datetime,
	IN p_is_final int,
	IN p_uri nvarchar(255),
	IN p_last_modified datetime,
	IN p_dsd_id bigint,
	IN p_map_set_id bigint,
	OUT p_pk bigint)
BEGIN
	call INSERT_STRUCTURE_USAGE(p_id, p_version, p_agency, p_valid_from, p_valid_to, p_is_final, p_uri, p_last_modified, 0, null, null, 'Dataflow', p_pk);

INSERT INTO DATAFLOW
           (DF_ID
           ,DSD_ID
           ,MAP_SET_ID)
     VALUES
           (p_pk
           ,p_dsd_id
           ,p_map_set_id);
END
;
