alter table ARTEFACT add column if not exists (
  IS_STUB int NOT NULL DEFAULT 0,
	SERVICE_URL nvarchar(255) NULL,
	STRUCTURE_URL nvarchar(255) NULL,
	ARTEFACT_TYPE varchar(50) NULL
)
;
