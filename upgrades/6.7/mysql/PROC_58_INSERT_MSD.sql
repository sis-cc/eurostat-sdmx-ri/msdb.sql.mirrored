

DROP PROCEDURE IF EXISTS INSERT_MSD
;
CREATE PROCEDURE INSERT_MSD(
	IN p_id varchar(50),
    IN p_version  varchar(50),
    IN p_agency varchar(50),
    IN p_valid_from   datetime,
    IN p_valid_to   datetime,
    IN p_is_final  bigint,
    IN p_uri  nvarchar(255),
    IN p_last_modified  datetime,
	OUT p_pk  bigint)
BEGIN
	call INSERT_ARTEFACT(p_id, p_version, p_agency, p_valid_from, p_valid_to, p_is_final, p_uri, p_last_modified, 0, null, null, 'MetadataStructure', p_pk);
	INSERT INTO METADATA_STRUCTURE_DEFINITION
           (MSD_ID)
     VALUES
           (p_pk);

END
;
