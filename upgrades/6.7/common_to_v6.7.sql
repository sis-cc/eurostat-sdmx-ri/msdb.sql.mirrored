update ARTEFACT SET ARTEFACT_TYPE = 'DataStructure' where exists (select 1 from DSD where DSD_ID = ART_ID)
;
update ARTEFACT SET ARTEFACT_TYPE = 'Dataflow' where exists (select 1 from DATAFLOW  where DF_ID = ART_ID)
;
update ARTEFACT SET ARTEFACT_TYPE = 'Categorisation' where exists (select 1 from CATEGORISATION  where CATN_ID = ART_ID)
;
update ARTEFACT SET ARTEFACT_TYPE = 'CategoryScheme' where exists (select 1 from CATEGORY_SCHEME  where CAT_SCH_ID = ART_ID)
;
update ARTEFACT SET ARTEFACT_TYPE = 'Codelist' where exists (select 1 from CODELIST  where CL_ID = ART_ID)
;
update ARTEFACT SET ARTEFACT_TYPE = 'ConceptScheme' where exists (select 1 from CONCEPT_SCHEME  where CON_SCH_ID = ART_ID)
;
update ARTEFACT SET ARTEFACT_TYPE = 'AgencyScheme' where exists (select 1 from AGENCY_SCHEME  where AG_SCH_ID = ART_ID)
;
update ARTEFACT SET ARTEFACT_TYPE = 'DataConsumerScheme' where exists (select 1 from DATACONSUMER_SCHEME  where DC_SCH_ID = ART_ID)
;
update ARTEFACT SET ARTEFACT_TYPE = 'DataProviderScheme' where exists (select 1 from DATAPROVIDER_SCHEME  where DP_SCH_ID = ART_ID)
;
update ARTEFACT SET ARTEFACT_TYPE = 'OrganisationUnitScheme' where exists (select 1 from ORGANISATION_UNIT_SCHEME  where ORG_UNIT_SCH_ID = ART_ID)
;
update ARTEFACT SET ARTEFACT_TYPE = 'StructureSet' where exists (select 1 from STRUCTURE_SET  where SS_ID = ART_ID)
;
update ARTEFACT SET ARTEFACT_TYPE = 'ContentConstraint' where exists (select 1 from CONTENT_CONSTRAINT  where CONT_CONS_ID = ART_ID)
;
update ARTEFACT SET ARTEFACT_TYPE = 'Metadataflow' where exists (select 1 from METADATAFLOW  where MDF_ID = ART_ID)
;
update ARTEFACT SET ARTEFACT_TYPE = 'MetadataStructure' where exists (select 1 from METADATA_STRUCTURE_DEFINITION  where MSD_ID = ART_ID)
;
update ARTEFACT SET ARTEFACT_TYPE = 'ProvisionAgreement' where exists (select 1 from PROVISION_AGREEMENT  where PA_ID = ART_ID)
;
