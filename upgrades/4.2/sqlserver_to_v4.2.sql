﻿-- Upgrade script to 4.2 from 4.1

ALTER TABLE ITEM ALTER COLUMN ID varchar(150)
;
GO
;
IF OBJECT_ID('INSERT_LOCAL_CODE') IS NOT NULL
DROP PROC INSERT_LOCAL_CODE
;
GO
;
CREATE PROCEDURE INSERT_LOCAL_CODE
	@p_id varchar(150), 
	@p_column_id bigint,
	@p_pk bigint OUT 
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

		insert into ITEM (ID) VALUES (@p_id);
		set @p_pk = SCOPE_IDENTITY();
		insert into LOCAL_CODE (LCD_ID, COLUMN_ID) VALUES (@p_pk, @p_column_id);
	
		IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
GO
;
IF OBJECT_ID('INSERT_ITEM') IS NOT NULL
DROP PROC INSERT_ITEM
;
GO
;
CREATE PROCEDURE INSERT_ITEM
	@p_id varchar(150),
	@p_pk bigint out
AS
BEGIN
	SET NOCOUNT ON;
	insert into ITEM (ID) values (@p_id);
	set @p_pk=SCOPE_IDENTITY();
END
;
GO
;
IF OBJECT_ID('INSERT_DSD_CODE') IS NOT NULL
DROP PROC INSERT_DSD_CODE
;
GO
;
CREATE PROCEDURE INSERT_DSD_CODE
	@p_id varchar(150), 
	@p_cl_id bigint,
	@p_parent_code_id bigint = null,
	@p_pk bigint OUT 
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

		insert into ITEM (ID) VALUES (@p_id);
		set @p_pk = SCOPE_IDENTITY();
		insert into DSD_CODE (LCD_ID, CL_ID, PARENT_CODE_ID) VALUES (@p_pk, @p_cl_id, @p_parent_code_id);
	
		IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
GO
;
IF OBJECT_ID('INSERT_CATEGORY') IS NOT NULL
DROP PROC INSERT_CATEGORY
;
GO
;
CREATE PROCEDURE INSERT_CATEGORY
	@p_id varchar(150), 
	@p_cat_sch_id bigint,
	@p_parent_cat_id bigint = null,
	@p_pk bigint OUT 
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

		insert into ITEM (ID) VALUES (@p_id);
		set @p_pk = SCOPE_IDENTITY();
		insert into CATEGORY (CAT_ID, CAT_SCH_ID, PARENT_CAT_ID) VALUES (@p_pk, @p_cat_sch_id, @p_parent_cat_id);
	
		IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
GO
;
IF OBJECT_ID('INSERT_CONCEPT') IS NOT NULL
DROP PROC INSERT_CONCEPT
;
GO
;
CREATE PROCEDURE INSERT_CONCEPT
	@p_id varchar(150), 
	@p_con_sch_id bigint,
	@p_parent_con_id bigint,
	@p_pk bigint OUT 
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

		insert into ITEM (ID) VALUES (@p_id);
		set @p_pk = SCOPE_IDENTITY();
		insert into CONCEPT (CON_ID, CON_SCH_ID,PARENT_CONCEPT_ID) VALUES (@p_pk, @p_con_sch_id,@p_parent_con_id);
	
		IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
GO
;
IF OBJECT_ID('INSERT_AGENCY') IS NOT NULL
DROP PROC INSERT_AGENCY
;
GO
;
CREATE PROCEDURE INSERT_AGENCY
	@p_id varchar(150), 
	@p_ag_sch_id bigint,
	@p_pk bigint OUT 
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

		insert into ITEM (ID) VALUES (@p_id);
		set @p_pk = SCOPE_IDENTITY();
		insert into AGENCY (AG_ID, AG_SCH_ID) VALUES (@p_pk, @p_ag_sch_id);
	
		IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
GO
;
IF OBJECT_ID('INSERT_DATACONSUMER') IS NOT NULL
DROP PROC INSERT_DATACONSUMER
;
GO
;
CREATE PROCEDURE INSERT_DATACONSUMER
	@p_id varchar(150), 
	@p_datacons_sch_id bigint,
	@p_pk bigint OUT 
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

		insert into ITEM (ID) VALUES (@p_id);
		set @p_pk = SCOPE_IDENTITY();
		insert into DATACONSUMER (DC_ID, DC_SCH_ID) VALUES (@p_pk, @p_datacons_sch_id);
	
		IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
GO
;
IF OBJECT_ID('INSERT_DATAPROVIDER') IS NOT NULL
DROP PROC INSERT_DATAPROVIDER
;
GO
;
CREATE PROCEDURE INSERT_DATAPROVIDER
	@p_id varchar(150), 
	@p_dataprov_sch_id bigint,
	@p_pk bigint OUT 
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

		insert into ITEM (ID) VALUES (@p_id);
		set @p_pk = SCOPE_IDENTITY();
		insert into DATAPROVIDER (DP_ID, DP_SCH_ID) VALUES (@p_pk, @p_dataprov_sch_id);
	
		IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
GO
;
IF OBJECT_ID('INSERT_ORGANISATION_UNIT') IS NOT NULL
DROP PROC INSERT_ORGANISATION_UNIT
;
GO
;
CREATE PROCEDURE INSERT_ORGANISATION_UNIT
	@p_id varchar(150), 
	@p_org_unit_sch_id bigint,
	@p_parent_org_unit_id bigint,
	@p_pk bigint OUT 
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

		insert into ITEM (ID) VALUES (@p_id);
		set @p_pk = SCOPE_IDENTITY();
		insert into ORGANISATION_UNIT (ORG_UNIT_ID, ORG_UNIT_SCH_ID, PARENT_ORG_UNIT_ID) VALUES (@p_pk,  @p_org_unit_sch_id,@p_parent_org_unit_id);
	
		IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
GO
;
IF OBJECT_ID('INSERT_CODELIST_MAP') IS NOT NULL
DROP PROC INSERT_CODELIST_MAP
;
GO
;
CREATE PROCEDURE INSERT_CODELIST_MAP
	@p_id varchar(150),
	@p_ss_id bigint,
	@p_source_cl_id bigint,
	@p_target_cl_id bigint,
	@p_pk bigint OUT 
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION
		insert into ITEM (ID) VALUES (@p_id);
		set @p_pk = SCOPE_IDENTITY();
		insert into CODELIST_MAP (CLM_ID, SS_ID, SOURCE_CL_ID, TARGET_CL_ID) VALUES (@p_pk, @p_ss_id ,@p_source_cl_id, @p_target_cl_id);
	
	IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
GO
;

IF OBJECT_ID('INSERT_STRUCTURE_MAP') IS NOT NULL
DROP PROC INSERT_STRUCTURE_MAP
;
GO
;
CREATE PROCEDURE INSERT_STRUCTURE_MAP
	@p_id varchar(150),
	@p_ss_id bigint,
	@p_source_str_id bigint,
	@p_target_str_id bigint,
	@p_pk bigint OUT 
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION
		insert into ITEM (ID) VALUES (@p_id);
		set @p_pk = SCOPE_IDENTITY();
		insert into STRUCTURE_MAP (SM_ID, SS_ID, SOURCE_STR_ID, TARGET_STR_ID) VALUES (@p_pk, @p_ss_id ,@p_source_str_id, @p_target_str_id);
	
	IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
GO
;

-- always last
update DB_VERSION SET VERSION = '4.2'
;
