﻿-- Upgrade script to 4.2 from 4.1


ALTER TABLE ITEM MODIFY (ID VARCHAR2(150))
;

-- always last
update DB_VERSION SET VERSION = '4.2'
;
