﻿-- Upgrade script to 4.2 from 4.1

ALTER TABLE ITEM MODIFY ID VARCHAR(150)
;

DROP PROCEDURE IF EXISTS INSERT_LOCAL_CODE
;
CREATE PROCEDURE INSERT_LOCAL_CODE(
	IN p_id varchar(150),
	IN p_column_id bigint,
	OUT p_pk bigint) 
BEGIN
		insert into ITEM (ID) VALUES (p_id);
		set p_pk = LAST_INSERT_ID();
		insert into LOCAL_CODE (LCD_ID, COLUMN_ID) VALUES (p_pk, p_column_id);
END
;
DROP PROCEDURE IF EXISTS INSERT_ITEM
;
CREATE PROCEDURE INSERT_ITEM(
	IN p_id varchar(150),
	OUT p_pk bigint)
BEGIN
	insert into ITEM (ID) values (p_id);
	set p_pk = LAST_INSERT_ID();
END
;
DROP PROCEDURE IF EXISTS INSERT_DSD_CODE
;
CREATE PROCEDURE INSERT_DSD_CODE(
	IN p_id varchar(150),
	IN p_cl_id bigint,
	IN p_parent_code_id bigint,
	OUT p_pk bigint) 
BEGIN
		insert into ITEM (ID) VALUES (p_id);
		set p_pk = LAST_INSERT_ID();
		insert into DSD_CODE (LCD_ID, CL_ID, PARENT_CODE_ID) VALUES (p_pk, p_cl_id, p_parent_code_id);
	
END
;

DROP PROCEDURE IF EXISTS INSERT_CATEGORY
;
CREATE PROCEDURE INSERT_CATEGORY(
	IN p_id varchar(150),
	IN p_cat_sch_id bigint,
	IN p_parent_cat_id bigint,
	OUT p_pk bigint) 
BEGIN
		insert into ITEM (ID) VALUES (p_id);
		set p_pk = LAST_INSERT_ID();
		insert into CATEGORY (CAT_ID, CAT_SCH_ID, PARENT_CAT_ID) VALUES (p_pk, p_cat_sch_id, p_parent_cat_id);
	
END
;

DROP PROCEDURE IF EXISTS INSERT_CONCEPT
;
CREATE PROCEDURE INSERT_CONCEPT(
	IN p_id varchar(150),
	IN p_con_sch_id bigint,
	IN p_parent_con_id bigint,
	OUT p_pk bigint) 
BEGIN
		insert into ITEM (ID) VALUES (p_id);
		set p_pk = LAST_INSERT_ID();
		insert into CONCEPT (CON_ID, CON_SCH_ID,PARENT_CONCEPT_ID) VALUES (p_pk, p_con_sch_id,p_parent_con_id);
	
END
;

DROP PROCEDURE IF EXISTS INSERT_AGENCY
;
CREATE PROCEDURE INSERT_AGENCY(
	IN p_id varchar(150),
	IN p_ag_sch_id bigint,
	OUT p_pk bigint) 
BEGIN
		insert into ITEM (ID) VALUES (p_id);
		set p_pk = LAST_INSERT_ID();
		insert into AGENCY (AG_ID, AG_SCH_ID) VALUES (p_pk, p_ag_sch_id);
	
END
;

DROP PROCEDURE IF EXISTS INSERT_DATACONSUMER
;
CREATE PROCEDURE INSERT_DATACONSUMER(
	IN p_id varchar(150),
	IN p_datacons_sch_id bigint,
	OUT p_pk bigint) 
BEGIN
		insert into ITEM (ID) VALUES (p_id);
		set p_pk = LAST_INSERT_ID();
		insert into DATACONSUMER (DC_ID, DC_SCH_ID) VALUES (p_pk, p_datacons_sch_id);
	
END
;

DROP PROCEDURE IF EXISTS INSERT_DATAPROVIDER
;
CREATE PROCEDURE INSERT_DATAPROVIDER(
	IN p_id varchar(150),
	IN p_dataprov_sch_id bigint,
	OUT p_pk bigint) 
BEGIN
		insert into ITEM (ID) VALUES (p_id);
		set p_pk = LAST_INSERT_ID();
		insert into DATAPROVIDER (DP_ID, DP_SCH_ID) VALUES (p_pk, p_dataprov_sch_id);
	
END
;

DROP PROCEDURE IF EXISTS INSERT_ORGANISATION_UNIT
;
CREATE PROCEDURE INSERT_ORGANISATION_UNIT(
	IN p_id varchar(150),
	IN p_org_unit_sch_id bigint,
	IN p_parent_org_unit_id bigint,
	OUT p_pk bigint) 
BEGIN
		insert into ITEM (ID) VALUES (p_id);
		set p_pk = LAST_INSERT_ID();
		insert into ORGANISATION_UNIT (ORG_UNIT_ID, ORG_UNIT_SCH_ID,PARENT_ORG_UNIT_ID) VALUES (p_pk, p_org_unit_sch_id,p_parent_org_unit_id);
	
END
;
DROP PROCEDURE IF EXISTS INSERT_CODELIST_MAP
;
CREATE PROCEDURE INSERT_CODELIST_MAP(
	IN p_id varchar(150),
	IN p_ss_id bigint,
	IN p_source_cl_id bigint,
	IN p_target_cl_id bigint,
	OUT p_pk bigint) 
BEGIN
		insert into ITEM (ID) VALUES (p_id);
		set p_pk = LAST_INSERT_ID();
		insert into CODELIST_MAP (CLM_ID, SS_ID, SOURCE_CL_ID, TARGET_CL_ID) VALUES (p_pk, p_ss_id ,p_source_cl_id, p_target_cl_id);
END
;
DROP PROCEDURE IF EXISTS INSERT_STRUCTURE_MAP
;
CREATE PROCEDURE INSERT_STRUCTURE_MAP(
	IN p_id varchar(150),
	IN p_ss_id bigint,
	IN p_source_str_id bigint,
	IN p_target_str_id bigint,
	OUT p_pk bigint) 
BEGIN
		insert into ITEM (ID) VALUES (p_id);
		set p_pk = LAST_INSERT_ID();
		insert into STRUCTURE_MAP (SM_ID, SS_ID, SOURCE_STR_ID, TARGET_STR_ID) VALUES (p_pk, p_ss_id ,p_source_str_id, p_target_str_id);
END
;

-- always last
update DB_VERSION SET VERSION = '4.2'
;