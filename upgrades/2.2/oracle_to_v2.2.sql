ALTER TABLE DATASET ADD	EDITOR_TYPE VARCHAR2(250) NULL
;

UPDATE DATASET SET EDITOR_TYPE = 'org.estat.ma.gui.userControls.dataset.QueryEditor.CustomQueryEditor, MappingAssistant' 
;

UPDATE DATASET SET EDITOR_TYPE = 'org.estat.ma.gui.userControls.dataset.QueryEditor.QueryDesignerUC, MappingAssistant' WHERE CUSTOM_QUERY=0
;

ALTER TABLE DATASET DROP COLUMN CUSTOM_QUERY
;

update DB_VERSION SET VERSION = '2.2'
;
