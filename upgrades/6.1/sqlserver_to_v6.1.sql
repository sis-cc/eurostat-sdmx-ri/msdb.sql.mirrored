-- updates for 6.1

-- Migration of N components to 1 column to 1 to 1
begin

declare @MAP_ID bigint, @COMP_ID bigint, @COL_ID bigint, @MAP_SET_ID bigint, @MappingType varchar(10), @CONSTANT nvarchar(255), @TR_ID bigint, @EXPRESSION varchar(150);
declare @LAST_MAP_ID bigint;
declare @p_pk bigint;
declare @new_tr_id bigint;
declare @TR_RULE_ID bigint, @LCD_ID bigint, @CD_ID bigint;
declare @new_tr_rule_id bigint;

declare cm_cursor cursor for 
select c.MAP_ID, c.COMP_ID, cc.COL_ID, cm.MAP_SET_ID, cm.TYPE as MappingType, cm.CONSTANT, t.TR_ID, t.EXPRESSION 
from  COM_COL_MAPPING_COMPONENT c 
inner join COMPONENT_MAPPING  cm ON c.MAP_ID = cm.MAP_ID
INNER JOIN COM_COL_MAPPING_COLUMN cc on cm.MAP_ID = cc.MAP_ID
LEFT OUTER JOIN TRANSCODING t ON t.MAP_ID = cm.MAP_ID 
where exists (select MAP_ID from COM_COL_MAPPING_COMPONENT c2 where c.COMP_ID != c2.COMP_ID and c.MAP_ID = c2.MAP_ID) 
ORDER BY cm.MAP_ID;

open cm_cursor;

fetch next from cm_cursor into @MAP_ID, @COMP_ID, @COL_ID, @MAP_SET_ID, @MappingType, @CONSTANT, @TR_ID, @EXPRESSION;
set @LAST_MAP_ID = 0;
while @@FETCH_STATUS = 0
begin;
	if @LAST_MAP_ID <> @MAP_ID
	begin
		if @LAST_MAP_ID <> 0
			DELETE FROM COMPONENT_MAPPING WHERE MAP_ID = @LAST_MAP_ID;

		SET @LAST_MAP_ID = @MAP_ID;
	end
	exec INSERT_COMPONENT_MAPPING @p_map_set_id=@MAP_SET_ID, @p_type=@MappingType,@p_constant=@CONSTANT, @p_pk = @p_pk OUTPUT;
	INSERT INTO COM_COL_MAPPING_COLUMN (COL_ID, MAP_ID) VALUES (@COL_ID, @p_pk);
	INSERT INTO COM_COL_MAPPING_COMPONENT (COMP_ID, MAP_ID) VALUES (@COMP_ID, @p_pk);
	if @TR_ID is not null
	begin
		exec INSERT_TRANSCODING @p_pk, @EXPRESSION, @new_tr_id OUTPUT;
		INSERT INTO TRANSCODING_SCRIPT (TR_ID, SCRIPT_TITLE, SCRIPT_CONTENT) 
			SELECT @new_tr_id, SCRIPT_TITLE, SCRIPT_CONTENT FROM TRANSCODING_SCRIPT WHERE TR_ID = @TR_ID;
        -- copy transcoding rules
		declare tr_cursor cursor for
		 select tr.TR_RULE_ID, lcd.LCD_ID, cd.CD_ID 
		 FROM TRANSCODING_RULE tr  
		 INNER JOIN TRANSCODING_RULE_DSD_CODE cd ON tr.TR_RULE_ID = cd.TR_RULE_ID  
		 INNER JOIN TRANSCODING_RULE_LOCAL_CODE lcd ON tr.TR_RULE_ID = lcd.TR_RULE_ID  
		 WHERE tr.TR_ID = @TR_ID AND 
		 EXISTS( 
			SELECT c.COMP_ID  FROM DSD_CODE dc  
			INNER JOIN COMPONENT c ON dc.CL_ID = c.CL_ID  
			WHERE dc.LCD_ID = cd.CD_ID AND c.COMP_ID = @COMP_ID);

		
		open tr_cursor;
		fetch next from tr_cursor into @TR_RULE_ID, @LCD_ID, @CD_ID;
		while @@FETCH_STATUS = 0
		begin
			exec UPDATE_TRANSCODING_RULE @new_tr_id,null ,@LCD_ID, @CD_ID, @new_tr_rule_id OUTPUT;
			fetch next from tr_cursor into @TR_RULE_ID, @LCD_ID, @CD_ID;
		end

		close tr_cursor;
		deallocate tr_cursor;
	end
	
	fetch next from cm_cursor into @MAP_ID, @COMP_ID, @COL_ID, @MAP_SET_ID, @MappingType, @CONSTANT, @TR_ID, @EXPRESSION;
end;
close cm_cursor;
deallocate cm_cursor;

if @LAST_MAP_ID <> 0
	DELETE FROM COMPONENT_MAPPING WHERE MAP_ID = @LAST_MAP_ID;
end;
;
GO
;
-- Support for default value
ALTER TABLE COMPONENT_MAPPING ADD DEFAULT_VALUE NVARCHAR(255) NULL
;

IF EXISTS (SELECT * FROM dbo.SYSOBJECTS WHERE id = object_id('INSERT_COMPONENT_MAPPING') AND  OBJECTPROPERTY(id, 'IsProcedure') = 1)
DROP PROC INSERT_COMPONENT_MAPPING
;

CREATE PROCEDURE INSERT_COMPONENT_MAPPING
@p_map_set_id bigint ,
@p_type varchar(10) ,
@p_constant nvarchar(255) ,
@p_default_value nvarchar(255) ,
@p_pk bigint OUT
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

		INSERT INTO COMPONENT_MAPPING (MAP_SET_ID,TYPE,CONSTANT,DEFAULT_VALUE) VALUES (@p_map_set_id, @p_type, @p_constant, @p_default_value);
		set @p_pk = SCOPE_IDENTITY();
	IF @starttrancount = 0
	COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	RAISERROR (@ErrorMessage, -- Message text.
				@ErrorSeverity, -- Severity.
				@ErrorState -- State.
				);
	END CATCH
END
;
-- Transcode uncoded components
ALTER TABLE TRANSCODING_RULE ADD UNCODED_VALUE NVARCHAR(255) NULL
;
IF EXISTS (SELECT * FROM dbo.SYSOBJECTS WHERE id = object_id('UPDATE_TRANSCODING_RULE') AND  OBJECTPROPERTY(id, 'IsProcedure') = 1)
DROP PROC UPDATE_TRANSCODING_RULE
;

CREATE PROCEDURE UPDATE_TRANSCODING_RULE
	-- Add the parameters for the stored procedure here 
	@p_tr_id bigint,
	@p_tr_rule_id bigint,
	@p_lcd_id bigint,
	@p_cd_id bigint,
	@p_uncoded_value nvarchar(255),
	@p_pk bigint OUT 
AS
BEGIN
	
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

		-- Insert statements for procedure here
		if @p_tr_rule_id is null
		begin
			INSERT INTO TRANSCODING_RULE (TR_ID,UNCODED_VALUE) VALUES (@p_tr_id, @p_uncoded_value);
			set @p_pk = SCOPE_IDENTITY();
		end
		else
		begin
			-- clean up
			delete from TRANSCODING_RULE_DSD_CODE where TR_RULE_ID = @p_tr_rule_id;
			delete from TRANSCODING_RULE_LOCAL_CODE where TR_RULE_ID = @p_tr_rule_id;
			update TRANSCODING_RULE SET UNCODED_VALUE=@p_uncoded_value where TR_RULE_ID = @p_tr_rule_id;
			set @p_pk = @p_tr_rule_id
		end
		
		-- p_cd_id could be null if p_uncoded_value is used
		if @p_cd_id is not null
			insert into TRANSCODING_RULE_DSD_CODE (TR_RULE_ID, CD_ID) VALUES (@p_pk, @p_cd_id);
		insert into TRANSCODING_RULE_LOCAL_CODE (TR_RULE_ID, LCD_ID) VALUES (@p_pk, @p_lcd_id);
		
		IF @starttrancount = 0 
			COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );	
	END CATCH
END
;
GO
;


IF EXISTS (SELECT * FROM dbo.SYSOBJECTS WHERE id = object_id('INSERT_TRANSCODING_RULE') AND  OBJECTPROPERTY(id, 'IsProcedure') = 1)
DROP PROC INSERT_TRANSCODING_RULE
;

CREATE PROCEDURE INSERT_TRANSCODING_RULE
@p_tr_id bigint ,
@p_uncoded_value nvarchar(255) ,
@p_pk bigint OUT
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

		INSERT INTO TRANSCODING_RULE (TR_ID,UNCODED_VALUE) VALUES (@p_tr_id, @p_uncoded_value);
		set @p_pk = SCOPE_IDENTITY();
	IF @starttrancount = 0
	COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	RAISERROR (@ErrorMessage, -- Message text.
				@ErrorSeverity, -- Severity.
				@ErrorState -- State.
				);
	END CATCH
END
;

-- always last
update DB_VERSION SET VERSION = '6.1'
;

