-- updates for 6.1

-- Migration of N components to 1 column to 1 to 1
declare 
PMAP_ID number;
PCOMP_ID number;
PCOL_ID number;
PMAP_SET_ID number;
PTYPE varchar2(200);
PCONSTANT nvarchar2(200);
PTR_ID number;
PEXPRESSION varchar2(150);
LAST_MAP_ID number := 0;
p_pk number;
new_tr_id number;
PTR_RULE_ID number;
PLCD_ID number;
PCD_ID number;
new_tr_rule_id number;
cursor cm_cursor is
select c.MAP_ID, c.COMP_ID, cc.COL_ID, cm.MAP_SET_ID, cm.TYPE as MappingType, cm.CONSTANT, t.TR_ID, t.EXPRESSION 
from  COM_COL_MAPPING_COMPONENT c 
inner join COMPONENT_MAPPING  cm ON c.MAP_ID = cm.MAP_ID
INNER JOIN COM_COL_MAPPING_COLUMN cc on cm.MAP_ID = cc.MAP_ID
LEFT OUTER JOIN TRANSCODING t ON t.MAP_ID = cm.MAP_ID 
where exists (select MAP_ID from COM_COL_MAPPING_COMPONENT c2 where c.COMP_ID != c2.COMP_ID and c.MAP_ID = c2.MAP_ID) 
ORDER BY cm.MAP_ID;
CURSOR tr_cursor (p_tr_id in number, p_comp_id in number) is
select tr.TR_RULE_ID, lcd.LCD_ID, cd.CD_ID 
		 FROM TRANSCODING_RULE tr  
		 INNER JOIN TRANSCODING_RULE_DSD_CODE cd ON tr.TR_RULE_ID = cd.TR_RULE_ID  
		 INNER JOIN TRANSCODING_RULE_LOCAL_CODE lcd ON tr.TR_RULE_ID = lcd.TR_RULE_ID  
		 WHERE tr.TR_ID = p_tr_id AND 
		 EXISTS( 
			SELECT c.COMP_ID  FROM DSD_CODE dc  
			INNER JOIN COMPONENT c ON dc.CL_ID = c.CL_ID 
			WHERE dc.LCD_ID = cd.CD_ID AND c.COMP_ID = p_comp_id);
begin
open cm_cursor;
FETCH cm_cursor into PMAP_ID, PCOMP_ID, PCOL_ID, PMAP_SET_ID, PTYPE, PCONSTANT, PTR_ID, PEXPRESSION;
while cm_cursor%FOUND
loop
     if LAST_MAP_ID != PMAP_ID then
        if LAST_MAP_ID != 0 then
          DELETE FROM COMPONENT_MAPPING WHERE MAP_ID = LAST_MAP_ID;
        end if;
        LAST_MAP_ID := PMAP_ID;
    end if;
    INSERT_COMPONENT_MAPPING(
        P_MAP_SET_ID => PMAP_SET_ID,
        P_TYPE => PTYPE,
        P_CONSTANT => PCONSTANT,
        P_PK => p_pk
    );
    
    INSERT INTO COM_COL_MAPPING_COLUMN (COL_ID, MAP_ID) VALUES (PCOL_ID, p_pk);
	INSERT INTO COM_COL_MAPPING_COMPONENT (COMP_ID, MAP_ID) VALUES (PCOMP_ID, p_pk);
    if PTR_ID is not null then
        INSERT_TRANSCODING(p_map_id => p_pk, p_expression => PEXPRESSION, p_pk => new_tr_id);
        INSERT INTO TRANSCODING_SCRIPT (TR_ID, SCRIPT_TITLE, SCRIPT_CONTENT) 
			SELECT new_tr_id, SCRIPT_TITLE, SCRIPT_CONTENT FROM TRANSCODING_SCRIPT WHERE TR_ID = PTR_ID;
        
        open tr_cursor(PTR_ID, PCOMP_ID);
        FETCH tr_cursor into PTR_RULE_ID, PLCD_ID, PCD_ID;
        while tr_cursor%FOUND
        loop
            UPDATE_TRANSCODING_RULE(p_tr_id => new_tr_id, p_tr_rule_id => null, p_lcd_id => PLCD_ID, p_cd_id => PCD_ID, p_pk => new_tr_rule_id);
            FETCH tr_cursor into PTR_RULE_ID, PLCD_ID, PCD_ID;
        end loop;
        
        close tr_cursor;
    end if; 

    FETCH cm_cursor into PMAP_ID, PCOMP_ID, PCOL_ID, PMAP_SET_ID, PTYPE, PCONSTANT, PTR_ID, PEXPRESSION;
end loop;

close cm_cursor;
if LAST_MAP_ID != 0 then
    DELETE FROM COMPONENT_MAPPING WHERE MAP_ID = LAST_MAP_ID;
end if;
end;
/


-- Support for default value
ALTER TABLE COMPONENT_MAPPING ADD (DEFAULT_VALUE NVARCHAR2(255) NULL)
;

CREATE OR REPLACE PROCEDURE INSERT_COMPONENT_MAPPING(
p_map_set_id IN number,
p_type IN varchar2,
p_constant IN nvarchar2,
p_default_value IN nvarchar2,
p_pk OUT number)
AS
BEGIN
		INSERT INTO COMPONENT_MAPPING (MAP_SET_ID,TYPE,CONSTANT,DEFAULT_VALUE) VALUES (p_map_set_id, p_type, p_constant, p_default_value) RETURNING MAP_ID INTO p_pk;
END;
/
;

-- Transcode uncoded components
ALTER TABLE TRANSCODING_RULE ADD (UNCODED_VALUE NVARCHAR2(255) NULL)
;

CREATE OR REPLACE PROCEDURE UPDATE_TRANSCODING_RULE(
	-- Add the parameters for the stored procedure here 
	p_tr_id IN number,
	p_tr_rule_id IN number,
	p_lcd_id IN number,
	p_cd_id IN number,
	p_uncoded_value IN nvarchar2,
	p_pk OUT NUMBER) 
AS
BEGIN
		-- Insert statements for procedure here
		if (p_tr_rule_id) is null
		then
			INSERT INTO TRANSCODING_RULE (TR_ID,UNCODED_VALUE) VALUES (p_tr_id, p_uncoded_value) RETURNING TR_RULE_ID INTO p_pk;
		else
			-- clean up
			delete from TRANSCODING_RULE_DSD_CODE where TR_RULE_ID = p_tr_rule_id;
			delete from TRANSCODING_RULE_LOCAL_CODE where TR_RULE_ID = p_tr_rule_id;
			update TRANSCODING_RULE SET UNCODED_VALUE=p_uncoded_value where TR_RULE_ID = p_tr_rule_id;
			p_pk := p_tr_rule_id;
		end if;
		
		-- p_cd_id could be null if p_uncoded_value is used
		if (p_cd_id) is not null
		then
			insert into TRANSCODING_RULE_DSD_CODE (TR_RULE_ID, CD_ID) VALUES (p_pk, p_cd_id);
		end if;
		insert into TRANSCODING_RULE_LOCAL_CODE (TR_RULE_ID, LCD_ID) VALUES (p_pk, p_lcd_id);		
END;
/

CREATE OR REPLACE PROCEDURE INSERT_TRANSCODING_RULE(
p_tr_id IN number,
p_uncoded_value IN nvarchar2,
p_pk OUT number)
AS
BEGIN
		INSERT INTO TRANSCODING_RULE (TR_ID,UNCODED_VALUE) VALUES (p_tr_id, p_uncoded_value) RETURNING TR_RULE_ID INTO p_pk;
END;
/
;

-- always last
update DB_VERSION SET VERSION = '6.1'
;

