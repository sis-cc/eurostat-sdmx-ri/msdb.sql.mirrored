-- updates for 6.1

-- Migration of N components to 1 column to 1 to 1

DROP PROCEDURE IF EXISTS TMP_PROC
;

create procedure TMP_PROC()
begin
	DECLARE done INT DEFAULT FALSE;
	declare PMAP_ID bigint;
	declare PCOMP_ID bigint;
	declare PCOL_ID bigint;
	declare PMAP_SET_ID bigint;
	declare PTYPE varchar(200);
	declare PCONSTANT nvarchar(200);
	declare PTR_ID bigint;
	declare PEXPRESSION varchar(150);
	declare LAST_MAP_ID bigint  DEFAULT 0;
	declare p_pk bigint;
	declare new_tr_id bigint;
	declare PTR_RULE_ID bigint;
	declare PLCD_ID bigint;
	declare PCD_ID bigint;
	declare new_tr_rule_id bigint;
	declare cm_cursor cursor for
		select c.MAP_ID, c.COMP_ID, cc.COL_ID, cm.MAP_SET_ID, cm.TYPE as MappingType, cm.CONSTANT, t.TR_ID, t.EXPRESSION 
		from  COM_COL_MAPPING_COMPONENT c 
		inner join COMPONENT_MAPPING  cm ON c.MAP_ID = cm.MAP_ID
		INNER JOIN COM_COL_MAPPING_COLUMN cc on cm.MAP_ID = cc.MAP_ID
		LEFT OUTER JOIN TRANSCODING t ON t.MAP_ID = cm.MAP_ID 
		where exists (select MAP_ID from COM_COL_MAPPING_COMPONENT c2 where c.COMP_ID != c2.COMP_ID and c.MAP_ID = c2.MAP_ID) 
		ORDER BY cm.MAP_ID;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

	open cm_cursor;
	loopLabel : LOOP
	FETCH cm_cursor into PMAP_ID, PCOMP_ID, PCOL_ID, PMAP_SET_ID, PTYPE, PCONSTANT, PTR_ID, PEXPRESSION;
	if done then
		close cm_cursor;
		leave loopLabel;
	end if;
	if LAST_MAP_ID != PMAP_ID then
		if LAST_MAP_ID != 0 then
			DELETE FROM COMPONENT_MAPPING WHERE MAP_ID = LAST_MAP_ID;
		end if;
		set LAST_MAP_ID = PMAP_ID;
	end if;
	call INSERT_COMPONENT_MAPPING(
		PMAP_SET_ID,
		PTYPE,
		PCONSTANT,		
		p_pk
	);
	
	INSERT INTO COM_COL_MAPPING_COLUMN (COL_ID, MAP_ID) VALUES (PCOL_ID, p_pk);
	INSERT INTO COM_COL_MAPPING_COMPONENT (COMP_ID, MAP_ID) VALUES (PCOMP_ID, p_pk);
	if PTR_ID is not null then
		call INSERT_TRANSCODING(p_pk, PEXPRESSION, new_tr_id);
		INSERT INTO TRANSCODING_SCRIPT (TR_ID, SCRIPT_TITLE, SCRIPT_CONTENT) 
		SELECT new_tr_id, SCRIPT_TITLE, SCRIPT_CONTENT FROM TRANSCODING_SCRIPT WHERE TR_ID = PTR_ID;
		TR_BLOCK : begin
		declare trdone INT DEFAULT FALSE;
		declare  tr_cursor CURSOR for
		select tr.TR_RULE_ID, lcd.LCD_ID, cd.CD_ID 
		FROM TRANSCODING_RULE tr  
		INNER JOIN TRANSCODING_RULE_DSD_CODE cd ON tr.TR_RULE_ID = cd.TR_RULE_ID  
		INNER JOIN TRANSCODING_RULE_LOCAL_CODE lcd ON tr.TR_RULE_ID = lcd.TR_RULE_ID  
		WHERE tr.TR_ID = PTR_ID AND 
		EXISTS( 
			SELECT c.COMP_ID  FROM DSD_CODE dc  
			INNER JOIN COMPONENT c ON dc.CL_ID = c.CL_ID 
			WHERE dc.LCD_ID = cd.CD_ID AND c.COMP_ID = PCOMP_ID);
		DECLARE CONTINUE HANDLER FOR NOT FOUND SET trdone = TRUE;
	
		open tr_cursor;
		trLoop: LOOP
	
		FETCH tr_cursor into PTR_RULE_ID, PLCD_ID, PCD_ID;
		if trdone then
			close tr_cursor;
			leave trLoop;
		end if;
		call UPDATE_TRANSCODING_RULE(new_tr_id, null, PLCD_ID, PCD_ID, new_tr_rule_id);
		end loop;
	end TR_BLOCK;
	end if; 
	end loop;
	if LAST_MAP_ID != 0 then
		DELETE FROM COMPONENT_MAPPING WHERE MAP_ID = LAST_MAP_ID;
	end if;
end;
/
;
CALL TMP_PROC()
;
DROP PROCEDURE IF EXISTS TMP_PROC
;
-- Support for default value

ALTER TABLE COMPONENT_MAPPING ADD (DEFAULT_VALUE NVARCHAR(255) NULL)
;

DROP PROCEDURE IF EXISTS INSERT_COMPONENT_MAPPING
;

CREATE PROCEDURE INSERT_COMPONENT_MAPPING(
IN p_map_set_id bigint,
IN p_type varchar(10),
IN p_constant nvarchar(255),
IN p_default_value nvarchar(255),
OUT p_pk bigint)
BEGIN
		INSERT INTO COMPONENT_MAPPING (MAP_SET_ID,TYPE,CONSTANT,DEFAULT_VALUE) VALUES (p_map_set_id, p_type, p_constant, p_default_value);
		set p_pk = LAST_INSERT_ID();
END;
;

-- Transcode uncoded components
ALTER TABLE TRANSCODING_RULE ADD (UNCODED_VALUE NVARCHAR(255) NULL)
;
DROP PROCEDURE IF EXISTS UPDATE_TRANSCODING_RULE
;
CREATE PROCEDURE UPDATE_TRANSCODING_RULE(
	IN p_tr_id bigint,
	IN p_tr_rule_id bigint,
	IN p_lcd_id bigint,
	IN p_cd_id bigint,
	IN p_uncoded_value nvarchar(255),
	OUT p_pk bigint) 
BEGIN
		if p_tr_rule_id is null then
			INSERT INTO TRANSCODING_RULE (TR_ID,UNCODED_VALUE) VALUES (p_tr_id, p_uncoded_value);
			set p_pk = LAST_INSERT_ID();
		else
			-- clean up
			delete from TRANSCODING_RULE_DSD_CODE where TR_RULE_ID = p_tr_rule_id;
			delete from TRANSCODING_RULE_LOCAL_CODE where TR_RULE_ID = p_tr_rule_id;
			update TRANSCODING_RULE SET UNCODED_VALUE=p_uncoded_value where TR_RULE_ID = p_tr_rule_id;
			set p_pk = p_tr_rule_id;
		end if;

		-- p_cd_id could be null if p_uncoded_value is used
		if p_cd_id is not null then	
			insert into TRANSCODING_RULE_DSD_CODE (TR_RULE_ID, CD_ID) VALUES (p_pk, p_cd_id);
		end if;
		insert into TRANSCODING_RULE_LOCAL_CODE (TR_RULE_ID, LCD_ID) VALUES (p_pk, p_lcd_id);
END
;
DROP PROCEDURE IF EXISTS INSERT_TRANSCODING_RULE
;

CREATE PROCEDURE INSERT_TRANSCODING_RULE(
IN p_tr_id bigint,
IN p_uncoded_value nvarchar(255),
OUT p_pk bigint)
BEGIN
		INSERT INTO TRANSCODING_RULE (TR_ID,UNCODED_VALUE) VALUES (p_tr_id, p_uncoded_value);
		set p_pk = LAST_INSERT_ID();
END;
;


-- always last
update DB_VERSION SET VERSION = '6.1'
;

