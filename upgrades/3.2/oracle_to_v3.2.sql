﻿-- Upgrade script to 3.2 from 3.1.

ALTER TABLE ARTEFACT ADD ( URI NVARCHAR2(255) NULL, LAST_MODIFIED TIMESTAMP NULL)
;
ALTER TABLE CONCEPT ADD ( PARENT_CONCEPT_ID NUMBER(18) NULL)
;

CREATE TABLE AGENCY ( 
	AG_ID NUMBER(18) NOT NULL,
	AG_SCH_ID NUMBER(18) NOT NULL
)
;

CREATE TABLE AGENCY_SCHEME ( 
	AG_SCH_ID NUMBER(18) NOT NULL,
    is_partial NUMBER(1) DEFAULT 0 NOT NULL 
)
;

CREATE TABLE DATACONSUMER ( 
	DC_ID NUMBER(18) NOT NULL,
	DC_SCH_ID NUMBER(18) NOT NULL
)
;

CREATE TABLE DATACONSUMER_SCHEME ( 
	DC_SCH_ID NUMBER(18) NOT NULL,
    is_partial NUMBER(1) DEFAULT 0 NOT NULL 
)
;

CREATE TABLE DATAPROVIDER ( 
	DP_ID NUMBER(18) NOT NULL,
	DP_SCH_ID NUMBER(18) NOT NULL
)
;

CREATE TABLE DATAPROVIDER_SCHEME ( 
	DP_SCH_ID NUMBER(18) NOT NULL,
    is_partial NUMBER(1) DEFAULT 0 NOT NULL 
)
;

CREATE TABLE ORGANISATION_UNIT ( 
	ORG_UNIT_ID NUMBER(18) NOT NULL,
	ORG_UNIT_SCH_ID NUMBER(18) NOT NULL,
	PARENT_ORG_UNIT_ID NUMBER(18) NULL
)
;

CREATE TABLE ORGANISATION_UNIT_SCHEME ( 
	ORG_UNIT_SCH_ID NUMBER(18) NOT NULL,
    is_partial NUMBER(1) DEFAULT 0 NOT NULL 
)
;

CREATE TABLE STRUCTURE_SET ( 
	SS_ID NUMBER(18) NOT NULL
)
;
CREATE TABLE CODELIST_MAP
(
	CLM_ID NUMBER(18) NOT NULL,
	SS_ID NUMBER(18) NOT NULL,
	SOURCE_CL_ID NUMBER(18) NOT NULL,
	TARGET_CL_ID NUMBER(18) NOT NULL
)
;
CREATE TABLE CODE_MAP
(
	DCM_ID NUMBER(18)  NOT NULL,
	CLM_ID NUMBER(18) NOT NULL,
	SOURCE_LCD_ID NUMBER(18) NOT NULL,
	TARGET_LCD_ID NUMBER(18) NOT NULL
)
;
CREATE TABLE STRUCTURE_MAP
(
	SM_ID NUMBER(18) NOT NULL,
	SS_ID NUMBER(18) NOT NULL,
	SOURCE_STR_ID NUMBER(18) NOT NULL,
	TARGET_STR_ID NUMBER(18) NOT NULL
)
;
CREATE TABLE COMPONENT_MAP
(
	CM_ID NUMBER(18) NOT NULL ,
	SM_ID NUMBER(18) NOT NULL,
	SOURCE_COMP_ID NUMBER(18) NOT NULL,
	TARGET_COMP_ID NUMBER(18) NOT NULL
)
;
CREATE TABLE ANNOTATION
(
	ANN_ID NUMBER(18) NOT NULL, 
	ID nvarchar2(50) NULL,
	TITLE nvarchar2(70) NULL,
	TYPE nvarchar2(50) NULL,
	URL nvarchar2(1000) NULL
)
;
CREATE TABLE ANNOTATION_TEXT
(
	ANN_TEXT_ID NUMBER(18) NOT NULL, 
	ANN_ID NUMBER(18) NOT NULL,
	LANGUAGE varchar2(50) NOT NULL,
	TEXT nvarchar2(2000) NOT NULL
)
;
CREATE TABLE ARTEFACT_ANNOTATION
(
	ANN_ID NUMBER(18) NOT NULL, 
	ART_ID NUMBER(18) NOT NULL
)
;
CREATE TABLE ITEM_ANNOTATION
(
	ANN_ID NUMBER(18) NOT NULL, 
	ITEM_ID NUMBER(18) NOT NULL
)
;
CREATE TABLE COMPONENT_ANNOTATION
(
	ANN_ID NUMBER(18) NOT NULL, 
	COMP_ID NUMBER(18) NOT NULL
)
;
CREATE TABLE GROUP_ANNOTATION
(
	ANN_ID NUMBER(18) NOT NULL, 
	GR_ID NUMBER(18) NOT NULL
)
;


ALTER TABLE AGENCY ADD CONSTRAINT PK_AGENCY 
	PRIMARY KEY (AG_ID)
;

ALTER TABLE AGENCY_SCHEME ADD CONSTRAINT PK_AGENCY_SCHEME 
	PRIMARY KEY (AG_SCH_ID)
;

ALTER TABLE DATACONSUMER ADD CONSTRAINT PK_DATACONSUMER 
	PRIMARY KEY (DC_ID)
;

ALTER TABLE DATACONSUMER_SCHEME ADD CONSTRAINT PK_DATACONSUMER_SCHEME 
	PRIMARY KEY (DC_SCH_ID)
;

ALTER TABLE DATAPROVIDER ADD CONSTRAINT PK_DATAPROVIDER 
	PRIMARY KEY (DP_ID)
;

ALTER TABLE DATAPROVIDER_SCHEME ADD CONSTRAINT PK_DATAPROVIDER_SCHEME 
	PRIMARY KEY (DP_SCH_ID)
;

ALTER TABLE ORGANISATION_UNIT ADD CONSTRAINT PK_ORGANISATION_UNIT
	PRIMARY KEY (ORG_UNIT_ID)
;

ALTER TABLE ORGANISATION_UNIT_SCHEME ADD CONSTRAINT PK_ORGANISATION_UNIT_SCHEME 
	PRIMARY KEY (ORG_UNIT_SCH_ID)
;

ALTER TABLE STRUCTURE_SET ADD CONSTRAINT PK_STRUCTURE_SET
	PRIMARY KEY (SS_ID)
;

ALTER TABLE AGENCY ADD CONSTRAINT FK_AGENCY_ID 
	FOREIGN KEY (AG_ID) REFERENCES ITEM (ITEM_ID)
ON DELETE CASCADE
;

ALTER TABLE AGENCY ADD CONSTRAINT FK_AGENCY_AGENCY_SCHEME 
	FOREIGN KEY (AG_SCH_ID) REFERENCES AGENCY_SCHEME (AG_SCH_ID)
ON DELETE CASCADE
;

ALTER TABLE AGENCY_SCHEME ADD CONSTRAINT FK_AGENCY_SCHEME_ARTEFACT 
	FOREIGN KEY (AG_SCH_ID) REFERENCES ARTEFACT (ART_ID)
ON DELETE CASCADE
;

ALTER TABLE DATACONSUMER ADD CONSTRAINT FK_DATACONSUMER_ID 
	FOREIGN KEY (DC_ID) REFERENCES ITEM (ITEM_ID)
ON DELETE CASCADE
;

ALTER TABLE DATACONSUMER ADD CONSTRAINT FK_DATACONSUMER_DC_SCH_ID 
	FOREIGN KEY (DC_SCH_ID) REFERENCES DATACONSUMER_SCHEME (DC_SCH_ID)
ON DELETE CASCADE
;

ALTER TABLE DATACONSUMER_SCHEME ADD CONSTRAINT FK_DATACONSUMER_SCHEME_ART_ID 
	FOREIGN KEY (DC_SCH_ID) REFERENCES ARTEFACT (ART_ID)
ON DELETE CASCADE
;

ALTER TABLE DATAPROVIDER ADD CONSTRAINT FK_DATAPROVIDER_ID 
	FOREIGN KEY (DP_ID) REFERENCES ITEM (ITEM_ID)
ON DELETE CASCADE
;

ALTER TABLE DATAPROVIDER ADD CONSTRAINT FK_DATAPROVIDER_DP_SCH_ID 
	FOREIGN KEY (DP_SCH_ID) REFERENCES DATAPROVIDER_SCHEME (DP_SCH_ID)
ON DELETE CASCADE
;

ALTER TABLE DATAPROVIDER_SCHEME ADD CONSTRAINT FK_DATAPROVIDER_SCHEME_ART_ID 
	FOREIGN KEY (DP_SCH_ID) REFERENCES ARTEFACT (ART_ID)
ON DELETE CASCADE
;

ALTER TABLE ORGANISATION_UNIT ADD CONSTRAINT FK_ORGANISATION_UNIT_ID 
	FOREIGN KEY (ORG_UNIT_ID) REFERENCES ITEM (ITEM_ID)
ON DELETE CASCADE
;

ALTER TABLE ORGANISATION_UNIT ADD CONSTRAINT FK_ORGANISATION_UNIT_SCH_ID 
	FOREIGN KEY (ORG_UNIT_SCH_ID) REFERENCES ORGANISATION_UNIT_SCHEME (ORG_UNIT_SCH_ID)
ON DELETE CASCADE
;

ALTER TABLE ORGANISATION_UNIT ADD CONSTRAINT FK_ORGANISATION_UNIT_ITEM 
	FOREIGN KEY (PARENT_ORG_UNIT_ID) REFERENCES ORGANISATION_UNIT (ORG_UNIT_ID)
	ON DELETE SET NULL
;

ALTER TABLE ORGANISATION_UNIT_SCHEME ADD CONSTRAINT FK_ORGANISATION_UNIT_SCH_ART 
	FOREIGN KEY (ORG_UNIT_SCH_ID) REFERENCES ARTEFACT (ART_ID)
ON DELETE CASCADE
;

ALTER TABLE STRUCTURE_SET ADD CONSTRAINT FK_STRUCTURE_SET_ARTEFACT 
	FOREIGN KEY (SS_ID) REFERENCES ARTEFACT (ART_ID)
ON DELETE CASCADE
;

ALTER TABLE CONCEPT ADD CONSTRAINT FK_CONCEPT_ITEM 
	FOREIGN KEY (PARENT_CONCEPT_ID) REFERENCES CONCEPT (CON_ID)
	ON DELETE SET NULL
;
ALTER TABLE CODELIST_MAP ADD CONSTRAINT PK_CODELIST_MAP PRIMARY KEY(CLM_ID)
;
ALTER TABLE CODE_MAP ADD CONSTRAINT PK_CODE_MAP PRIMARY KEY(DCM_ID)
;
ALTER TABLE STRUCTURE_MAP ADD CONSTRAINT PK_STRUCTURE_MAP PRIMARY KEY(SM_ID)
;
ALTER TABLE COMPONENT_MAP ADD CONSTRAINT PK_COMPONENT_MAP PRIMARY KEY(CM_ID)
;
ALTER TABLE ANNOTATION ADD CONSTRAINT PK_ANNOTATION PRIMARY KEY(ANN_ID)
;
ALTER TABLE ANNOTATION_TEXT ADD CONSTRAINT PK_ANNOTATION_TEXT PRIMARY KEY(ANN_TEXT_ID)
;
ALTER TABLE ARTEFACT_ANNOTATION ADD CONSTRAINT PK_ARTEFACT_ANNOTATION PRIMARY KEY(ANN_ID, ART_ID)
;
ALTER TABLE ITEM_ANNOTATION ADD CONSTRAINT PK_ITEM_ANNOTATION PRIMARY KEY(ANN_ID, ITEM_ID)
;
ALTER TABLE COMPONENT_ANNOTATION ADD CONSTRAINT PK_COMPONENT_ANNOTATION PRIMARY KEY(ANN_ID, COMP_ID)
;
ALTER TABLE GROUP_ANNOTATION ADD CONSTRAINT PK_GROUP_ANNOTATION PRIMARY KEY(ANN_ID, GR_ID)
;

ALTER TABLE CODELIST_MAP ADD CONSTRAINT FK_CL_MAP_ITEM FOREIGN KEY(CLM_ID) REFERENCES ITEM(ITEM_ID) ON DELETE CASCADE
;
ALTER TABLE CODELIST_MAP ADD CONSTRAINT FK_CL_MAP_SS FOREIGN KEY(SS_ID) REFERENCES STRUCTURE_SET(SS_ID) ON DELETE CASCADE
;
ALTER TABLE CODELIST_MAP ADD CONSTRAINT FK_CL_MAP_SOURCE FOREIGN KEY(SOURCE_CL_ID) REFERENCES CODELIST(CL_ID)
;
ALTER TABLE CODELIST_MAP ADD CONSTRAINT FK_CL_MAP_TARGET FOREIGN KEY(TARGET_CL_ID) REFERENCES CODELIST(CL_ID)
;

ALTER TABLE CODE_MAP ADD CONSTRAINT FK_CD_MAP_CLM FOREIGN KEY(CLM_ID) REFERENCES CODELIST_MAP(CLM_ID) ON DELETE CASCADE
;
ALTER TABLE CODE_MAP ADD CONSTRAINT FK_CD_MAP_SOURCE FOREIGN KEY(SOURCE_LCD_ID) REFERENCES DSD_CODE(LCD_ID)
;
ALTER TABLE CODE_MAP ADD CONSTRAINT FK_CD_MAP_TARGET FOREIGN KEY(TARGET_LCD_ID) REFERENCES DSD_CODE(LCD_ID)
;

ALTER TABLE STRUCTURE_MAP ADD CONSTRAINT FK_STR_MAP_ITEM FOREIGN KEY(SM_ID) REFERENCES ITEM(ITEM_ID) ON DELETE CASCADE
;
ALTER TABLE STRUCTURE_MAP ADD CONSTRAINT FK_STR_MAP_SS FOREIGN KEY(SS_ID) REFERENCES STRUCTURE_SET(SS_ID) ON DELETE CASCADE
;
ALTER TABLE STRUCTURE_MAP ADD CONSTRAINT FK_STR_MAP_SOURCE FOREIGN KEY(SOURCE_STR_ID) REFERENCES ARTEFACT(ART_ID)
;
ALTER TABLE STRUCTURE_MAP ADD CONSTRAINT FK_STR_MAP_TARGET FOREIGN KEY(TARGET_STR_ID) REFERENCES ARTEFACT(ART_ID)
;

ALTER TABLE COMPONENT_MAP ADD CONSTRAINT FK_C_MAP_SM FOREIGN KEY(SM_ID) REFERENCES STRUCTURE_MAP(SM_ID) ON DELETE CASCADE
;
ALTER TABLE COMPONENT_MAP ADD CONSTRAINT FK_C_MAP_SOURCE FOREIGN KEY(SOURCE_COMP_ID) REFERENCES COMPONENT(COMP_ID)
;
ALTER TABLE COMPONENT_MAP ADD CONSTRAINT FK_C_MAP_TARGET FOREIGN KEY(TARGET_COMP_ID) REFERENCES COMPONENT(COMP_ID)
;

ALTER TABLE ANNOTATION_TEXT ADD CONSTRAINT FK_ANN_TEXT FOREIGN KEY(ANN_ID) REFERENCES ANNOTATION(ANN_ID) ON DELETE CASCADE
;

ALTER TABLE ARTEFACT_ANNOTATION ADD CONSTRAINT FK_ART_ANN_ANN FOREIGN KEY(ANN_ID) REFERENCES ANNOTATION(ANN_ID) ON DELETE CASCADE
;
ALTER TABLE ARTEFACT_ANNOTATION ADD CONSTRAINT FK_ART_ANN_ART FOREIGN KEY(ART_ID) REFERENCES ARTEFACT(ART_ID) ON DELETE CASCADE
;
ALTER TABLE ITEM_ANNOTATION ADD CONSTRAINT FK_ITEM_ANN_ANN FOREIGN KEY(ANN_ID) REFERENCES ANNOTATION(ANN_ID) ON DELETE CASCADE
;
ALTER TABLE ITEM_ANNOTATION ADD CONSTRAINT FK_ITEM_ANN_ITEM FOREIGN KEY(ITEM_ID) REFERENCES ITEM(ITEM_ID) ON DELETE CASCADE
;
ALTER TABLE COMPONENT_ANNOTATION ADD CONSTRAINT FK_COMP_ANN_ANN FOREIGN KEY(ANN_ID) REFERENCES ANNOTATION(ANN_ID) ON DELETE CASCADE
;
ALTER TABLE COMPONENT_ANNOTATION ADD CONSTRAINT FK_COMP_ANN_COMP FOREIGN KEY(COMP_ID) REFERENCES COMPONENT(COMP_ID) ON DELETE CASCADE
;
ALTER TABLE GROUP_ANNOTATION ADD CONSTRAINT FK_GR_ANN_ANN FOREIGN KEY(ANN_ID) REFERENCES ANNOTATION(ANN_ID) ON DELETE CASCADE
;
ALTER TABLE GROUP_ANNOTATION ADD CONSTRAINT FK_GR_ANN_GR FOREIGN KEY(GR_ID) REFERENCES DSD_GROUP(GR_ID) ON DELETE CASCADE
;



CREATE OR REPLACE PROCEDURE INSERT_ARTEFACT(
	p_id IN varchar2,
	p_version IN varchar2,
	p_agency IN varchar2,
	p_valid_from IN  timestamp DEFAULT NULL,
	p_valid_to IN  timestamp DEFAULT NULL,
	p_is_final IN number DEFAULT NULL,
	p_uri IN nvarchar2,
	p_last_modified IN timestamp DEFAULT NULL,
	p_pk OUT NUMBER)
AS
  P_VERSION1 NUMBER;
  P_VERSION2 NUMBER;
  P_VERSION3 NUMBER;
BEGIN
    SPLIT_VERSION(
        P_VERSION => P_VERSION,
        P_VERSION1 => P_VERSION1,
        P_VERSION2 => P_VERSION2,
        P_VERSION3 => P_VERSION3
    );
   	insert into ARTEFACT (ART_ID, ID,VERSION1, VERSION2, VERSION3, AGENCY, VALID_FROM, VALID_TO, IS_FINAL, URI, LAST_MODIFIED) values (ARTEFACT_SEQ.nextval, p_id,p_version1,p_version2,p_version3,p_agency,p_valid_from,p_valid_to,p_is_final,p_uri,p_last_modified);
	select ARTEFACT_SEQ.currval into p_pk from dual;
END;
/

CREATE OR REPLACE PROCEDURE INSERT_HCL_CODE(
	p_id IN varchar2,
	p_version IN varchar2,
	p_agency IN varchar2,
	p_valid_from IN  timestamp DEFAULT NULL,
	p_valid_to IN  timestamp DEFAULT NULL,
	p_is_final IN number DEFAULT NULL,
	p_uri IN nvarchar2,
	p_last_modified IN timestamp DEFAULT NULL,
	p_parent_hcode_id IN number DEFAULT NULL,
	p_lcd_id IN number,
	p_h_id IN number,
	p_level_id IN number DEFAULT NULL,
	p_pk OUT NUMBER)
AS
BEGIN

	INSERT_ARTEFACT(p_id => p_id,p_version => p_version,p_agency => p_agency,p_valid_from => p_valid_from,p_valid_to => p_valid_to,p_is_final => p_is_final,p_uri => p_uri,p_last_modified => p_last_modified,p_pk => p_pk);
	
	INSERT INTO HCL_CODE
           (HCODE_ID
           ,PARENT_HCODE_ID
           ,LCD_ID
           ,H_ID
           ,LEVEL_ID)
	VALUES
		(p_pk
        ,p_parent_hcode_id
        ,p_lcd_id
        ,p_h_id
         ,p_level_id);
END;
/


CREATE OR REPLACE PROCEDURE INSERT_HCL(
	p_id IN varchar2,
	p_version IN varchar2,
	p_agency IN varchar2,
	p_valid_from IN  timestamp DEFAULT NULL,
	p_valid_to IN  timestamp DEFAULT NULL,
	p_is_final IN number DEFAULT NULL,
	p_uri IN nvarchar2,
	p_last_modified IN timestamp DEFAULT NULL,
	p_pk OUT NUMBER)
AS
BEGIN

	INSERT_ARTEFACT(p_id => p_id,p_version => p_version,p_agency => p_agency,p_valid_from => p_valid_from,p_valid_to => p_valid_to,p_is_final => p_is_final,p_uri => p_uri,p_last_modified => p_last_modified,p_pk => p_pk);
	insert into HCL (HCL_ID) values(p_pk);
END;
/

CREATE OR REPLACE PROCEDURE INSERT_HIERACHY(
	p_id IN varchar2,
	p_version IN varchar2,
	p_agency IN varchar2,
	p_valid_from IN  timestamp DEFAULT NULL,
	p_valid_to IN  timestamp DEFAULT NULL,
	p_is_final IN number DEFAULT NULL,
	p_uri IN nvarchar2,
	p_last_modified IN timestamp DEFAULT NULL,
	p_hcl_id IN number,
	p_pk OUT NUMBER)
AS
BEGIN

	INSERT_ARTEFACT(p_id => p_id,p_version => p_version,p_agency => p_agency,p_valid_from => p_valid_from,p_valid_to => p_valid_to,p_is_final => p_is_final,p_uri => p_uri,p_last_modified => p_last_modified,p_pk => p_pk);
	
	INSERT INTO HIERARCHY
           (H_ID
           ,HCL_ID)
     VALUES
           (p_pk
           ,p_hcl_id);
END;
/


CREATE OR REPLACE PROCEDURE INSERT_HLEVEL(
	p_id IN varchar2,
	p_version IN varchar2,
	p_agency IN varchar2,
	p_valid_from IN  timestamp DEFAULT NULL,
	p_valid_to IN  timestamp DEFAULT NULL,
	p_is_final IN number DEFAULT NULL,
	p_uri IN nvarchar2,
	p_last_modified IN timestamp DEFAULT NULL,
	p_h_id IN number,
	p_parent_level_id IN number DEFAULT NULL,
	p_pk OUT NUMBER)
AS
BEGIN

	INSERT_ARTEFACT(p_id => p_id,p_version => p_version,p_agency => p_agency,p_valid_from => p_valid_from,p_valid_to => p_valid_to,p_is_final => p_is_final,p_uri => p_uri,p_last_modified => p_last_modified,p_pk => p_pk);
	
	INSERT INTO HLEVEL
           (LEVEL_ID
           ,H_ID
           ,PARENT_LEVEL_ID)
     VALUES
           (p_pk
           ,p_h_id
           ,p_parent_level_id);
END;
/

CREATE OR REPLACE PROCEDURE INSERT_DSD(
	p_id IN varchar2,
	p_version IN varchar2,
	p_agency IN varchar2,
	p_valid_from IN  timestamp DEFAULT NULL,
	p_valid_to IN  timestamp DEFAULT NULL,
	p_is_final IN number DEFAULT NULL,
	p_uri IN nvarchar2,
	p_last_modified IN timestamp DEFAULT NULL,
	p_pk OUT NUMBER)
AS
BEGIN
    INSERT_ARTEFACT(p_id => p_id,p_version => p_version,p_agency => p_agency,p_valid_from => p_valid_from,p_valid_to => p_valid_to,p_is_final => p_is_final,p_uri => p_uri,p_last_modified => p_last_modified,p_pk => p_pk);
	
	INSERT INTO DSD
           (DSD_ID)
     VALUES
           (p_pk);
END;
/


CREATE OR REPLACE PROCEDURE INSERT_DATAFLOW(
	p_id IN varchar2,
	p_version IN varchar2,
	p_agency IN varchar2,
	p_valid_from IN  timestamp DEFAULT NULL,
	p_valid_to IN  timestamp DEFAULT NULL,
	p_is_final IN number DEFAULT NULL,
	p_uri IN nvarchar2,
	p_last_modified IN timestamp DEFAULT NULL,
	p_dsd_id IN number,
	p_map_set_id IN number DEFAULT NULL,
	p_pk OUT NUMBER)
AS
BEGIN

	INSERT_ARTEFACT(p_id => p_id,p_version => p_version,p_agency => p_agency,p_valid_from => p_valid_from,p_valid_to => p_valid_to,p_is_final => p_is_final,p_uri => p_uri,p_last_modified => p_last_modified,p_pk => p_pk);
	
INSERT INTO DATAFLOW
           (DF_ID
           ,DSD_ID
           ,MAP_SET_ID)
     VALUES
           (p_pk
           ,p_dsd_id
           ,p_map_set_id);
END;
/


CREATE OR REPLACE PROCEDURE INSERT_CATEGORISATION(
	p_id IN varchar2,
	p_version IN varchar2,
	p_agency IN varchar2,
	p_valid_from IN  timestamp DEFAULT NULL,
	p_valid_to IN  timestamp DEFAULT NULL,
	p_is_final IN number DEFAULT NULL,
	p_uri IN nvarchar2,
	p_last_modified IN timestamp DEFAULT NULL,
	p_art_id IN number,
	p_cat_id IN number,
	p_dc_order IN number DEFAULT NULL,
	p_pk OUT NUMBER)
AS
BEGIN

	INSERT_ARTEFACT(p_id => p_id,p_version => p_version,p_agency => p_agency,p_valid_from => p_valid_from,p_valid_to => p_valid_to,p_is_final => p_is_final,p_uri =>p_uri,p_last_modified => p_last_modified,p_pk => p_pk);
	
INSERT INTO CATEGORISATION
           (CATN_ID
           ,ART_ID
           ,CAT_ID
           ,DC_ORDER)
     VALUES
           (p_pk
           ,p_art_id
           ,p_cat_id
           ,p_dc_order);
END;
/


CREATE OR REPLACE PROCEDURE INSERT_CATEGORY_SCHEME(
	p_id IN varchar2,
	p_version IN varchar2,
	p_agency IN varchar2,
	p_valid_from IN  timestamp DEFAULT NULL,
	p_valid_to IN  timestamp DEFAULT NULL,
	p_is_final IN number DEFAULT NULL,
	p_uri IN nvarchar2,
	p_last_modified IN timestamp DEFAULT NULL,
	p_cs_order IN number default 0,
    p_is_partial IN number default 0,
	p_pk OUT NUMBER)
AS
BEGIN

	INSERT_ARTEFACT(p_id => p_id,p_version => p_version,p_agency => p_agency,p_valid_from => p_valid_from,p_valid_to => p_valid_to,p_is_final => p_is_final,p_uri => p_uri,p_last_modified => p_last_modified,p_pk => p_pk);
	
	INSERT INTO CATEGORY_SCHEME
           (CAT_SCH_ID, CS_ORDER, IS_PARTIAL)
     VALUES
           (p_pk, p_cs_order, p_is_partial);
END;
/


CREATE OR REPLACE PROCEDURE INSERT_CODELIST(
	p_id IN varchar2,
	p_version IN varchar2,
	p_agency IN varchar2,
	p_valid_from IN  timestamp DEFAULT NULL,
	p_valid_to IN  timestamp DEFAULT NULL,
	p_is_final IN number DEFAULT NULL,
	p_uri IN nvarchar2,
	p_last_modified IN timestamp DEFAULT NULL,
    p_is_partial IN number default 0,
	p_pk OUT NUMBER)
AS
BEGIN

	INSERT_ARTEFACT(p_id => p_id,p_version => p_version,p_agency => p_agency,p_valid_from => p_valid_from,p_valid_to => p_valid_to,p_is_final => p_is_final,p_uri => p_uri, p_last_modified => p_last_modified,p_pk => p_pk);
	
	INSERT INTO CODELIST
           (CL_ID, IS_PARTIAL)
     VALUES
           (p_pk, p_is_partial);
END;
/


CREATE OR REPLACE PROCEDURE INSERT_CONCEPT_SCHEME(
	p_id IN varchar2,
	p_version IN varchar2,
	p_agency IN varchar2,
	p_valid_from IN  timestamp DEFAULT NULL,
	p_valid_to IN  timestamp DEFAULT NULL,
	p_is_final IN number DEFAULT NULL,
	p_uri IN nvarchar2,
	p_last_modified IN timestamp DEFAULT NULL,
    p_is_partial IN number default 0,
	p_pk OUT NUMBER)
AS
BEGIN
	INSERT_ARTEFACT(p_id => p_id,p_version => p_version,p_agency => p_agency,p_valid_from => p_valid_from,p_valid_to => p_valid_to,p_is_final => p_is_final,p_uri => p_uri, p_last_modified => p_last_modified,p_pk => p_pk);
	
	INSERT INTO CONCEPT_SCHEME
           (CON_SCH_ID, IS_PARTIAL)
     VALUES
           (p_pk, p_is_partial);
END;
/

CREATE OR REPLACE PROCEDURE INSERT_AGENCY(
	p_id IN item.id%type,
	p_ag_sch_id IN AGENCY.AG_SCH_ID%type,
	p_pk OUT item.item_id%type)
AS
BEGIN

		insert into ITEM (ITEM_ID, ID) VALUES (ITEM_SEQ.nextval, p_id);
		select ITEM_SEQ.currval into p_pk from dual;
		insert into AGENCY (AG_ID, AG_SCH_ID) VALUES (p_pk, p_ag_sch_id);
END;
/

CREATE OR REPLACE PROCEDURE INSERT_DATACONSUMER(
	p_id IN item.id%type,
	p_datacons_sch_id IN DATACONSUMER.DC_SCH_ID%type,
	p_pk OUT item.item_id%type)
AS
BEGIN

		insert into ITEM (ITEM_ID, ID) VALUES (ITEM_SEQ.nextval, p_id);
		select ITEM_SEQ.currval into p_pk from dual;
		insert into DATACONSUMER (DC_ID, DC_SCH_ID) VALUES (p_pk, p_datacons_sch_id);
END;
/

CREATE OR REPLACE PROCEDURE INSERT_DATAPROVIDER(
	p_id IN item.id%type,
	p_dataprov_sch_id IN DATAPROVIDER.DP_SCH_ID%type,
	p_pk OUT item.item_id%type)
AS
BEGIN

		insert into ITEM (ITEM_ID, ID) VALUES (ITEM_SEQ.nextval, p_id);
		select ITEM_SEQ.currval into p_pk from dual;
		insert into DATAPROVIDER (DP_ID, DP_SCH_ID) VALUES (p_pk, p_dataprov_sch_id);
END;
/

CREATE OR REPLACE PROCEDURE INSERT_ORGANISATION_UNIT(
	p_id IN item.id%type,
	p_org_unit_sch_id IN ORGANISATION_UNIT_SCHEME.ORG_UNIT_SCH_ID%type,
	p_parent_org_unit_id IN ORGANISATION_UNIT.PARENT_ORG_UNIT_ID%type,
	p_pk OUT item.item_id%type)
AS
BEGIN

		insert into ITEM (ITEM_ID, ID) VALUES (ITEM_SEQ.nextval, p_id);
		select ITEM_SEQ.currval into p_pk from dual;
		insert into ORGANISATION_UNIT (ORG_UNIT_ID, ORG_UNIT_SCH_ID,PARENT_ORG_UNIT_ID) VALUES (p_pk, p_org_unit_sch_id,p_parent_org_unit_id);
END;
/

CREATE OR REPLACE PROCEDURE INSERT_AGENCY_SCHEME(
	p_id IN varchar2,
	p_version IN varchar2,
	p_agency IN varchar2,
	p_valid_from IN  timestamp DEFAULT NULL,
	p_valid_to IN  timestamp DEFAULT NULL,
	p_is_final IN number DEFAULT NULL,
	p_uri IN nvarchar2,
	p_last_modified IN timestamp DEFAULT NULL,
    p_is_partial IN number default 0,
	p_pk OUT NUMBER)
AS
BEGIN
	INSERT_ARTEFACT(p_id => p_id,p_version => p_version,p_agency => p_agency,p_valid_from => p_valid_from,p_valid_to => p_valid_to,p_is_final => p_is_final,p_uri => p_uri, p_last_modified => p_last_modified,p_pk => p_pk);
	
	INSERT INTO AGENCY_SCHEME
           (AG_SCH_ID, IS_PARTIAL)
     VALUES
           (p_pk, p_is_partial);
END;
/

CREATE OR REPLACE PROCEDURE INSERT_DATACONSUMER_SCHEME(
	p_id IN varchar2,
	p_version IN varchar2,
	p_agency IN varchar2,
	p_valid_from IN  timestamp DEFAULT NULL,
	p_valid_to IN  timestamp DEFAULT NULL,
	p_is_final IN number DEFAULT NULL,
	p_uri IN nvarchar2,
	p_last_modified IN timestamp DEFAULT NULL,
    p_is_partial IN number default 0,
	p_pk OUT NUMBER)
AS
BEGIN
	INSERT_ARTEFACT(p_id => p_id,p_version => p_version,p_agency => p_agency,p_valid_from => p_valid_from,p_valid_to => p_valid_to,p_is_final => p_is_final,p_uri => p_uri, p_last_modified => p_last_modified,p_pk => p_pk);
	
	INSERT INTO DATACONSUMER_SCHEME
           (DC_SCH_ID, IS_PARTIAL)
     VALUES
           (p_pk, p_is_partial);
END;
/

CREATE OR REPLACE PROCEDURE INSERT_DATAPROVIDER_SCHEME(
	p_id IN varchar2,
	p_version IN varchar2,
	p_agency IN varchar2,
	p_valid_from IN  timestamp DEFAULT NULL,
	p_valid_to IN  timestamp DEFAULT NULL,
	p_is_final IN number DEFAULT NULL,
	p_uri IN nvarchar2,
	p_last_modified IN timestamp DEFAULT NULL,
    p_is_partial IN number default 0,
	p_pk OUT NUMBER)
AS
BEGIN
	INSERT_ARTEFACT(p_id => p_id,p_version => p_version,p_agency => p_agency,p_valid_from => p_valid_from,p_valid_to => p_valid_to,p_is_final => p_is_final,p_uri => p_uri, p_last_modified => p_last_modified,p_pk => p_pk);
	
	INSERT INTO DATAPROVIDER_SCHEME
           (DP_SCH_ID, IS_PARTIAL)
     VALUES
           (p_pk, p_is_partial);
END;
/

CREATE OR REPLACE PROCEDURE INSERT_ORGANISATION_UNIT_SCH(
	p_id IN varchar2,
	p_version IN varchar2,
	p_agency IN varchar2,
	p_valid_from IN  timestamp DEFAULT NULL,
	p_valid_to IN  timestamp DEFAULT NULL,
	p_is_final IN number DEFAULT NULL,
	p_uri IN nvarchar2,
	p_last_modified IN timestamp DEFAULT NULL,
    p_is_partial IN number default 0,
	p_pk OUT NUMBER)
AS
BEGIN
	INSERT_ARTEFACT(p_id => p_id,p_version => p_version,p_agency => p_agency,p_valid_from => p_valid_from,p_valid_to => p_valid_to,p_is_final => p_is_final,p_uri => p_uri, p_last_modified => p_last_modified,p_pk => p_pk);
	
	INSERT INTO ORGANISATION_UNIT_SCHEME
           (ORG_UNIT_SCH_ID, IS_PARTIAL)
     VALUES
           (p_pk, p_is_partial);
END;
/

CREATE OR REPLACE PROCEDURE INSERT_STRUCTURE_SET(
	p_id IN varchar2,
	p_version IN varchar2,
	p_agency IN varchar2,
	p_valid_from IN  timestamp DEFAULT NULL,
	p_valid_to IN  timestamp DEFAULT NULL,
	p_is_final IN number DEFAULT NULL,
	p_uri IN nvarchar2,
	p_last_modified IN timestamp DEFAULT NULL,
	p_pk OUT NUMBER)
AS
BEGIN
	INSERT_ARTEFACT(p_id => p_id,p_version => p_version,p_agency => p_agency,p_valid_from => p_valid_from,p_valid_to => p_valid_to,p_is_final => p_is_final,p_uri => p_uri, p_last_modified => p_last_modified,p_pk => p_pk);
	
	INSERT INTO STRUCTURE_SET
           (SS_ID)
     VALUES
           (p_pk);
END;
/

CREATE OR REPLACE PROCEDURE INSERT_CONCEPT(
	p_id IN item.id%type,
	p_con_sch_id IN CONCEPT.CON_SCH_ID%type,
	p_parent_con_id IN CONCEPT.PARENT_CONCEPT_ID%type,
	p_pk OUT item.item_id%type)
AS
BEGIN

		insert into ITEM (ITEM_ID, ID) VALUES (ITEM_SEQ.nextval, p_id);
		select ITEM_SEQ.currval into p_pk from dual;
		insert into CONCEPT (CON_ID, CON_SCH_ID,PARENT_CONCEPT_ID) VALUES (p_pk, p_con_sch_id,p_parent_con_id);
END;
/
CREATE OR REPLACE PROCEDURE INSERT_CODELIST_MAP(
	p_id IN ITEM.ID%type,
	p_ss_id IN STRUCTURE_SET.SS_ID%type,
	p_source_cl_id IN CODELIST_MAP.SOURCE_CL_ID%type,
	p_target_cl_id IN CODELIST_MAP.TARGET_CL_ID%type,
	p_pk OUT ITEM.ITEM_ID%type)
AS
BEGIN

		insert into ITEM (ITEM_ID, ID) VALUES (ITEM_SEQ.nextval, p_id);
		select ITEM_SEQ.currval into p_pk from dual;
		insert into CODELIST_MAP (CLM_ID, SS_ID, SOURCE_CL_ID, TARGET_CL_ID) VALUES (p_pk, p_ss_id ,p_source_cl_id, p_target_cl_id);
END;
/

CREATE OR REPLACE PROCEDURE INSERT_STRUCTURE_MAP(
	p_id IN ITEM.ID%type,
	p_ss_id IN STRUCTURE_SET.SS_ID%type,
	p_source_str_id IN STRUCTURE_MAP.SOURCE_STR_ID%type,
	p_target_str_id IN STRUCTURE_MAP.TARGET_STR_ID%type,
	p_pk OUT ITEM.ITEM_ID%type)
AS
BEGIN

		insert into ITEM (ITEM_ID, ID) VALUES (ITEM_SEQ.nextval, p_id);
		select ITEM_SEQ.currval into p_pk from dual;
		insert into STRUCTURE_MAP (SM_ID, SS_ID, SOURCE_STR_ID, TARGET_STR_ID) VALUES (p_pk, p_ss_id ,p_source_str_id, p_target_str_id);
END;
/

CREATE OR REPLACE PROCEDURE INSERT_CODE_MAP(
	p_clm_id IN NUMBER,
	p_source_lcd_id IN NUMBER,
	p_target_lcd_id IN NUMBER,
	p_pk OUT NUMBER) 
AS
BEGIN
		insert into CODE_MAP (CLM_ID, SOURCE_LCD_ID, TARGET_LCD_ID) VALUES (p_clm_id, p_source_lcd_id, p_target_lcd_id);
		select CODE_MAP_SEQ.currval into p_pk from dual;
END;
/
CREATE OR REPLACE PROCEDURE INSERT_COMPONENT_MAP(
	p_sm_id IN NUMBER,
	p_source_comp_id IN NUMBER,
	p_target_comp_id IN NUMBER,
	p_pk OUT NUMBER) 
AS
BEGIN
		insert into COMPONENT_MAP (SM_ID, SOURCE_COMP_ID, TARGET_COMP_ID) VALUES (p_sm_id, p_source_comp_id, p_target_comp_id);
		select COMPONENT_MAP_SEQ.currval into p_pk from dual;
END;
/

CREATE OR REPLACE PROCEDURE INSERT_ANNOTATION 
(
	p_id IN NVARCHAR2,
	p_title IN NVARCHAR2,
	p_type IN NVARCHAR2,
	p_url IN NVARCHAR2,
	p_pk OUT NUMBER) 
AS
BEGIN
	insert into ANNOTATION (ID, TITLE, TYPE, URL) VALUES (p_id, p_title, p_type, p_url);
	select ANNOTATION_SEQ.currval into p_pk from dual;
END;
/
CREATE OR REPLACE PROCEDURE INSERT_ANNOTATION_TEXT 
(
	p_ann_id IN NUMBER,
	p_language IN VARCHAR2,
	p_text IN NVARCHAR2,
	p_pk OUT NUMBER) 
AS
BEGIN
	insert into ANNOTATION_TEXT (ANN_ID, LANGUAGE, TEXT) VALUES (p_ann_id, p_language, p_text);
	select ANNOTATION_TEXT_SEQ.currval into p_pk from dual;
END;
/
CREATE OR REPLACE PROCEDURE INSERT_ARTEFACT_ANNOTATION 
(
	p_art_id IN NUMBER,
	p_id IN NVARCHAR2,
	p_title IN NVARCHAR2,
	p_type IN NVARCHAR2,
	p_url IN NVARCHAR2,
	p_pk OUT NUMBER) 
AS
BEGIN
	INSERT_ANNOTATION (p_id => p_id, p_title => p_title, p_type => p_type, p_url => p_url, p_pk => p_pk);
	insert into ARTEFACT_ANNOTATION (ANN_ID, ART_ID) VALUES (p_pk, p_art_id);
END;
/
CREATE OR REPLACE PROCEDURE INSERT_ITEM_ANNOTATION 
(
	p_item_id IN NUMBER,
	p_id IN NVARCHAR2,
	p_title IN NVARCHAR2,
	p_type IN NVARCHAR2,
	p_url IN NVARCHAR2,
	p_pk OUT NUMBER) 
AS
BEGIN
	INSERT_ANNOTATION (p_id => p_id, p_title => p_title, p_type => p_type, p_url => p_url, p_pk => p_pk);
	insert into ITEM_ANNOTATION (ANN_ID, ITEM_ID) VALUES (p_pk, p_item_id);
END;
/
CREATE OR REPLACE PROCEDURE INSERT_COMPONENT_ANNOTATION 
(
	p_comp_id IN NUMBER,
	p_id IN NVARCHAR2,
	p_title IN NVARCHAR2,
	p_type IN NVARCHAR2,
	p_url IN NVARCHAR2,
	p_pk OUT NUMBER) 
AS
BEGIN
	INSERT_ANNOTATION (p_id => p_id, p_title => p_title, p_type => p_type, p_url => p_url, p_pk => p_pk);
	insert into COMPONENT_ANNOTATION (ANN_ID, COMP_ID) VALUES (p_pk, p_comp_id);
END;
/
CREATE OR REPLACE PROCEDURE INSERT_GROUP_ANNOTATION 
(
	p_gr_id IN NUMBER,
	p_id IN NVARCHAR2,
	p_title IN NVARCHAR2,
	p_type IN NVARCHAR2,
	p_url IN NVARCHAR2,
	p_pk OUT NUMBER) 
AS
BEGIN
	INSERT_ANNOTATION (p_id => p_id, p_title => p_title, p_type => p_type, p_url => p_url, p_pk => p_pk);
	insert into GROUP_ANNOTATION (ANN_ID, GR_ID) VALUES (p_pk, p_gr_id);
END;
/


-- always last
update DB_VERSION SET VERSION = '3.2'
;
