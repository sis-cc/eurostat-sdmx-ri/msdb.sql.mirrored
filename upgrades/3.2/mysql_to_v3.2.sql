﻿-- Upgrade script to 3.2 from 3.1.

ALTER TABLE ARTEFACT ADD COLUMN URI NVARCHAR(255) NULL, ADD COLUMN LAST_MODIFIED DATETIME NULL
;

ALTER TABLE CONCEPT ADD COLUMN PARENT_CONCEPT_ID BIGINT NULL
;

CREATE TABLE AGENCY_SCHEME
(
	AG_SCH_ID BIGINT NOT NULL,
    IS_PARTIAL BOOL NOT NULL DEFAULT FALSE,
	PRIMARY KEY (AG_SCH_ID),
	KEY (AG_SCH_ID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
;


CREATE TABLE AGENCY
(
	AG_ID BIGINT NOT NULL,
	AG_SCH_ID BIGINT NOT NULL,
	PRIMARY KEY (AG_ID),
	KEY (AG_ID),
	KEY (AG_SCH_ID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
;


CREATE TABLE DATACONSUMER_SCHEME
(
	DC_SCH_ID BIGINT NOT NULL,
    IS_PARTIAL BOOL NOT NULL DEFAULT FALSE,
	PRIMARY KEY (DC_SCH_ID),
	KEY (DC_SCH_ID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
;


CREATE TABLE DATACONSUMER
(
	DC_ID BIGINT NOT NULL,
	DC_SCH_ID BIGINT NOT NULL,
	PRIMARY KEY (DC_ID),
	KEY (DC_ID),
	KEY (DC_SCH_ID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
;


CREATE TABLE DATAPROVIDER_SCHEME
(
	DP_SCH_ID BIGINT NOT NULL,
    IS_PARTIAL BOOL NOT NULL DEFAULT FALSE,
	PRIMARY KEY (DP_SCH_ID),
	KEY (DP_SCH_ID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
;


CREATE TABLE DATAPROVIDER
(
	DP_ID BIGINT NOT NULL,
	DP_SCH_ID BIGINT NOT NULL,
	PRIMARY KEY (DP_ID),
	KEY (DP_ID),
	KEY (DP_SCH_ID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
;


CREATE TABLE ORGANISATION_UNIT_SCHEME
(
	ORG_UNIT_SCH_ID BIGINT NOT NULL,
    IS_PARTIAL BOOL NOT NULL DEFAULT FALSE,
	PRIMARY KEY (ORG_UNIT_SCH_ID),
	KEY (ORG_UNIT_SCH_ID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
;


CREATE TABLE ORGANISATION_UNIT
(
	ORG_UNIT_ID BIGINT NOT NULL,
	ORG_UNIT_SCH_ID BIGINT NOT NULL,
    PARENT_ORG_UNIT_ID BIGINT NULL,
	PRIMARY KEY (ORG_UNIT_ID),
	KEY (ORG_UNIT_ID),
	KEY (ORG_UNIT_SCH_ID),
	KEY (PARENT_ORG_UNIT_ID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
;

CREATE TABLE STRUCTURE_SET
(
	SS_ID BIGINT NOT NULL,
    PRIMARY KEY (SS_ID),
	KEY (SS_ID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
;
CREATE TABLE CODELIST_MAP
(
	CLM_ID bigint NOT NULL,
	SS_ID bigint NOT NULL,
	SOURCE_CL_ID bigint NOT NULL,
	TARGET_CL_ID bigint NOT NULL,
	PRIMARY KEY (CLM_ID),
	KEY (SS_ID),
	KEY (SOURCE_CL_ID),
	KEY (TARGET_CL_ID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
;
CREATE TABLE CODE_MAP
(
	DCM_ID BIGINT AUTO_INCREMENT NOT NULL,
	CLM_ID BIGINT NOT NULL,
	SOURCE_LCD_ID BIGINT NOT NULL,
	TARGET_LCD_ID BIGINT NOT NULL,	
	PRIMARY KEY(DCM_ID),
	KEY(CLM_ID),
	KEY(SOURCE_LCD_ID),
	KEY(TARGET_LCD_ID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
;
CREATE TABLE STRUCTURE_MAP
(
	SM_ID bigint NOT NULL,
	SS_ID bigint NOT NULL,
	SOURCE_STR_ID bigint NOT NULL,
	TARGET_STR_ID bigint NOT NULL,
	PRIMARY KEY(SM_ID),
	KEY(SS_ID),
	KEY(SOURCE_STR_ID),
	KEY(TARGET_STR_ID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
;
CREATE TABLE COMPONENT_MAP
(
	CM_ID BIGINT NOT NULL AUTO_INCREMENT,
	SM_ID BIGINT NOT NULL,
	SOURCE_COMP_ID BIGINT NOT NULL,
	TARGET_COMP_ID BIGINT NOT NULL,
	PRIMARY KEY(CM_ID),
	KEY(SM_ID),
	KEY (SOURCE_COMP_ID),
	KEY (TARGET_COMP_ID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
;
CREATE TABLE ANNOTATION
(
	ANN_ID bigint AUTO_INCREMENT NOT NULL, 
	ID nvarchar(50) NULL,
	TITLE nvarchar(70) NULL,
	TYPE nvarchar(50) NULL,
	URL nvarchar(1000) NULL,
	PRIMARY KEY(ANN_ID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
;
CREATE TABLE ANNOTATION_TEXT
(
	ANN_TEXT_ID bigint AUTO_INCREMENT NOT NULL , 
	ANN_ID bigint NOT NULL ,
	LANGUAGE varchar(50) NOT NULL,
	TEXT nvarchar(4000) NOT NULL,
	PRIMARY KEY(ANN_TEXT_ID),
	KEY(ANN_ID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
;
CREATE TABLE ARTEFACT_ANNOTATION
(
	ANN_ID bigint NOT NULL, 
	ART_ID bigint NOT NULL,
	PRIMARY KEY(ANN_ID, ART_ID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
;
CREATE TABLE ITEM_ANNOTATION
(
	ANN_ID bigint NOT NULL, 
	ITEM_ID bigint NOT NULL,
	PRIMARY KEY(ANN_ID, ITEM_ID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
;
CREATE TABLE COMPONENT_ANNOTATION
(
	ANN_ID bigint NOT NULL, 
	COMP_ID bigint NOT NULL,
	PRIMARY KEY(ANN_ID, COMP_ID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
;
CREATE TABLE GROUP_ANNOTATION
(
	ANN_ID bigint NOT NULL, 
	GR_ID bigint NOT NULL,
	PRIMARY KEY(ANN_ID, GR_ID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
;


ALTER TABLE AGENCY_SCHEME ADD CONSTRAINT FK_AGENCY_SCHEME_ARTEFACT
	FOREIGN KEY (AG_SCH_ID) REFERENCES ARTEFACT (ART_ID)
	ON DELETE CASCADE
;

ALTER TABLE AGENCY ADD CONSTRAINT FK_AGENCY_ID 
	FOREIGN KEY (AG_ID) REFERENCES ITEM (ITEM_ID)
	ON DELETE CASCADE
;

ALTER TABLE AGENCY ADD CONSTRAINT FK_AGENCY_AGENCY_SCHEME 
	FOREIGN KEY (AG_SCH_ID) REFERENCES AGENCY_SCHEME (AG_SCH_ID)
	ON DELETE CASCADE
;

ALTER TABLE DATACONSUMER_SCHEME ADD CONSTRAINT FK_DATACONSUMER_SCHEME_ART_ID 
	FOREIGN KEY (DC_SCH_ID) REFERENCES ARTEFACT (ART_ID)
	ON DELETE CASCADE
;

ALTER TABLE DATACONSUMER ADD CONSTRAINT FK_DATACONSUMER_ID 
	FOREIGN KEY (DC_ID) REFERENCES ITEM (ITEM_ID)
	ON DELETE CASCADE
;

ALTER TABLE DATACONSUMER ADD CONSTRAINT FK_DATACONSUMER_DC_SCH_ID 
	FOREIGN KEY (DC_SCH_ID) REFERENCES DATACONSUMER_SCHEME (DC_SCH_ID)
	ON DELETE CASCADE
;

ALTER TABLE DATAPROVIDER_SCHEME ADD CONSTRAINT FK_DATAPROVIDER_SCHEME_ART_ID 
	FOREIGN KEY (DP_SCH_ID) REFERENCES ARTEFACT (ART_ID)
	ON DELETE CASCADE
;

ALTER TABLE DATAPROVIDER ADD CONSTRAINT FK_DATAPROVIDER_ID 
	FOREIGN KEY (DP_ID) REFERENCES ITEM (ITEM_ID)
	ON DELETE CASCADE
;

ALTER TABLE DATAPROVIDER ADD CONSTRAINT FK_DATAPROVIDER_DP_SCH_ID 
	FOREIGN KEY (DP_SCH_ID) REFERENCES DATAPROVIDER_SCHEME (DP_SCH_ID)
	ON DELETE CASCADE
;

ALTER TABLE ORGANISATION_UNIT_SCHEME ADD CONSTRAINT FK_ORGANISATION_UNIT_SCH_ART 
	FOREIGN KEY (ORG_UNIT_SCH_ID) REFERENCES ARTEFACT (ART_ID)
	ON DELETE CASCADE
;

ALTER TABLE ORGANISATION_UNIT ADD CONSTRAINT FK_ORGANISATION_UNIT_ID 
	FOREIGN KEY (ORG_UNIT_ID) REFERENCES ITEM (ITEM_ID)
	ON DELETE CASCADE
;

ALTER TABLE ORGANISATION_UNIT ADD CONSTRAINT FK_ORGANISATION_UNIT_SCH_ID 
	FOREIGN KEY (ORG_UNIT_SCH_ID) REFERENCES ORGANISATION_UNIT_SCHEME (ORG_UNIT_SCH_ID)
	ON DELETE CASCADE
;

ALTER TABLE ORGANISATION_UNIT ADD CONSTRAINT FK_ORGANISATION_UNIT_ITEM 
	FOREIGN KEY (PARENT_ORG_UNIT_ID) REFERENCES ORGANISATION_UNIT (ORG_UNIT_ID)
	ON DELETE SET NULL
;

ALTER TABLE STRUCTURE_SET ADD CONSTRAINT FK_STRUCTURE_SET_ARTEFACT
	FOREIGN KEY (SS_ID) REFERENCES ARTEFACT (ART_ID)
	ON DELETE CASCADE
;

ALTER TABLE CONCEPT ADD CONSTRAINT FK_CONCEPT_ITEM 
	FOREIGN KEY (PARENT_CONCEPT_ID) REFERENCES CONCEPT (CON_ID)
	ON DELETE SET NULL
;

ALTER TABLE CODELIST_MAP ADD CONSTRAINT FK_CL_MAP_ITEM FOREIGN KEY(CLM_ID) REFERENCES ITEM(ITEM_ID) ON DELETE CASCADE
;
ALTER TABLE CODELIST_MAP ADD CONSTRAINT FK_CL_MAP_SS FOREIGN KEY(SS_ID) REFERENCES STRUCTURE_SET(SS_ID) ON DELETE CASCADE
;
ALTER TABLE CODELIST_MAP ADD CONSTRAINT FK_CL_MAP_SOURCE FOREIGN KEY(SOURCE_CL_ID) REFERENCES CODELIST(CL_ID)
;
ALTER TABLE CODELIST_MAP ADD CONSTRAINT FK_CL_MAP_TARGET FOREIGN KEY(TARGET_CL_ID) REFERENCES CODELIST(CL_ID)
;

ALTER TABLE CODE_MAP ADD CONSTRAINT FK_CD_MAP_CLM FOREIGN KEY(CLM_ID) REFERENCES CODELIST_MAP(CLM_ID) ON DELETE CASCADE
;
ALTER TABLE CODE_MAP ADD CONSTRAINT FK_CD_MAP_SOURCE FOREIGN KEY(SOURCE_LCD_ID) REFERENCES DSD_CODE(LCD_ID)
;
ALTER TABLE CODE_MAP ADD CONSTRAINT FK_CD_MAP_TARGET FOREIGN KEY(TARGET_LCD_ID) REFERENCES DSD_CODE(LCD_ID)
;

ALTER TABLE STRUCTURE_MAP ADD CONSTRAINT FK_STR_MAP_ITEM FOREIGN KEY(SM_ID) REFERENCES ITEM(ITEM_ID) ON DELETE CASCADE
;
ALTER TABLE STRUCTURE_MAP ADD CONSTRAINT FK_STR_MAP_SS FOREIGN KEY(SS_ID) REFERENCES STRUCTURE_SET(SS_ID) ON DELETE CASCADE
;
ALTER TABLE STRUCTURE_MAP ADD CONSTRAINT FK_STR_MAP_SOURCE FOREIGN KEY(SOURCE_STR_ID) REFERENCES ARTEFACT(ART_ID)
;
ALTER TABLE STRUCTURE_MAP ADD CONSTRAINT FK_STR_MAP_TARGET FOREIGN KEY(TARGET_STR_ID) REFERENCES ARTEFACT(ART_ID)
;

ALTER TABLE COMPONENT_MAP ADD CONSTRAINT FK_C_MAP_SM FOREIGN KEY(SM_ID) REFERENCES STRUCTURE_MAP(SM_ID) ON DELETE CASCADE
;
ALTER TABLE COMPONENT_MAP ADD CONSTRAINT FK_C_MAP_SOURCE FOREIGN KEY(SOURCE_COMP_ID) REFERENCES COMPONENT(COMP_ID)
;
ALTER TABLE COMPONENT_MAP ADD CONSTRAINT FK_C_MAP_TARGET FOREIGN KEY(TARGET_COMP_ID) REFERENCES COMPONENT(COMP_ID)
;

ALTER TABLE ANNOTATION_TEXT ADD CONSTRAINT FK_ANN_TEXT FOREIGN KEY(ANN_ID) REFERENCES ANNOTATION(ANN_ID) ON DELETE CASCADE
;

ALTER TABLE ARTEFACT_ANNOTATION ADD CONSTRAINT FK_ART_ANN_ANN FOREIGN KEY(ANN_ID) REFERENCES ANNOTATION(ANN_ID) ON DELETE CASCADE
;
ALTER TABLE ARTEFACT_ANNOTATION ADD CONSTRAINT FK_ART_ANN_ART FOREIGN KEY(ART_ID) REFERENCES ARTEFACT(ART_ID) ON DELETE CASCADE
;
ALTER TABLE ITEM_ANNOTATION ADD CONSTRAINT FK_ITEM_ANN_ANN FOREIGN KEY(ANN_ID) REFERENCES ANNOTATION(ANN_ID) ON DELETE CASCADE
;
ALTER TABLE ITEM_ANNOTATION ADD CONSTRAINT FK_ITEM_ANN_ITEM FOREIGN KEY(ITEM_ID) REFERENCES ITEM(ITEM_ID) ON DELETE CASCADE
;
ALTER TABLE COMPONENT_ANNOTATION ADD CONSTRAINT FK_COMP_ANN_ANN FOREIGN KEY(ANN_ID) REFERENCES ANNOTATION(ANN_ID) ON DELETE CASCADE
;
ALTER TABLE COMPONENT_ANNOTATION ADD CONSTRAINT FK_COMP_ANN_COMP FOREIGN KEY(COMP_ID) REFERENCES COMPONENT(COMP_ID) ON DELETE CASCADE
;
ALTER TABLE GROUP_ANNOTATION ADD CONSTRAINT FK_GR_ANN_ANN FOREIGN KEY(ANN_ID) REFERENCES ANNOTATION(ANN_ID) ON DELETE CASCADE
;
ALTER TABLE GROUP_ANNOTATION ADD CONSTRAINT FK_GR_ANN_GR FOREIGN KEY(GR_ID) REFERENCES DSD_GROUP(GR_ID) ON DELETE CASCADE
;


DROP PROCEDURE IF EXISTS INSERT_ARTEFACT
;

DROP PROCEDURE IF EXISTS INSERT_HCL_CODE
;

DROP PROCEDURE IF EXISTS INSERT_HCL
;

DROP PROCEDURE IF EXISTS INSERT_HIERACHY
;

DROP PROCEDURE IF EXISTS INSERT_HLEVEL
;

DROP PROCEDURE IF EXISTS INSERT_DSD
;

DROP PROCEDURE IF EXISTS INSERT_DATAFLOW
;

DROP PROCEDURE IF EXISTS INSERT_CATEGORISATION
;

DROP PROCEDURE IF EXISTS INSERT_CATEGORY_SCHEME
;

DROP PROCEDURE IF EXISTS INSERT_CODELIST
;

DROP PROCEDURE IF EXISTS INSERT_CONCEPT_SCHEME
;

DROP PROCEDURE IF EXISTS INSERT_CONCEPT
;

-- DELIMITER $$

CREATE PROCEDURE INSERT_ARTEFACT(
	IN p_id varchar(50),
	IN p_version varchar(50),
	IN p_agency varchar(50),
	IN p_valid_from datetime,
	IN p_valid_to datetime,
	IN p_is_final int,
	IN p_uri nvarchar(255),
	IN p_last_modified datetime,
	OUT p_pk bigint)
BEGIN
    declare ver1 bigint;
    declare ver2 bigint;
    declare ver3 bigint;
    CALL SPLIT_VERSION(p_version, ver1, ver2, ver3);
	insert into ARTEFACT (ID,VERSION1, VERSION2, VERSION3, AGENCY, VALID_FROM, VALID_TO, IS_FINAL, URI, LAST_MODIFIED) values (p_id, ver1, ver2, ver3, p_agency, p_valid_from, p_valid_to, p_is_final, p_uri, p_last_modified);
	set p_pk = LAST_INSERT_ID();
END
;

CREATE PROCEDURE INSERT_HCL_CODE(
	IN p_id varchar(50),
	IN p_version varchar(50),
	IN p_agency varchar(50),
	IN p_valid_from datetime,
	IN p_valid_to datetime,
	IN p_is_final int,
	IN p_uri nvarchar(255),
	IN p_last_modified datetime,
	IN p_parent_hcode_id bigint,
	IN p_lcd_id bigint,
	IN p_h_id bigint,
	IN p_level_id bigint,
	OUT p_pk bigint)
BEGIN
	call INSERT_ARTEFACT (p_id,p_version,p_agency,p_valid_from,p_valid_to,p_is_final,p_uri,p_last_modified, p_pk);
	
	INSERT INTO HCL_CODE
           (HCODE_ID
           ,PARENT_HCODE_ID
           ,LCD_ID
           ,H_ID
           ,LEVEL_ID)
	VALUES
		(p_pk
        ,p_parent_hcode_id
        ,p_lcd_id
        ,p_h_id
         ,p_level_id);
END
;


CREATE PROCEDURE INSERT_HCL(
	IN p_id varchar(50),
	IN p_version varchar(50),
	IN p_agency varchar(50),
	IN p_valid_from datetime,
	IN p_valid_to datetime,
	IN p_is_final int,
	IN p_uri nvarchar(255),
	IN p_last_modified datetime,
	OUT p_pk bigint)
BEGIN
	call INSERT_ARTEFACT (p_id,p_version,p_agency,p_valid_from,p_valid_to,p_is_final,p_uri,p_last_modified, p_pk);
	insert into HCL (HCL_ID) values(p_pk);
END
;

CREATE PROCEDURE INSERT_HIERACHY(
	IN p_id varchar(50),
	IN p_version varchar(50),
	IN p_agency varchar(50),
	IN p_valid_from datetime,
	IN p_valid_to datetime,
	IN p_is_final int,
	IN p_uri nvarchar(255),
	IN p_last_modified datetime,
	IN p_hcl_id bigint,
	OUT p_pk bigint)
BEGIN
	call INSERT_ARTEFACT (p_id,p_version,p_agency,p_valid_from,p_valid_to,p_is_final,p_uri,p_last_modified, p_pk);
	
	INSERT INTO HIERARCHY
           (H_ID
           ,HCL_ID)
     VALUES
           (p_pk
           ,p_hcl_id);
END
;

CREATE PROCEDURE INSERT_HLEVEL(
	IN p_id varchar(50),
	IN p_version varchar(50),
	IN p_agency varchar(50),
	IN p_valid_from datetime,
	IN p_valid_to datetime,
	IN p_is_final int,
	IN p_uri nvarchar(255),
	IN p_last_modified datetime,
	IN p_h_id bigint,
	IN p_parent_level_id bigint,
	OUT p_pk bigint)
BEGIN
	call INSERT_ARTEFACT (p_id,p_version,p_agency,p_valid_from,p_valid_to,p_is_final,p_uri,p_last_modified, p_pk);
	
	INSERT INTO HLEVEL
           (LEVEL_ID
           ,H_ID
           ,PARENT_LEVEL_ID)
     VALUES
           (p_pk
           ,p_h_id
           ,p_parent_level_id);
END
;

CREATE PROCEDURE INSERT_DSD(
	IN p_id varchar(50),
	IN p_version varchar(50),
	IN p_agency varchar(50),
	IN p_valid_from datetime,
	IN p_valid_to datetime,
	IN p_is_final int,
	IN p_uri nvarchar(255),
	IN p_last_modified datetime,
	OUT p_pk bigint)
BEGIN
	call INSERT_ARTEFACT (p_id,p_version,p_agency,p_valid_from,p_valid_to,p_is_final,p_uri,p_last_modified, p_pk);
	
	INSERT INTO DSD
           (DSD_ID)
     VALUES
           (p_pk);
END
;

CREATE PROCEDURE INSERT_DATAFLOW(
	IN p_id varchar(50),
	IN p_version varchar(50),
	IN p_agency varchar(50),
	IN p_valid_from datetime,
	IN p_valid_to datetime,
	IN p_is_final int,
	IN p_uri nvarchar(255),
	IN p_last_modified datetime,
	IN p_dsd_id bigint,
	IN p_map_set_id bigint,
	OUT p_pk bigint)
BEGIN
	call INSERT_ARTEFACT (p_id,p_version,p_agency,p_valid_from,p_valid_to,p_is_final,p_uri,p_last_modified, p_pk);
	
INSERT INTO DATAFLOW
           (DF_ID
           ,DSD_ID
           ,MAP_SET_ID)
     VALUES
           (p_pk
           ,p_dsd_id
           ,p_map_set_id);
END
;

CREATE PROCEDURE INSERT_CATEGORISATION(
	IN p_id varchar(50),
	IN p_version varchar(50),
	IN p_agency varchar(50),
	IN p_valid_from datetime,
	IN p_valid_to datetime,
	IN p_is_final int,
	IN p_uri nvarchar(255),
	IN p_last_modified datetime,
	IN p_art_id bigint,
	IN p_cat_id bigint,
	IN p_dc_order bigint,
	OUT p_pk bigint)
BEGIN
	call INSERT_ARTEFACT (p_id,p_version,p_agency,p_valid_from,p_valid_to,p_is_final,p_uri,p_last_modified, p_pk);
	
INSERT INTO CATEGORISATION
           (CATN_ID
           ,ART_ID
           ,CAT_ID
           ,DC_ORDER)
     VALUES
           (p_pk
           ,p_art_id
           ,p_cat_id
           ,p_dc_order);
END
;

CREATE PROCEDURE INSERT_CATEGORY_SCHEME(
	IN p_id varchar(50),
	IN p_version varchar(50),
	IN p_agency varchar(50),
	IN p_valid_from datetime,
	IN p_valid_to datetime,
	IN p_is_final int,
	IN p_uri nvarchar(255),
	IN p_last_modified datetime,
	IN p_cs_order bigint,
    IN p_is_partial int,
	OUT p_pk bigint)
BEGIN
	if p_cs_order is null then
		set p_cs_order = 0;
	end if;
	call INSERT_ARTEFACT (p_id,p_version,p_agency,p_valid_from,p_valid_to,p_is_final,p_uri,p_last_modified, p_pk);
	
	INSERT INTO CATEGORY_SCHEME
           (CAT_SCH_ID, CS_ORDER, IS_PARTIAL)
     VALUES
           (p_pk, p_cs_order, p_is_partial);
END
;

CREATE PROCEDURE INSERT_CODELIST(
	IN p_id varchar(50),
	IN p_version varchar(50),
	IN p_agency varchar(50),
	IN p_valid_from datetime,
	IN p_valid_to datetime,
	IN p_is_final int,
	IN p_uri nvarchar(255),
	IN p_last_modified datetime,
    IN p_is_partial int,
	OUT p_pk bigint)
BEGIN
	call INSERT_ARTEFACT (p_id,p_version,p_agency,p_valid_from,p_valid_to,p_is_final,p_uri,p_last_modified, p_pk);
	
	INSERT INTO CODELIST
           (CL_ID, IS_PARTIAL)
     VALUES
           (p_pk, p_is_partial);
END
;

CREATE PROCEDURE INSERT_CONCEPT_SCHEME(
	IN p_id varchar(50),
	IN p_version varchar(50),
	IN p_agency varchar(50),
	IN p_valid_from datetime,
	IN p_valid_to datetime,
	IN p_is_final int,
	IN p_uri nvarchar(255),
	IN p_last_modified datetime,
    IN p_is_partial int,
	OUT p_pk bigint)
BEGIN
	call INSERT_ARTEFACT (p_id,p_version,p_agency,p_valid_from,p_valid_to,p_is_final,p_uri,p_last_modified, p_pk);
	
	INSERT INTO CONCEPT_SCHEME
           (CON_SCH_ID, IS_PARTIAL)
     VALUES
           (p_pk, p_is_partial);
END
;

CREATE PROCEDURE INSERT_AGENCY(
	IN p_id varchar(50),
	IN p_ag_sch_id bigint,
	OUT p_pk bigint) 
BEGIN
		insert into ITEM (ID) VALUES (p_id);
		set p_pk = LAST_INSERT_ID();
		insert into AGENCY (AG_ID, AG_SCH_ID) VALUES (p_pk, p_ag_sch_id);
	
END
;

CREATE PROCEDURE INSERT_DATACONSUMER(
	IN p_id varchar(50),
	IN p_datacons_sch_id bigint,
	OUT p_pk bigint) 
BEGIN
		insert into ITEM (ID) VALUES (p_id);
		set p_pk = LAST_INSERT_ID();
		insert into DATACONSUMER (DC_ID, DC_SCH_ID) VALUES (p_pk, p_datacons_sch_id);
	
END
;

CREATE PROCEDURE INSERT_DATAPROVIDER(
	IN p_id varchar(50),
	IN p_dataprov_sch_id bigint,
	OUT p_pk bigint) 
BEGIN
		insert into ITEM (ID) VALUES (p_id);
		set p_pk = LAST_INSERT_ID();
		insert into DATAPROVIDER (DP_ID, DP_SCH_ID) VALUES (p_pk, p_dataprov_sch_id);
	
END
;

CREATE PROCEDURE INSERT_ORGANISATION_UNIT(
	IN p_id varchar(50),
	IN p_org_unit_sch_id bigint,
	IN p_parent_org_unit_id bigint,
	OUT p_pk bigint) 
BEGIN
		insert into ITEM (ID) VALUES (p_id);
		set p_pk = LAST_INSERT_ID();
		insert into ORGANISATION_UNIT (ORG_UNIT_ID, ORG_UNIT_SCH_ID,PARENT_ORG_UNIT_ID) VALUES (p_pk, p_org_unit_sch_id,p_parent_org_unit_id);
	
END
;

CREATE PROCEDURE INSERT_AGENCY_SCHEME(
	IN p_id varchar(50),
	IN p_version varchar(50),
	IN p_agency varchar(50),
	IN p_valid_from datetime,
	IN p_valid_to datetime,
	IN p_is_final int,
	IN p_uri nvarchar(255),
	IN p_last_modified datetime,
    IN p_is_partial int,
	OUT p_pk bigint)
BEGIN
	call INSERT_ARTEFACT (p_id,p_version,p_agency,p_valid_from,p_valid_to,p_is_final,p_uri,p_last_modified, p_pk);
	
	INSERT INTO AGENCY_SCHEME
           (AG_SCH_ID, IS_PARTIAL)
     VALUES
           (p_pk, p_is_partial);
END
;

CREATE PROCEDURE INSERT_DATACONSUMER_SCHEME(
	IN p_id varchar(50),
	IN p_version varchar(50),
	IN p_agency varchar(50),
	IN p_valid_from datetime,
	IN p_valid_to datetime,
	IN p_is_final int,
	IN p_uri nvarchar(255),
	IN p_last_modified datetime,
    IN p_is_partial int,
	OUT p_pk bigint)
BEGIN
	call INSERT_ARTEFACT (p_id,p_version,p_agency,p_valid_from,p_valid_to,p_is_final,p_uri,p_last_modified, p_pk);
	
	INSERT INTO DATACONSUMER_SCHEME
           (DC_SCH_ID, IS_PARTIAL)
     VALUES
           (p_pk, p_is_partial);
END
;

CREATE PROCEDURE INSERT_DATAPROVIDER_SCHEME(
	IN p_id varchar(50),
	IN p_version varchar(50),
	IN p_agency varchar(50),
	IN p_valid_from datetime,
	IN p_valid_to datetime,
	IN p_is_final int,
	IN p_uri nvarchar(255),
	IN p_last_modified datetime,
    IN p_is_partial int,
	OUT p_pk bigint)
BEGIN
	call INSERT_ARTEFACT (p_id,p_version,p_agency,p_valid_from,p_valid_to,p_is_final,p_uri,p_last_modified, p_pk);
	
	INSERT INTO DATAPROVIDER_SCHEME
           (DP_SCH_ID, IS_PARTIAL)
     VALUES
           (p_pk, p_is_partial);
END
;

CREATE PROCEDURE INSERT_ORGANISATION_UNIT_SCH(
	IN p_id varchar(50),
	IN p_version varchar(50),
	IN p_agency varchar(50),
	IN p_valid_from datetime,
	IN p_valid_to datetime,
	IN p_is_final int,
	IN p_uri nvarchar(255),
	IN p_last_modified datetime,
    IN p_is_partial int,
	OUT p_pk bigint)
BEGIN
	call INSERT_ARTEFACT (p_id,p_version,p_agency,p_valid_from,p_valid_to,p_is_final,p_uri,p_last_modified, p_pk);
	
	INSERT INTO ORGANISATION_UNIT_SCHEME
           (ORG_UNIT_SCH_ID, IS_PARTIAL)
     VALUES
           (p_pk, p_is_partial);
END
;

CREATE PROCEDURE INSERT_STRUCTURE_SET(
	IN p_id varchar(50),
	IN p_version varchar(50),
	IN p_agency varchar(50),
	IN p_valid_from datetime,
	IN p_valid_to datetime,
	IN p_is_final int,
	IN p_uri nvarchar(255),
	IN p_last_modified datetime,
	OUT p_pk bigint)
BEGIN
	call INSERT_ARTEFACT (p_id,p_version,p_agency,p_valid_from,p_valid_to,p_is_final,p_uri,p_last_modified, p_pk);
	
	INSERT INTO STRUCTURE_SET
           (SS_ID)
     VALUES
           (p_pk);
END
;

CREATE PROCEDURE INSERT_CONCEPT(
	IN p_id varchar(50),
	IN p_con_sch_id bigint,
	IN p_parent_con_id bigint,
	OUT p_pk bigint) 
BEGIN
		insert into ITEM (ID) VALUES (p_id);
		set p_pk = LAST_INSERT_ID();
		insert into CONCEPT (CON_ID, CON_SCH_ID,PARENT_CONCEPT_ID) VALUES (p_pk, p_con_sch_id,p_parent_con_id);
	
END
;
CREATE PROCEDURE INSERT_CODELIST_MAP(
	IN p_id varchar(50),
	IN p_ss_id bigint,
	IN p_source_cl_id bigint,
	IN p_target_cl_id bigint,
	OUT p_pk bigint) 
BEGIN
		insert into ITEM (ID) VALUES (p_id);
		set p_pk = LAST_INSERT_ID();
		insert into CODELIST_MAP (CLM_ID, SS_ID, SOURCE_CL_ID, TARGET_CL_ID) VALUES (p_pk, p_ss_id ,p_source_cl_id, p_target_cl_id);
END
;
CREATE PROCEDURE INSERT_CODE_MAP(
	IN p_clm_id bigint,
	IN p_source_lcd_id bigint,
	IN p_target_lcd_id bigint,
	OUT p_pk bigint) 
BEGIN
		insert into CODE_MAP (CLM_ID, SOURCE_LCD_ID, TARGET_LCD_ID) VALUES (p_clm_id, p_source_lcd_id, p_target_lcd_id);
		set p_pk = LAST_INSERT_ID();
END
;
CREATE PROCEDURE INSERT_STRUCTURE_MAP(
	IN p_id varchar(50),
	IN p_ss_id bigint,
	IN p_source_str_id bigint,
	IN p_target_str_id bigint,
	OUT p_pk bigint) 
BEGIN
		insert into ITEM (ID) VALUES (p_id);
		set p_pk = LAST_INSERT_ID();
		insert into STRUCTURE_MAP (SM_ID, SS_ID, SOURCE_STR_ID, TARGET_STR_ID) VALUES (p_pk, p_ss_id ,p_source_str_id, p_target_str_id);
END
;
CREATE PROCEDURE INSERT_COMPONENT_MAP(
	IN p_sm_id bigint,
	IN p_source_comp_id bigint,
	IN p_target_comp_id bigint,
	OUT p_pk bigint) 
BEGIN
		insert into COMPONENT_MAP (SM_ID, SOURCE_COMP_ID, TARGET_COMP_ID) VALUES (p_sm_id, p_source_comp_id, p_target_comp_id);
		set p_pk = LAST_INSERT_ID();
END
;

CREATE PROCEDURE INSERT_ANNOTATION 
(
	IN p_id nvarchar(50),
	IN p_title nvarchar(70),
	IN p_type nvarchar(50),
	IN p_url nvarchar(1000),
	OUT p_pk bigint) 
BEGIN
	insert into ANNOTATION (ID, TITLE, TYPE, URL) VALUES (p_id, p_title, p_type, p_url);
	set p_pk = LAST_INSERT_ID();
END
;
CREATE PROCEDURE INSERT_ANNOTATION_TEXT 
(
	IN p_ann_id bigint,
	IN p_language varchar(50),
	IN p_text nvarchar(4000),
	OUT p_pk bigint) 
BEGIN
	insert into ANNOTATION_TEXT (ANN_ID, LANGUAGE, TEXT) VALUES (p_ann_id, p_language, p_text);
	set p_pk = LAST_INSERT_ID();
END
;
CREATE PROCEDURE INSERT_ARTEFACT_ANNOTATION 
(
	IN p_art_id bigint,
	IN p_id nvarchar(50),
	IN p_title nvarchar(70),
	IN p_type nvarchar(50),
	IN p_url nvarchar(1000),
	OUT p_pk bigint) 
BEGIN
	CALL INSERT_ANNOTATION (p_id, p_title, p_type, p_url, p_pk);
	insert into ARTEFACT_ANNOTATION (ANN_ID, ART_ID) VALUES (p_pk, p_art_id);
END
;
CREATE PROCEDURE INSERT_ITEM_ANNOTATION 
(
	IN p_item_id bigint,
	IN p_id nvarchar(50),
	IN p_title nvarchar(70),
	IN p_type nvarchar(50),
	IN p_url nvarchar(1000),
	OUT p_pk bigint) 
BEGIN
	CALL INSERT_ANNOTATION (p_id, p_title, p_type, p_url, p_pk);
	insert into ITEM_ANNOTATION (ANN_ID, ITEM_ID) VALUES (p_pk, p_item_id);
END
;
CREATE PROCEDURE INSERT_COMPONENT_ANNOTATION 
(
	IN p_comp_id bigint,
	IN p_id nvarchar(50),
	IN p_title nvarchar(70),
	IN p_type nvarchar(50),
	IN p_url nvarchar(1000),
	OUT p_pk bigint) 
BEGIN
	CALL INSERT_ANNOTATION (p_id, p_title, p_type, p_url, p_pk);
	insert into COMPONENT_ANNOTATION (ANN_ID, COMP_ID) VALUES (p_pk, p_comp_id);
END
;
CREATE PROCEDURE INSERT_GROUP_ANNOTATION 
(
	IN p_gr_id bigint,
	IN p_id nvarchar(50),
	IN p_title nvarchar(70),
	IN p_type nvarchar(50),
	IN p_url nvarchar(1000),
	OUT p_pk bigint) 
BEGIN
	CALL INSERT_ANNOTATION (p_id, p_title, p_type, p_url, p_pk);
	insert into GROUP_ANNOTATION (ANN_ID, GR_ID) VALUES (p_pk, p_gr_id);
END
;
SET FOREIGN_KEY_CHECKS=1
;

-- always last
update DB_VERSION SET VERSION = '3.2'
;
