﻿-- Upgrade script to 3.2 from 3.1.

ALTER TABLE ARTEFACT ADD URI NVARCHAR(255) NULL, LAST_MODIFIED DATETIME NULL
;

ALTER TABLE CONCEPT ADD PARENT_CONCEPT_ID BIGINT NULL
;

CREATE TABLE AGENCY_SCHEME ( 
	AG_SCH_ID bigint NOT NULL,
    IS_PARTIAL BIT NOT NULL DEFAULT 0
)
;

CREATE TABLE DATACONSUMER_SCHEME ( 
	DC_SCH_ID bigint NOT NULL,
    IS_PARTIAL BIT NOT NULL DEFAULT 0
)
;

CREATE TABLE DATAPROVIDER_SCHEME ( 
	DP_SCH_ID bigint NOT NULL,
    IS_PARTIAL BIT NOT NULL DEFAULT 0
)
;

CREATE TABLE ORGANISATION_UNIT_SCHEME ( 
	ORG_UNIT_SCH_ID bigint NOT NULL,
    IS_PARTIAL BIT NOT NULL DEFAULT 0
)
;

CREATE TABLE STRUCTURE_SET
    (
     SS_ID bigint NOT NULL
    )
;


CREATE TABLE AGENCY ( 
	AG_ID bigint NOT NULL,
	AG_SCH_ID bigint NOT NULL
)
;

CREATE TABLE DATACONSUMER ( 
	DC_ID bigint NOT NULL,
	DC_SCH_ID bigint NOT NULL
)
;

CREATE TABLE DATAPROVIDER ( 
	DP_ID bigint NOT NULL,
	DP_SCH_ID bigint NOT NULL
)
;

CREATE TABLE ORGANISATION_UNIT ( 
	ORG_UNIT_ID bigint NOT NULL,
	ORG_UNIT_SCH_ID bigint NOT NULL,
	PARENT_ORG_UNIT_ID bigint NULL
)
;

CREATE TABLE CODELIST_MAP
(
	CLM_ID bigint NOT NULL,
	SS_ID bigint NOT NULL,
	SOURCE_CL_ID bigint NOT NULL,
	TARGET_CL_ID bigint NOT NULL
)
;
CREATE TABLE CODE_MAP
(
	DCM_ID BIGINT identity(1,1) NOT NULL,
	CLM_ID BIGINT NOT NULL,
	SOURCE_LCD_ID BIGINT NOT NULL,
	TARGET_LCD_ID BIGINT NOT NULL	
) 
;
CREATE TABLE STRUCTURE_MAP
(
	SM_ID bigint NOT NULL,
	SS_ID bigint NOT NULL,
	SOURCE_STR_ID bigint NOT NULL,
	TARGET_STR_ID bigint NOT NULL
) 
;
CREATE TABLE COMPONENT_MAP
(
	CM_ID BIGINT NOT NULL identity(1,1),
	SM_ID BIGINT NOT NULL,
	SOURCE_COMP_ID BIGINT NOT NULL,
	TARGET_COMP_ID BIGINT NOT NULL
)
;
CREATE TABLE ANNOTATION
(
	ANN_ID bigint identity(1,1) NOT NULL, 
	ID nvarchar(50) NULL,
	TITLE nvarchar(70) NULL,
	TYPE nvarchar(50) NULL,
	URL nvarchar(1000) NULL
)
;
CREATE TABLE ANNOTATION_TEXT
(
	ANN_TEXT_ID bigint identity(1,1) NOT NULL , 
	ANN_ID bigint NOT NULL ,
	LANGUAGE varchar(50) NOT NULL,
	TEXT nvarchar(4000) NOT NULL
) 
;
CREATE TABLE ARTEFACT_ANNOTATION
(
	ANN_ID bigint NOT NULL, 
	ART_ID bigint NOT NULL
) 
;
CREATE TABLE ITEM_ANNOTATION
(
	ANN_ID bigint NOT NULL, 
	ITEM_ID bigint NOT NULL
)
;
CREATE TABLE COMPONENT_ANNOTATION
(
	ANN_ID bigint NOT NULL, 
	COMP_ID bigint NOT NULL
) 
;
CREATE TABLE GROUP_ANNOTATION
(
	ANN_ID bigint NOT NULL, 
	GR_ID bigint NOT NULL
)
;

ALTER TABLE AGENCY_SCHEME ADD CONSTRAINT PK_AGENCY_SCHEME 
	PRIMARY KEY CLUSTERED (AG_SCH_ID)
;

ALTER TABLE DATACONSUMER_SCHEME ADD CONSTRAINT PK_DATACONSUMER_SCHEME 
	PRIMARY KEY CLUSTERED (DC_SCH_ID)
;

ALTER TABLE DATAPROVIDER_SCHEME ADD CONSTRAINT PK_DATAPROVIDER_SCHEME
	PRIMARY KEY CLUSTERED (DP_SCH_ID)
;

ALTER TABLE ORGANISATION_UNIT_SCHEME ADD CONSTRAINT PK_ORGANISATION_UNIT_SCHEME 
	PRIMARY KEY CLUSTERED (ORG_UNIT_SCH_ID)
;

ALTER TABLE STRUCTURE_SET ADD CONSTRAINT PK_STRUCTURE_SET
    PRIMARY KEY CLUSTERED(SS_ID)
;

ALTER TABLE AGENCY ADD CONSTRAINT PK_AGENCY
	PRIMARY KEY CLUSTERED (AG_ID)
;
ALTER TABLE DATACONSUMER ADD CONSTRAINT PK_DATACONSUMER
	PRIMARY KEY CLUSTERED (DC_ID)
;
ALTER TABLE DATAPROVIDER ADD CONSTRAINT PK_DATAPROVIDER
	PRIMARY KEY CLUSTERED (DP_ID)
;
ALTER TABLE ORGANISATION_UNIT ADD CONSTRAINT PK_ORGANISATION_UNIT
	PRIMARY KEY CLUSTERED (ORG_UNIT_ID)
;

ALTER TABLE CODELIST_MAP ADD CONSTRAINT PK_CODELIST_MAP
	PRIMARY KEY CLUSTERED (CLM_ID)
;

ALTER TABLE CODE_MAP ADD CONSTRAINT PK_CODE_MAP
	PRIMARY KEY CLUSTERED (DCM_ID)
;

ALTER TABLE STRUCTURE_MAP ADD CONSTRAINT PK_STRUCTURE_MAP
	PRIMARY KEY CLUSTERED (SM_ID)
;

ALTER TABLE COMPONENT_MAP ADD CONSTRAINT PK_COMPONENT_MAP
	PRIMARY KEY CLUSTERED (CM_ID)
;

ALTER TABLE ANNOTATION ADD CONSTRAINT PK_ANNOTATION
	PRIMARY KEY CLUSTERED (ANN_ID)
;

ALTER TABLE ANNOTATION_TEXT ADD CONSTRAINT PK_ANNOTATION_TEXT
	PRIMARY KEY CLUSTERED (ANN_TEXT_ID)
;

ALTER TABLE ARTEFACT_ANNOTATION ADD CONSTRAINT PK_ARTEFACT_ANNOTATION
	PRIMARY KEY CLUSTERED (ANN_ID, ART_ID)
;

ALTER TABLE ITEM_ANNOTATION ADD CONSTRAINT PK_ITEM_ANNOTATION
	PRIMARY KEY CLUSTERED (ANN_ID, ITEM_ID)
;

ALTER TABLE COMPONENT_ANNOTATION ADD CONSTRAINT PK_COMPONENT_ANNOTATION
	PRIMARY KEY CLUSTERED (ANN_ID, COMP_ID)
;

ALTER TABLE GROUP_ANNOTATION ADD CONSTRAINT PK_GROUP_ANNOTATION
	PRIMARY KEY CLUSTERED (ANN_ID, GR_ID)
;

ALTER TABLE AGENCY_SCHEME ADD CONSTRAINT FK_AGENCY_SCHEME_ARTEFACT 
	FOREIGN KEY (AG_SCH_ID) REFERENCES ARTEFACT (ART_ID)
ON DELETE CASCADE
;

ALTER TABLE DATACONSUMER_SCHEME ADD CONSTRAINT FK_DATACONSUMER_SCHEME_ART_ID 
	FOREIGN KEY (DC_SCH_ID) REFERENCES ARTEFACT (ART_ID)
ON DELETE CASCADE
;

ALTER TABLE DATAPROVIDER_SCHEME ADD CONSTRAINT FK_DATAPROVIDER_SCHEME_ART_ID
	FOREIGN KEY (DP_SCH_ID) REFERENCES ARTEFACT (ART_ID)
ON DELETE CASCADE
;

ALTER TABLE ORGANISATION_UNIT_SCHEME ADD CONSTRAINT FK_ORGANISATION_UNIT_SCH_ART
	FOREIGN KEY (ORG_UNIT_SCH_ID) REFERENCES ARTEFACT (ART_ID)
ON DELETE CASCADE
;

ALTER TABLE STRUCTURE_SET ADD CONSTRAINT FK_STRUCTURE_SET_ARTEFACT 
	FOREIGN KEY (SS_ID) REFERENCES ARTEFACT (ART_ID)
ON DELETE CASCADE
;

ALTER TABLE AGENCY ADD CONSTRAINT FK_AGENCY_ID 
	FOREIGN KEY (AG_ID) REFERENCES ITEM (ITEM_ID)
ON DELETE CASCADE
;

ALTER TABLE AGENCY ADD CONSTRAINT FK_AGENCY_AGENCY_SCHEME 
	FOREIGN KEY (AG_SCH_ID) REFERENCES AGENCY_SCHEME (AG_SCH_ID)
ON DELETE CASCADE
;

ALTER TABLE DATACONSUMER ADD CONSTRAINT FK_DATACONSUMER_ID 
	FOREIGN KEY (DC_ID) REFERENCES ITEM (ITEM_ID)
ON DELETE CASCADE
;

ALTER TABLE DATACONSUMER ADD CONSTRAINT FK_DATACONSUMER_DC_SCH_ID 
	FOREIGN KEY (DC_SCH_ID) REFERENCES DATACONSUMER_SCHEME (DC_SCH_ID)
ON DELETE CASCADE
;

ALTER TABLE DATAPROVIDER ADD CONSTRAINT FK_DATAPROVIDER_ID 
	FOREIGN KEY (DP_ID) REFERENCES ITEM (ITEM_ID)
ON DELETE CASCADE
;

ALTER TABLE DATAPROVIDER ADD CONSTRAINT FK_DATAPROVIDER_DP_SCH_ID
	FOREIGN KEY (DP_SCH_ID) REFERENCES DATAPROVIDER_SCHEME (DP_SCH_ID)
ON DELETE CASCADE
;

ALTER TABLE ORGANISATION_UNIT ADD CONSTRAINT FK_ORGANISATION_UNIT_ID
	FOREIGN KEY (ORG_UNIT_ID) REFERENCES ITEM (ITEM_ID)
ON DELETE CASCADE
;

ALTER TABLE ORGANISATION_UNIT ADD CONSTRAINT FK_ORGANISATION_UNIT_SCH_ID
	FOREIGN KEY (ORG_UNIT_SCH_ID) REFERENCES ORGANISATION_UNIT_SCHEME (ORG_UNIT_SCH_ID)
ON DELETE CASCADE
;

ALTER TABLE ORGANISATION_UNIT ADD CONSTRAINT FK_ORGANISATION_UNIT_ITEM
	FOREIGN KEY (PARENT_ORG_UNIT_ID) REFERENCES ORGANISATION_UNIT (ORG_UNIT_ID)
;

ALTER TABLE CONCEPT ADD CONSTRAINT FK_CONCEPT_ITEM 
	FOREIGN KEY (PARENT_CONCEPT_ID) REFERENCES CONCEPT (CON_ID)
;

ALTER TABLE CODELIST_MAP ADD CONSTRAINT FK_CL_MAP_ITEM FOREIGN KEY(CLM_ID) REFERENCES ITEM(ITEM_ID) ON DELETE CASCADE
;
ALTER TABLE CODELIST_MAP ADD CONSTRAINT FK_CL_MAP_SS FOREIGN KEY(SS_ID) REFERENCES STRUCTURE_SET(SS_ID) ON DELETE CASCADE
;
ALTER TABLE CODELIST_MAP ADD CONSTRAINT FK_CL_MAP_SOURCE FOREIGN KEY(SOURCE_CL_ID) REFERENCES CODELIST(CL_ID)
;
ALTER TABLE CODELIST_MAP ADD CONSTRAINT FK_CL_MAP_TARGET FOREIGN KEY(TARGET_CL_ID) REFERENCES CODELIST(CL_ID)
;

ALTER TABLE CODE_MAP ADD CONSTRAINT FK_CD_MAP_CLM FOREIGN KEY(CLM_ID) REFERENCES CODELIST_MAP(CLM_ID) ON DELETE CASCADE
;
ALTER TABLE CODE_MAP ADD CONSTRAINT FK_CD_MAP_SOURCE FOREIGN KEY(SOURCE_LCD_ID) REFERENCES DSD_CODE(LCD_ID)
;
ALTER TABLE CODE_MAP ADD CONSTRAINT FK_CD_MAP_TARGET FOREIGN KEY(TARGET_LCD_ID) REFERENCES DSD_CODE(LCD_ID)
;

ALTER TABLE STRUCTURE_MAP ADD CONSTRAINT FK_STR_MAP_ITEM FOREIGN KEY(SM_ID) REFERENCES ITEM(ITEM_ID) ON DELETE CASCADE
;
ALTER TABLE STRUCTURE_MAP ADD CONSTRAINT FK_STR_MAP_SS FOREIGN KEY(SS_ID) REFERENCES STRUCTURE_SET(SS_ID) ON DELETE CASCADE
;
ALTER TABLE STRUCTURE_MAP ADD CONSTRAINT FK_STR_MAP_SOURCE FOREIGN KEY(SOURCE_STR_ID) REFERENCES ARTEFACT(ART_ID)
;
ALTER TABLE STRUCTURE_MAP ADD CONSTRAINT FK_STR_MAP_TARGET FOREIGN KEY(TARGET_STR_ID) REFERENCES ARTEFACT(ART_ID)
;

ALTER TABLE COMPONENT_MAP ADD CONSTRAINT FK_C_MAP_SM FOREIGN KEY(SM_ID) REFERENCES STRUCTURE_MAP(SM_ID) ON DELETE CASCADE
;
ALTER TABLE COMPONENT_MAP ADD CONSTRAINT FK_C_MAP_SOURCE FOREIGN KEY(SOURCE_COMP_ID) REFERENCES COMPONENT(COMP_ID)
;
ALTER TABLE COMPONENT_MAP ADD CONSTRAINT FK_C_MAP_TARGET FOREIGN KEY(TARGET_COMP_ID) REFERENCES COMPONENT(COMP_ID)
;

ALTER TABLE ANNOTATION_TEXT ADD CONSTRAINT FK_ANN_TEXT FOREIGN KEY(ANN_ID) REFERENCES ANNOTATION(ANN_ID) ON DELETE CASCADE
;

ALTER TABLE ARTEFACT_ANNOTATION ADD CONSTRAINT FK_ART_ANN_ANN FOREIGN KEY(ANN_ID) REFERENCES ANNOTATION(ANN_ID) ON DELETE CASCADE
;
ALTER TABLE ARTEFACT_ANNOTATION ADD CONSTRAINT FK_ART_ANN_ART FOREIGN KEY(ART_ID) REFERENCES ARTEFACT(ART_ID) ON DELETE CASCADE
;
ALTER TABLE ITEM_ANNOTATION ADD CONSTRAINT FK_ITEM_ANN_ANN FOREIGN KEY(ANN_ID) REFERENCES ANNOTATION(ANN_ID) ON DELETE CASCADE
;
ALTER TABLE ITEM_ANNOTATION ADD CONSTRAINT FK_ITEM_ANN_ITEM FOREIGN KEY(ITEM_ID) REFERENCES ITEM(ITEM_ID) ON DELETE CASCADE
;
ALTER TABLE COMPONENT_ANNOTATION ADD CONSTRAINT FK_COMP_ANN_ANN FOREIGN KEY(ANN_ID) REFERENCES ANNOTATION(ANN_ID) ON DELETE CASCADE
;
ALTER TABLE COMPONENT_ANNOTATION ADD CONSTRAINT FK_COMP_ANN_COMP FOREIGN KEY(COMP_ID) REFERENCES COMPONENT(COMP_ID) ON DELETE CASCADE
;
ALTER TABLE GROUP_ANNOTATION ADD CONSTRAINT FK_GR_ANN_ANN FOREIGN KEY(ANN_ID) REFERENCES ANNOTATION(ANN_ID) ON DELETE CASCADE
;
ALTER TABLE GROUP_ANNOTATION ADD CONSTRAINT FK_GR_ANN_GR FOREIGN KEY(GR_ID) REFERENCES DSD_GROUP(GR_ID) ON DELETE CASCADE
;



IF OBJECT_ID('INSERT_ARTEFACT') IS NOT NULL
DROP PROC INSERT_ARTEFACT
;

IF OBJECT_ID('INSERT_HCL_CODE') IS NOT NULL
DROP PROC INSERT_HCL_CODE
;

IF OBJECT_ID('INSERT_HCL') IS NOT NULL
DROP PROC INSERT_HCL
;

IF OBJECT_ID('INSERT_HIERACHY') IS NOT NULL
DROP PROC INSERT_HIERACHY
;

IF OBJECT_ID('INSERT_HLEVEL') IS NOT NULL
DROP PROC INSERT_HLEVEL
;

IF OBJECT_ID('INSERT_DSD') IS NOT NULL
DROP PROC INSERT_DSD
;

IF OBJECT_ID('INSERT_DATAFLOW') IS NOT NULL
DROP PROC INSERT_DATAFLOW
;

IF OBJECT_ID('INSERT_CATEGORISATION') IS NOT NULL
DROP PROC INSERT_CATEGORISATION
;

IF OBJECT_ID('INSERT_CATEGORY_SCHEME') IS NOT NULL
DROP PROC INSERT_CATEGORY_SCHEME
;

IF OBJECT_ID('INSERT_CODELIST') IS NOT NULL
DROP PROC INSERT_CODELIST
;

IF OBJECT_ID('INSERT_CONCEPT_SCHEME') IS NOT NULL
DROP PROC INSERT_CONCEPT_SCHEME
;

IF OBJECT_ID('INSERT_CONCEPT') IS NOT NULL
DROP PROC INSERT_CONCEPT
;

GO
;
CREATE PROCEDURE INSERT_ARTEFACT
	@p_id varchar(50),
	@p_version varchar(50),
	@p_agency varchar(50),
	@p_valid_from datetime = NULL,
	@p_valid_to datetime = NULL,
	@p_is_final int = NULL,
	@p_uri nvarchar(255),
	@p_last_modified datetime = NULL,
	@p_pk bigint out
AS
BEGIN
		SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
    
    DECLARE	@p_version1 bigint,
		@p_version2 bigint,
		@p_version3 bigint;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

        exec SPLIT_VERSION @p_version = @p_version, @p_version1 = @p_version1 OUTPUT, @p_version2 = @p_version2 OUTPUT, @p_version3 = @p_version3 OUTPUT;
	    insert into  ARTEFACT (ID,VERSION1, VERSION2, VERSION3, AGENCY, VALID_FROM, VALID_TO, IS_FINAL, URI, LAST_MODIFIED) values (@p_id,@p_version1,@p_version2,@p_version3,@p_agency,@p_valid_from,@p_valid_to,@p_is_final,@p_uri,@p_last_modified);
	    set @p_pk=SCOPE_IDENTITY();
        IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
GO
;

CREATE PROCEDURE INSERT_HCL_CODE
	@p_id varchar(50),
	@p_version varchar(50),
	@p_agency varchar(50),
	@p_valid_from datetime = NULL,
	@p_valid_to datetime= NULL,
	@p_is_final int = NULL,
	@p_uri nvarchar(255),
	@p_last_modified datetime = NULL,
	@p_parent_hcode_id bigint = null,
	@p_lcd_id bigint,
	@p_h_id bigint,
	@p_level_id bigint = null,
	@p_pk bigint out
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

	exec INSERT_ARTEFACT @p_id = @p_id,@p_version = @p_version,@p_agency = @p_agency,@p_valid_from = @p_valid_from,@p_valid_to = @p_valid_to,@p_is_final = @p_is_final,@p_uri = @p_uri,@p_last_modified = @p_last_modified, @p_pk = @p_pk OUTPUT;
	
	INSERT INTO HCL_CODE
           (HCODE_ID
           ,PARENT_HCODE_ID
           ,LCD_ID
           ,H_ID
           ,LEVEL_ID)
	VALUES
		(@p_pk
        ,@p_parent_hcode_id
        ,@p_lcd_id
        ,@p_h_id
         ,@p_level_id);

	IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
GO
;
CREATE PROCEDURE INSERT_HCL
	@p_id varchar(50),
	@p_version varchar(50),
	@p_agency varchar(50),
	@p_valid_from datetime = NULL,
	@p_valid_to datetime= NULL,
	@p_is_final int = NULL,
	@p_uri nvarchar(255),
	@p_last_modified datetime = NULL,
	@p_pk bigint out
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

	exec INSERT_ARTEFACT @p_id = @p_id,@p_version = @p_version,@p_agency = @p_agency,@p_valid_from = @p_valid_from,@p_valid_to = @p_valid_to,@p_is_final = @p_is_final,@p_uri = @p_uri, @p_last_modified = @p_last_modified, @p_pk = @p_pk OUTPUT;
	insert into HCL (HCL_ID) values(@p_pk);

	IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
GO
;
CREATE PROCEDURE INSERT_HIERACHY
	@p_id varchar(50),
	@p_version varchar(50),
	@p_agency varchar(50),
	@p_valid_from datetime = NULL,
	@p_valid_to datetime= NULL,
	@p_is_final int = NULL,
	@p_uri nvarchar(255),
	@p_last_modified datetime = NULL,
	@p_hcl_id bigint,
	@p_pk bigint out
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

	exec INSERT_ARTEFACT @p_id = @p_id,@p_version = @p_version,@p_agency = @p_agency,@p_valid_from = @p_valid_from,@p_valid_to = @p_valid_to,@p_is_final = @p_is_final, @p_uri = @p_uri,@p_last_modified = @p_last_modified,@p_pk = @p_pk OUTPUT;
	
	INSERT INTO HIERARCHY
           (H_ID
           ,HCL_ID)
     VALUES
           (@p_pk
           ,@p_hcl_id);

	IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
GO
;
CREATE PROCEDURE INSERT_HLEVEL
	@p_id varchar(50),
	@p_version varchar(50),
	@p_agency varchar(50),
	@p_valid_from datetime = NULL,
	@p_valid_to datetime= NULL,
	@p_is_final int = NULL,
	@p_uri nvarchar(255),
	@p_last_modified datetime = NULL,
	@p_h_id bigint,
	@p_parent_level_id bigint = null,
	@p_pk bigint out
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

	exec INSERT_ARTEFACT @p_id = @p_id,@p_version = @p_version,@p_agency = @p_agency,@p_valid_from = @p_valid_from,@p_valid_to = @p_valid_to,@p_is_final = @p_is_final,@p_uri = @p_uri,@p_last_modified = @p_last_modified, @p_pk = @p_pk OUTPUT;
	
	INSERT INTO HLEVEL
           (LEVEL_ID
           ,H_ID
           ,PARENT_LEVEL_ID)
     VALUES
           (@p_pk
           ,@p_h_id
           ,@p_parent_level_id);

	IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
GO
;
CREATE PROCEDURE INSERT_DSD
	@p_id varchar(50),
	@p_version varchar(50),
	@p_agency varchar(50),
	@p_valid_from datetime = NULL,
	@p_valid_to datetime= NULL,
	@p_is_final int = NULL,
	@p_uri nvarchar(255),
	@p_last_modified datetime = NULL,
	@p_pk bigint out
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

	exec INSERT_ARTEFACT @p_id = @p_id,@p_version = @p_version,@p_agency = @p_agency,@p_valid_from = @p_valid_from,@p_valid_to = @p_valid_to,@p_is_final = @p_is_final,@p_uri = @p_uri,@p_last_modified = @p_last_modified,@p_pk = @p_pk OUTPUT;
	
	INSERT INTO DSD
           (DSD_ID)
     VALUES
           (@p_pk);


	IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;

GO
;
CREATE PROCEDURE INSERT_DATAFLOW
	@p_id varchar(50),
	@p_version varchar(50),
	@p_agency varchar(50),
	@p_valid_from datetime = NULL,
	@p_valid_to datetime= NULL,
	@p_is_final int = NULL,
	@p_uri nvarchar(255),
	@p_last_modified datetime = NULL,
	@p_dsd_id bigint,
    @p_map_set_id bigint = null,
	@p_pk bigint out
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

	exec INSERT_ARTEFACT @p_id = @p_id,@p_version = @p_version,@p_agency = @p_agency,@p_valid_from = @p_valid_from,@p_valid_to = @p_valid_to,@p_is_final = @p_is_final,@p_uri = @p_uri,@p_last_modified = @p_last_modified,@p_pk = @p_pk OUTPUT;
	
INSERT INTO DATAFLOW
           (DF_ID
           ,DSD_ID
           ,MAP_SET_ID)
     VALUES
           (@p_pk
           ,@p_dsd_id
           ,@p_map_set_id);

	IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
GO
;
CREATE PROCEDURE INSERT_CATEGORISATION
	@p_id varchar(50),
	@p_version varchar(50),
	@p_agency varchar(50),
	@p_valid_from datetime = NULL,
	@p_valid_to datetime= NULL,
	@p_is_final int = NULL,
	@p_uri nvarchar(255),
	@p_last_modified datetime = NULL,
	@p_art_id bigint,
    @p_cat_id bigint,
    @p_dc_order bigint = NULL,
	@p_pk bigint out
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

	exec INSERT_ARTEFACT @p_id = @p_id,@p_version = @p_version,@p_agency = @p_agency,@p_valid_from = @p_valid_from,@p_valid_to = @p_valid_to,@p_is_final = @p_is_final,@p_uri=@p_uri,@p_last_modified=@p_last_modified,@p_pk = @p_pk OUTPUT;
	
INSERT INTO CATEGORISATION
           (CATN_ID
           ,ART_ID
           ,CAT_ID
           ,DC_ORDER)
     VALUES
           (@p_pk
           ,@p_art_id
           ,@p_cat_id
           ,@p_dc_order);

	IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
GO
;
CREATE PROCEDURE INSERT_CATEGORY_SCHEME
	@p_id varchar(50),
	@p_version varchar(50),
	@p_agency varchar(50),
	@p_valid_from datetime = NULL,
	@p_valid_to datetime= NULL,
	@p_is_final int = NULL,
	@p_uri nvarchar(255),
	@p_last_modified datetime = NULL,
	@p_cs_order bigint = 0,
    @p_is_partial bit = 0,
	@p_pk bigint out
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

	exec INSERT_ARTEFACT @p_id = @p_id,@p_version = @p_version,@p_agency = @p_agency,@p_valid_from = @p_valid_from,@p_valid_to = @p_valid_to,@p_is_final = @p_is_final,@p_uri=@p_uri,@p_last_modified = @p_last_modified,@p_pk = @p_pk OUTPUT;
	
	INSERT INTO CATEGORY_SCHEME
           (CAT_SCH_ID, CS_ORDER, IS_PARTIAL)
     VALUES
           (@p_pk, @p_cs_order, @p_is_partial);

	IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
GO
;

CREATE PROCEDURE INSERT_CODELIST
	@p_id varchar(50),
	@p_version varchar(50),
	@p_agency varchar(50),
	@p_valid_from datetime = NULL,
	@p_valid_to datetime= NULL,
	@p_is_final int = NULL,
	@p_uri nvarchar(255),
	@p_last_modified datetime = NULL,
    @p_is_partial bit = 0,
	@p_pk bigint out
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

	exec INSERT_ARTEFACT @p_id = @p_id,@p_version = @p_version,@p_agency = @p_agency,@p_valid_from = @p_valid_from,@p_valid_to = @p_valid_to,@p_is_final = @p_is_final,@p_uri = @p_uri,@p_last_modified = @p_last_modified,@p_pk = @p_pk OUTPUT;
	
	INSERT INTO CODELIST
           (CL_ID, IS_PARTIAL)
     VALUES
           (@p_pk, @p_is_partial);

	IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
GO
;

CREATE PROCEDURE INSERT_CONCEPT_SCHEME
	@p_id varchar(50),
	@p_version varchar(50),
	@p_agency varchar(50),
	@p_valid_from datetime = NULL,
	@p_valid_to datetime= NULL,
	@p_is_final int = NULL,
	@p_uri nvarchar(255),
	@p_last_modified datetime = NULL,
    @p_is_partial bit = 0,
	@p_pk bigint out
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

	exec INSERT_ARTEFACT @p_id = @p_id,@p_version = @p_version,@p_agency = @p_agency,@p_valid_from = @p_valid_from,@p_valid_to = @p_valid_to,@p_is_final = @p_is_final,@p_uri = @p_uri, @p_last_modified = @p_last_modified,@p_pk = @p_pk OUTPUT;
	
	INSERT INTO CONCEPT_SCHEME
           (CON_SCH_ID, IS_PARTIAL)
     VALUES
           (@p_pk, @p_is_partial);

	IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
GO
;

CREATE PROCEDURE INSERT_AGENCY
	@p_id varchar(50), 
	@p_ag_sch_id bigint,
	@p_pk bigint OUT 
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

		insert into ITEM (ID) VALUES (@p_id);
		set @p_pk = SCOPE_IDENTITY();
		insert into AGENCY (AG_ID, AG_SCH_ID) VALUES (@p_pk, @p_ag_sch_id);
	
		IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
GO
;
CREATE PROCEDURE INSERT_DATACONSUMER
	@p_id varchar(50), 
	@p_datacons_sch_id bigint,
	@p_pk bigint OUT 
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

		insert into ITEM (ID) VALUES (@p_id);
		set @p_pk = SCOPE_IDENTITY();
		insert into DATACONSUMER (DC_ID, DC_SCH_ID) VALUES (@p_pk, @p_datacons_sch_id);
	
		IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
GO
;
CREATE PROCEDURE INSERT_DATAPROVIDER
	@p_id varchar(50), 
	@p_dataprov_sch_id bigint,
	@p_pk bigint OUT 
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

		insert into ITEM (ID) VALUES (@p_id);
		set @p_pk = SCOPE_IDENTITY();
		insert into DATAPROVIDER (DP_ID, DP_SCH_ID) VALUES (@p_pk, @p_dataprov_sch_id);
	
		IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
GO
;
CREATE PROCEDURE INSERT_ORGANISATION_UNIT
	@p_id varchar(50), 
	@p_org_unit_sch_id bigint,
	@p_parent_org_unit_id bigint,
	@p_pk bigint OUT 
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

		insert into ITEM (ID) VALUES (@p_id);
		set @p_pk = SCOPE_IDENTITY();
		insert into ORGANISATION_UNIT (ORG_UNIT_ID, ORG_UNIT_SCH_ID, PARENT_ORG_UNIT_ID) VALUES (@p_pk, @p_org_unit_sch_id,@p_parent_org_unit_id);
	
		IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
GO
;

CREATE PROCEDURE INSERT_AGENCY_SCHEME
	@p_id varchar(50),
	@p_version varchar(50),
	@p_agency varchar(50),
	@p_valid_from datetime = NULL,
	@p_valid_to datetime= NULL,
	@p_is_final int = NULL,
	@p_uri nvarchar(255),
	@p_last_modified datetime = NULL,
    @p_is_partial bit = 0,
	@p_pk bigint out
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

	exec INSERT_ARTEFACT @p_id = @p_id,@p_version = @p_version,@p_agency = @p_agency,@p_valid_from = @p_valid_from,@p_valid_to = @p_valid_to,@p_is_final = @p_is_final,@p_uri = @p_uri, @p_last_modified = @p_last_modified,@p_pk = @p_pk OUTPUT;
	
	INSERT INTO AGENCY_SCHEME
           (AG_SCH_ID, IS_PARTIAL)
     VALUES
           (@p_pk, @p_is_partial);

	IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
GO
;

CREATE PROCEDURE INSERT_DATACONSUMER_SCHEME
	@p_id varchar(50),
	@p_version varchar(50),
	@p_agency varchar(50),
	@p_valid_from datetime = NULL,
	@p_valid_to datetime= NULL,
	@p_is_final int = NULL,
	@p_uri nvarchar(255),
	@p_last_modified datetime = NULL,
    @p_is_partial bit = 0,
	@p_pk bigint out
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

	exec INSERT_ARTEFACT @p_id = @p_id,@p_version = @p_version,@p_agency = @p_agency,@p_valid_from = @p_valid_from,@p_valid_to = @p_valid_to,@p_is_final = @p_is_final,@p_uri = @p_uri, @p_last_modified = @p_last_modified,@p_pk = @p_pk OUTPUT;
	
	INSERT INTO DATACONSUMER_SCHEME
           (DC_SCH_ID, IS_PARTIAL)
     VALUES
           (@p_pk, @p_is_partial);

	IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
GO
;

CREATE PROCEDURE INSERT_DATAPROVIDER_SCHEME
	@p_id varchar(50),
	@p_version varchar(50),
	@p_agency varchar(50),
	@p_valid_from datetime = NULL,
	@p_valid_to datetime= NULL,
	@p_is_final int = NULL,
	@p_uri nvarchar(255),
	@p_last_modified datetime = NULL,
    @p_is_partial bit = 0,
	@p_pk bigint out
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

	exec INSERT_ARTEFACT @p_id = @p_id,@p_version = @p_version,@p_agency = @p_agency,@p_valid_from = @p_valid_from,@p_valid_to = @p_valid_to,@p_is_final = @p_is_final,@p_uri = @p_uri, @p_last_modified = @p_last_modified,@p_pk = @p_pk OUTPUT;
	
	INSERT INTO DATAPROVIDER_SCHEME
           (DP_SCH_ID, IS_PARTIAL)
     VALUES
           (@p_pk, @p_is_partial);

	IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
GO
;

CREATE PROCEDURE INSERT_ORGANISATION_UNIT_SCH
	@p_id varchar(50),
	@p_version varchar(50),
	@p_agency varchar(50),
	@p_valid_from datetime = NULL,
	@p_valid_to datetime= NULL,
	@p_is_final int = NULL,
	@p_uri nvarchar(255),
	@p_last_modified datetime = NULL,
    @p_is_partial bit = 0,
	@p_pk bigint out
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

	exec INSERT_ARTEFACT @p_id = @p_id,@p_version = @p_version,@p_agency = @p_agency,@p_valid_from = @p_valid_from,@p_valid_to = @p_valid_to,@p_is_final = @p_is_final,@p_uri = @p_uri, @p_last_modified = @p_last_modified,@p_pk = @p_pk OUTPUT;
	
	INSERT INTO ORGANISATION_UNIT_SCHEME
           (ORG_UNIT_SCH_ID, IS_PARTIAL)
     VALUES
           (@p_pk, @p_is_partial);

	IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
GO
;

CREATE PROCEDURE INSERT_STRUCTURE_SET
	@p_id varchar(50),
	@p_version varchar(50),
	@p_agency varchar(50),
	@p_valid_from datetime = NULL,
	@p_valid_to datetime= NULL,
	@p_is_final int = NULL,
	@p_uri nvarchar(255),
	@p_last_modified datetime = NULL,
	@p_pk bigint out
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

	exec INSERT_ARTEFACT @p_id = @p_id,@p_version = @p_version,@p_agency = @p_agency,@p_valid_from = @p_valid_from,@p_valid_to = @p_valid_to,@p_is_final = @p_is_final,@p_uri = @p_uri, @p_last_modified = @p_last_modified,@p_pk = @p_pk OUTPUT;
	
	INSERT INTO STRUCTURE_SET
           (SS_ID)
     VALUES
           (@p_pk);

	IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
GO
;

CREATE PROCEDURE INSERT_CONCEPT
	@p_id varchar(50), 
	@p_con_sch_id bigint,
	@p_parent_con_id bigint,
	@p_pk bigint OUT 
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

		insert into ITEM (ID) VALUES (@p_id);
		set @p_pk = SCOPE_IDENTITY();
		insert into CONCEPT (CON_ID, CON_SCH_ID,PARENT_CONCEPT_ID) VALUES (@p_pk, @p_con_sch_id,@p_parent_con_id);
	
		IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
GO
;

CREATE PROCEDURE INSERT_CODELIST_MAP
	@p_id varchar(50),
	@p_ss_id bigint,
	@p_source_cl_id bigint,
	@p_target_cl_id bigint,
	@p_pk bigint OUT 
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION
		insert into ITEM (ID) VALUES (@p_id);
		set @p_pk = SCOPE_IDENTITY();
		insert into CODELIST_MAP (CLM_ID, SS_ID, SOURCE_CL_ID, TARGET_CL_ID) VALUES (@p_pk, @p_ss_id ,@p_source_cl_id, @p_target_cl_id);
	
	IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
GO
;

CREATE PROCEDURE INSERT_CODE_MAP
	@p_clm_id bigint,
	@p_source_lcd_id bigint,
	@p_target_lcd_id bigint,
	@p_pk bigint OUT
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION
		insert into CODE_MAP (CLM_ID, SOURCE_LCD_ID, TARGET_LCD_ID) VALUES (@p_clm_id, @p_source_lcd_id, @p_target_lcd_id);
	    set @p_pk = SCOPE_IDENTITY();

	IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
GO
;
CREATE PROCEDURE INSERT_STRUCTURE_MAP
	@p_id varchar(50),
	@p_ss_id bigint,
	@p_source_str_id bigint,
	@p_target_str_id bigint,
	@p_pk bigint OUT 
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION
		insert into ITEM (ID) VALUES (@p_id);
		set @p_pk = SCOPE_IDENTITY();
		insert into STRUCTURE_MAP (SM_ID, SS_ID, SOURCE_STR_ID, TARGET_STR_ID) VALUES (@p_pk, @p_ss_id ,@p_source_str_id, @p_target_str_id);
	
	IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
GO
;
CREATE PROCEDURE INSERT_COMPONENT_MAP
	@p_sm_id bigint,
	@p_source_comp_id bigint,
	@p_target_comp_id bigint,
	@p_pk bigint OUT  
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION
		insert into COMPONENT_MAP (SM_ID, SOURCE_COMP_ID, TARGET_COMP_ID) VALUES (@p_sm_id, @p_source_comp_id, @p_target_comp_id);
		set @p_pk = SCOPE_IDENTITY();

    IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
GO
;

CREATE PROCEDURE INSERT_ANNOTATION 
	@p_id nvarchar(50),
	@p_title nvarchar(70),
	@p_type nvarchar(50),
	@p_url nvarchar(1000),
	@p_pk bigint OUT 
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION
	insert into ANNOTATION (ID, TITLE, TYPE, URL) VALUES (@p_id, @p_title, @p_type, @p_url);
	set @p_pk = SCOPE_IDENTITY();

    IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
GO
;
CREATE PROCEDURE INSERT_ANNOTATION_TEXT 
	@p_ann_id bigint,
	@p_language varchar(50),
	@p_text nvarchar(4000),
	@p_pk bigint OUT
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

	insert into ANNOTATION_TEXT (ANN_ID, LANGUAGE, TEXT) VALUES (@p_ann_id, @p_language, @p_text);
	set @p_pk = SCOPE_IDENTITY();

IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
GO
;
CREATE PROCEDURE INSERT_ARTEFACT_ANNOTATION 
	@p_art_id bigint,
	@p_id nvarchar(50),
	@p_title nvarchar(70),
	@p_type nvarchar(50),
	@p_url nvarchar(1000),
	@p_pk bigint OUT
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

	exec INSERT_ANNOTATION @p_id = @p_id, @p_title = @p_title, @p_type = @p_type, @p_url = @p_url, @p_pk = @p_pk OUTPUT;
	insert into ARTEFACT_ANNOTATION (ANN_ID, ART_ID) VALUES (@p_pk, @p_art_id);

IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
GO
;
CREATE PROCEDURE INSERT_ITEM_ANNOTATION 
	@p_item_id bigint,
	@p_id nvarchar(50),
	@p_title nvarchar(70),
	@p_type nvarchar(50),
	@p_url nvarchar(1000),
	@p_pk bigint OUT
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

	exec INSERT_ANNOTATION @p_id = @p_id, @p_title = @p_title, @p_type = @p_type, @p_url = @p_url, @p_pk = @p_pk OUTPUT;
	insert into ITEM_ANNOTATION (ANN_ID, ITEM_ID) VALUES (@p_pk, @p_item_id);

IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
GO
;
CREATE PROCEDURE INSERT_COMPONENT_ANNOTATION 
	@p_comp_id bigint,
	@p_id nvarchar(50),
	@p_title nvarchar(70),
	@p_type nvarchar(50),
	@p_url nvarchar(1000),
	@p_pk bigint OUT
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION
	exec INSERT_ANNOTATION @p_id = @p_id, @p_title = @p_title, @p_type = @p_type, @p_url = @p_url, @p_pk = @p_pk OUTPUT;
	insert into COMPONENT_ANNOTATION (ANN_ID, COMP_ID) VALUES (@p_pk, @p_comp_id);

IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
GO
;
CREATE PROCEDURE INSERT_GROUP_ANNOTATION 
	@p_gr_id bigint,
	@p_id nvarchar(50),
	@p_title nvarchar(70),
	@p_type nvarchar(50),
	@p_url nvarchar(1000),
	@p_pk bigint OUT 
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

	exec INSERT_ANNOTATION @p_id = @p_id, @p_title = @p_title, @p_type = @p_type, @p_url = @p_url, @p_pk = @p_pk OUTPUT;
	insert into GROUP_ANNOTATION (ANN_ID, GR_ID) VALUES (@p_pk, @p_gr_id);

IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
GO
;

-- always last
update DB_VERSION SET VERSION = '3.2'
;
