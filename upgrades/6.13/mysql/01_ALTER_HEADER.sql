-- updates for 6.13

-- SDMXRI-1430
alter table HEADER 
  add column STRUCTURE_TYPE varchar(50),
  add column DATASET_ID varchar(255),
  add column DATASET_ACTION varchar(50)
;
