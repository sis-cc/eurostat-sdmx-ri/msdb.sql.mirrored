


CREATE OR REPLACE PROCEDURE INSERT_HEADER(
p_test IN number,
p_dataset_agency IN varchar2,
p_df_id IN number,
p_structure_type IN varchar2 default null,
p_dataset_id IN varchar2 default null,
p_dataset_action IN varchar2 default null,
p_pk OUT number)
AS
BEGIN
		INSERT INTO HEADER (TEST,DATASET_AGENCY,DF_ID,STRUCTURE_TYPE,DATASET_ID,DATASET_ACTION) 
            VALUES (p_test, p_dataset_agency, p_df_id, p_structure_type, p_dataset_id, p_dataset_action) RETURNING HEADER_ID INTO p_pk;
END;
/
;
