-- updates for 6.13

-- SDMXRI-1430
alter table dbo.HEADER add 
  STRUCTURE_TYPE varchar(50),
  DATASET_ID varchar(255),
  DATASET_ACTION varchar(50)
;

