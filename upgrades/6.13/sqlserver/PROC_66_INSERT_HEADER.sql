IF EXISTS (SELECT * FROM dbo.SYSOBJECTS WHERE id = object_id('INSERT_HEADER') AND  OBJECTPROPERTY(id, 'IsProcedure') = 1)
DROP PROC INSERT_HEADER
;
GO
;

CREATE PROCEDURE INSERT_HEADER
@p_test tinyint ,
@p_dataset_agency varchar(255) ,
@p_df_id bigint ,
@p_structure_type varchar(50) ,
@p_dataset_id varchar(250) ,
@p_dataset_action varchar(50) ,
@p_pk bigint OUT
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

		INSERT INTO HEADER (TEST,DATASET_AGENCY,DF_ID,STRUCTURE_TYPE,DATASET_ID,DATASET_ACTION) 
      VALUES (@p_test, @p_dataset_agency, @p_df_id, @p_structure_type, @p_dataset_id, @p_dataset_action);
		set @p_pk = SCOPE_IDENTITY();
	IF @starttrancount = 0
	COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	RAISERROR (@ErrorMessage, -- Message text.
				@ErrorSeverity, -- Severity.
				@ErrorState -- State.
				);
	END CATCH
END
;
