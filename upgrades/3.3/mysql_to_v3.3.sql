﻿-- Upgrade script to 3.3 from 3.2.

CREATE TABLE CONTENT_CONSTRAINT
(
       CONT_CONS_ID bigint NOT NULL,
       ACTUAL_DATA BOOL  NOT NULL DEFAULT 1 ,
       PERIODICITY varchar(50) NULL,
       OFFSET varchar(50) NULL,
       TOLERANCE varchar(50) NULL,
	   PRIMARY KEY(CONT_CONS_ID),
	   KEY(CONT_CONS_ID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
;

CREATE TABLE CONTENT_CONSTRAINT_ATTACHMENT
(
       CONT_CONS_ID bigint NOT NULL,
       ART_ID bigint NOT NULL,
	   PRIMARY KEY(CONT_CONS_ID, ART_ID),
	   KEY(CONT_CONS_ID),
	   KEY(ART_ID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
;

CREATE TABLE CUBE_REGION
(
       CUBE_REGION_ID bigint NOT NULL AUTO_INCREMENT,
       CONT_CONS_ID bigint NOT NULL,
       INCLUDE BOOL NOT NULL DEFAULT 1 ,
	   PRIMARY KEY(CUBE_REGION_ID),
	   KEY(CONT_CONS_ID)

) ENGINE=InnoDB DEFAULT CHARSET=utf8
;

CREATE TABLE CUBE_REGION_KEY_VALUE
(
	CUBE_REGION_KEY_VALUE_ID bigint AUTO_INCREMENT NOT NULL,
	CUBE_REGION_ID bigint NOT NULL,
	MEMBER_ID VARCHAR(50) NOT NULL,
	COMPONENT_TYPE VARCHAR(50) NOT NULL,
	INCLUDE BOOL NOT NULL DEFAULT 1 ,
    PRIMARY KEY(CUBE_REGION_KEY_VALUE_ID),
	KEY(CUBE_REGION_ID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
;
CREATE TABLE CUBE_REGION_VALUE
(
	CUBE_REGION_KEY_VALUE_ID bigint NOT NULL,
	MEMBER_VALUE VARCHAR(50) NOT NULL,
	INCLUDE BOOL  NOT NULL DEFAULT 1,
	PRIMARY KEY(CUBE_REGION_KEY_VALUE_ID, MEMBER_VALUE),
	KEY(CUBE_REGION_KEY_VALUE_ID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
;

ALTER TABLE CONTENT_CONSTRAINT ADD CONSTRAINT FK_CONT_CONS_ART FOREIGN KEY(CONT_CONS_ID) REFERENCES ARTEFACT(ART_ID) ON DELETE CASCADE
;
ALTER TABLE CONTENT_CONSTRAINT_ATTACHMENT ADD CONSTRAINT FK_CONT_CONS_ATT_CONT_CONS FOREIGN KEY(CONT_CONS_ID) REFERENCES CONTENT_CONSTRAINT(CONT_CONS_ID) ON DELETE CASCADE
;
ALTER TABLE CONTENT_CONSTRAINT_ATTACHMENT ADD CONSTRAINT FK_CONT_CONS_ATT_ART FOREIGN KEY(ART_ID) REFERENCES ARTEFACT(ART_ID)
;
ALTER TABLE CUBE_REGION ADD CONSTRAINT FK_CUBE_REGION_CONT_CONS FOREIGN KEY(CONT_CONS_ID) REFERENCES CONTENT_CONSTRAINT(CONT_CONS_ID) ON DELETE CASCADE
;
ALTER TABLE CUBE_REGION_KEY_VALUE ADD CONSTRAINT FK_CUBE_REGION_KEY_CR FOREIGN KEY(CUBE_REGION_ID) REFERENCES CUBE_REGION(CUBE_REGION_ID) ON DELETE CASCADE
;
ALTER TABLE CUBE_REGION_VALUE ADD CONSTRAINT FK_CUBE_REGION_VALUE_CRKV FOREIGN KEY(CUBE_REGION_KEY_VALUE_ID) REFERENCES CUBE_REGION_KEY_VALUE(CUBE_REGION_KEY_VALUE_ID) ON DELETE CASCADE
;

-- DELIMITER $$
CREATE PROCEDURE INSERT_CONTENT_CONSTRAINT(
      IN p_id varchar(50),
      IN p_version  varchar(50),
      IN p_agency varchar(50),
      IN p_valid_from   datetime,
      IN p_valid_to   datetime,
      IN p_is_final  bigint,
      IN p_uri  nvarchar(50),
      IN p_last_modified  datetime,
      IN p_actual_data bigint,
      IN p_periodicity varchar(50),
      IN p_offset varchar(50),
      IN p_tolerance varchar(50),
      OUT p_pk bigint )
BEGIN
    
	call INSERT_ARTEFACT(p_id,p_version,p_agency,p_valid_from,p_valid_to,p_is_final,p_uri,p_last_modified, p_pk);
       
       INSERT INTO CONTENT_CONSTRAINT
           (CONT_CONS_ID, ACTUAL_DATA, PERIODICITY, OFFSET, TOLERANCE)
     VALUES
           (p_pk, p_actual_data, p_periodicity, p_offset, p_tolerance);
END
;

CREATE PROCEDURE INSERT_CONSTRAINT_ATTACHMENT(
      IN p_art_id  bigint,
      IN p_cont_cons_id  bigint)
BEGIN
       
       INSERT INTO CONTENT_CONSTRAINT_ATTACHMENT
           (CONT_CONS_ID, ART_ID)
     VALUES
           (p_cont_cons_id, p_art_id);
END
;

CREATE PROCEDURE INSERT_CUBE_REGION(
      IN p_cont_cons_id  bigint,
      IN p_include  bigint,
      OUT p_pk  bigint)
BEGIN
       INSERT INTO CUBE_REGION
           (CONT_CONS_ID, INCLUDE)
     VALUES
           (p_cont_cons_id, p_include);
      	set p_pk = LAST_INSERT_ID();
END
;


CREATE PROCEDURE INSERT_CUBE_REGION_KEY_VALUE(
	IN p_cube_region_id  bigint,
	IN p_member_id  varchar(50),
	IN p_component_type  varchar(50),
	IN p_include  bigint,
	OUT p_pk bigint)
BEGIN
	INSERT INTO CUBE_REGION_KEY_VALUE
           (CUBE_REGION_ID, MEMBER_ID, COMPONENT_TYPE, INCLUDE)
     VALUES
           (p_cube_region_id, p_member_id, p_component_type, p_include);
		set p_pk = LAST_INSERT_ID();
END
;

CREATE PROCEDURE INSERT_CUBE_REGION_VALUE(
	IN p_cube_region_key_value_id  bigint,
	IN p_member_value  varchar(50),
	IN p_include  bigint)
BEGIN
	INSERT INTO CUBE_REGION_VALUE
           (CUBE_REGION_KEY_VALUE_ID, MEMBER_VALUE, INCLUDE)
     VALUES
           (p_cube_region_key_value_id, p_member_value, p_include);
END
;


-- always last
update DB_VERSION SET VERSION = '3.3'
;
