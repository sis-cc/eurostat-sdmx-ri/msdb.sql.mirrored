-- Upgrade script to 5.2 from 5.1.


-- Add new Stored procedures
DROP PROCEDURE IF EXISTS INSERT_COMPONENT_MAPPING
;

CREATE PROCEDURE INSERT_COMPONENT_MAPPING(
IN p_map_set_id bigint,
IN p_type varchar(10),
IN p_constant nvarchar(255),
OUT p_pk bigint)
BEGIN
		INSERT INTO COMPONENT_MAPPING (MAP_SET_ID,TYPE,CONSTANT) VALUES (p_map_set_id, p_type, p_constant);
		set p_pk = LAST_INSERT_ID();
END;

;

DROP PROCEDURE IF EXISTS INSERT_CONTACT
;

CREATE PROCEDURE INSERT_CONTACT(
IN p_party_id bigint,
OUT p_pk bigint)
BEGIN
		INSERT INTO CONTACT (PARTY_ID) VALUES (p_party_id);
		set p_pk = LAST_INSERT_ID();
END;

;

DROP PROCEDURE IF EXISTS INSERT_CONTACT_DETAIL
;

CREATE PROCEDURE INSERT_CONTACT_DETAIL(
IN p_contact_id bigint,
IN p_type varchar(50),
IN p_value varchar(255),
OUT p_pk bigint)
BEGIN
		INSERT INTO CONTACT_DETAIL (CONTACT_ID,TYPE,VALUE) VALUES (p_contact_id, p_type, p_value);
		set p_pk = LAST_INSERT_ID();
END;

;

DROP PROCEDURE IF EXISTS INSERT_DATASET
;

CREATE PROCEDURE INSERT_DATASET(
IN p_name varchar(255),
IN p_query text,
IN p_connection_id bigint,
IN p_description varchar(1024),
IN p_xml_query text,
IN p_user_id bigint,
IN p_editor_type varchar(250),
OUT p_pk bigint)
BEGIN
		INSERT INTO DATASET (NAME,QUERY,CONNECTION_ID,DESCRIPTION,XML_QUERY,USER_ID,EDITOR_TYPE) VALUES (p_name, p_query, p_connection_id, p_description, p_xml_query, p_user_id, p_editor_type);
		set p_pk = LAST_INSERT_ID();
END;

;

DROP PROCEDURE IF EXISTS INSERT_DATASET_COLUMN
;

CREATE PROCEDURE INSERT_DATASET_COLUMN(
IN p_name varchar(255),
IN p_description varchar(1024),
IN p_ds_id bigint,
IN p_lastretrieval datetime,
OUT p_pk bigint)
BEGIN
		INSERT INTO DATASET_COLUMN (NAME,DESCRIPTION,DS_ID,LASTRETRIEVAL) VALUES (p_name, p_description, p_ds_id, p_lastretrieval);
		set p_pk = LAST_INSERT_ID();
END;

;

DROP PROCEDURE IF EXISTS INSERT_DB_CONNECTION
;

CREATE PROCEDURE INSERT_DB_CONNECTION(
IN p_db_name varchar(1000),
IN p_db_type varchar(50),
IN p_name varchar(50),
IN p_db_password varchar(50),
IN p_db_port int,
IN p_db_server varchar(100),
IN p_db_user varchar(50),
IN p_ado_connection_string varchar(2000),
IN p_jdbc_connection_string varchar(2000),
IN p_user_id bigint,
OUT p_pk bigint)
BEGIN
		INSERT INTO DB_CONNECTION (DB_NAME,DB_TYPE,NAME,DB_PASSWORD,DB_PORT,DB_SERVER,DB_USER,ADO_CONNECTION_STRING,JDBC_CONNECTION_STRING,USER_ID) VALUES (p_db_name, p_db_type, p_name, p_db_password, p_db_port, p_db_server, p_db_user, p_ado_connection_string, p_jdbc_connection_string, p_user_id);
		set p_pk = LAST_INSERT_ID();
END;

;

DROP PROCEDURE IF EXISTS INSERT_DESC_SOURCE
;

CREATE PROCEDURE INSERT_DESC_SOURCE(
IN p_desc_table varchar(255),
IN p_related_field varchar(255),
IN p_desc_field varchar(255),
IN p_col_id bigint,
OUT p_pk bigint)
BEGIN
		INSERT INTO DESC_SOURCE (DESC_TABLE,RELATED_FIELD,DESC_FIELD,COL_ID) VALUES (p_desc_table, p_related_field, p_desc_field, p_col_id);
		set p_pk = LAST_INSERT_ID();
END;

;

DROP PROCEDURE IF EXISTS INSERT_HEADER
;

CREATE PROCEDURE INSERT_HEADER(
IN p_test bool,
IN p_dataset_agency varchar(255),
IN p_df_id bigint,
OUT p_pk bigint)
BEGIN
		INSERT INTO HEADER (TEST,DATASET_AGENCY,DF_ID) VALUES (p_test, p_dataset_agency, p_df_id);
		set p_pk = LAST_INSERT_ID();
END;

;

DROP PROCEDURE IF EXISTS INSERT_HEADER_LOCALISED_STRING
;

CREATE PROCEDURE INSERT_HEADER_LOCALISED_STRING(
IN p_type varchar(50),
IN p_header_id bigint,
IN p_party_id bigint,
IN p_contact_id bigint,
IN p_language varchar(50),
IN p_text varchar(255),
OUT p_pk bigint)
BEGIN
		INSERT INTO HEADER_LOCALISED_STRING (TYPE,HEADER_ID,PARTY_ID,CONTACT_ID,LANGUAGE,TEXT) VALUES (p_type, p_header_id, p_party_id, p_contact_id, p_language, p_text);
		set p_pk = LAST_INSERT_ID();
END;

;


DROP PROCEDURE IF EXISTS INSERT_MAPPING_SET
;

CREATE PROCEDURE INSERT_MAPPING_SET(
IN p_id varchar(50),
IN p_description varchar(1024),
IN p_ds_id bigint,
IN p_df_id bigint,
IN p_user_id bigint,
OUT p_pk bigint)
BEGIN
		INSERT INTO MAPPING_SET (ID,DESCRIPTION,DS_ID,DF_ID,USER_ID) VALUES (p_id, p_description, p_ds_id, p_df_id, p_user_id);
		set p_pk = LAST_INSERT_ID();
END;

;

DROP PROCEDURE IF EXISTS INSERT_PARTY
;

CREATE PROCEDURE INSERT_PARTY(
IN p_id varchar(255),
IN p_header_id bigint,
IN p_type varchar(50),
OUT p_pk bigint)
BEGIN
		INSERT INTO PARTY (ID,HEADER_ID,TYPE) VALUES (p_id, p_header_id, p_type);
		set p_pk = LAST_INSERT_ID();
END;

;

DROP PROCEDURE IF EXISTS INSERT_TRANSCODING_RULE
;

CREATE PROCEDURE INSERT_TRANSCODING_RULE(
IN p_tr_id bigint,
OUT p_pk bigint)
BEGIN
		INSERT INTO TRANSCODING_RULE (TR_ID) VALUES (p_tr_id);
		set p_pk = LAST_INSERT_ID();
END;

;


-- always last
update DB_VERSION SET VERSION = '5.2'
;


