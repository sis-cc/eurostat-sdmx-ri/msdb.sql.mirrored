-- Upgrade script to 5.2 from 5.1.

-- Add new Stored procedures
IF EXISTS (SELECT * FROM dbo.SYSOBJECTS WHERE id = object_id('INSERT_PARTY') AND  OBJECTPROPERTY(id, 'IsProcedure') = 1)
DROP PROC INSERT_PARTY
;

CREATE PROCEDURE INSERT_PARTY
@p_id varchar(255) ,
@p_header_id bigint ,
@p_type varchar(50) ,
@p_pk bigint OUT
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

		INSERT INTO PARTY (ID,HEADER_ID,TYPE) VALUES (@p_id, @p_header_id, @p_type);
		set @p_pk = SCOPE_IDENTITY();
	IF @starttrancount = 0
	COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	RAISERROR (@ErrorMessage, -- Message text.
				@ErrorSeverity, -- Severity.
				@ErrorState -- State.
				);
	END CATCH
END

;
IF EXISTS (SELECT * FROM dbo.SYSOBJECTS WHERE id = object_id('INSERT_MAPPING_SET') AND  OBJECTPROPERTY(id, 'IsProcedure') = 1)
DROP PROC INSERT_MAPPING_SET
;

CREATE PROCEDURE INSERT_MAPPING_SET
@p_id varchar(50) ,
@p_description varchar(1024) ,
@p_ds_id bigint ,
@p_df_id bigint ,
@p_user_id bigint ,
@p_pk bigint OUT
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

		INSERT INTO MAPPING_SET (ID,DESCRIPTION,DS_ID,DF_ID,USER_ID) VALUES (@p_id, @p_description, @p_ds_id, @p_df_id, @p_user_id);
		set @p_pk = SCOPE_IDENTITY();
	IF @starttrancount = 0
	COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	RAISERROR (@ErrorMessage, -- Message text.
				@ErrorSeverity, -- Severity.
				@ErrorState -- State.
				);
	END CATCH
END

;

IF EXISTS (SELECT * FROM dbo.SYSOBJECTS WHERE id = object_id('INSERT_HEADER') AND  OBJECTPROPERTY(id, 'IsProcedure') = 1)
DROP PROC INSERT_HEADER
;

CREATE PROCEDURE INSERT_HEADER
@p_test tinyint ,
@p_dataset_agency varchar(255) ,
@p_df_id bigint ,
@p_pk bigint OUT
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

		INSERT INTO HEADER (TEST,DATASET_AGENCY,DF_ID) VALUES (@p_test, @p_dataset_agency, @p_df_id);
		set @p_pk = SCOPE_IDENTITY();
	IF @starttrancount = 0
	COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	RAISERROR (@ErrorMessage, -- Message text.
				@ErrorSeverity, -- Severity.
				@ErrorState -- State.
				);
	END CATCH
END

;

IF EXISTS (SELECT * FROM dbo.SYSOBJECTS WHERE id = object_id('INSERT_HEADER_LOCALISED_STRING') AND  OBJECTPROPERTY(id, 'IsProcedure') = 1)
DROP PROC INSERT_HEADER_LOCALISED_STRING
;

CREATE PROCEDURE INSERT_HEADER_LOCALISED_STRING
@p_type varchar(50) ,
@p_header_id bigint ,
@p_party_id bigint ,
@p_contact_id bigint ,
@p_language varchar(50) ,
@p_text varchar(255) ,
@p_pk bigint OUT
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

		INSERT INTO HEADER_LOCALISED_STRING (TYPE,HEADER_ID,PARTY_ID,CONTACT_ID,LANGUAGE,TEXT) VALUES (@p_type, @p_header_id, @p_party_id, @p_contact_id, @p_language, @p_text);
		set @p_pk = SCOPE_IDENTITY();
	IF @starttrancount = 0
	COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	RAISERROR (@ErrorMessage, -- Message text.
				@ErrorSeverity, -- Severity.
				@ErrorState -- State.
				);
	END CATCH
END

;


IF EXISTS (SELECT * FROM dbo.SYSOBJECTS WHERE id = object_id('INSERT_HEADER') AND  OBJECTPROPERTY(id, 'IsProcedure') = 1)
DROP PROC INSERT_HEADER
;

CREATE PROCEDURE INSERT_HEADER
@p_test tinyint ,
@p_dataset_agency varchar(255) ,
@p_df_id bigint ,
@p_pk bigint OUT
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

		INSERT INTO HEADER (TEST,DATASET_AGENCY,DF_ID) VALUES (@p_test, @p_dataset_agency, @p_df_id);
		set @p_pk = SCOPE_IDENTITY();
	IF @starttrancount = 0
	COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	RAISERROR (@ErrorMessage, -- Message text.
				@ErrorSeverity, -- Severity.
				@ErrorState -- State.
				);
	END CATCH
END

;

IF EXISTS (SELECT * FROM dbo.SYSOBJECTS WHERE id = object_id('INSERT_CONTACT_DETAIL') AND  OBJECTPROPERTY(id, 'IsProcedure') = 1)
DROP PROC INSERT_CONTACT_DETAIL
;

CREATE PROCEDURE INSERT_CONTACT_DETAIL
@p_contact_id bigint ,
@p_type varchar(50) ,
@p_value varchar(255) ,
@p_pk bigint OUT
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

		INSERT INTO CONTACT_DETAIL (CONTACT_ID,TYPE,VALUE) VALUES (@p_contact_id, @p_type, @p_value);
		set @p_pk = SCOPE_IDENTITY();
	IF @starttrancount = 0
	COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	RAISERROR (@ErrorMessage, -- Message text.
				@ErrorSeverity, -- Severity.
				@ErrorState -- State.
				);
	END CATCH
END

;
IF EXISTS (SELECT * FROM dbo.SYSOBJECTS WHERE id = object_id('INSERT_CONTACT') AND  OBJECTPROPERTY(id, 'IsProcedure') = 1)
DROP PROC INSERT_CONTACT
;

CREATE PROCEDURE INSERT_CONTACT
@p_party_id bigint ,
@p_pk bigint OUT
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

		INSERT INTO CONTACT (PARTY_ID) VALUES (@p_party_id);
		set @p_pk = SCOPE_IDENTITY();
	IF @starttrancount = 0
	COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	RAISERROR (@ErrorMessage, -- Message text.
				@ErrorSeverity, -- Severity.
				@ErrorState -- State.
				);
	END CATCH
END

;

IF EXISTS (SELECT * FROM dbo.SYSOBJECTS WHERE id = object_id('INSERT_CONTACT_DETAIL') AND  OBJECTPROPERTY(id, 'IsProcedure') = 1)
DROP PROC INSERT_CONTACT_DETAIL
;

CREATE PROCEDURE INSERT_CONTACT_DETAIL
@p_contact_id bigint ,
@p_type varchar(50) ,
@p_value varchar(255) ,
@p_pk bigint OUT
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

		INSERT INTO CONTACT_DETAIL (CONTACT_ID,TYPE,VALUE) VALUES (@p_contact_id, @p_type, @p_value);
		set @p_pk = SCOPE_IDENTITY();
	IF @starttrancount = 0
	COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	RAISERROR (@ErrorMessage, -- Message text.
				@ErrorSeverity, -- Severity.
				@ErrorState -- State.
				);
	END CATCH
END

;


IF EXISTS (SELECT * FROM dbo.SYSOBJECTS WHERE id = object_id('INSERT_COMPONENT_MAPPING') AND  OBJECTPROPERTY(id, 'IsProcedure') = 1)
DROP PROC INSERT_COMPONENT_MAPPING
;

CREATE PROCEDURE INSERT_COMPONENT_MAPPING
@p_map_set_id bigint ,
@p_type varchar(10) ,
@p_constant nvarchar(255) ,
@p_pk bigint OUT
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

		INSERT INTO COMPONENT_MAPPING (MAP_SET_ID,TYPE,CONSTANT) VALUES (@p_map_set_id, @p_type, @p_constant);
		set @p_pk = SCOPE_IDENTITY();
	IF @starttrancount = 0
	COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	RAISERROR (@ErrorMessage, -- Message text.
				@ErrorSeverity, -- Severity.
				@ErrorState -- State.
				);
	END CATCH
END

;

IF EXISTS (SELECT * FROM dbo.SYSOBJECTS WHERE id = object_id('INSERT_DESC_SOURCE') AND  OBJECTPROPERTY(id, 'IsProcedure') = 1)
DROP PROC INSERT_DESC_SOURCE
;

CREATE PROCEDURE INSERT_DESC_SOURCE
@p_desc_table varchar(255) ,
@p_related_field varchar(255) ,
@p_desc_field varchar(255) ,
@p_col_id bigint ,
@p_pk bigint OUT
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

		INSERT INTO DESC_SOURCE (DESC_TABLE,RELATED_FIELD,DESC_FIELD,COL_ID) VALUES (@p_desc_table, @p_related_field, @p_desc_field, @p_col_id);
		set @p_pk = SCOPE_IDENTITY();
	IF @starttrancount = 0
	COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	RAISERROR (@ErrorMessage, -- Message text.
				@ErrorSeverity, -- Severity.
				@ErrorState -- State.
				);
	END CATCH
END

;


IF EXISTS (SELECT * FROM dbo.SYSOBJECTS WHERE id = object_id('INSERT_TRANSCODING_RULE') AND  OBJECTPROPERTY(id, 'IsProcedure') = 1)
DROP PROC INSERT_TRANSCODING_RULE
;

CREATE PROCEDURE INSERT_TRANSCODING_RULE
@p_tr_id bigint ,
@p_pk bigint OUT
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

		INSERT INTO TRANSCODING_RULE (TR_ID) VALUES (@p_tr_id);
		set @p_pk = SCOPE_IDENTITY();
	IF @starttrancount = 0
	COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	RAISERROR (@ErrorMessage, -- Message text.
				@ErrorSeverity, -- Severity.
				@ErrorState -- State.
				);
	END CATCH
END

;

IF EXISTS (SELECT * FROM dbo.SYSOBJECTS WHERE id = object_id('INSERT_DATASET') AND  OBJECTPROPERTY(id, 'IsProcedure') = 1)
DROP PROC INSERT_DATASET
;

CREATE PROCEDURE INSERT_DATASET
@p_name varchar(255) ,
@p_query text ,
@p_connection_id bigint ,
@p_description varchar(1024) ,
@p_xml_query text ,
@p_user_id bigint ,
@p_editor_type varchar(250) ,
@p_pk bigint OUT
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

		INSERT INTO DATASET (NAME,QUERY,CONNECTION_ID,DESCRIPTION,XML_QUERY,USER_ID,EDITOR_TYPE) VALUES (@p_name, @p_query, @p_connection_id, @p_description, @p_xml_query, @p_user_id, @p_editor_type);
		set @p_pk = SCOPE_IDENTITY();
	IF @starttrancount = 0
	COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	RAISERROR (@ErrorMessage, -- Message text.
				@ErrorSeverity, -- Severity.
				@ErrorState -- State.
				);
	END CATCH
END

;

IF EXISTS (SELECT * FROM dbo.SYSOBJECTS WHERE id = object_id('INSERT_DATASET_COLUMN') AND  OBJECTPROPERTY(id, 'IsProcedure') = 1)
DROP PROC INSERT_DATASET_COLUMN
;

CREATE PROCEDURE INSERT_DATASET_COLUMN
@p_name varchar(255) ,
@p_description varchar(1024) ,
@p_ds_id bigint ,
@p_lastretrieval datetime ,
@p_pk bigint OUT
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

		INSERT INTO DATASET_COLUMN (NAME,DESCRIPTION,DS_ID,LASTRETRIEVAL) VALUES (@p_name, @p_description, @p_ds_id, @p_lastretrieval);
		set @p_pk = SCOPE_IDENTITY();
	IF @starttrancount = 0
	COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	RAISERROR (@ErrorMessage, -- Message text.
				@ErrorSeverity, -- Severity.
				@ErrorState -- State.
				);
	END CATCH
END

;

IF EXISTS (SELECT * FROM dbo.SYSOBJECTS WHERE id = object_id('INSERT_DB_CONNECTION') AND  OBJECTPROPERTY(id, 'IsProcedure') = 1)
DROP PROC INSERT_DB_CONNECTION
;

CREATE PROCEDURE INSERT_DB_CONNECTION
@p_db_name varchar(1000) ,
@p_db_type varchar(50) ,
@p_name varchar(50) ,
@p_db_password varchar(50) ,
@p_db_port int ,
@p_db_server varchar(100) ,
@p_db_user varchar(50) ,
@p_ado_connection_string varchar(2000) ,
@p_jdbc_connection_string varchar(2000) ,
@p_user_id bigint ,
@p_pk bigint OUT
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

		INSERT INTO DB_CONNECTION (DB_NAME,DB_TYPE,NAME,DB_PASSWORD,DB_PORT,DB_SERVER,DB_USER,ADO_CONNECTION_STRING,JDBC_CONNECTION_STRING,USER_ID) VALUES (@p_db_name, @p_db_type, @p_name, @p_db_password, @p_db_port, @p_db_server, @p_db_user, @p_ado_connection_string, @p_jdbc_connection_string, @p_user_id);
		set @p_pk = SCOPE_IDENTITY();
	IF @starttrancount = 0
	COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	RAISERROR (@ErrorMessage, -- Message text.
				@ErrorSeverity, -- Severity.
				@ErrorState -- State.
				);
	END CATCH
END

;


-- always last
update DB_VERSION SET VERSION = '5.2'
;
