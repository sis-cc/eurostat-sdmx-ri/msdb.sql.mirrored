-- Upgrade script to 5.2 from 5.1.

-- Add new Stored procedures
CREATE OR REPLACE PROCEDURE INSERT_COMPONENT_MAPPING(
p_map_set_id IN number,
p_type IN varchar2,
p_constant IN nvarchar2,
p_pk OUT number)
AS
BEGIN
		INSERT INTO COMPONENT_MAPPING (MAP_SET_ID,TYPE,CONSTANT) VALUES (p_map_set_id, p_type, p_constant) RETURNING MAP_ID INTO p_pk;
END;
/

;

CREATE OR REPLACE PROCEDURE INSERT_CONTACT(
p_party_id IN number,
p_pk OUT number)
AS
BEGIN
		INSERT INTO CONTACT (PARTY_ID) VALUES (p_party_id) RETURNING CONTACT_ID INTO p_pk;
END;
/

;


CREATE OR REPLACE PROCEDURE INSERT_CONTACT_DETAIL(
p_contact_id IN number,
p_type IN varchar2,
p_value IN varchar2,
p_pk OUT number)
AS
BEGIN
		INSERT INTO CONTACT_DETAIL (CONTACT_ID,TYPE,VALUE) VALUES (p_contact_id, p_type, p_value) RETURNING CD_ID INTO p_pk;
END;
/

;


CREATE OR REPLACE PROCEDURE INSERT_DATASET(
p_name IN varchar2,
p_query IN CLOB,
p_connection_id IN number,
p_description IN varchar2,
p_xml_query IN CLOB,
p_user_id IN number,
p_editor_type IN varchar2,
p_pk OUT number)
AS
BEGIN
		INSERT INTO DATASET (NAME,QUERY,CONNECTION_ID,DESCRIPTION,XML_QUERY,USER_ID,EDITOR_TYPE) VALUES (p_name, p_query, p_connection_id, p_description, p_xml_query, p_user_id, p_editor_type) RETURNING DS_ID INTO p_pk;
END;
/

;


CREATE OR REPLACE PROCEDURE INSERT_DATASET_COLUMN(
p_name IN varchar2,
p_description IN varchar2,
p_ds_id IN number,
p_lastretrieval IN timestamp,
p_pk OUT number)
AS
BEGIN
		INSERT INTO DATASET_COLUMN (NAME,DESCRIPTION,DS_ID,LASTRETRIEVAL) VALUES (p_name, p_description, p_ds_id, p_lastretrieval) RETURNING COL_ID INTO p_pk;
END;
/

;


CREATE OR REPLACE PROCEDURE INSERT_DB_CONNECTION(
p_db_name IN varchar2,
p_db_type IN varchar2,
p_name IN varchar2,
p_db_password IN varchar2,
p_db_port IN number,
p_db_server IN varchar2,
p_db_user IN varchar2,
p_ado_connection_string IN varchar2,
p_jdbc_connection_string IN varchar2,
p_user_id IN number,
p_pk OUT number)
AS
BEGIN
		INSERT INTO DB_CONNECTION (DB_NAME,DB_TYPE,NAME,DB_PASSWORD,DB_PORT,DB_SERVER,DB_USER,ADO_CONNECTION_STRING,JDBC_CONNECTION_STRING,USER_ID) VALUES (p_db_name, p_db_type, p_name, p_db_password, p_db_port, p_db_server, p_db_user, p_ado_connection_string, p_jdbc_connection_string, p_user_id) RETURNING CONNECTION_ID INTO p_pk;
END;
/

;

CREATE OR REPLACE PROCEDURE INSERT_DESC_SOURCE(
p_desc_table IN varchar2,
p_related_field IN varchar2,
p_desc_field IN varchar2,
p_col_id IN number,
p_pk OUT number)
AS
BEGIN
		INSERT INTO DESC_SOURCE (DESC_TABLE,RELATED_FIELD,DESC_FIELD,COL_ID) VALUES (p_desc_table, p_related_field, p_desc_field, p_col_id) RETURNING DESC_SOURCE_ID INTO p_pk;
END;
/

;


CREATE OR REPLACE PROCEDURE INSERT_HEADER(
p_test IN number,
p_dataset_agency IN varchar2,
p_df_id IN number,
p_pk OUT number)
AS
BEGIN
		INSERT INTO HEADER (TEST,DATASET_AGENCY,DF_ID) VALUES (p_test, p_dataset_agency, p_df_id) RETURNING HEADER_ID INTO p_pk;
END;
/

;


CREATE OR REPLACE PROCEDURE INSERT_HEADER_LOCALISED_STRING(
p_type IN varchar2,
p_header_id IN number,
p_party_id IN number,
p_contact_id IN number,
p_language IN varchar2,
p_text IN varchar2,
p_pk OUT number)
AS
BEGIN
		INSERT INTO HEADER_LOCALISED_STRING (TYPE,HEADER_ID,PARTY_ID,CONTACT_ID,LANGUAGE,TEXT) VALUES (p_type, p_header_id, p_party_id, p_contact_id, p_language, p_text) RETURNING HLS_ID INTO p_pk;
END;
/

;

CREATE OR REPLACE PROCEDURE INSERT_MAPPING_SET(
p_id IN varchar2,
p_description IN varchar2,
p_ds_id IN number,
p_df_id IN number,
p_user_id IN number,
p_pk OUT number)
AS
BEGIN
		INSERT INTO MAPPING_SET (ID,DESCRIPTION,DS_ID,DF_ID,USER_ID) VALUES (p_id, p_description, p_ds_id, p_df_id, p_user_id) RETURNING MAP_SET_ID INTO p_pk;
END;
/

;



CREATE OR REPLACE PROCEDURE INSERT_PARTY(
p_id IN varchar2,
p_header_id IN number,
p_type IN varchar2,
p_pk OUT number)
AS
BEGIN
		INSERT INTO PARTY (ID,HEADER_ID,TYPE) VALUES (p_id, p_header_id, p_type) RETURNING PARTY_ID INTO p_pk;
END;
/

;


CREATE OR REPLACE PROCEDURE INSERT_TRANSCODING_RULE(
p_tr_id IN number,
p_pk OUT number)
AS
BEGIN
		INSERT INTO TRANSCODING_RULE (TR_ID) VALUES (p_tr_id) RETURNING TR_RULE_ID INTO p_pk;
END;
/

;


-- always last
update DB_VERSION SET VERSION = '5.2'
;
