DROP PROCEDURE IF EXISTS INSERT_DATASET_PROPERTY
;

CREATE PROCEDURE INSERT_DATASET_PROPERTY
(
	IN p_dataset_id bigint, 
	IN p_property_type_enum bigint, 
	IN p_property_value nvarchar(4000),
	OUT p_pk bigint
)
BEGIN
	INSERT 
	  INTO DATASET_PROPERTY 
	       (DATASET_ID, PROPERTY_TYPE_ENUM, PROPERTY_VALUE) 
	VALUES (p_dataset_id, p_property_type_enum, p_property_value);
	
	SET p_pk = LAST_INSERT_ID();
END;
;
