-- Upgrade script to 3.1 from 3.0.
-- add missing sequence
DECLARE
  descsource_id INTEGER := 1;
  seq_exist INTEGER := 0;
  has_data INTEGER := 0;
BEGIN
  -- first check if we have the sequence DESC_SOURCE_SEQ
  SELECT COUNT(*)
  INTO seq_exist
  FROM user_sequences
  WHERE sequence_name = 'DESC_SOURCE_SEQ';
  IF seq_exist = 0 THEN
    -- we do
    -- then check if we have any data
    SELECT COUNT(*) INTO has_data FROM DESC_SOURCE;
    if has_data > 0 THEN
      SELECT MAX(DESC_SOURCE_ID)+1 INTO descsource_id FROM DESC_SOURCE;
    END IF;
    IF descsource_id IS NULL OR descsource_id < 1 THEN
      descsource_id  := 1;
    END IF;
    EXECUTE immediate 'create sequence DESC_SOURCE_SEQ INCREMENT BY 1 start with '||TO_CHAR(descsource_id) ||' NOMAXVALUE MINVALUE 1 NOCYCLE NOCACHE NOORDER';
  END IF;
END;
/

-- make sure we have positive values in DESC_SOURCE_ID as a lot of code assumes that primary key values are always  > 0
update DESC_SOURCE set DESC_SOURCE_ID = DESC_SOURCE_SEQ.NEXTVAL where DESC_SOURCE_ID < 1
;

CREATE OR REPLACE TRIGGER SET_DESC_SOURCE_ID BEFORE
  INSERT ON DESC_SOURCE FOR EACH ROW BEGIN
  SELECT DESC_SOURCE_SEQ.NEXTVAL INTO :NEW.DESC_SOURCE_ID FROM DUAL;
END;
/

-- always last
update DB_VERSION SET VERSION = '3.1'
;
