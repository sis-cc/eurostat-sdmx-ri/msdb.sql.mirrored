insert into LOCALISED_STRING (ART_ID, TYPE, LANGUAGE, TEXT)
select A.ART_ID, 'Name', 'en', A.ID
from ARTEFACT A
where not exists (select 1 from LOCALISED_STRING L where L.ART_ID = A.ART_ID)
;
