insert into LOCALISED_STRING (ITEM_ID, TYPE, LANGUAGE, TEXT)
select I.ITEM_ID, 'Name', 'en', NVL(I.ID, 'Missing ID and Name')
from ITEM I
where not exists (select 1 from LOCALISED_STRING L where L.ITEM_ID = I.ITEM_ID)
;
