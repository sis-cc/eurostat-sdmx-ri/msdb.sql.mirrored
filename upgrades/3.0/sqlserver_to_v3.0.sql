-- Upgrade script to 3.0 from 2.8.


-- change valid_from/to to datetime. Should be first because it can fail

alter table artefact add valid_from_d datetime null
;

alter table artefact add valid_to_d datetime null
;

update artefact set valid_from_d = convert(datetime, valid_from, 127 ) where len(valid_from)>0 and valid_from not like '%[-+][0-2][0-9]:[0-5][0-9]'
;
update artefact set valid_to_d = convert(datetime, valid_to, 127 ) where len(valid_to)>0 and valid_to not like '%[-+][0-2][0-9]:[0-5][0-9]'
;
update artefact set valid_from_d =dateadd(mi,datediff(mi,'00:00',substring(valid_from,len(valid_from) -4,6)),convert(datetime,substring(valid_from,0,len(valid_from) -5), 127)) where valid_from like '%+[0-2][0-9]:[0-5][0-9]'
;
update artefact set valid_to_d =dateadd(mi,datediff(mi,'00:00',substring(valid_to,len(valid_to) -4,6)),convert(datetime,substring(valid_to,0,len(valid_to) -5), 127)) where valid_to like '%+[0-2][0-9]:[0-5][0-9]'
;
update artefact set valid_from_d =dateadd(mi,datediff(mi,substring(valid_from,len(valid_from) -4,6),'00:00'),convert(datetime,substring(valid_from,0,len(valid_from) -5), 127)) where len(valid_from)>0 and valid_from like '%-[0-2][0-9]:[0-5][0-9]'
;
update artefact set valid_to_d =dateadd(mi,datediff(mi,substring(valid_to,len(valid_to) -4,6),'00:00'),convert(datetime,substring(valid_to,0,len(valid_to) -5), 127)) where len(valid_to)>0 and valid_to like '%-[0-2][0-9]:[0-5][0-9]'
;


alter table artefact drop column valid_from
;
alter table artefact drop column valid_to
;

-- ms sql server 2005 doesn't support renaming columns via alter table...

alter table artefact add valid_from datetime null
;

alter table artefact add valid_to datetime null
;

update artefact set valid_from = valid_from_d, valid_to = valid_to_d
;

alter table artefact drop column valid_from_d
;
alter table artefact drop column valid_to_d
;
GO
;
-- VERSION to 3 fields. Note we have to check for SDMX v2.1 incompatible versions before hand. 

IF OBJECT_ID (N'getVersion', N'FN') IS NOT NULL
    DROP FUNCTION getVersion
;
GO
;
create function getVersion (@p_version varchar(50), @particle int)
RETURNS bigint
begin
declare @point int;
declare @currentParticle int;
declare @returnValue varchar(50);
set @point = charindex('.', @p_version);
set @currentParticle = 0;
set @returnValue = null;

if @point = 0 and len(@p_version) > 0 and @particle = 1
begin
    set @returnValue = @p_version;
end;

while @returnValue is null and @point > 0
begin
    SET @currentParticle = @currentParticle + 1;
		if @currentParticle = @particle
			set @returnValue = substring(@p_version,1,@point-1);
		else
        begin
			set @point = charindex('.', @p_version);
			set @p_version = substring(@p_version, @point + 1,100);
		end;
end;

return cast(@returnValue as bigint);
end
;
GO
;
alter table artefact add version1 bigint NOT NULL DEFAULT 0
;

alter table artefact add version2 bigint NOT NULL DEFAULT 0
;

alter table artefact add version3 bigint NULL DEFAULT NULL
;

update artefact set version1 = cast(version as bigint) where version not like '%.%' and len(version) > 0
;

update artefact set version1 = dbo.getVersion(version,1), version2 = dbo.getVersion(version,2) where version like '%.%' and version not like '%.%.%'
;

update artefact set version1 = dbo.getVersion(version,1), version2 = dbo.getVersion(version,2), version3=dbo.getVersion(version,3)  where version like '%.%.%'
;
-- change version temporarily to null-able in order to insert categorisations; we drop last. 
alter table artefact alter column version varchar(50) null
;
GO
;
IF OBJECT_ID (N'versionToString', N'FN') IS NOT NULL
    DROP FUNCTION versionToString
;
GO
;
create function versionToString(@particle1 bigint, @particle2 bigint, @particle3 bigint) 
RETURNS varchar(50)
BEGIN
	declare @version varchar(50);
	if @particle1 is null
		set @particle1 = 0;

	if @particle2 is null
		set @particle2 = 0;
	
	if @particle3 is null
		set @version = cast(@particle1 as varchar) +'.' + cast(@particle2 as varchar);
	else
		set @version = cast(@particle1 as varchar) + '.' + cast(@particle2 as varchar) + '.' + cast(@particle3 as varchar);

	return @version;
END
;
GO
;
IF OBJECT_ID (N'isGreaterVersion', N'FN') IS NOT NULL
    DROP FUNCTION isGreaterVersion
;
GO
;
create function isGreaterVersion(@x1 bigint, @y1 bigint, @z1 bigint, @x2 bigint, @y2 bigint, @z2 bigint)
RETURNS BIT
begin
    declare @returnValue bit;
    set @returnValue = 0;
    if @x1 is null
		set @x1 = 0;
	if @x2 is null
		set @x2 = 0;
	if @y1 is null
		set @y1 = 0;
	if @y2 is null
		set @y2 = 0;
    if ( @x1 > @x2 ) or  ( ( @x1 = @x2 ) and( ( @y1 > @y2 ) or( ( @y1 = @y2 ) and ( (@z1 is not null) and ( (@z2 is null) or (@z1 > @z2) ) ) ) ) )
        set @returnValue = 1;
    
    return @returnValue;
end
;
GO
;
IF OBJECT_ID (N'isEqualVersion', N'FN') IS NOT NULL
    DROP FUNCTION isEqualVersion
;
GO
;
create function isEqualVersion(@x1 bigint, @y1 bigint, @z1 bigint, @x2 bigint, @y2 bigint, @z2 bigint)
RETURNS BIT
begin
    declare @returnValue bit;
    set @returnValue = 0;
    if @x1 is null
		set @x1 = 0;
	if @x2 is null
		set @x2 = 0;
	if @y1 is null
		set @y1 = 0;
	if @y2 is null
		set @y2 = 0;
    if ( @x1 = @x2 ) and ( @y1 = @y2 ) and (((@z1 is null) and (@z2 is null)) or (((@z1 is not null) and (@z2 is not null)) and ( @z1 = @z2 )))
        set @returnValue = 1;
    
    return @returnValue;
end
;
GO
;
CREATE PROCEDURE SPLIT_VERSION(
@p_version varchar(50), 
@p_version1 bigint out, 
@p_version2 bigint out,
@p_version3 bigint out)
AS
BEGIN
declare @point1 int;
declare @point2 int;
declare @l int;
set @p_version1 = dbo.getVersion(@p_version, 1);
set @p_version2 = dbo.getVersion(@p_version, 2);
set @p_version3 = dbo.getVersion(@p_version, 3);
if @p_version1 is null
   set @p_version1 = 0;
    
if @p_version2 is null
   set @p_version2 = 0;
END
;
GO
;
CREATE VIEW ARTEFACT_VIEW AS SELECT ART_ID, ID, AGENCY, dbo.versionToString(VERSION1,VERSION2,VERSION3) as VERSION, VALID_FROM, VALID_TO, IS_FINAL FROM ARTEFACT
;
GO
;

-- add is_partial to item scheme artefacts

alter table codelist add is_partial BIT NOT NULL DEFAULT 0
;

alter table concept_scheme add is_partial BIT NOT NULL DEFAULT 0
;

alter table category_scheme add is_partial BIT NOT NULL DEFAULT 0
;

-- change procedures

IF OBJECT_ID('INSERT_ARTEFACT') IS NOT NULL
DROP PROC INSERT_ARTEFACT
;
GO
;
CREATE PROCEDURE INSERT_ARTEFACT
	@p_id varchar(50),
	@p_version varchar(50),
	@p_agency varchar(50),
	@p_valid_from datetime = NULL,
	@p_valid_to datetime = NULL,
	@p_is_final int = NULL,
	@p_pk bigint out
AS
BEGIN
		SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
    
    DECLARE	@p_version1 bigint,
		@p_version2 bigint,
		@p_version3 bigint;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

        exec SPLIT_VERSION @p_version = @p_version, @p_version1 = @p_version1 OUTPUT, @p_version2 = @p_version2 OUTPUT, @p_version3 = @p_version3 OUTPUT;
	    insert into  ARTEFACT (ID,VERSION1, VERSION2, VERSION3, AGENCY, VALID_FROM, VALID_TO, IS_FINAL) values (@p_id,@p_version1,@p_version2,@p_version3,@p_agency,@p_valid_from,@p_valid_to,@p_is_final);
	    set @p_pk=SCOPE_IDENTITY();
        IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
GO
;
IF OBJECT_ID('INSERT_DSD') IS NOT NULL
DROP PROC INSERT_DSD
;

IF OBJECT_ID('INSERT_DATAFLOW') IS NOT NULL
DROP PROC INSERT_DATAFLOW
;

IF OBJECT_ID('INSERT_CATEGORY_SCHEME') IS NOT NULL
DROP PROC INSERT_CATEGORY_SCHEME
;

IF OBJECT_ID('INSERT_CODELIST') IS NOT NULL
DROP PROC INSERT_CODELIST
;

IF OBJECT_ID('INSERT_CONCEPT_SCHEME') IS NOT NULL
DROP PROC INSERT_CONCEPT_SCHEME
;
GO
;
CREATE PROCEDURE INSERT_DSD
	@p_id varchar(50),
	@p_version varchar(50),
	@p_agency varchar(50),
	@p_valid_from datetime = NULL,
	@p_valid_to datetime= NULL,
	@p_is_final int = NULL,
	@p_pk bigint out
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

	exec INSERT_ARTEFACT @p_id = @p_id,@p_version = @p_version,@p_agency = @p_agency,@p_valid_from = @p_valid_from,@p_valid_to = @p_valid_to,@p_is_final = @p_is_final,@p_pk = @p_pk OUTPUT;
	
	INSERT INTO DSD
           (DSD_ID)
     VALUES
           (@p_pk);


	IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;

GO
;
CREATE PROCEDURE INSERT_DATAFLOW
	@p_id varchar(50),
	@p_version varchar(50),
	@p_agency varchar(50),
	@p_valid_from datetime = NULL,
	@p_valid_to datetime= NULL,
	@p_is_final int = NULL,
	@p_dsd_id bigint,
    @p_map_set_id bigint = null,
	@p_pk bigint out
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

	exec INSERT_ARTEFACT @p_id = @p_id,@p_version = @p_version,@p_agency = @p_agency,@p_valid_from = @p_valid_from,@p_valid_to = @p_valid_to,@p_is_final = @p_is_final,@p_pk = @p_pk OUTPUT;
	
INSERT INTO DATAFLOW
           (DF_ID
           ,DSD_ID
           ,MAP_SET_ID)
     VALUES
           (@p_pk
           ,@p_dsd_id
           ,@p_map_set_id);

	IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
GO
;

CREATE PROCEDURE INSERT_CATEGORY_SCHEME
	@p_id varchar(50),
	@p_version varchar(50),
	@p_agency varchar(50),
	@p_valid_from datetime = NULL,
	@p_valid_to datetime= NULL,
	@p_is_final int = NULL,
	@p_cs_order bigint = 0,
    @p_is_partial bit = 0,
	@p_pk bigint out
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

	exec INSERT_ARTEFACT @p_id = @p_id,@p_version = @p_version,@p_agency = @p_agency,@p_valid_from = @p_valid_from,@p_valid_to = @p_valid_to,@p_is_final = @p_is_final,@p_pk = @p_pk OUTPUT;
	
	INSERT INTO CATEGORY_SCHEME
           (CAT_SCH_ID, CS_ORDER, IS_PARTIAL)
     VALUES
           (@p_pk, @p_cs_order, @p_is_partial);

	IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
GO
;

CREATE PROCEDURE INSERT_CODELIST
	@p_id varchar(50),
	@p_version varchar(50),
	@p_agency varchar(50),
	@p_valid_from datetime = NULL,
	@p_valid_to datetime= NULL,
	@p_is_final int = NULL,
    @p_is_partial bit = 0,
	@p_pk bigint out
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

	exec INSERT_ARTEFACT @p_id = @p_id,@p_version = @p_version,@p_agency = @p_agency,@p_valid_from = @p_valid_from,@p_valid_to = @p_valid_to,@p_is_final = @p_is_final,@p_pk = @p_pk OUTPUT;
	
	INSERT INTO CODELIST
           (CL_ID, IS_PARTIAL)
     VALUES
           (@p_pk, @p_is_partial);

	IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
GO
;

CREATE PROCEDURE INSERT_CONCEPT_SCHEME
	@p_id varchar(50),
	@p_version varchar(50),
	@p_agency varchar(50),
	@p_valid_from datetime = NULL,
	@p_valid_to datetime= NULL,
	@p_is_final int = NULL,
    @p_is_partial bit = 0,
	@p_pk bigint out
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

	exec INSERT_ARTEFACT @p_id = @p_id,@p_version = @p_version,@p_agency = @p_agency,@p_valid_from = @p_valid_from,@p_valid_to = @p_valid_to,@p_is_final = @p_is_final,@p_pk = @p_pk OUTPUT;
	
	INSERT INTO CONCEPT_SCHEME
           (CON_SCH_ID, IS_PARTIAL)
     VALUES
           (@p_pk, @p_is_partial);

	IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
GO
;
IF OBJECT_ID('INSERT_HCL') IS NOT NULL DROP PROC INSERT_HCL
;

GO

;
CREATE PROCEDURE INSERT_HCL
	@p_id varchar(50),
	@p_version varchar(50),
	@p_agency varchar(50),
	@p_valid_from datetime = NULL,
	@p_valid_to datetime= NULL,
	@p_is_final int = NULL,
	@p_pk bigint out
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

	exec INSERT_ARTEFACT @p_id = @p_id,@p_version = @p_version,@p_agency = @p_agency,@p_valid_from = @p_valid_from,@p_valid_to = @p_valid_to,@p_is_final = @p_is_final,@p_pk = @p_pk OUTPUT;
	insert into HCL (HCL_ID) values(@p_pk);

	IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
GO
;

-- categorisations

create table CATEGORISATION (
    CATN_ID bigint NOT NULL,
    ART_ID bigint NOT NULL,
	CAT_ID bigint NOT NULL,
	DC_ORDER BIGINT
)
;
go
;
CREATE PROCEDURE INSERT_CATEGORISATION
	@p_id varchar(50),
	@p_version varchar(50),
	@p_agency varchar(50),
	@p_valid_from datetime = NULL,
	@p_valid_to datetime= NULL,
	@p_is_final int = NULL,
	@p_art_id bigint,
    @p_cat_id bigint,
    @p_dc_order bigint = NULL,
	@p_pk bigint out
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

	exec INSERT_ARTEFACT @p_id = @p_id,@p_version = @p_version,@p_agency = @p_agency,@p_valid_from = @p_valid_from,@p_valid_to = @p_valid_to,@p_is_final = @p_is_final,@p_pk = @p_pk OUTPUT;
	
INSERT INTO CATEGORISATION
           (CATN_ID
           ,ART_ID
           ,CAT_ID
           ,DC_ORDER)
     VALUES
           (@p_pk
           ,@p_art_id
           ,@p_cat_id
           ,@p_dc_order);

	IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
GO
;

-- move data from dataflow_categorisation to categorisation
begin
declare @id varchar(50), @agency varchar(50), @art_id bigint, @cat_id bigint, @dc_order bigint, @pk bigint;

declare cat_cursor cursor for 
select SUBSTRING(REPLACE(AD.ID + '@' + AD.AGENCY + '@' +  AD.VERSION + '@' + AC.ID + '@' + AC.VERSION + '@' + I.ID, '.', ''),1,50) as ID , AC.AGENCY,  AD.ART_ID, C.CAT_ID, dc.dc_order from dataflow_Category dc inner join artefact_view ad on dc.df_id = ad.art_id inner join category c on dc.cat_id = c.cat_id inner join artefact_view ac on c.cat_sch_id = ac.art_id inner join item i on i.item_id = dc.cat_id;

open cat_cursor;

FETCH NEXT from cat_cursor into @id, @agency, @art_id, @cat_id, @dc_order;
while @@FETCH_STATUS <> -1
begin;
	exec INSERT_CATEGORISATION @p_id = @id, @p_version = '1.0', @p_agency = @agency,@p_valid_from = null,@p_valid_to = null, @p_is_final = 1, @p_art_id = @art_id, @p_cat_id = @cat_id, @p_dc_order = @dc_order, @p_pk = @pk OUTPUT;
	exec INSERT_LOCALISED_STRING @p_art_id = @pk, @p_item_id = null, @p_text = @id, @p_type = 'Name', @p_language = 'en', @p_pk = null;
	FETCH NEXT from cat_cursor into @id, @agency, @art_id, @cat_id, @dc_order;

end;

close cat_cursor;
deallocate cat_cursor;
end
;
go
;
ALTER TABLE CATEGORISATION ADD CONSTRAINT PK_CATEGORISATION
	PRIMARY KEY (CATN_ID)
;

ALTER TABLE CATEGORISATION ADD CONSTRAINT FK_CATEGORISATION_ARTEFACT 
	FOREIGN KEY (CATN_ID) REFERENCES ARTEFACT (ART_ID)
	ON DELETE CASCADE
;

ALTER TABLE CATEGORISATION ADD CONSTRAINT FK_ARTEFACT_CATEGORY_CATEGORY 
	FOREIGN KEY (CAT_ID) REFERENCES CATEGORY (CAT_ID)
;

ALTER TABLE CATEGORISATION ADD CONSTRAINT FK_ARTEFACT_CATEGORY_ARTEFACT 
	FOREIGN KEY (ART_ID) REFERENCES ARTEFACT(ART_ID)
;

-- update component
alter table COMPONENT add ID varchar(50) NULL
;


update COMPONENT set ID = 'TIME_PERIOD' where COMPONENT.TYPE = 'TimeDimension'
;

update COMPONENT set ID = 'OBS_VALUE' where COMPONENT.TYPE = 'PrimaryMeasure'
;
update COMPONENT set ID = (SELECT I.ID FROM ITEM I WHERE COMPONENT.CON_ID = I.ITEM_ID) where ID is null 
;

alter table COMPONENT ALTER column ID varchar(50) NOT NULL
;

alter table COMPONENT add CON_SCH_ID bigint NULL
;

ALTER TABLE COMPONENT ADD CONSTRAINT FK_COMPONENT_MEASURE
	FOREIGN KEY (CON_SCH_ID) REFERENCES CONCEPT_SCHEME (CON_SCH_ID)
;

create table ATTR_DIMS ( 
 ATTR_ID    bigint NOT NULL,
 DIM_ID     bigint NOT NULL
)
;

ALTER TABLE ATTR_DIMS ADD CONSTRAINT PK_ATTR_DIMS
 PRIMARY KEY(ATTR_ID, DIM_ID)
;

ALTER TABLE ATTR_DIMS ADD CONSTRAINT FK_ATTR_DIMS_ATTR
	FOREIGN KEY (ATTR_ID) REFERENCES COMPONENT (COMP_ID)
    ON DELETE CASCADE
;

ALTER TABLE ATTR_DIMS ADD CONSTRAINT FK_ATTR_DIMS_DIMS
	FOREIGN KEY (DIM_ID) REFERENCES COMPONENT (COMP_ID)
;


IF OBJECT_ID('INSERT_COMPONENT') IS NOT NULL
DROP PROC INSERT_COMPONENT
;
GO
;
CREATE PROCEDURE INSERT_COMPONENT
	@p_type varchar(50) ,
    @p_id   varchar(50),
	@p_dsd_id bigint,
	@p_con_id bigint,
	@p_cl_id bigint=NULL,
	@p_con_sch_id bigint=NULL,
	@p_is_freq_dim int=NULL,
	@p_is_measure_dim int=NULL,
	@p_att_ass_level varchar(11)=NULL,
	@p_att_status varchar(11)=NULL,
	@p_att_is_time_format int=NULL,
	@p_xs_attlevel_ds int=NULL,
	@p_xs_attlevel_group int=NULL,
	@p_xs_attlevel_section int=NULL,
	@p_xs_attlevel_obs int=NULL,
	@p_xs_measure_code varchar(50)=NULL,

	@p_pk bigint OUT 
AS
BEGIN
	SET NOCOUNT ON;
	insert into COMPONENT (TYPE, ID, DSD_ID, CON_ID, CL_ID, IS_FREQ_DIM, IS_MEASURE_DIM, ATT_ASS_LEVEL, ATT_STATUS, ATT_IS_TIME_FORMAT, XS_ATTLEVEL_DS, XS_ATTLEVEL_GROUP, XS_ATTLEVEL_SECTION, XS_ATTLEVEL_OBS, XS_MEASURE_CODE, CON_SCH_ID) 
		values (@p_type, @p_id, @p_dsd_id, @p_con_id, @p_cl_id, @p_is_freq_dim, @p_is_measure_dim, @p_att_ass_level, @p_att_status, @p_att_is_time_format, @p_xs_attlevel_ds, @p_xs_attlevel_group, @p_xs_attlevel_section, @p_xs_attlevel_obs, @p_xs_measure_code, @p_con_sch_id);
	set @p_pk = SCOPE_IDENTITY();
END
;
GO
;

--- HCL related procedures expect HCL it self

IF OBJECT_ID('INSERT_HCL_CODE') IS NOT NULL
DROP PROC INSERT_HCL_CODE
;
GO
;
CREATE PROCEDURE INSERT_HCL_CODE
	@p_id varchar(50),
	@p_version varchar(50),
	@p_agency varchar(50),
	@p_valid_from datetime = NULL,
	@p_valid_to datetime= NULL,
	@p_is_final int = NULL,
	@p_parent_hcode_id bigint = null,
	@p_lcd_id bigint,
	@p_h_id bigint,
	@p_level_id bigint = null,
	@p_pk bigint out
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

	exec INSERT_ARTEFACT @p_id = @p_id,@p_version = @p_version,@p_agency = @p_agency,@p_valid_from = @p_valid_from,@p_valid_to = @p_valid_to,@p_is_final = @p_is_final,@p_pk = @p_pk OUTPUT;
	
	INSERT INTO HCL_CODE
           (HCODE_ID
           ,PARENT_HCODE_ID
           ,LCD_ID
           ,H_ID
           ,LEVEL_ID)
	VALUES
		(@p_pk
        ,@p_parent_hcode_id
        ,@p_lcd_id
        ,@p_h_id
         ,@p_level_id);

	IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
GO
;
IF OBJECT_ID('INSERT_HIERACHY') IS NOT NULL
DROP PROC INSERT_HIERACHY
;
GO
;
CREATE PROCEDURE INSERT_HIERACHY
	@p_id varchar(50),
	@p_version varchar(50),
	@p_agency varchar(50),
	@p_valid_from datetime = NULL,
	@p_valid_to datetime= NULL,
	@p_is_final int = NULL,
	@p_hcl_id bigint,
	@p_pk bigint out
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

	exec INSERT_ARTEFACT @p_id = @p_id,@p_version = @p_version,@p_agency = @p_agency,@p_valid_from = @p_valid_from,@p_valid_to = @p_valid_to,@p_is_final = @p_is_final,@p_pk = @p_pk OUTPUT;
	
	INSERT INTO HIERARCHY
           (H_ID
           ,HCL_ID)
     VALUES
           (@p_pk
           ,@p_hcl_id);

	IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
GO
;
IF OBJECT_ID('INSERT_HLEVEL') IS NOT NULL
DROP PROC INSERT_HLEVEL
;
GO
;
CREATE PROCEDURE INSERT_HLEVEL
	@p_id varchar(50),
	@p_version varchar(50),
	@p_agency varchar(50),
	@p_valid_from datetime = NULL,
	@p_valid_to datetime= NULL,
	@p_is_final int = NULL,
	@p_h_id bigint,
	@p_parent_level_id bigint = null,
	@p_pk bigint out
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

	exec INSERT_ARTEFACT @p_id = @p_id,@p_version = @p_version,@p_agency = @p_agency,@p_valid_from = @p_valid_from,@p_valid_to = @p_valid_to,@p_is_final = @p_is_final,@p_pk = @p_pk OUTPUT;
	
	INSERT INTO HLEVEL
           (LEVEL_ID
           ,H_ID
           ,PARENT_LEVEL_ID)
     VALUES
           (@p_pk
           ,@p_h_id
           ,@p_parent_level_id);

	IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
GO
;

-- TIME TRANSCODING
create table TIME_TRANSCODING ( 
  TR_ID bigint NOT NULL,
  FREQ varchar(5) NOT NULL,
  YEAR_COL_ID bigint NULL,
  PERIOD_COL_ID bigint NULL,
  DATE_COL_ID bigint NULL,
  EXPRESSION varchar(150) NULL
)
;
insert into TIME_TRANSCODING (TR_ID, FREQ, YEAR_COL_ID, PERIOD_COL_ID, EXPRESSION)
select 
tr_id,
case charindex('freq',expression) when 0 then null else substring(expression, charindex('freq',expression) + 5, charindex(';', expression,charindex('freq',expression)) - (charindex('freq',expression) + 5)) END as freq,
case charindex('year', expression) when 0 then null else cast(substring(expression, charindex('year', expression) + 5,charindex(',', expression,charindex('year',expression) + 5 )- (charindex('year', expression) + 5)) as int) END as year_col_id,
case charindex('period', expression) when 0 then null else cast(substring(expression, charindex('period', expression) + 7, charindex(',', expression,charindex('period',expression) + 7 ) - (charindex('period', expression) + 7)) as int) END as period_col_id,
substring(expression, charindex(';',expression) + 1,150) as expression
from transcoding where expression is not null and len(expression) > 0
; 

update TIME_TRANSCODING set EXPRESSION = REPLACE(EXPRESSION, 'year='+cast(YEAR_COL_ID as varchar)+',', 'year=') where YEAR_COL_ID is not null
;

update TIME_TRANSCODING set EXPRESSION = REPLACE(EXPRESSION, 'period='+cast(PERIOD_COL_ID as varchar)+',' ,'period=') where PERIOD_COL_ID is not null
;

update TIME_TRANSCODING SET DATE_COL_ID = (SELECT C.COL_ID  FROM TRANSCODING T inner join com_col_mapping_column C on T.MAP_ID = C.MAP_ID WHERE  T.TR_ID = TIME_TRANSCODING.TR_ID)  WHERE EXPRESSION like '%datetime=1%'
;


ALTER TABLE TIME_TRANSCODING ADD CONSTRAINT PK_TIME_TRANSCODING
    PRIMARY KEY(TR_ID, FREQ)
;

ALTER TABLE TIME_TRANSCODING ADD CONSTRAINT FK_TIME_TRANSCODING_TRAN
FOREIGN KEY (TR_ID) REFERENCES TRANSCODING (TR_ID)
    ON DELETE CASCADE
;

ALTER TABLE TIME_TRANSCODING ADD CONSTRAINT FK_TIME_TRANSCODING_YEAR
    FOREIGN KEY (YEAR_COL_ID) REFERENCES DATASET_COLUMN (COL_ID)
;

ALTER TABLE TIME_TRANSCODING ADD CONSTRAINT FK_TIME_TRANSCODING_PERIOD
    FOREIGN KEY (PERIOD_COL_ID) REFERENCES DATASET_COLUMN (COL_ID)
;

ALTER TABLE TIME_TRANSCODING ADD CONSTRAINT FK_TIME_TRANSCODING_DATE
    FOREIGN KEY (DATE_COL_ID) REFERENCES DATASET_COLUMN (COL_ID)
;


-- migrate dsd code descriptions to names
update LOCALISED_STRING set type = 'Name' WHERE ITEM_ID IN (select LCD_ID from DSD_CODE) and type='Desc'
;

-- erase old transcoding.expression
update TRANSCODING set EXPRESSION = null where TR_ID in (select distinct TR_ID from TIME_TRANSCODING)
;
-- do we drop old VERSION
alter table artefact drop column version
;

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('DF_ID') AND OBJECTPROPERTY(id, 'IsForeignKey') = 1)
ALTER TABLE DATAFLOW_CATEGORY DROP CONSTRAINT DF_ID
;

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('FK_DATAFLOW_CATEGORY_CATEGORY') AND OBJECTPROPERTY(id, 'IsForeignKey') = 1)
ALTER TABLE DATAFLOW_CATEGORY DROP CONSTRAINT FK_DATAFLOW_CATEGORY_CATEGORY
;

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('FK_DF_ID') AND OBJECTPROPERTY(id, 'IsForeignKey') = 1)
ALTER TABLE DATAFLOW_CATEGORY DROP CONSTRAINT FK_DF_ID
;

IF EXISTS (SELECT * FROM dbo.SYSOBJECTS WHERE id = object_id('DATAFLOW_CATEGORY') AND  OBJECTPROPERTY(id, 'IsUserTable') = 1)
DROP TABLE DATAFLOW_CATEGORY
;

-- always last
update DB_VERSION SET VERSION = '3.0'
;
