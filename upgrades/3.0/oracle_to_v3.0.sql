-- Upgrade script to 3.0 from 2.8.

-- valid_from/to switch to timestamp
alter table artefact add ( valid_from_d timestamp null, valid_to_d timestamp null)
;

update artefact set valid_from_d = SYS_EXTRACT_UTC(to_timestamp_tz(valid_from, 'YYYY-MM-DD"T"HH24:MI:SS.ff3TZH:TZM')), valid_to_d = SYS_EXTRACT_UTC(to_timestamp_tz(valid_to, 'YYYY-MM-DD"T"HH24:MI:SS.ff3TZH:TZM'))
;

alter table artefact drop column valid_from
;

alter table artefact drop column valid_to
;

alter table artefact rename column valid_from_d to valid_from
;

alter table artefact rename column valid_to_d to valid_to
;

-- VERSION to 3 fields. Note we have to check for SDMX v2.1 incompatible versions before hand. 

alter table artefact add (  version1 number(18) DEFAULT 0 NOT NULL, version2 number(18) DEFAULT 0 NOT NULL, version3 number(18) default NULL NULL)
;
create or replace 
function getVersion(p_version in varchar2, particle in number) 
RETURN number DETERMINISTIC
AS
point number;
currentParticle number;
returnValue varchar2(50);
tmp_version varchar2(50);
BEGIN
	point := instr(p_version, '.');
	currentParticle := 0;
	returnValue := null;

	if point = 0 and length(p_version) > 0 and particle = 1 then
		returnValue := p_version;
	end if;
    tmp_version := p_version;
	WHILE returnValue is null and point > 0 LOOP
		currentParticle := currentParticle + 1;
		if currentParticle = particle then
			returnValue := substr(tmp_version,1,point - 1);
		else
			point := instr(tmp_version, '.');
			tmp_version := substr(tmp_version, point + 1);
		end if;
	END LOOP;
	return to_number(returnValue);
END;
/
update artefact set version1 = to_number(version) where REGEXP_LIKE(version,'^[0-9]+$')
;
update artefact set version1 = getVersion(version,1), version2 = getVersion(version,2)  where REGEXP_LIKE(version,'^[0-9]+\.[0-9]+$')
;
update artefact set version1 = getVersion(version,1), version2 = getVersion(version,2), version3 = getVersion(version,3) where REGEXP_LIKE(version,'^[0-9]+\.[0-9]+\.[0-9]+$')
;
create or replace function versionToString(particle1 in number, particle2 in number, particle3 in number) 
RETURN varchar2 DETERMINISTIC
AS
returnVersion varchar2(50);
p1 number;
p2 number;
BEGIN
	p1 := particle1;
  p2 := particle2;
	if p1 is null then
		p1 := 0;
	end if;
	if p2 is null then
		p2 := 0;
	end if;
	
	if particle3 is null then
		returnVersion := p1 || '.' || p2;
	else
		returnVersion := p1 || '.' || p2 || '.' || particle3; 
	end if;

	return returnVersion;
END;
/
create or replace 
function isGreaterVersion(x1 in number, y1  in number, z1  in number, x2  in number, y2  in number, z2  in number)
RETURN NUMBER DETERMINISTIC
as
returnValue number;
xx1 number;
xx2 number;
yy1 number;
yy2 number;
BEGIN
  xx1 := x1;
  xx2 := x2;
  yy1 := y1;
  yy2 := y2;
	if xx1 is null then
		xx1 := 0;
	end if;
	if xx2 is null then
		xx2 := 0;
	end if;
	if yy1 is null then
		yy1 := 0;
	end if;
	if yy2 is null then
		yy2 := 0;
	end if;
  
  if ( xx1 > xx2 ) or  ( ( xx1 = xx2 ) and( ( yy1 > yy2 ) or( ( yy1 = yy2 ) and ( (z1 is not null) and ( (z2 is null) or (z1 > z2) ) ) ) ) ) then
    returnValue := 1;
  else
    returnValue := 0;  
  end if;
  return returnValue;
END;
/
create or replace function isEqualVersion(x1 in number, y1  in number, z1  in number, x2  in number, y2  in number, z2  in number)
RETURN NUMBER DETERMINISTIC
as
returnValue number;
xx1 number;
xx2 number;
yy1 number;
yy2 number;
BEGIN
  xx1 := x1;
  xx2 := x2;
  yy1 := y1;
  yy2 := y2;
	if xx1 is null then
		xx1 := 0;
	end if;
	if xx2 is null then
		xx2 := 0;
	end if;
	if yy1 is null then
		yy1 := 0;
	end if;
	if yy2 is null then
		yy2 := 0;
	end if;
  
  if ( xx1 = xx2 ) and ( yy1 = yy2 ) and (((z1 is null) and (z2 is null)) or (((z1 is not null) and (z2 is not null)) and ( z1 = z2 ))) then
    returnValue := 1;
  else
    returnValue := 0;  
  end if;
  return returnValue;
END;
/
alter table artefact modify ( version varchar2(50) null )
;

create or replace 
procedure SPLIT_VERSION(
    p_version IN varchar2,
    p_version1 OUT NUMBER,
    p_version2 OUT NUMBER,
    p_version3 OUT NUMBER
)
IS
point1 number;
point2 number;
BEGIN
    p_version1 := getVersion(p_version,1);
    p_version2 := getVersion(p_version,2);
    p_version3 := getVersion(p_version,3);
    if p_version1 is null then
         p_version1 := 0;
    end if;
    if p_version2 is null then
        p_version2 := 0;
    end if;
END;
/


CREATE OR REPLACE VIEW ARTEFACT_VIEW AS SELECT ART_ID, ID, AGENCY, versionToString(VERSION1,VERSION2,VERSION3) as VERSION, VALID_FROM, VALID_TO, IS_FINAL FROM ARTEFACT
;


-- add is_partial to item scheme artefacts
alter table codelist add ( is_partial NUMBER(1) DEFAULT 0 NOT NULL )
;

alter table concept_scheme add ( is_partial NUMBER(1) DEFAULT 0 NOT NULL )
;

alter table category_scheme add ( is_partial NUMBER(1) DEFAULT 0 NOT NULL)
;


-- update stored proc

CREATE OR REPLACE PROCEDURE INSERT_ARTEFACT(
	p_id IN varchar2,
	p_version IN varchar2,
	p_agency IN varchar2,
	p_valid_from IN  timestamp DEFAULT NULL,
	p_valid_to IN  timestamp DEFAULT NULL,
	p_is_final IN number DEFAULT NULL,
	p_pk OUT NUMBER)
AS
  P_VERSION1 NUMBER;
  P_VERSION2 NUMBER;
  P_VERSION3 NUMBER;
BEGIN
    SPLIT_VERSION(
        P_VERSION => P_VERSION,
        P_VERSION1 => P_VERSION1,
        P_VERSION2 => P_VERSION2,
        P_VERSION3 => P_VERSION3
    );
   	insert into ARTEFACT (ART_ID, ID,VERSION1, VERSION2, VERSION3, AGENCY, VALID_FROM, VALID_TO, IS_FINAL) values (ARTEFACT_SEQ.nextval, p_id,p_version1,p_version2,p_version3,p_agency,p_valid_from,p_valid_to,p_is_final);
	select ARTEFACT_SEQ.currval into p_pk from dual;
END;
/

CREATE OR REPLACE PROCEDURE INSERT_DSD(
	p_id IN varchar2,
	p_version IN varchar2,
	p_agency IN varchar2,
	p_valid_from IN  timestamp DEFAULT NULL,
	p_valid_to IN  timestamp DEFAULT NULL,
	p_is_final IN number DEFAULT NULL,
	p_pk OUT NUMBER)
AS
BEGIN
    INSERT_ARTEFACT(p_id => p_id,p_version => p_version,p_agency => p_agency,p_valid_from => p_valid_from,p_valid_to => p_valid_to,p_is_final => p_is_final,p_pk => p_pk);
	
	INSERT INTO DSD
           (DSD_ID)
     VALUES
           (p_pk);
END;
/


CREATE OR REPLACE PROCEDURE INSERT_DATAFLOW(
	p_id IN varchar2,
	p_version IN varchar2,
	p_agency IN varchar2,
	p_valid_from IN  timestamp DEFAULT NULL,
	p_valid_to IN  timestamp DEFAULT NULL,
	p_is_final IN number DEFAULT NULL,
	p_dsd_id IN number,
	p_map_set_id IN number DEFAULT NULL,
	p_pk OUT NUMBER)
AS
BEGIN

	INSERT_ARTEFACT(p_id => p_id,p_version => p_version,p_agency => p_agency,p_valid_from => p_valid_from,p_valid_to => p_valid_to,p_is_final => p_is_final,p_pk => p_pk);
	
INSERT INTO DATAFLOW
           (DF_ID
           ,DSD_ID
           ,MAP_SET_ID)
     VALUES
           (p_pk
           ,p_dsd_id
           ,p_map_set_id);
END;
/


CREATE OR REPLACE PROCEDURE INSERT_CATEGORY_SCHEME(
	p_id IN varchar2,
	p_version IN varchar2,
	p_agency IN varchar2,
	p_valid_from IN  timestamp DEFAULT NULL,
	p_valid_to IN  timestamp DEFAULT NULL,
	p_is_final IN number DEFAULT NULL,
	p_cs_order IN number default 0,
    p_is_partial IN number default 0,
	p_pk OUT NUMBER)
AS
BEGIN

	INSERT_ARTEFACT(p_id => p_id,p_version => p_version,p_agency => p_agency,p_valid_from => p_valid_from,p_valid_to => p_valid_to,p_is_final => p_is_final,p_pk => p_pk);
	
	INSERT INTO CATEGORY_SCHEME
           (CAT_SCH_ID, CS_ORDER, IS_PARTIAL)
     VALUES
           (p_pk, p_cs_order, p_is_partial);
END;
/


CREATE OR REPLACE PROCEDURE INSERT_CODELIST(
	p_id IN varchar2,
	p_version IN varchar2,
	p_agency IN varchar2,
	p_valid_from IN  timestamp DEFAULT NULL,
	p_valid_to IN  timestamp DEFAULT NULL,
	p_is_final IN number DEFAULT NULL,
    p_is_partial IN number default 0,
	p_pk OUT NUMBER)
AS
BEGIN

	INSERT_ARTEFACT(p_id => p_id,p_version => p_version,p_agency => p_agency,p_valid_from => p_valid_from,p_valid_to => p_valid_to,p_is_final => p_is_final,p_pk => p_pk);
	
	INSERT INTO CODELIST
           (CL_ID, IS_PARTIAL)
     VALUES
           (p_pk, p_is_partial);
END;
/


CREATE OR REPLACE PROCEDURE INSERT_CONCEPT_SCHEME(
	p_id IN varchar2,
	p_version IN varchar2,
	p_agency IN varchar2,
	p_valid_from IN  timestamp DEFAULT NULL,
	p_valid_to IN  timestamp DEFAULT NULL,
	p_is_final IN number DEFAULT NULL,
    p_is_partial IN number default 0,
	p_pk OUT NUMBER)
AS
BEGIN
	INSERT_ARTEFACT(p_id => p_id,p_version => p_version,p_agency => p_agency,p_valid_from => p_valid_from,p_valid_to => p_valid_to,p_is_final => p_is_final,p_pk => p_pk);
	
	INSERT INTO CONCEPT_SCHEME
           (CON_SCH_ID, IS_PARTIAL)
     VALUES
           (p_pk, p_is_partial);
END;
/

CREATE OR REPLACE PROCEDURE INSERT_HCL(
	p_id IN varchar2,
	p_version IN varchar2,
	p_agency IN varchar2,
	p_valid_from IN  timestamp DEFAULT NULL,
	p_valid_to IN  timestamp DEFAULT NULL,
	p_is_final IN number DEFAULT NULL,
	p_pk OUT NUMBER)
AS
BEGIN

	INSERT_ARTEFACT(p_id => p_id,p_version => p_version,p_agency => p_agency,p_valid_from => p_valid_from,p_valid_to => p_valid_to,p_is_final => p_is_final,p_pk => p_pk);
	insert into HCL (HCL_ID) values(p_pk);
END;
/


-- hcl related proc expect hcl it self

CREATE OR REPLACE PROCEDURE INSERT_HCL_CODE(
	p_id IN varchar2,
	p_version IN varchar2,
	p_agency IN varchar2,
	p_valid_from IN  timestamp DEFAULT NULL,
	p_valid_to IN  timestamp DEFAULT NULL,
	p_is_final IN number DEFAULT NULL,
	p_parent_hcode_id IN number DEFAULT NULL,
	p_lcd_id IN number,
	p_h_id IN number,
	p_level_id IN number DEFAULT NULL,
	p_pk OUT NUMBER)
AS
BEGIN

	INSERT_ARTEFACT(p_id => p_id,p_version => p_version,p_agency => p_agency,p_valid_from => p_valid_from,p_valid_to => p_valid_to,p_is_final => p_is_final,p_pk => p_pk);
	
	INSERT INTO HCL_CODE
           (HCODE_ID
           ,PARENT_HCODE_ID
           ,LCD_ID
           ,H_ID
           ,LEVEL_ID)
	VALUES
		(p_pk
        ,p_parent_hcode_id
        ,p_lcd_id
        ,p_h_id
         ,p_level_id);
END;
/


CREATE OR REPLACE PROCEDURE INSERT_HIERACHY(
	p_id IN varchar2,
	p_version IN varchar2,
	p_agency IN varchar2,
	p_valid_from IN  timestamp DEFAULT NULL,
	p_valid_to IN  timestamp DEFAULT NULL,
	p_is_final IN number DEFAULT NULL,
	p_hcl_id IN number,
	p_pk OUT NUMBER)
AS
BEGIN

	INSERT_ARTEFACT(p_id => p_id,p_version => p_version,p_agency => p_agency,p_valid_from => p_valid_from,p_valid_to => p_valid_to,p_is_final => p_is_final,p_pk => p_pk);
	
	INSERT INTO HIERARCHY
           (H_ID
           ,HCL_ID)
     VALUES
           (p_pk
           ,p_hcl_id);
END;
/


CREATE OR REPLACE PROCEDURE INSERT_HLEVEL(
	p_id IN varchar2,
	p_version IN varchar2,
	p_agency IN varchar2,
	p_valid_from IN  timestamp DEFAULT NULL,
	p_valid_to IN  timestamp DEFAULT NULL,
	p_is_final IN number DEFAULT NULL,
	p_h_id IN number,
	p_parent_level_id IN number DEFAULT NULL,
	p_pk OUT NUMBER)
AS
BEGIN

	INSERT_ARTEFACT(p_id => p_id,p_version => p_version,p_agency => p_agency,p_valid_from => p_valid_from,p_valid_to => p_valid_to,p_is_final => p_is_final,p_pk => p_pk);
	
	INSERT INTO HLEVEL
           (LEVEL_ID
           ,H_ID
           ,PARENT_LEVEL_ID)
     VALUES
           (p_pk
           ,p_h_id
           ,p_parent_level_id);
END;
/

-- categorisations

create table CATEGORISATION (
    CATN_ID NUMBER(18) NOT NULL,
    ART_ID NUMBER(18) NOT NULL,
	CAT_ID NUMBER(18) NOT NULL,
    DC_ORDER NUMBER(18)
)
;

CREATE OR REPLACE PROCEDURE INSERT_CATEGORISATION(
	p_id IN varchar2,
	p_version IN varchar2,
	p_agency IN varchar2,
	p_valid_from IN  timestamp DEFAULT NULL,
	p_valid_to IN  timestamp DEFAULT NULL,
	p_is_final IN number DEFAULT NULL,
	p_art_id IN number,
	p_cat_id IN number,
	p_dc_order IN number DEFAULT NULL,
	p_pk OUT NUMBER)
AS
BEGIN

	INSERT_ARTEFACT(p_id => p_id,p_version => p_version,p_agency => p_agency,p_valid_from => p_valid_from,p_valid_to => p_valid_to,p_is_final => p_is_final,p_pk => p_pk);
	
INSERT INTO CATEGORISATION
           (CATN_ID
           ,ART_ID
           ,CAT_ID
           ,DC_ORDER)
     VALUES
           (p_pk
           ,p_art_id
           ,p_cat_id
           ,p_dc_order);
END;
/


declare 
cid varchar2(50);
agency varchar2(50); 
art_id number; 
cat_id number;
dc_order number;
pk number;
lpk number;
cursor cat_cursor is
select substr(REPLACE(AD.ID || '@' || AD.AGENCY || '@' ||  AD.VERSION || '@' || AC.ID || '@' || AC.VERSION || '@' || I.ID, '.', ''),1,50) cid , AC.AGENCY,  AD.ART_ID, C.CAT_ID, dc.dc_order
from dataflow_Category dc, artefact ad ,category c, artefact ac,  item i  
where dc.df_id = ad.art_id and dc.cat_id = c.cat_id and c.cat_sch_id = ac.art_id and i.item_id = dc.cat_id ;
begin
open cat_cursor;

FETCH cat_cursor into cid, agency, art_id, cat_id, dc_order;
while cat_cursor%FOUND
loop
  INSERT_CATEGORISATION(p_id => cid, p_version => '1.0', p_agency => agency,p_valid_from => null,p_valid_to => null, p_is_final => 1, p_art_id => art_id, p_cat_id => cat_id, p_dc_order => dc_order, p_pk => pk);
	INSERT_LOCALISED_STRING(p_art_id => pk, p_item_id => null, p_text => cid, p_type => 'Name', p_language => 'en', p_pk => lpk);
  FETCH cat_cursor into cid, agency, art_id, cat_id, dc_order;
end loop;

close cat_cursor;
end;
/


ALTER TABLE CATEGORISATION ADD CONSTRAINT PK_CATEGORISATION
	PRIMARY KEY (CATN_ID)
;
ALTER TABLE CATEGORISATION ADD CONSTRAINT FK_CATEGORISATION_ARTEFACT 
	FOREIGN KEY (CATN_ID) REFERENCES ARTEFACT (ART_ID)
	ON DELETE CASCADE
;

ALTER TABLE CATEGORISATION ADD CONSTRAINT FK_ARTEFACT_CATEGORY_CATEGORY 
	FOREIGN KEY (CAT_ID) REFERENCES CATEGORY (CAT_ID)
	ON DELETE CASCADE
;

ALTER TABLE CATEGORISATION ADD CONSTRAINT FK_ARTEFACT_CATEGORY_ARTEFACT 
	FOREIGN KEY (ART_ID) REFERENCES ARTEFACT (ART_ID)
	ON DELETE CASCADE
;


-- update component

alter table COMPONENT add ( ID varchar2(50) NULL )
;


update COMPONENT set ID = 'TIME_PERIOD' where COMPONENT.TYPE = 'TimeDimension'
;

update COMPONENT set ID = 'OBS_VALUE' where COMPONENT.TYPE = 'PrimaryMeasure'
;
update COMPONENT set ID = (SELECT I.ID FROM ITEM I WHERE COMPONENT.CON_ID = I.ITEM_ID) where ID is null 
;


alter table COMPONENT modify ( ID varchar2(50) not null )
;

alter table COMPONENT add ( CON_SCH_ID NUMBER(18) NULL )
;

ALTER TABLE COMPONENT ADD CONSTRAINT FK_COMPONENT_MEASURE
	FOREIGN KEY (CON_SCH_ID) REFERENCES CONCEPT_SCHEME (CON_SCH_ID)
;

create table ATTR_DIMS ( 
 ATTR_ID    number(18) NOT NULL,
 DIM_ID     number(18) NOT NULL
)
;

ALTER TABLE ATTR_DIMS ADD CONSTRAINT PK_ATTR_DIMS
 PRIMARY KEY(ATTR_ID, DIM_ID)
;

ALTER TABLE ATTR_DIMS ADD CONSTRAINT FK_ATTR_DIMS_ATTR
	FOREIGN KEY (ATTR_ID) REFERENCES COMPONENT (COMP_ID)
    ON DELETE CASCADE
;

ALTER TABLE ATTR_DIMS ADD CONSTRAINT FK_ATTR_DIMS_DIMS
	FOREIGN KEY (DIM_ID) REFERENCES COMPONENT (COMP_ID)
    ON DELETE CASCADE
;


CREATE OR REPLACE PROCEDURE INSERT_COMPONENT(
	p_type IN varchar2,
	p_id IN varchar2,
	p_dsd_id IN number,
	p_con_id IN number,
	p_cl_id IN number DEFAULT NULL,
	p_con_sch_id IN number DEFAULT NULL,
	p_is_freq_dim IN number DEFAULT NULL,
	p_is_measure_dim IN number DEFAULT NULL,
	p_att_ass_level IN varchar2 DEFAULT NULL,
	p_att_status IN varchar2 DEFAULT NULL,
	p_att_is_time_format IN number DEFAULT NULL,
	p_xs_attlevel_ds IN number DEFAULT NULL,
	p_xs_attlevel_group IN number DEFAULT NULL,
	p_xs_attlevel_section IN number DEFAULT NULL,
	p_xs_attlevel_obs IN number DEFAULT NULL,
	p_xs_measure_code IN varchar2 DEFAULT NULL,
	p_pk OUT NUMBER) 
AS
BEGIN
	insert into COMPONENT (COMP_ID, ID, TYPE, DSD_ID, CON_ID, CL_ID, IS_FREQ_DIM, IS_MEASURE_DIM, ATT_ASS_LEVEL, ATT_STATUS, ATT_IS_TIME_FORMAT, XS_ATTLEVEL_DS, XS_ATTLEVEL_GROUP, XS_ATTLEVEL_SECTION, XS_ATTLEVEL_OBS, XS_MEASURE_CODE, CON_SCH_ID) 
		values (COMPONENT_SEQ.nextval, p_id, p_type, p_dsd_id, p_con_id, p_cl_id, p_is_freq_dim, p_is_measure_dim, p_att_ass_level, p_att_status, p_att_is_time_format, p_xs_attlevel_ds, p_xs_attlevel_group, p_xs_attlevel_section, p_xs_attlevel_obs, p_xs_measure_code, p_con_sch_id);
	select COMPONENT_SEQ.currval into p_pk from dual;
END;
/


-- TIME TRANSCODING
create table TIME_TRANSCODING ( 
  TR_ID number(18) NOT NULL,
  FREQ varchar2(5) NOT NULL,
  YEAR_COL_ID number(18) NULL,
  PERIOD_COL_ID number(18) NULL,
  DATE_COL_ID number(18) NULL,
  EXPRESSION varchar2(150) NULL
)
;

insert into TIME_TRANSCODING (TR_ID, FREQ, YEAR_COL_ID, PERIOD_COL_ID, EXPRESSION)
select 
tr_id,
regexp_replace(expression,'^.*freq=([A-Za-z]);.*$','\1') as freq,
regexp_replace(expression,'^freq=[^;];(year=([0-9]+),)?.*$','\2') as year_col_id,
regexp_replace(expression,'^freq=[^;];(year=[[:digit:],-]+;)?(period=([:digit:]]+),)?.*$','\3') as period_col_id,
regexp_replace(expression,'^freq=[^;];(datetime=[01];)?((year=)[[:digit:]]+,([[:digit:],-]+;)((period=)[[:digit:]]+,([[:digit:],-]+;))?)?$','\1\3\4\6\7') as expression
from transcoding where expression is not null
;

update TIME_TRANSCODING SET DATE_COL_ID = (SELECT C.COL_ID  FROM TRANSCODING T inner join com_col_mapping_column C on T.MAP_ID = C.MAP_ID WHERE  T.TR_ID = TIME_TRANSCODING.TR_ID)  WHERE EXPRESSION like '%datetime=1%'
;
ALTER TABLE TIME_TRANSCODING ADD CONSTRAINT PK_TIME_TRANSCODING
    PRIMARY KEY(TR_ID, FREQ)
;

ALTER TABLE TIME_TRANSCODING ADD CONSTRAINT FK_TIME_TRANSCODING_TRAN
FOREIGN KEY (TR_ID) REFERENCES TRANSCODING (TR_ID)
    ON DELETE CASCADE
;

ALTER TABLE TIME_TRANSCODING ADD CONSTRAINT FK_TIME_TRANSCODING_YEAR
    FOREIGN KEY (YEAR_COL_ID) REFERENCES DATASET_COLUMN (COL_ID)
;

ALTER TABLE TIME_TRANSCODING ADD CONSTRAINT FK_TIME_TRANSCODING_PERIOD
    FOREIGN KEY (PERIOD_COL_ID) REFERENCES DATASET_COLUMN (COL_ID)
;

ALTER TABLE TIME_TRANSCODING ADD CONSTRAINT FK_TIME_TRANSCODING_DATE
    FOREIGN KEY (DATE_COL_ID) REFERENCES DATASET_COLUMN (COL_ID)
;

-- migrate dsd code descriptions to names
update LOCALISED_STRING set type = 'Name' WHERE ITEM_ID IN (select LCD_ID from DSD_CODE) and type='Desc'
;

-- erase old transcoding.expression
update TRANSCODING set EXPRESSION = null where TR_ID in (select distinct TR_ID from TIME_TRANSCODING)
;
-- do we drop old VERSION

alter table artefact drop column version
;

-- always last
update DB_VERSION SET VERSION = '3.0'
;
