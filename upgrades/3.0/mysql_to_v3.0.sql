-- Upgrade script to 3.0 from 2.8.

-- change valid_from/to to datetime. Should be first because it can fail


alter table artefact add column valid_from_d datetime null, add column valid_to_d datetime null
;

update artefact set valid_from_d = valid_from where valid_from regexp '^[0-9]{4}-[0-9]{2}-[0-9]{2}(T[0-9]{2}:[0-9]{2}:[0-9]{2}(\\.[0-9]+)?)?$'
;
update artefact set valid_to_d = valid_to where valid_to regexp '^[0-9]{4}-[0-9]{2}-[0-9]{2}(T[0-9]{2}:[0-9]{2}:[0-9]{2}(\\.[0-9]+)?)?$'
;
update artefact set valid_from_d = substr(valid_from,1,length(valid_from)-1)  where valid_from regexp '^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}(\\.[0-9]+)?Z$'
;
update artefact set valid_to_d = substr(valid_to,1,length(valid_to)-1)  where valid_to regexp '^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}(\\.[0-9]+)?Z$'
;
update artefact set valid_from_d = convert_tz(substr(valid_from,1,length(valid_from)-6),substr(valid_from,length(valid_from)-5,6),'+00:00')  where valid_from regexp '^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}(\\.[0-9]+)?[+\\-][0-9]{2}:[0-9]{2}$'
;
update artefact set valid_to_d = convert_tz(substr(valid_to,1,length(valid_to)-6),substr(valid_to,length(valid_to)-5,6),'+00:00')  where valid_to regexp '^[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}(\\.[0-9]+)?[+\\-][0-9]{2}:[0-9]{2}$'
;

alter table artefact drop column valid_from, drop column valid_to
;

alter table artefact change column valid_from_d valid_from datetime null
;  
alter table artefact change column valid_to_d valid_to datetime null
;  

-- VERSION to 3 fields. Note we have to check for SDMX v2.1 incompatible versions before hand. 

alter table artefact add column version1 bigint NOT NULL DEFAULT 0, add column version2 bigint NOT NULL DEFAULT 0, add column version3 bigint  null default NULL 
;
DROP FUNCTION IF EXISTS `getVersion`
;
-- DELIMITER $$
create function `getVersion`(p_version varchar(50), particle int) 
RETURNS bigint DETERMINISTIC
BEGIN
	declare point int;
	declare currentParticle int;
	declare returnValue bigint;
	set point = locate('.', p_version);
	set currentParticle = 0;
	set returnValue = null;

	if point = 0 and length(p_version) > 0 and particle = 1 then
		set returnValue = p_version;
	end if;

	WHILE returnValue is null and point > 0 DO
		SET currentParticle = currentParticle + 1;
		if currentParticle = particle then
			set returnValue = substring(p_version,1,point - 1);
		else
			set point = locate('.', p_version);
			set p_version = substring(p_version, point + 1);
		end if;
	END WHILE;
	return returnValue;
END
;
-- $$
-- DELIMITER ;

update artefact set version1 = getVersion(version,1) where version regexp '^[0-9]+$'
;

update artefact set version1 =  getVersion(version,1),  version2 =  getVersion(version,2) where version regexp '^[0-9]+\.[0-9]+$'
;

update artefact set version1 =  getVersion(version,1),  version2 =  getVersion(version,2) , version3 =  getVersion(version,3) where version regexp '^[0-9]+\.[0-9]+\.[0-9]+$'
;


DROP FUNCTION IF EXISTS `versionToString`
;
create function `versionToString`(particle1 bigint, particle2 bigint, particle3 bigint) 
RETURNS varchar(50) DETERMINISTIC
BEGIN
	declare version varchar(50);
	if particle1 is null then
		set particle1 = 0;
	end if;
	if particle2 is null then
		set particle2 = 0;
	end if;
	
	if particle3 is null then
		set version = concat(particle1, '.', particle2);
	else
		set version = concat(particle1, '.', particle2, '.', particle3);
	end if;

	return version;
END
;
-- $$
-- DELIMITER ;
DROP FUNCTION IF EXISTS `isGreaterVersion`
;
-- DELIMITER $$
create function `isGreaterVersion`(x1 bigint, y1 bigint, z1 bigint, x2 bigint, y2 bigint, z2 bigint)
RETURNS BOOL DETERMINISTIC
BEGIN
	if x1 is null then
		set x1 = 0;
	end if;
	if x2 is null then
		set x2 = 0;
	end if;
	if y1 is null then
		set y1 = 0;
	end if;
	if y2 is null then
		set y2 = 0;
	end if;
RETURN ( x1 > x2 ) or  ( ( x1 = x2 ) and( ( y1 > y2 ) or( ( y1 = y2 ) and ( (z1 is not null) and ( (z2 is null) or (z1 > z2) ) ) ) ) );
END;
;
-- $$
-- DELIMITER ;
DROP FUNCTION IF EXISTS `isEqualVersion`
;

-- DELIMITER $$
create function `isEqualVersion`(x1 bigint, y1 bigint, z1 bigint, x2 bigint, y2 bigint, z2 bigint)
RETURNS BOOL DETERMINISTIC
BEGIN
	if x1 is null then
		set x1 = 0;
	end if;
	if x2 is null then
		set x2 = 0;
	end if;
	if y1 is null then
		set y1 = 0;
	end if;
	if y2 is null then
		set y2 = 0;
	end if;
	RETURN ( x1 = x2 ) and ( y1 = y2 ) and (((z1 is null) and (z2 is null)) or (((z1 is not null) and (z2 is not null)) and ( z1 = z2 )));
END;
;
-- $$
-- DELIMITER ;
DROP PROCEDURE IF EXISTS `split_version`
;
-- DELIMITER $$
CREATE PROCEDURE `split_version`(IN p_version varchar(50), out p_version1 bigint, out p_version2 bigint, out p_version3 bigint)
BEGIN
    declare point1 int;
    declare point2 int;
    set p_version1 = getVersion(p_version, 1);
    set p_version2 = getVersion(p_version, 2);
    set p_version3 = getVersion(p_version, 3);
    if p_version1 is null then
        set p_version1 = 0;
    end if;
    if p_version2 is null then
        set p_version2 = 0;
    end if;
END
;
-- $$
-- DELIMITER ;

CREATE OR REPLACE VIEW ARTEFACT_VIEW AS SELECT ART_ID, ID, AGENCY, versionToString(VERSION1,VERSION2,VERSION3) as VERSION, VALID_FROM, VALID_TO, IS_FINAL FROM ARTEFACT
;

-- add is_partial to item scheme artefacts

alter table codelist add column is_partial BOOL NOT NULL DEFAULT FALSE
;

alter table concept_scheme add column is_partial BOOL NOT NULL DEFAULT FALSE
;

alter table category_scheme add column is_partial BOOL NOT NULL DEFAULT FALSE
;


-- change procs

DROP PROCEDURE IF EXISTS INSERT_ARTEFACT
;

DROP PROCEDURE IF EXISTS INSERT_HCL
;

DROP PROCEDURE IF EXISTS INSERT_DSD
;

DROP PROCEDURE IF EXISTS INSERT_DATAFLOW
;

DROP PROCEDURE IF EXISTS INSERT_CATEGORY_SCHEME
;

DROP PROCEDURE IF EXISTS INSERT_CODELIST
;

DROP PROCEDURE IF EXISTS INSERT_CONCEPT_SCHEME
;

CREATE PROCEDURE INSERT_ARTEFACT(
	IN p_id varchar(50),
	IN p_version varchar(50),
	IN p_agency varchar(50),
	IN p_valid_from datetime,
	IN p_valid_to datetime,
	IN p_is_final int,
	OUT p_pk bigint)
BEGIN
    declare ver1 bigint;
    declare ver2 bigint;
    declare ver3 bigint;
    CALL SPLIT_VERSION(p_version, ver1, ver2, ver3);
	insert into ARTEFACT (ID,VERSION1, VERSION2, VERSION3, AGENCY, VALID_FROM, VALID_TO, IS_FINAL) values (p_id, ver1, ver2, ver3, p_agency, p_valid_from, p_valid_to, p_is_final);
	set p_pk = LAST_INSERT_ID();
END
;

CREATE PROCEDURE INSERT_HCL(
	IN p_id varchar(50),
	IN p_version varchar(50),
	IN p_agency varchar(50),
	IN p_valid_from datetime,
	IN p_valid_to datetime,
	IN p_is_final int,
	OUT p_pk bigint)
BEGIN
	call INSERT_ARTEFACT (p_id,p_version,p_agency,p_valid_from,p_valid_to,p_is_final, p_pk);
	insert into HCL (HCL_ID) values(p_pk);
END
;

CREATE PROCEDURE INSERT_DSD(
	IN p_id varchar(50),
	IN p_version varchar(50),
	IN p_agency varchar(50),
	IN p_valid_from datetime,
	IN p_valid_to datetime,
	IN p_is_final int,
	OUT p_pk bigint)
BEGIN
	call INSERT_ARTEFACT (p_id,p_version,p_agency,p_valid_from,p_valid_to,p_is_final, p_pk);
	
	INSERT INTO DSD
           (DSD_ID)
     VALUES
           (p_pk);
END
;

CREATE PROCEDURE INSERT_DATAFLOW(
	IN p_id varchar(50),
	IN p_version varchar(50),
	IN p_agency varchar(50),
	IN p_valid_from datetime,
	IN p_valid_to datetime,
	IN p_is_final int,
	IN p_dsd_id bigint,
	IN p_map_set_id bigint,
	OUT p_pk bigint)
BEGIN
	call INSERT_ARTEFACT (p_id,p_version,p_agency,p_valid_from,p_valid_to,p_is_final, p_pk);
	
INSERT INTO DATAFLOW
           (DF_ID
           ,DSD_ID
           ,MAP_SET_ID)
     VALUES
           (p_pk
           ,p_dsd_id
           ,p_map_set_id);
END
;

CREATE PROCEDURE INSERT_CATEGORY_SCHEME(
	IN p_id varchar(50),
	IN p_version varchar(50),
	IN p_agency varchar(50),
	IN p_valid_from datetime,
	IN p_valid_to datetime,
	IN p_is_final int,
	IN p_cs_order bigint,
    IN p_is_partial int,
	OUT p_pk bigint)
BEGIN
	if p_cs_order is null then
		set p_cs_order = 0;
	end if;
	call INSERT_ARTEFACT (p_id,p_version,p_agency,p_valid_from,p_valid_to,p_is_final, p_pk);
	
	INSERT INTO CATEGORY_SCHEME
           (CAT_SCH_ID, CS_ORDER, IS_PARTIAL)
     VALUES
           (p_pk, p_cs_order, p_is_partial);
END
;

CREATE PROCEDURE INSERT_CODELIST(
	IN p_id varchar(50),
	IN p_version varchar(50),
	IN p_agency varchar(50),
	IN p_valid_from datetime,
	IN p_valid_to datetime,
	IN p_is_final int,
    IN p_is_partial int,
	OUT p_pk bigint)
BEGIN
	call INSERT_ARTEFACT (p_id,p_version,p_agency,p_valid_from,p_valid_to,p_is_final, p_pk);
	
	INSERT INTO CODELIST
           (CL_ID, IS_PARTIAL)
     VALUES
           (p_pk, p_is_partial);
END
;

CREATE PROCEDURE INSERT_CONCEPT_SCHEME(
	IN p_id varchar(50),
	IN p_version varchar(50),
	IN p_agency varchar(50),
	IN p_valid_from datetime,
	IN p_valid_to datetime,
	IN p_is_final int,
    IN p_is_partial int,
	OUT p_pk bigint)
BEGIN
	call INSERT_ARTEFACT (p_id,p_version,p_agency,p_valid_from,p_valid_to,p_is_final, p_pk);
	
	INSERT INTO CONCEPT_SCHEME
           (CON_SCH_ID, IS_PARTIAL)
     VALUES
           (p_pk, p_is_partial);
END
;

-- Categorisations

create table CATEGORISATION (
    CATN_ID BIGINT NOT NULL,
    ART_ID BIGINT NOT NULL,
	CAT_ID BIGINT NOT NULL,
	DC_ORDER BIGINT,
	PRIMARY KEY (CATN_ID),
	KEY (CAT_ID),
	KEY (ART_ID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
;

-- TODO import of DATAFLOW_CATEGORY

CREATE PROCEDURE INSERT_CATEGORISATION(
	IN p_id varchar(50),
	IN p_version varchar(50),
	IN p_agency varchar(50),
	IN p_valid_from datetime,
	IN p_valid_to datetime,
	IN p_is_final int,
	IN p_art_id bigint,
	IN p_cat_id bigint,
	IN p_dc_order bigint,
	OUT p_pk bigint)
BEGIN
	call INSERT_ARTEFACT (p_id,p_version,p_agency,p_valid_from,p_valid_to,p_is_final, p_pk);
	
INSERT INTO CATEGORISATION
           (CATN_ID
           ,ART_ID
           ,CAT_ID
           ,DC_ORDER)
     VALUES
           (p_pk
           ,p_art_id
           ,p_cat_id
           ,p_dc_order);
END
;

DROP PROCEDURE IF EXISTS TMP_PROC
;

create procedure TMP_PROC()
begin
	DECLARE done INT DEFAULT FALSE;
declare cid varchar(50);
declare agency varchar(50); 
declare art_id bigint; 
declare cat_id bigint;
declare dc_order bigint;
declare pk bigint;
declare lpk bigint;
declare cat_cursor cursor for
select substring(REPLACE(CONCAT(AD.ID , '@' , AD.AGENCY , '@' ,  AD.VERSION , '@' , AC.ID , '@' , AC.VERSION , '@' , I.ID), '.', ''),1,50) as cid , AC.AGENCY,  AD.ART_ID, C.CAT_ID, dc.dc_order
from dataflow_Category dc, artefact ad ,category c, artefact ac,  item i  
where dc.df_id = ad.art_id and dc.cat_id = c.cat_id and c.cat_sch_id = ac.art_id and i.item_id = dc.cat_id ;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

open cat_cursor;

loopLabel : LOOP
    FETCH cat_cursor into cid, agency, art_id, cat_id, dc_order;
    if done then
        close cat_cursor;
        leave loopLabel;
    end if;
    call INSERT_CATEGORISATION (cid,'1.0',agency,null,null,1, art_id, cat_id, dc_order, pk);
    call INSERT_LOCALISED_STRING (null, pk, cid, 'Name', 'en', lpk);
end loop;
end;
;
call TMP_PROC()
;

DROP PROCEDURE IF EXISTS TMP_PROC
;

ALTER TABLE CATEGORISATION ADD CONSTRAINT FK_CATEGORISATION_ARTEFACT 
	FOREIGN KEY (CATN_ID) REFERENCES ARTEFACT (ART_ID)
	ON DELETE CASCADE
;

ALTER TABLE CATEGORISATION ADD CONSTRAINT FK_ARTEFACT_CATEGORY_CATEGORY 
	FOREIGN KEY (CAT_ID) REFERENCES CATEGORY (CAT_ID)
	ON DELETE CASCADE
;


ALTER TABLE CATEGORISATION ADD CONSTRAINT FK_ARTEFACT_CATEGORY_ARTEFACT 
	FOREIGN KEY (ART_ID) REFERENCES ARTEFACT (ART_ID)
	ON DELETE CASCADE
;

-- update component

alter table COMPONENT add column ID varchar(50) NULL DEFAULT NULL
;

update COMPONENT set ID = 'TIME_PERIOD' where COMPONENT.TYPE = 'TimeDimension'
;

update COMPONENT set ID = 'OBS_VALUE' where COMPONENT.TYPE = 'PrimaryMeasure'
;
update COMPONENT C INNER JOIN ITEM I ON C.CON_ID = I.ITEM_ID set C.ID = I.ID where C.ID is null
;

alter table COMPONENT MODIFY column ID varchar(50) NOT NULL
;

alter table COMPONENT add column CON_SCH_ID bigint NULL DEFAULT NULL
;

ALTER TABLE COMPONENT ADD CONSTRAINT FK_COMPONENT_MEASURE
	FOREIGN KEY (CON_SCH_ID) REFERENCES CONCEPT_SCHEME (CON_SCH_ID)
;

create table ATTR_DIMS ( 
 ATTR_ID    bigint NOT NULL,
 DIM_ID     bigint NOT NULL,
 PRIMARY KEY(ATTR_ID, DIM_ID),
 KEY(ATTR_ID),
 KEY(DIM_ID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
;

ALTER TABLE ATTR_DIMS ADD CONSTRAINT FK_ATTR_DIMS_ATTR
	FOREIGN KEY (ATTR_ID) REFERENCES COMPONENT (COMP_ID)
    ON DELETE CASCADE
;

ALTER TABLE ATTR_DIMS ADD CONSTRAINT FK_ATTR_DIMS_DIMS
	FOREIGN KEY (DIM_ID) REFERENCES COMPONENT (COMP_ID)
    ON DELETE CASCADE
;


DROP PROCEDURE IF EXISTS INSERT_COMPONENT
;

CREATE PROCEDURE INSERT_COMPONENT(
	IN p_type varchar(50) ,
	IN p_id varchar(50) ,
	IN p_dsd_id bigint ,
	IN p_con_id bigint ,
	IN p_cl_id bigint,
	IN p_con_sch_id bigint,
	IN p_is_freq_dim int,
	IN p_is_measure_dim int,
	IN p_att_ass_level varchar(11),
	IN p_att_status varchar(11),
	IN p_att_is_time_format int,
	IN p_xs_attlevel_ds int,
	IN p_xs_attlevel_group int,
	IN p_xs_attlevel_section int,
	IN p_xs_attlevel_obs int,
	IN p_xs_measure_code varchar(50),
	OUT p_pk bigint) 
BEGIN
	insert into COMPONENT (ID, TYPE, DSD_ID, CON_ID, CL_ID, IS_FREQ_DIM, IS_MEASURE_DIM, ATT_ASS_LEVEL, ATT_STATUS, ATT_IS_TIME_FORMAT, XS_ATTLEVEL_DS, XS_ATTLEVEL_GROUP, XS_ATTLEVEL_SECTION, XS_ATTLEVEL_OBS, XS_MEASURE_CODE, CON_SCH_ID) 
		values (p_id, p_type, p_dsd_id, p_con_id, p_cl_id, p_is_freq_dim, p_is_measure_dim, p_att_ass_level, p_att_status, p_att_is_time_format, p_xs_attlevel_ds, p_xs_attlevel_group, p_xs_attlevel_section, p_xs_attlevel_obs, p_xs_measure_code, p_con_sch_id);
	set p_pk = LAST_INSERT_ID();
END
;

-- update HCL related procs (except HCL it self)

DROP PROCEDURE IF EXISTS INSERT_HCL_CODE
;

DROP PROCEDURE IF EXISTS INSERT_HIERACHY
;

DROP PROCEDURE IF EXISTS INSERT_HLEVEL
;

CREATE PROCEDURE INSERT_HCL_CODE(
	IN p_id varchar(50),
	IN p_version varchar(50),
	IN p_agency varchar(50),
	IN p_valid_from datetime,
	IN p_valid_to datetime,
	IN p_is_final int,
	IN p_parent_hcode_id bigint,
	IN p_lcd_id bigint,
	IN p_h_id bigint,
	IN p_level_id bigint,
	OUT p_pk bigint)
BEGIN
	call INSERT_ARTEFACT (p_id,p_version,p_agency,p_valid_from,p_valid_to,p_is_final, p_pk);
	
	INSERT INTO HCL_CODE
           (HCODE_ID
           ,PARENT_HCODE_ID
           ,LCD_ID
           ,H_ID
           ,LEVEL_ID)
	VALUES
		(p_pk
        ,p_parent_hcode_id
        ,p_lcd_id
        ,p_h_id
         ,p_level_id);
END
;

CREATE PROCEDURE INSERT_HIERACHY(
	IN p_id varchar(50),
	IN p_version varchar(50),
	IN p_agency varchar(50),
	IN p_valid_from datetime,
	IN p_valid_to datetime,
	IN p_is_final int,
	IN p_hcl_id bigint,
	OUT p_pk bigint)
BEGIN
	call INSERT_ARTEFACT (p_id,p_version,p_agency,p_valid_from,p_valid_to,p_is_final, p_pk);
	
	INSERT INTO HIERARCHY
           (H_ID
           ,HCL_ID)
     VALUES
           (p_pk
           ,p_hcl_id);
END
;

CREATE PROCEDURE INSERT_HLEVEL(
	IN p_id varchar(50),
	IN p_version varchar(50),
	IN p_agency varchar(50),
	IN p_valid_from datetime,
	IN p_valid_to datetime,
	IN p_is_final int,
	IN p_h_id bigint,
	IN p_parent_level_id bigint,
	OUT p_pk bigint)
BEGIN
	call INSERT_ARTEFACT (p_id,p_version,p_agency,p_valid_from,p_valid_to,p_is_final, p_pk);
	
	INSERT INTO HLEVEL
           (LEVEL_ID
           ,H_ID
           ,PARENT_LEVEL_ID)
     VALUES
           (p_pk
           ,p_h_id
           ,p_parent_level_id);
END
;

-- TIME TRANSCODING
create table TIME_TRANSCODING ( 
  TR_ID bigint NOT NULL,
  FREQ varchar(5) NOT NULL,
  YEAR_COL_ID bigint NULL,
  PERIOD_COL_ID bigint NULL,
  DATE_COL_ID bigint NULL,
  EXPRESSION varchar(150) NULL,
  PRIMARY KEY(TR_ID, FREQ),
  KEY(TR_ID)
) ENGINE=InnoDB DEFAULT CHARSET=utf8
;

insert into TIME_TRANSCODING (TR_ID, FREQ, YEAR_COL_ID, PERIOD_COL_ID, EXPRESSION)
select 
tr_id,
case instr(expression, 'freq') when 0 then null else substring_index(substring(expression, instr(expression, 'freq') + 5),';',1) END as freq,
case instr(expression, 'year') when 0 then null else substring_index(substring(expression, instr(expression, 'year') + 5),',',1) END as year_col_id,
case instr(expression, 'period') when 0 then null else substring_index(substring(expression, instr(expression, 'period') + 7),',',1) END as period_col_id,
substring(expression, instr(expression,';') + 1) as expression
from transcoding where expression is not null and length(expression) > 0
; 

update TIME_TRANSCODING set EXPRESSION = REPLACE(EXPRESSION, CONCAT('year=',YEAR_COL_ID,','),'year=') where YEAR_COL_ID is not null
;

update TIME_TRANSCODING set EXPRESSION = REPLACE(EXPRESSION, CONCAT('period=',PERIOD_COL_ID,','),'period=') where PERIOD_COL_ID is not null
;

update TIME_TRANSCODING TT inner join TRANSCODING T on T.TR_ID = TT.TR_ID inner join com_col_mapping_column C on T.MAP_ID = C.MAP_ID SET TT.DATE_COL_ID = C.COL_ID WHERE TT.EXPRESSION like '%datetime=1%'
;

ALTER TABLE TIME_TRANSCODING ADD CONSTRAINT FK_TIME_TRANSCODING_TRANS
    FOREIGN KEY (TR_ID) REFERENCES TRANSCODING (TR_ID)
    ON DELETE CASCADE
;

ALTER TABLE TIME_TRANSCODING ADD CONSTRAINT FK_TIME_TRANSCODING_YEAR
    FOREIGN KEY (YEAR_COL_ID) REFERENCES DATASET_COLUMN (COL_ID)
;

ALTER TABLE TIME_TRANSCODING ADD CONSTRAINT FK_TIME_TRANSCODING_PERIOD
    FOREIGN KEY (PERIOD_COL_ID) REFERENCES DATASET_COLUMN (COL_ID)
;

ALTER TABLE TIME_TRANSCODING ADD CONSTRAINT FK_TIME_TRANSCODING_DATE
    FOREIGN KEY (DATE_COL_ID) REFERENCES DATASET_COLUMN (COL_ID)
;


-- migrate dsd code descriptions to names
update LOCALISED_STRING set type = 'Name' WHERE ITEM_ID IN (select LCD_ID from DSD_CODE) and type='Desc'
;

-- erase old transcoding.expression
update TRANSCODING set EXPRESSION = null where TR_ID in (select distinct TR_ID from TIME_TRANSCODING)
;
-- do we drop old VERSION

alter table artefact drop column version
;

-- always last
update DB_VERSION SET VERSION = '3.0'
;

