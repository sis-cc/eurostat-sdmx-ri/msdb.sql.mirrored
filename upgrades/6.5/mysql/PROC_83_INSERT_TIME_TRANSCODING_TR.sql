
DROP PROCEDURE IF EXISTS INSERT_TIME_TRANSCODING_TR
;
CREATE PROCEDURE INSERT_TIME_TRANSCODING_TR(
IN p_expression varchar(150),
IN p_map_id bigint,
IN p_criteria_column bigint,
OUT p_pk bigint)
BEGIN
		call INSERT_TRANSCODING(p_expression, p_map_id, p_pk);
		INSERT INTO TIME_TRANSCODING_TR (TR_ID,CRITERIA_COLUMN) VALUES (p_pk, p_criteria_column);
END;
;
