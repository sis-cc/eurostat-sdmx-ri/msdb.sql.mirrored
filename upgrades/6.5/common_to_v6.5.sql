INSERT INTO DB_TIME_FORMAT (FORMAT) VALUES ('Annual')
;

INSERT INTO DB_TIME_FORMAT (FORMAT) VALUES ('Monthly')
;

INSERT INTO DB_TIME_FORMAT (FORMAT) VALUES ('Weekly')
;

INSERT INTO DB_TIME_FORMAT (FORMAT) VALUES ('Quarterly')
;

INSERT INTO DB_TIME_FORMAT (FORMAT) VALUES ('Semester')
;

INSERT INTO DB_TIME_FORMAT (FORMAT) VALUES ('Trimester')
;

INSERT INTO DB_TIME_FORMAT (FORMAT) VALUES ('TimestampOrDate')
;

INSERT INTO DB_TIME_FORMAT (FORMAT) VALUES ('Iso8601Compatible')
;

INSERT INTO TIME_COLUMN_TYPE (COLUMN_TYPE) VALUES ('YEAR')
;

INSERT INTO TIME_COLUMN_TYPE (COLUMN_TYPE) VALUES ('PERIOD')
;

INSERT INTO TIME_COLUMN_TYPE (COLUMN_TYPE) VALUES ('DATE')
;