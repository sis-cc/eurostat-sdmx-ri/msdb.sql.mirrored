-- updates for 6.5
-- in folder oracle
CREATE INDEX DSD_CODE_CL_ID_IDX ON DSD_CODE (CL_ID, LCD_ID, PARENT_CODE_ID)
;

CREATE INDEX LOCALISED_STRING_ART_ID_IDX ON LOCALISED_STRING (ART_ID, TYPE,LANGUAGE)
;

-- always last
update DB_VERSION SET VERSION = '6.5'
;

