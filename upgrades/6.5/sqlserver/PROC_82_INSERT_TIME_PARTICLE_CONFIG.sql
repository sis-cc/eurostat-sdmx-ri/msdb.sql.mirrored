IF EXISTS (SELECT * FROM dbo.SYSOBJECTS WHERE id = object_id('INSERT_TIME_PARTICLE_CONFIG') AND  OBJECTPROPERTY(id, 'IsProcedure') = 1)
   DROP PROC INSERT_TIME_PARTICLE_CONFIG
;
CREATE PROCEDURE INSERT_TIME_PARTICLE_CONFIG
@p_db_format varchar(50) ,
@p_pk bigint OUT
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

		INSERT INTO TIME_PARTICLE_CONFIG (DB_FORMAT) VALUES (@p_db_format);
		set @p_pk = SCOPE_IDENTITY();
	IF @starttrancount = 0
	COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	RAISERROR (@ErrorMessage, -- Message text.
				@ErrorSeverity, -- Severity.
				@ErrorState -- State.
				);
	END CATCH
END
;
