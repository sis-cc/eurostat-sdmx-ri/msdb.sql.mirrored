IF EXISTS (SELECT * FROM dbo.SYSOBJECTS WHERE id = object_id('INSERT_TIME_TRANSCODING_TR') AND  OBJECTPROPERTY(id, 'IsProcedure') = 1)
   DROP PROC INSERT_TIME_TRANSCODING_TR
;
CREATE PROCEDURE INSERT_TIME_TRANSCODING_TR
@p_expression varchar(150) ,
@p_map_id bigint ,
@p_criteria_column bigint ,
@p_pk bigint OUT
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

		EXEC INSERT_TRANSCODING @p_expression = @p_expression , @p_map_id = @p_map_id , @p_pk = @p_pk OUTPUT;
		INSERT INTO TIME_TRANSCODING_TR (TR_ID,CRITERIA_COLUMN) VALUES (@p_pk, @p_criteria_column);
	IF @starttrancount = 0
	COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	RAISERROR (@ErrorMessage, -- Message text.
				@ErrorSeverity, -- Severity.
				@ErrorState -- State.
				);
	END CATCH
END
;
