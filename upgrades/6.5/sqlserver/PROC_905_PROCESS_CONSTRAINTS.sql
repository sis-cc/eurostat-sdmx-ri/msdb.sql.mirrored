IF EXISTS (SELECT * FROM dbo.SYSOBJECTS WHERE id = object_id('PROCESS_CONSTRAINTS') AND  OBJECTPROPERTY(id, 'IsProcedure') = 1)
DROP PROC PROCESS_CONSTRAINTS
;
GO
;
CREATE PROCEDURE PROCESS_CONSTRAINTS(
	@p_art_agency varchar(50),
	@p_art_id varchar(50),
	@p_art_version varchar(50),
	@p_art_type varchar(30),
	@p_cl_sys_id bigint,
	@p_const_type varchar(30),
	@p_is_cl_partial bit OUTPUT)
AS
BEGIN
	SET NOCOUNT ON;
	
	BEGIN TRY
		DECLARE @pra_sys_id BIGINT, @df_sys_id BIGINT, @dsd_sys_id BIGINT, @comp_sys_id BIGINT, @comp_id VARCHAR(50);
		DECLARE @comp_count INT, @constraints_exist INT;
		
		IF (@p_art_type = 'ProvisionAgreement')
		BEGIN
			SELECT @pra_sys_id = pr.PA_ID, @df_sys_id = df.DF_ID, @dsd_sys_id = df.DSD_ID
			FROM PROVISION_AGREEMENT pr INNER JOIN ARTEFACT_VIEW pra ON pr.PA_ID = pra.ART_ID
			INNER JOIN DATAFLOW df ON df.DF_ID = pr.SU_ID
			WHERE pra.AGENCY = @p_art_agency AND pra.ID = @p_art_id AND pra.VERSION = @p_art_version
			
		END
		ELSE IF (@p_art_type = 'Dataflow')
		BEGIN
			SELECT @df_sys_id = df.DF_ID, @dsd_sys_id = df.DSD_ID
			FROM DATAFLOW df INNER JOIN ARTEFACT_VIEW dfa ON dfa.ART_ID = df.DF_ID
			WHERE dfa.AGENCY = @p_art_agency AND dfa.ID = @p_art_id AND dfa.VERSION = @p_art_version
		END
		ELSE IF (@p_art_type = 'DataStructure')
		BEGIN
			SELECT @dsd_sys_id = dsd.DSD_ID
			FROM DSD dsd INNER JOIN ARTEFACT_VIEW art ON art.ART_ID = dsd.DSD_ID
			WHERE art.ID = @p_art_id AND art.AGENCY = @p_art_agency AND art.VERSION = @p_art_version
		END
	
		-- check it there are constraints for the given codelist and for all components that use the codelist.
    	SELECT @comp_count = COUNT(COMP.ID) FROM COMPONENT COMP 
    	LEFT OUTER JOIN CONTENT_CONSTRAINT_V CC ON CC.MEMBER_ID = COMP.ID AND CC.ART_ID IN (@pra_sys_id, @df_sys_id, @dsd_sys_id)
    	WHERE COMP.DSD_ID = @dsd_sys_id AND COMP.CL_ID = @p_cl_sys_id AND CC.ART_ID IS NULL;
    
    	-- check if the requested artefact has attached constraints.
    	SELECT @constraints_exist = COUNT(1) FROM CONTENT_CONSTRAINT_V CC 
    	WHERE CC.ART_ID IN (@pra_sys_id, @df_sys_id, @dsd_sys_id);

    	IF (@constraints_exist = 0 OR @comp_count > 0)
			SET @p_is_cl_partial = 0;
		ELSE
		BEGIN
			DECLARE components_cursor CURSOR LOCAL FOR
			SELECT COMP.COMP_ID, COMP.ID FROM COMPONENT COMP WHERE DSD_ID = @dsd_sys_id AND COMP.CL_ID = @p_cl_sys_id
			
			OPEN components_cursor;  

			FETCH NEXT FROM components_cursor INTO @comp_sys_id, @comp_id;  
			WHILE @@FETCH_STATUS = 0  
			BEGIN
				-- copy the initial codelist
				INSERT INTO #TEMP_PROCESS_CONSTRAINTS_TABLE(COMP_ID, CODE_SYS_ID, CODE_ID, PARENT_CODE_SYS_ID, INCLUDED)
				SELECT COMP.ID, cd.LCD_ID, it.ID, cd.PARENT_CODE_ID, 1
				FROM COMPONENT COMP INNER JOIN CODELIST CL ON COMP.CL_ID = CL.CL_ID
				INNER JOIN DSD_CODE CD ON CL.CL_ID = CD.CL_ID INNER JOIN ITEM IT ON CD.LCD_ID = IT.ITEM_ID
				WHERE COMP.COMP_ID = @comp_sys_id AND CL.CL_ID = @p_cl_sys_id
				
				-- start the processing of constraints
				IF (@p_art_type = 'ProvisionAgreement')
				BEGIN
					PRINT '-- PROCESS CONSTRAINTS FOR ProvisionAgreement: ' + @p_art_agency+ '+'+@p_art_id+'+'+@p_art_version
					
					-- process DSD constraints
					EXEC PROCESS_CONSTRAINTS_INTERNAL @p_art_sys_id = @dsd_sys_id, @p_comp_id = @comp_id;
					-- process Dataflow constraints
					EXEC PROCESS_CONSTRAINTS_INTERNAL @p_art_sys_id = @df_sys_id, @p_comp_id = @comp_id;
					-- process Provision Agreement constraints
					EXEC PROCESS_CONSTRAINTS_INTERNAL @p_art_sys_id = @pra_sys_id, @p_comp_id = @comp_id;
				END
				ELSE IF (@p_art_type = 'Dataflow')
				BEGIN
					PRINT '-- PROCESS CONSTRAINTS FOR Dataflow: ' + @p_art_agency+ '+'+@p_art_id+'+'+@p_art_version
					
					-- process DSD constraints
					EXEC PROCESS_CONSTRAINTS_INTERNAL @p_art_sys_id = @dsd_sys_id, @p_comp_id = @comp_id;
					-- process Dataflow constraints
					EXEC PROCESS_CONSTRAINTS_INTERNAL @p_art_sys_id = @df_sys_id, @p_comp_id = @comp_id;
				END
				ELSE IF (@p_art_type = 'DataStructure')
				BEGIN
					PRINT '-- PROCESS CONSTRAINTS FOR DataStructure: ' + @p_art_agency+ '+'+@p_art_id+'+'+@p_art_version
					
					-- process DSD constraints
					EXEC PROCESS_CONSTRAINTS_INTERNAL @p_art_sys_id = @dsd_sys_id, @p_comp_id = @comp_id;
				END
				FETCH NEXT FROM components_cursor INTO @comp_sys_id, @comp_id;  
			END
			CLOSE components_cursor;
			DEALLOCATE components_cursor;
			
			SET @p_is_cl_partial = 1;
		END
	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
	   RAISERROR (@ErrorMessage, @ErrorSeverity,  @ErrorState);
	END CATCH
END
;
GO
;
