
CREATE TABLE PERIOD_RULES(
TPC_ID bigint NOT NULL ,
SDMX_PERIOD_CODE varchar(50) NOT NULL ,
LOCAL_PERIOD_CODE nvarchar(150) NOT NULL ,
PRIMARY KEY (TPC_ID,SDMX_PERIOD_CODE))
;
