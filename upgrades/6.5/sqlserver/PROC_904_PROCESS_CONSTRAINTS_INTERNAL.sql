IF EXISTS (SELECT * FROM dbo.SYSOBJECTS WHERE id = object_id('PROCESS_CONSTRAINTS_INTERNAL') AND  OBJECTPROPERTY(id, 'IsProcedure') = 1)
DROP PROC PROCESS_CONSTRAINTS_INTERNAL
;
GO
;
CREATE PROCEDURE PROCESS_CONSTRAINTS_INTERNAL(
	@p_art_sys_id BIGINT,
	@p_comp_id VARCHAR(50))
AS
BEGIN
	PRINT '-- Constrainable artefact =' + CAST(@p_art_sys_id AS VARCHAR(MAX))
	PRINT '-- COMP = ' + CAST(@p_comp_id AS VARCHAR(MAX))
	
	EXEC PROCESS_INCLUSIVE_KEY_VALUES @p_art_sys_id = @p_art_sys_id, @p_comp_id = @p_comp_id
	EXEC PROCESS_EXCLUSIVE_KEY_VALUES @p_art_sys_id = @p_art_sys_id, @p_comp_id = @p_comp_id
END
;
GO
;
