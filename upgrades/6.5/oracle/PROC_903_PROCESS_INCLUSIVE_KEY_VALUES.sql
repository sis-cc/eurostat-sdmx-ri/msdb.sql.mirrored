CREATE OR REPLACE PROCEDURE PROCESS_INCLUSIVE_KEY_VALUES(
	p_art_sys_id NUMBER,
	p_comp_id VARCHAR2)
AS
    member_value VARCHAR2(200);
    cascade_values NUMBER;
	counter INT := 0;
    
    CURSOR key_values_cursor IS
	SELECT cc.MEMBER_VALUE, cc.CASCADE_VALUES
	FROM CONTENT_CONSTRAINT_V cc
	WHERE cc.ART_ID = p_art_sys_id AND cc.CUBE_INCLUDE = 1 AND cc.MEMBER_ID = p_comp_id;
BEGIN
	
    OPEN key_values_cursor;
    LOOP
        FETCH key_values_cursor INTO member_value, cascade_values;
        EXIT WHEN key_values_cursor%NOTFOUND;
        
		IF (counter = 0) THEN -- reset included flag
			UPDATE TEMP_PROCESS_CONSTRAINTS_TABLE SET INCLUDED = 0;
        END IF;		
    
		IF (cascade_values = 1) THEN
			
            UPDATE TEMP_PROCESS_CONSTRAINTS_TABLE SET INCLUDED = 1 WHERE CODE_SYS_ID IN
            (
                WITH CHILD_CODES(CODE_SYS_ID, CODE_ID, PARENT_CODE_SYS_ID) AS (
				SELECT cd.CODE_SYS_ID, cd.CODE_ID, cd.PARENT_CODE_SYS_ID
				FROM TEMP_PROCESS_CONSTRAINTS_TABLE cd
				WHERE cd.CODE_ID = member_value AND cd.COMP_ID = p_comp_id
				UNION ALL
				SELECT c.CODE_SYS_ID, c.CODE_ID, c.PARENT_CODE_SYS_ID
				FROM TEMP_PROCESS_CONSTRAINTS_TABLE c
				INNER JOIN CHILD_CODES p ON c.PARENT_CODE_SYS_ID = p.CODE_SYS_ID AND c.COMP_ID = p_comp_id
                ) SELECT CODE_SYS_ID FROM CHILD_CODES
            ) AND COMP_ID = p_comp_id;
		ELSE
			UPDATE TEMP_PROCESS_CONSTRAINTS_TABLE SET INCLUDED = 1 WHERE CODE_ID = member_value AND COMP_ID = p_comp_id;
		END IF;
        
		counter := counter + 1;
    END LOOP;
    
    CLOSE key_values_cursor;
	
	DELETE FROM TEMP_PROCESS_CONSTRAINTS_TABLE WHERE INCLUDED = 0 AND COMP_ID = p_comp_id;
END;
/
;
