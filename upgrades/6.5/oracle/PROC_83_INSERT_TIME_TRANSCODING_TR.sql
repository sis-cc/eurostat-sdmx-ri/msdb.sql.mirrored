

CREATE OR REPLACE PROCEDURE INSERT_TIME_TRANSCODING_TR(
p_expression IN varchar2,
p_map_id IN number,
p_criteria_column IN number,
p_pk OUT number)
AS
BEGIN
		INSERT_TRANSCODING(p_expression => p_expression, p_map_id => p_map_id, p_pk => p_pk);
		INSERT INTO TIME_TRANSCODING_TR (TR_ID,CRITERIA_COLUMN) VALUES (p_pk, p_criteria_column);
END;
/
;
