
CREATE TABLE PERIOD_RULES(
TPC_ID number(19) NOT NULL,
SDMX_PERIOD_CODE varchar2(50) NOT NULL,
LOCAL_PERIOD_CODE nvarchar2(150) NOT NULL,
PRIMARY KEY (TPC_ID,SDMX_PERIOD_CODE))
;
