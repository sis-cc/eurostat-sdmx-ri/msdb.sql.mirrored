CREATE OR REPLACE PROCEDURE PROCESS_CONSTRAINTS(
	p_art_agency IN VARCHAR2,
	p_art_id IN VARCHAR2,
	p_art_version IN VARCHAR2,
	p_art_type IN VARCHAR2,
	p_cl_sys_id IN NUMBER,
	p_const_type IN VARCHAR2,
    p_is_cl_partial OUT INT)
AS
    pra_sys_id NUMBER(18);
    df_sys_id NUMBER(18);
    dsd_sys_id NUMBER(18);
    
    comp_sys_id NUMBER(18);
    comp_id VARCHAR2(50);
    
    constraints_exist INT := 0;
    comp_count INT := 0;
    art_cursor SYS_REFCURSOR;
    
    CURSOR components_cursor IS
    SELECT COMP.COMP_ID, COMP.ID FROM COMPONENT COMP WHERE DSD_ID = dsd_sys_id AND COMP.CL_ID = p_cl_sys_id;
            
BEGIN	
    IF (p_art_type = 'ProvisionAgreement') THEN
    	OPEN art_cursor FOR
        SELECT pr.PA_ID, df.DF_ID, df.DSD_ID
    	FROM PROVISION_AGREEMENT pr INNER JOIN ARTEFACT_VIEW pra ON pr.PA_ID = pra.ART_ID
    	INNER JOIN DATAFLOW df ON df.DF_ID = pr.SU_ID
    	WHERE pra.AGENCY = p_art_agency AND pra.ID = p_art_id AND pra.VERSION = p_art_version;	
        FETCH art_cursor INTO pra_sys_id, df_sys_id, dsd_sys_id;
        CLOSE art_cursor;
    ELSIF (p_art_type = 'Dataflow') THEN
        OPEN art_cursor FOR
        SELECT df.DF_ID, df.DSD_ID
        FROM DATAFLOW df INNER JOIN ARTEFACT_VIEW dfa ON dfa.ART_ID = df.DF_ID
        WHERE dfa.AGENCY = p_art_agency AND dfa.ID = p_art_id AND dfa.VERSION = p_art_version;
        FETCH art_cursor INTO df_sys_id, dsd_sys_id;
        CLOSE art_cursor;
    ELSIF (p_art_type = 'DataStructure') THEN
        OPEN art_cursor FOR
        SELECT dsd.DSD_ID
        FROM DSD dsd INNER JOIN ARTEFACT_VIEW art ON art.ART_ID = dsd.DSD_ID
        WHERE art.ID = p_art_id AND art.AGENCY = p_art_agency AND art.VERSION = p_art_version;
        FETCH art_cursor INTO dsd_sys_id;
        CLOSE art_cursor;
    END IF;
	
    -- check it there are constraints for the given codelist and for all components that use the codelist.
    SELECT COUNT(COMP.ID) INTO comp_count FROM COMPONENT COMP 
    LEFT OUTER JOIN CONTENT_CONSTRAINT_V CC ON CC.MEMBER_ID = COMP.ID AND CC.ART_ID IN (pra_sys_id, df_sys_id, dsd_sys_id)
    WHERE COMP.DSD_ID = dsd_sys_id AND COMP.CL_ID = p_cl_sys_id AND CC.ART_ID IS NULL;
    
    -- check if the requested artefact has attached constraints.
    SELECT COUNT(1) INTO constraints_exist FROM CONTENT_CONSTRAINT_V CC 
    WHERE CC.ART_ID IN (pra_sys_id, df_sys_id, dsd_sys_id);
    
    IF (constraints_exist = 0 OR comp_count > 0) THEN
		p_is_cl_partial := 0;
	ELSE
        OPEN components_cursor;
        
        LOOP
            FETCH components_cursor INTO comp_sys_id, comp_id;
            EXIT WHEN components_cursor%NOTFOUND;
            	
            -- copy the initial codelist
            INSERT INTO TEMP_PROCESS_CONSTRAINTS_TABLE(COMP_ID, CODE_SYS_ID, CODE_ID, PARENT_CODE_SYS_ID, INCLUDED)
            SELECT COMP.ID, cd.LCD_ID, it.ID, cd.PARENT_CODE_ID, 1
            FROM COMPONENT COMP INNER JOIN CODELIST CL ON COMP.CL_ID = CL.CL_ID
            INNER JOIN DSD_CODE CD ON CL.CL_ID = CD.CL_ID INNER JOIN ITEM IT ON CD.LCD_ID = IT.ITEM_ID
            WHERE COMP.COMP_ID = comp_sys_id AND CL.CL_ID = p_cl_sys_id;
				
            -- start the processing of constraints
            IF (p_art_type = 'ProvisionAgreement') THEN
                -- process DSD constraints
                PROCESS_CONSTRAINTS_INTERNAL (p_art_sys_id => dsd_sys_id, p_comp_id => comp_id);
                -- process Dataflow constraints
                PROCESS_CONSTRAINTS_INTERNAL (p_art_sys_id => df_sys_id, p_comp_id => comp_id);
                -- process Provision Agreement constraints
                PROCESS_CONSTRAINTS_INTERNAL (p_art_sys_id => pra_sys_id, p_comp_id => comp_id);
            ELSIF (p_art_type = 'Dataflow') THEN
                -- process DSD constraints
                PROCESS_CONSTRAINTS_INTERNAL (p_art_sys_id => dsd_sys_id, p_comp_id => comp_id);
                -- process Dataflow constraints
                PROCESS_CONSTRAINTS_INTERNAL (p_art_sys_id => df_sys_id, p_comp_id => comp_id);
            ELSIF (p_art_type = 'DataStructure') THEN
                -- process DSD constraints
                PROCESS_CONSTRAINTS_INTERNAL (p_art_sys_id => dsd_sys_id, p_comp_id => comp_id);
            END IF;
        END LOOP;
        CLOSE components_cursor;
        p_is_cl_partial := 1;
    END IF;
END;
/
;
