-- updates for 6.4

declare
index_not_exists EXCEPTION;
PRAGMA EXCEPTION_INIT(index_not_exists, -1418);
begin

    execute immediate 'drop index LOCALISED_ITEM_IDX';
exception
    when index_not_exists then null;
end;
/

CREATE INDEX LOCALISED_ITEM_IDX ON LOCALISED_STRING(ITEM_ID)
;

-- always last
update DB_VERSION SET VERSION = '6.4'
;

