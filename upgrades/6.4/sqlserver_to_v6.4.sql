-- updates for 6.4

CREATE NONCLUSTERED INDEX LOCALISED_ITEM_IDX
ON LOCALISED_STRING (ITEM_ID)
INCLUDE (LS_ID,TYPE,LANGUAGE,TEXT)
;
GO
;
IF EXISTS (SELECT * FROM dbo.SYSOBJECTS WHERE id = object_id('INSERT_CONTENT_CONSTRAINT') AND  OBJECTPROPERTY(id, 'IsProcedure') = 1)
DROP PROC INSERT_CONTENT_CONSTRAINT
;
CREATE PROCEDURE INSERT_CONTENT_CONSTRAINT
       @p_id varchar(50),
       @p_version varchar(50),
       @p_agency varchar(50),
       @p_valid_from datetime = NULL,
       @p_valid_to datetime = NULL,
       @p_is_final bigint = NULL,
       @p_uri nvarchar(255),
       @p_last_modified  datetime = NULL,
       @p_actual_data bigint = 1,
       @p_periodicity varchar(50) = NULL,
       @p_offset varchar(50) = NULL,
       @p_tolerance varchar(50) = NULL,
       @p_pk bigint OUT
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION
	
	   exec INSERT_ARTEFACT @p_id = @p_id,@p_version = @p_version,@p_agency = @p_agency,@p_valid_from = @p_valid_from,@p_valid_to = @p_valid_to,@p_is_final = @p_is_final,@p_uri = @p_uri, @p_last_modified = @p_last_modified, @p_pk = @p_pk OUTPUT;

       INSERT INTO CONTENT_CONSTRAINT
           (CONT_CONS_ID, ACTUAL_DATA, PERIODICITY, OFFSET, TOLERANCE)
     VALUES
           (@p_pk, @p_actual_data, @p_periodicity, @p_offset, @p_tolerance);
IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
GO
;
IF EXISTS (SELECT * FROM dbo.SYSOBJECTS WHERE id = object_id('INSERT_STRUCTURE_USAGE') AND  OBJECTPROPERTY(id, 'IsProcedure') = 1)
DROP PROC INSERT_STRUCTURE_USAGE
;
CREATE PROCEDURE INSERT_STRUCTURE_USAGE(
 @p_id varchar(50),
 @p_version  varchar(50),
 @p_agency varchar(50),
 @p_valid_from datetime = NULL,
 @p_valid_to datetime = NULL,
 @p_is_final bigint = NULL,
 @p_uri  nvarchar(255),
 @p_last_modified datetime = NULL,
 @p_pk bigint OUT
	)
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

	EXEC INSERT_ARTEFACT @p_id = @p_id,@p_version = @p_version,@p_agency = @p_agency,@p_valid_from = @p_valid_from,@p_valid_to = @p_valid_to,@p_is_final = @p_is_final,@p_uri = @p_uri,@p_last_modified = @p_last_modified, @p_pk = @p_pk OUTPUT;
	insert into STRUCTURE_USAGE (SU_ID) VALUES (@p_pk);

	IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
GO
;
IF EXISTS (SELECT * FROM dbo.SYSOBJECTS WHERE id = object_id('INSERT_METADATAFLOW') AND  OBJECTPROPERTY(id, 'IsProcedure') = 1)
DROP PROC INSERT_METADATAFLOW
;
CREATE PROCEDURE INSERT_METADATAFLOW(
    @p_id varchar(50),
    @p_version  varchar(50),
    @p_agency varchar(50),
    @p_valid_from datetime = NULL,
    @p_valid_to datetime = NULL,
    @p_is_final bigint = NULL,
    @p_uri  nvarchar(255),
    @p_last_modified datetime = NULL,
 @p_msd_id  bigint,
 @p_pk bigint OUT)
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

	 EXEC INSERT_STRUCTURE_USAGE @p_id = @p_id,@p_version = @p_version,@p_agency = @p_agency,@p_valid_from = @p_valid_from,@p_valid_to = @p_valid_to,@p_is_final = @p_is_final,@p_uri = @p_uri,@p_last_modified = @p_last_modified, @p_pk = @p_pk OUTPUT;
	 INSERT INTO METADATAFLOW
           (MSD_ID,MDF_ID)
     VALUES
           (@p_msd_id,@p_pk);
	IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
GO
;
IF EXISTS (SELECT * FROM dbo.SYSOBJECTS WHERE id = object_id('INSERT_MSD') AND  OBJECTPROPERTY(id, 'IsProcedure') = 1)
DROP PROC INSERT_MSD
;
CREATE PROCEDURE INSERT_MSD(
 @p_id varchar(50),
    @p_version  varchar(50),
    @p_agency varchar(50),
    @p_valid_from datetime = NULL,
    @p_valid_to datetime = NULL,
    @p_is_final bigint = NULL,
    @p_uri  nvarchar(255),
    @p_last_modified datetime = NULL,
 @p_pk  bigint OUT)
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

	EXEC INSERT_ARTEFACT @p_id = @p_id,@p_version = @p_version,@p_agency = @p_agency,@p_valid_from = @p_valid_from,@p_valid_to = @p_valid_to,@p_is_final = @p_is_final,@p_uri = @p_uri,@p_last_modified = @p_last_modified, @p_pk = @p_pk OUTPUT;
	INSERT INTO METADATA_STRUCTURE_DEFINITION
           (MSD_ID)
     VALUES
           (@p_pk);
	IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
GO
;
IF EXISTS (SELECT * FROM dbo.SYSOBJECTS WHERE id = object_id('INSERT_PROVISION_AGREEMENT') AND  OBJECTPROPERTY(id, 'IsProcedure') = 1)
DROP PROC INSERT_PROVISION_AGREEMENT
;
CREATE PROCEDURE INSERT_PROVISION_AGREEMENT(
 @p_id varchar(50),
    @p_version  varchar(50),
    @p_agency varchar(50),
    @p_valid_from datetime = NULL,
    @p_valid_to datetime = NULL,
    @p_is_final bigint = NULL,
    @p_uri  nvarchar(255),
 @p_last_modified  datetime = NULL,
 @p_su_id bigint,
 @p_dp_id bigint,
 @p_pk  bigint OUT)
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

	EXEC INSERT_ARTEFACT @p_id = @p_id,@p_version = @p_version,@p_agency = @p_agency,@p_valid_from = @p_valid_from,@p_valid_to = @p_valid_to,@p_is_final = @p_is_final,@p_uri = @p_uri,@p_last_modified = @p_last_modified, @p_pk = @p_pk OUTPUT;
	INSERT INTO PROVISION_AGREEMENT
           (PA_ID,SU_ID,DP_ID)
     VALUES
           (@p_pk,@p_su_id,@p_dp_id);
	 IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH	
END
;
GO
;
-- always last
update DB_VERSION SET VERSION = '6.4'
;

