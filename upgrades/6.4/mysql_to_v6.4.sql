-- updates for 6.4

-- Requires MariaDB 10.1.x or later
CREATE OR REPLACE INDEX LOCALISED_ITEM_IDX ON LOCALISED_STRING(ITEM_ID)
;

DROP PROCEDURE IF EXISTS INSERT_CONTENT_CONSTRAINT
;

CREATE PROCEDURE INSERT_CONTENT_CONSTRAINT(
      IN p_id varchar(50),
      IN p_version  varchar(50),
      IN p_agency varchar(50),
      IN p_valid_from   datetime,
      IN p_valid_to   datetime,
      IN p_is_final  bigint,
      IN p_uri  nvarchar(255),
      IN p_last_modified  datetime,
      IN p_actual_data bigint,
      IN p_periodicity varchar(50),
      IN p_offset varchar(50),
      IN p_tolerance varchar(50),
      OUT p_pk bigint )
BEGIN
    
	call INSERT_ARTEFACT(p_id,p_version,p_agency,p_valid_from,p_valid_to,p_is_final,p_uri,p_last_modified, p_pk);
       
       INSERT INTO CONTENT_CONSTRAINT
           (CONT_CONS_ID, ACTUAL_DATA, PERIODICITY, OFFSET, TOLERANCE)
     VALUES
           (p_pk, p_actual_data, p_periodicity, p_offset, p_tolerance);
END
;

DROP PROCEDURE IF EXISTS INSERT_METADATAFLOW
;
CREATE PROCEDURE INSERT_METADATAFLOW(
	IN p_id varchar(50),
    IN p_version  varchar(50),
    IN p_agency varchar(50),
    IN p_valid_from   datetime,
    IN p_valid_to   datetime,
    IN p_is_final  bigint,
    IN p_uri  nvarchar(255),
    IN p_last_modified  datetime,
	IN p_msd_id  bigint,
	OUT p_pk bigint)
BEGIN
	 call INSERT_STRUCTURE_USAGE(p_id,p_version,p_agency,p_valid_from,p_valid_to,p_is_final,p_uri,p_last_modified, p_pk);
	 INSERT INTO METADATAFLOW
           (MSD_ID,MDF_ID)
     VALUES
           (p_msd_id,p_pk);
END
;

DROP PROCEDURE IF EXISTS INSERT_MSD
;

CREATE PROCEDURE INSERT_MSD(
	IN p_id varchar(50),
    IN p_version  varchar(50),
    IN p_agency varchar(50),
    IN p_valid_from   datetime,
    IN p_valid_to   datetime,
    IN p_is_final  bigint,
    IN p_uri  nvarchar(255),
    IN p_last_modified  datetime,
	OUT p_pk  bigint)
BEGIN
	call INSERT_ARTEFACT(p_id,p_version,p_agency,p_valid_from,p_valid_to,p_is_final,p_uri,p_last_modified, p_pk);
	INSERT INTO METADATA_STRUCTURE_DEFINITION
           (MSD_ID)
     VALUES
           (p_pk);
	
END
;

DROP PROCEDURE IF EXISTS INSERT_PROVISION_AGREEMENT
;
CREATE PROCEDURE INSERT_PROVISION_AGREEMENT(
	IN p_id varchar(50),
    IN p_version  varchar(50),
    IN p_agency varchar(50),
    IN p_valid_from   datetime,
    IN p_valid_to   datetime,
    IN p_is_final  bigint,
    IN p_uri  nvarchar(255),
	IN p_last_modified  datetime,
	IN p_su_id bigint,
	IN p_dp_id bigint,
	OUT p_pk  bigint)
BEGIN
	call INSERT_ARTEFACT(p_id,p_version,p_agency,p_valid_from,p_valid_to,p_is_final,p_uri,p_last_modified, p_pk);
	INSERT INTO PROVISION_AGREEMENT
           (PA_ID,SU_ID,DP_ID)
     VALUES
           (p_pk,p_su_id,p_dp_id);
	
END
;
-- always last
update DB_VERSION SET VERSION = '6.4'
;
