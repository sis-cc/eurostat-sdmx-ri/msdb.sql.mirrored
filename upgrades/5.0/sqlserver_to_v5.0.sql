﻿IF OBJECT_ID('INSERT_CATEGORY_SCHEME') IS NOT NULL 
DROP PROC INSERT_CATEGORY_SCHEME
;
IF OBJECT_ID('INSERT_CONCEPT_SCHEME') IS NOT NULL 
DROP PROC INSERT_CONCEPT_SCHEME
;
IF OBJECT_ID('INSERT_DATACONSUMER_SCHEME') IS NOT NULL 
DROP PROC  INSERT_DATACONSUMER_SCHEME
;
IF OBJECT_ID('INSERT_DATAPROVIDER_SCHEME') IS NOT NULL 
DROP PROC INSERT_DATAPROVIDER_SCHEME
;
IF OBJECT_ID('INSERT_ORGANISATION_UNIT_SCH') IS NOT NULL 
DROP PROC  INSERT_ORGANISATION_UNIT_SCH
;
IF OBJECT_ID('INSERT_AGENCY_SCHEME') IS NOT NULL 
DROP PROC INSERT_AGENCY_SCHEME
;
IF OBJECT_ID('INSERT_COMPONENT') IS NOT NULL 
DROP PROC  INSERT_COMPONENT
;
IF OBJECT_ID('INSERT_CONCEPT') IS NOT NULL 
DROP PROC  INSERT_CONCEPT
;
IF OBJECT_ID('INSERT_CODELIST') IS NOT NULL 
DROP PROC INSERT_CODELIST
;
IF OBJECT_ID('INSERT_DATAFLOW') IS NOT NULL 
DROP PROC  INSERT_DATAFLOW
;
IF OBJECT_ID('INSERT_STRUCTURE_USAGE') IS NOT NULL 
DROP PROC  INSERT_STRUCTURE_USAGE
;
IF OBJECT_ID('INSERT_MSD') IS NOT NULL 
DROP PROC  INSERT_MSD
;
IF OBJECT_ID('INSERT_METADATAFLOW') IS NOT NULL 
DROP PROC  INSERT_METADATAFLOW
;
IF OBJECT_ID('INSERT_METADATA_TARGET') IS NOT NULL 
DROP PROC  INSERT_METADATA_TARGET
;
IF OBJECT_ID('INSERT_TARGET_OBJECT') IS NOT NULL 
DROP PROC  INSERT_TARGET_OBJECT
;
IF OBJECT_ID('INSERT_REPORT_STRUCTURE') IS NOT NULL 
DROP PROC  INSERT_REPORT_STRUCTURE
;
IF OBJECT_ID('INSERT_METADATA_ATTRIBUTE') IS NOT NULL 
DROP PROC  INSERT_METADATA_ATTRIBUTE
;
IF OBJECT_ID('INSERT_COMPONENT_COMMON') IS NOT NULL 
DROP PROC  INSERT_COMPONENT_COMMON
;
IF OBJECT_ID('INSERT_ITEM_SCHEME') IS NOT NULL 
DROP PROC  INSERT_ITEM_SCHEME
;
IF OBJECT_ID('INSERT_PROVISION_AGREEMENT') IS NOT NULL 
DROP PROC  INSERT_PROVISION_AGREEMENT
;
GO
;
IF OBJECT_ID('INSERT_REGISTRATION') IS NOT NULL
DROP PROC INSERT_REGISTRATION
;
GO
;
IF OBJECT_ID('INSERT_DATA_SOURCE') IS NOT NULL
DROP PROC INSERT_DATA_SOURCE
;
GO
;

-- drop obsolete FK
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('FK_DATAFLOW_ARTEFACT') AND OBJECTPROPERTY(id, 'IsForeignKey') = 1)
ALTER TABLE DATAFLOW DROP CONSTRAINT FK_DATAFLOW_ARTEFACT
;
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('CAT_SCH_ART_ID') AND OBJECTPROPERTY(id, 'IsForeignKey') = 1)
ALTER TABLE CATEGORY_SCHEME DROP CONSTRAINT CAT_SCH_ART_ID
;
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('CL_ID') AND OBJECTPROPERTY(id, 'IsForeignKey') = 1)
ALTER TABLE CODELIST DROP CONSTRAINT CL_ID
;

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('CON_SCH_ART_ID') AND OBJECTPROPERTY(id, 'IsForeignKey') = 1)
ALTER TABLE CONCEPT_SCHEME DROP CONSTRAINT CON_SCH_ART_ID
;
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('FK_AGENCY_SCHEME_ARTEFACT') AND OBJECTPROPERTY(id, 'IsForeignKey') = 1)
ALTER TABLE AGENCY_SCHEME DROP CONSTRAINT FK_AGENCY_SCHEME_ARTEFACT
;

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('FK_DATACONSUMER_SCHEME_ART_ID') AND OBJECTPROPERTY(id, 'IsForeignKey') = 1)
ALTER TABLE DATACONSUMER_SCHEME DROP CONSTRAINT FK_DATACONSUMER_SCHEME_ART_ID
;

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('FK_DATAPROVIDER_SCHEME_ART_ID') AND OBJECTPROPERTY(id, 'IsForeignKey') = 1)
ALTER TABLE DATAPROVIDER_SCHEME DROP CONSTRAINT FK_DATAPROVIDER_SCHEME_ART_ID
;

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('FK_ORGANISATION_UNIT_SCH_ART') AND OBJECTPROPERTY(id, 'IsForeignKey') = 1)
ALTER TABLE ORGANISATION_UNIT_SCHEME DROP CONSTRAINT FK_ORGANISATION_UNIT_SCH_ART
;


ALTER TABLE CONCEPT ADD CL_ID BIGINT
;

CREATE TABLE METADATA_ATTRIBUTE(
	[MTD_ATTR_ID] bigint NOT NULL,
	[ID] varchar(50) NOT NULL,
	[MIN_OCCURS] int NULL,
	[MAX_OCCURS] int NULL,
	[IS_PRESENTATIONAL] Smallint NULL,
	[PARENT] bigint NULL,
	[CON_ID] bigint NOT NULL,
	[CL_ID] bigint NULL,
	[RS_ID] bigint NOT NULL,
 PRIMARY KEY (MTD_ATTR_ID) 
) 
;

CREATE TABLE ITEM_SCHEME(
	[ITEM_SCHEME_ID] bigint NOT NULL,
 PRIMARY KEY (ITEM_SCHEME_ID) 
) 
;


CREATE TABLE CONCEPT_ROLE(
	[COMP_ID] bigint NOT NULL,
	[CON_ID] bigint NOT NULL,
 PRIMARY KEY (COMP_ID,CON_ID) 
) 
;

CREATE TABLE TARGET_OBJECT(
	[TARGET_OBJ_ID] bigint NOT NULL,
	[ID] varchar(50) NOT NULL,
	[ITEM_SCHEME_ID] bigint NULL,
	[MDT_ID] bigint NOT NULL,
	[TYPE] varchar(50) NOT NULL,
PRIMARY KEY (TARGET_OBJ_ID) 
) 
;

CREATE TABLE STRUCTURE_USAGE(
	[SU_ID] bigint NOT NULL,
PRIMARY KEY (SU_ID) 
) 
;

CREATE TABLE COMPONENT_COMMON(
	[COMP_ID] bigint IDENTITY NOT NULL,
PRIMARY KEY (COMP_ID) 
) 
;
 
CREATE TABLE METADATA_STRUCTURE_DEFINITION(
	[MSD_ID] bigint NOT NULL,
PRIMARY KEY (MSD_ID) 
) 
;

CREATE TABLE METADATA_TARGET(
	[MDT_ID] bigint NOT NULL,
	[MSD_ID] bigint NOT NULL,
PRIMARY KEY (MDT_ID) 
) 
;

CREATE TABLE METADATAFLOW(
	[MSD_ID] bigint NOT NULL,
	[MDF_ID] bigint NOT NULL,
 PRIMARY KEY (MDF_ID) 
) 
;

CREATE TABLE REPORT_STRUCTURE(
	[RS_ID] bigint NOT NULL,
	[MSD_ID] bigint NOT NULL,
PRIMARY KEY (RS_ID) 
) 
;

CREATE TABLE REPORT_STRUCTURE_META_TARGET(
	[RS_ID] bigint NOT NULL,
	[MDT_ID] bigint NOT NULL,
  PRIMARY KEY (RS_ID,MDT_ID) 
) 
;

CREATE TABLE CONCEPT_TEXT_FORMAT(
	[CON_ID] bigint NOT NULL,
    [FACET_TYPE_ENUM] bigint NOT NULL,
    [FACET_VALUE] nvarchar(1024),
 PRIMARY KEY (CON_ID) 
) 
;

CREATE TABLE PROVISION_AGREEMENT(
	[PA_ID] bigint NOT NULL,
	[SU_ID] bigint NOT NULL,
	[DP_ID] bigint NOT NULL,
  PRIMARY KEY (PA_ID) 
) 
;

ALTER TABLE CONCEPT ADD CONSTRAINT CONCEPT_CODELIST 
	FOREIGN KEY (CL_ID) REFERENCES CODELIST (CL_ID)
;

SET IDENTITY_INSERT COMPONENT_COMMON ON
;

Insert into COMPONENT_COMMON(COMP_ID) SELECT comp_id from COMPONENT
;
SET IDENTITY_INSERT COMPONENT_COMMON OFF
;

-- remove obsolete constraint 

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('FK_COMP_ANN_COMP') AND OBJECTPROPERTY(id, 'IsForeignKey') = 1)
ALTER TABLE COMPONENT_ANNOTATION DROP CONSTRAINT FK_COMP_ANN_COMP
;

-- Remove identity from COMPONENT.COMP_ID 
ALTER TABLE COMPONENT ADD COMP_ID_TMP BIGINT NOT NULL DEFAULT -1
;
UPDATE COMPONENT SET COMP_ID_TMP = COMP_ID
;

-- Find all FK constraints for COMPONENT.COMP_ID

begin
declare @drop as NVARCHAR(MAX) = N'';
declare @create as NVARCHAR(MAX) = N'';

SELECT  @drop += N'ALTER TABLE ' + tab1.name + ' DROP CONSTRAINT ' + obj.name + '; ',
	@create += N'ALTER TABLE ' + tab1.name + ' ADD CONSTRAINT ' +  obj.name 
	+ ' FOREIGN KEY (' + col1.name + ') REFERENCES ' + tab2.name + ' (' + col2.name +') ON DELETE ' +
	(CASE fk.delete_referential_action WHEN 0 THEN N'NO ACTION' ELSE fk.delete_referential_action_desc END) +'; '
FROM sys.foreign_key_columns fkc
INNER JOIN sys.objects obj
    ON obj.object_id = fkc.constraint_object_id
INNER JOIN sys.tables tab1
    ON tab1.object_id = fkc.parent_object_id
INNER JOIN sys.columns col1
    ON col1.column_id = parent_column_id AND col1.object_id = tab1.object_id
INNER JOIN sys.tables tab2
    ON tab2.object_id = fkc.referenced_object_id
INNER JOIN sys.columns col2
    ON col2.column_id = referenced_column_id AND col2.object_id = tab2.object_id
INNER JOIN sys.foreign_keys fk
	ON obj.object_id = fk.object_id
where 'COMP_ID' in (col1.name,  col2.name) and 'COMPONENT' in ( tab1.name, tab2.name);

print N'Executing :' + @drop
-- drop all FK for COMPONENT.COMP_ID
EXEC sp_executeSQL @drop;

ALTER TABLE COMPONENT DROP CONSTRAINT PK_COMPONENT;

-- drop the COMPONENT.COMP_ID column
ALTER TABLE COMPONENT DROP COLUMN COMP_ID;

-- rename the COMPONENT.COMP_ID_TMP to COMP_ID
EXEC sp_RENAME 'COMPONENT.COMP_ID_TMP', 'COMP_ID', 'COLUMN';

ALTER TABLE COMPONENT ADD CONSTRAINT PK_COMPONENT 
	PRIMARY KEY CLUSTERED (COMP_ID);
-- recreate the drop FK constraints for COMP_ID.
print N'Executing :' + @create
EXEC sp_executeSQL @create;

end;
;
-- Populate ITEM SCHEME 

INSERT INTO ITEM_SCHEME 
SELECT CL_ID FROM CODELIST
UNION
SELECT DC_SCH_ID FROM DATACONSUMER_SCHEME
UNION
SELECT DP_SCH_ID FROM DATAPROVIDER_SCHEME
UNION
SELECT ORG_UNIT_SCH_ID FROM ORGANISATION_UNIT_SCHEME
UNION 
SELECT CON_SCH_ID FROM CONCEPT_SCHEME
UNION
SELECT CAT_SCH_ID FROM CATEGORY_SCHEME
UNION
SELECT AG_SCH_ID FROM AGENCY_SCHEME
;

INSERT INTO STRUCTURE_USAGE
SELECT DF_ID FROM DATAFLOW
;

ALTER TABLE [PROVISION_AGREEMENT]  ADD  CONSTRAINT [FK_PROVISION_AGREEMENT_ARTEFACT] FOREIGN KEY([PA_ID])
REFERENCES ARTEFACT ([ART_ID])
ON DELETE CASCADE
;

ALTER TABLE [PROVISION_AGREEMENT]  ADD  CONSTRAINT [FK_PROVISION_AGREEMENT_STRUCTURE_USAGE] FOREIGN KEY([SU_ID])
REFERENCES STRUCTURE_USAGE ([SU_ID])
;

ALTER TABLE [CONCEPT_TEXT_FORMAT]  ADD  CONSTRAINT [FK_CONCEPT_TEXT_FORMAT_CONCEPT] FOREIGN KEY([CON_ID])
REFERENCES CONCEPT ([CON_ID])
;

ALTER TABLE [CONCEPT_TEXT_FORMAT]  ADD  CONSTRAINT [FK_CONCEPT_TEXT_FORMAT_ENUMERATIONS] FOREIGN KEY([FACET_TYPE_ENUM])
REFERENCES ENUMERATIONS ([ENUM_ID])
;

ALTER TABLE [METADATA_ATTRIBUTE]  ADD  CONSTRAINT [FK_METADATA_ATTRIBUTE_CODELIST] FOREIGN KEY([CL_ID])
REFERENCES CODELIST ([CL_ID])
;

ALTER TABLE [METADATA_ATTRIBUTE]  ADD  CONSTRAINT [FK_METADATA_ATTRIBUTE_COMPONENT_COMMON] FOREIGN KEY([MTD_ATTR_ID])
REFERENCES COMPONENT_COMMON ([COMP_ID])
;

ALTER TABLE [METADATA_ATTRIBUTE]  ADD  CONSTRAINT [FK_METADATA_ATTRIBUTE_CONCEPT] FOREIGN KEY([CON_ID])
REFERENCES CONCEPT ([CON_ID])
;

ALTER TABLE [METADATA_ATTRIBUTE]  ADD  CONSTRAINT [FK_METADATA_ATTRIBUTE_METADATA_ATTRIBUTE] FOREIGN KEY([PARENT])
REFERENCES METADATA_ATTRIBUTE ([MTD_ATTR_ID])
;

ALTER TABLE [METADATA_ATTRIBUTE]  ADD  CONSTRAINT [FK_METADATA_ATTRIBUTE_REPORT_STRUCTURE] FOREIGN KEY([RS_ID])
REFERENCES REPORT_STRUCTURE ([RS_ID])
;


ALTER TABLE [AGENCY_SCHEME]  ADD  CONSTRAINT [FK_AGENCY_SCHEME_ITEM_SCH] FOREIGN KEY([AG_SCH_ID])
REFERENCES ITEM_SCHEME ([ITEM_SCHEME_ID])
ON DELETE CASCADE
;

ALTER TABLE [DATACONSUMER_SCHEME]   ADD  CONSTRAINT [FK_DATACONSUMER_SCHEME_ITM_SCH_ID] FOREIGN KEY([DC_SCH_ID])
REFERENCES ITEM_SCHEME ([ITEM_SCHEME_ID])
ON DELETE CASCADE
;

ALTER TABLE [CONCEPT_SCHEME]  ADD  CONSTRAINT [CON_SCH_ITM_SCH_ID] FOREIGN KEY([CON_SCH_ID])
REFERENCES ITEM_SCHEME ([ITEM_SCHEME_ID])
ON DELETE CASCADE
;

ALTER TABLE [DATAPROVIDER_SCHEME]   ADD  CONSTRAINT [FK_DATAPROVIDER_SCHEME_ITM_SCH_ID] FOREIGN KEY([DP_SCH_ID])
REFERENCES ITEM_SCHEME ([ITEM_SCHEME_ID])
ON DELETE CASCADE
;

ALTER TABLE [ITEM_SCHEME]   ADD  CONSTRAINT [FK_ITEM_SCHEME_ARTEFACT] FOREIGN KEY([ITEM_SCHEME_ID])
REFERENCES ARTEFACT ([ART_ID])
ON DELETE CASCADE
;

ALTER TABLE [CODELIST] ADD  CONSTRAINT [CL_ID_ITM_SCM] FOREIGN KEY([CL_ID])
REFERENCES ITEM_SCHEME ([ITEM_SCHEME_ID])
ON DELETE CASCADE
;

ALTER TABLE [ORGANISATION_UNIT_SCHEME]   ADD  CONSTRAINT [FK_ORGANISATION_UNIT_SCH_ITM_SCH] FOREIGN KEY([ORG_UNIT_SCH_ID])
REFERENCES ITEM_SCHEME ([ITEM_SCHEME_ID])
ON DELETE CASCADE
;


ALTER TABLE [TARGET_OBJECT]   ADD  CONSTRAINT [FK_TARGET_OBJECT_To_ITEM_SCHEME] FOREIGN KEY([ITEM_SCHEME_ID])
REFERENCES ITEM_SCHEME ([ITEM_SCHEME_ID])
;

ALTER TABLE [CONCEPT_ROLE]   ADD  CONSTRAINT [FK_CONCEPT_ROLE_COMPONENT] FOREIGN KEY([COMP_ID])
REFERENCES COMPONENT ([COMP_ID])
ON DELETE CASCADE
;


ALTER TABLE [CONCEPT_ROLE]   ADD  CONSTRAINT [FK_CONCEPT_ROLE_CONCEPT] FOREIGN KEY([CON_ID])
REFERENCES CONCEPT ([CON_ID])
;

ALTER TABLE [TARGET_OBJECT]   ADD  CONSTRAINT [FK_TARGET_OBJECT_To_CCOMMON] FOREIGN KEY([TARGET_OBJ_ID])
REFERENCES COMPONENT_COMMON ([COMP_ID])
;


ALTER TABLE [TARGET_OBJECT]   ADD  CONSTRAINT [FK_TARGET_OBJECT_To_MDT] FOREIGN KEY([MDT_ID])
REFERENCES METADATA_TARGET ([MDT_ID])
;


ALTER TABLE [COMPONENT_ANNOTATION] ADD  CONSTRAINT [FK_COMPONENT_ANNOTATION_COMPONENT_COMMON] FOREIGN KEY([COMP_ID])
REFERENCES COMPONENT_COMMON ([COMP_ID])
;


ALTER TABLE [COMPONENT] ADD  CONSTRAINT [FK_COMPONENT_COMMON_COMPONENT] FOREIGN KEY([COMP_ID])
REFERENCES COMPONENT_COMMON ([COMP_ID])
ON DELETE CASCADE
;


ALTER TABLE [METADATAFLOW]   ADD  CONSTRAINT [FK_METADATAFLOW_METADATA_STRUCTURE_DEFINITION] FOREIGN KEY([MSD_ID])
REFERENCES METADATA_STRUCTURE_DEFINITION ([MSD_ID])
;


ALTER TABLE [METADATAFLOW]   ADD  CONSTRAINT [FK_METADATAFLOW_STRUCTURE_USAGE] FOREIGN KEY([MDF_ID])
REFERENCES STRUCTURE_USAGE ([SU_ID])
ON DELETE CASCADE
;

ALTER TABLE [METADATA_TARGET]   ADD  CONSTRAINT [FK_METADATA_TARGET_TO_ITEM] FOREIGN KEY([MDT_ID])
REFERENCES ITEM ([ITEM_ID])
;


ALTER TABLE [METADATA_TARGET]   ADD  CONSTRAINT [FK_METADATA_TARGET_TO_MSD] FOREIGN KEY([MSD_ID])
REFERENCES METADATA_STRUCTURE_DEFINITION ([MSD_ID])
;

ALTER TABLE [METADATA_STRUCTURE_DEFINITION]   ADD  CONSTRAINT [FK_MSD_TO_ARTEFACT] FOREIGN KEY([MSD_ID])
REFERENCES ARTEFACT ([ART_ID])
;

ALTER TABLE [REPORT_STRUCTURE]   ADD  CONSTRAINT [FK_REPORT_STRUCTURE_To_ITEM] FOREIGN KEY([RS_ID])
REFERENCES ITEM ([ITEM_ID])
;


ALTER TABLE [REPORT_STRUCTURE]   ADD  CONSTRAINT [FK_REPORT_STRUCTURE_To_MSD] FOREIGN KEY([MSD_ID])
REFERENCES METADATA_STRUCTURE_DEFINITION ([MSD_ID])
;


ALTER TABLE [REPORT_STRUCTURE_META_TARGET]   ADD  CONSTRAINT [FK_REPORT_STRUCTURE_META_TARGET_METADATA_TARGET] FOREIGN KEY([MDT_ID])
REFERENCES METADATA_TARGET ([MDT_ID])
;


ALTER TABLE [REPORT_STRUCTURE_META_TARGET]   ADD  CONSTRAINT [FK_REPORT_STRUCTURE_META_TARGET_REPORT_STRUCTURE] FOREIGN KEY([RS_ID])
REFERENCES REPORT_STRUCTURE ([RS_ID])
;

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id('FK_TEXT_FORMAT_COMPONENT') AND OBJECTPROPERTY(id, 'IsForeignKey') = 1)
ALTER TABLE TEXT_FORMAT DROP CONSTRAINT FK_TEXT_FORMAT_COMPONENT
;

ALTER TABLE TEXT_FORMAT ADD CONSTRAINT
	FK_TEXT_FORMAT_COMPONENT FOREIGN KEY
	(COMP_ID) REFERENCES COMPONENT_COMMON (COMP_ID) ON DELETE CASCADE
;

GO
;


ALTER TABLE CATEGORY_SCHEME  ADD  CONSTRAINT CAT_SCH_ITM_SCH_ID FOREIGN KEY(CAT_SCH_ID)
REFERENCES ITEM_SCHEME (ITEM_SCHEME_ID)
ON DELETE CASCADE
;

ALTER TABLE STRUCTURE_USAGE ADD CONSTRAINT FK_STRUCTURE_USAGE_ARTEFACT  FOREIGN KEY(SU_ID)
REFERENCES ARTEFACT (ART_ID)
ON DELETE CASCADE
;

ALTER TABLE DATAFLOW   ADD  CONSTRAINT FK_DATAFLOW_STRUCTURE_USAGE FOREIGN KEY(DF_ID)
REFERENCES STRUCTURE_USAGE (SU_ID)
ON DELETE CASCADE
;
GO
;

-- first insert the base proc used by others.
CREATE PROCEDURE INSERT_COMPONENT_COMMON(
 @p_pk bigint OUT
	)
AS
BEGIN
SET NOCOUNT ON;
	INSERT INTO COMPONENT_COMMON DEFAULT VALUES;
	set @p_pk = SCOPE_IDENTITY();
END
;
GO
;

CREATE PROCEDURE INSERT_ITEM_SCHEME(
 @p_id varchar(50),
 @p_version varchar(50),
 @p_agency varchar(50),
 @p_valid_from datetime = NULL,
 @p_valid_to datetime = NULL,
 @p_is_final int = NULL,
 @p_uri nvarchar(255),
 @p_last_modified datetime = NULL,
 @p_pk bigint OUT)
AS
BEGIN
SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

		EXEC INSERT_ARTEFACT @p_id = @p_id,@p_version = @p_version,@p_agency = @p_agency,@p_valid_from = @p_valid_from,@p_valid_to = @p_valid_to,@p_is_final = @p_is_final,@p_uri = @p_uri,@p_last_modified = @p_last_modified, @p_pk = @p_pk OUTPUT;
		insert into ITEM_SCHEME (ITEM_SCHEME_ID) VALUES (@p_pk);

	IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
GO
;

CREATE PROCEDURE INSERT_COMPONENT
	@p_type varchar(50) ,
    @p_id varchar(50)=NULL,
	@p_dsd_id bigint,
	@p_con_id bigint,
	@p_cl_id bigint=NULL,
	@p_con_sch_id bigint=NULL,
	@p_is_freq_dim int=NULL,
	@p_is_measure_dim int=NULL,
	@p_att_ass_level varchar(11)=NULL,
	@p_att_status varchar(11)=NULL,
	@p_att_is_time_format int=NULL,
	@p_xs_attlevel_ds int=NULL,
	@p_xs_attlevel_group int=NULL,
	@p_xs_attlevel_section int=NULL,
	@p_xs_attlevel_obs int=NULL,
	@p_xs_measure_code varchar(50)=NULL,
	@p_pk bigint OUT  
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

		exec INSERT_COMPONENT_COMMON @p_pk = @p_pk OUTPUT ;
		insert into COMPONENT (COMP_ID,ID, TYPE, DSD_ID, CON_ID, CL_ID, IS_FREQ_DIM, IS_MEASURE_DIM, ATT_ASS_LEVEL, ATT_STATUS, ATT_IS_TIME_FORMAT, XS_ATTLEVEL_DS, XS_ATTLEVEL_GROUP, XS_ATTLEVEL_SECTION, XS_ATTLEVEL_OBS, XS_MEASURE_CODE, CON_SCH_ID) 
		values (@p_pk,@p_id, @p_type, @p_dsd_id, @p_con_id, @p_cl_id, @p_is_freq_dim, @p_is_measure_dim, @p_att_ass_level, @p_att_status, @p_att_is_time_format, @p_xs_attlevel_ds, @p_xs_attlevel_group, @p_xs_attlevel_section, @p_xs_attlevel_obs, @p_xs_measure_code, @p_con_sch_id);
		IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
GO
;
CREATE PROCEDURE INSERT_CONCEPT
	@p_id varchar(150), 
	@p_con_sch_id bigint,
	@p_parent_con_id bigint,
	@p_cl_id bigint,
	@p_pk bigint OUT 
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

		insert into ITEM (ID) VALUES (@p_id);
		set @p_pk = SCOPE_IDENTITY();
		insert into CONCEPT (CON_ID, CON_SCH_ID,PARENT_CONCEPT_ID,CL_ID) VALUES (@p_pk, @p_con_sch_id,@p_parent_con_id,@p_cl_id);
	
		IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
GO
;

CREATE PROCEDURE INSERT_STRUCTURE_USAGE(
 @p_id varchar(50),
 @p_version  varchar(50),
 @p_agency varchar(50),
 @p_valid_from datetime = NULL,
 @p_valid_to datetime = NULL,
 @p_is_final bigint = NULL,
 @p_uri  nvarchar(50),
 @p_last_modified datetime = NULL,
 @p_pk bigint OUT
	)
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

	EXEC INSERT_ARTEFACT @p_id = @p_id,@p_version = @p_version,@p_agency = @p_agency,@p_valid_from = @p_valid_from,@p_valid_to = @p_valid_to,@p_is_final = @p_is_final,@p_uri = @p_uri,@p_last_modified = @p_last_modified, @p_pk = @p_pk OUTPUT;
	insert into STRUCTURE_USAGE (SU_ID) VALUES (@p_pk);

	IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
GO
;
CREATE PROCEDURE INSERT_METADATAFLOW(
    @p_id varchar(50),
    @p_version  varchar(50),
    @p_agency varchar(50),
    @p_valid_from datetime = NULL,
    @p_valid_to datetime = NULL,
    @p_is_final bigint = NULL,
    @p_uri  nvarchar(50),
    @p_last_modified datetime = NULL,
 @p_msd_id  bigint,
 @p_pk bigint OUT)
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

	 EXEC INSERT_STRUCTURE_USAGE @p_id = @p_id,@p_version = @p_version,@p_agency = @p_agency,@p_valid_from = @p_valid_from,@p_valid_to = @p_valid_to,@p_is_final = @p_is_final,@p_uri = @p_uri,@p_last_modified = @p_last_modified, @p_pk = @p_pk OUTPUT;
	 INSERT INTO METADATAFLOW
           (MSD_ID,MDF_ID)
     VALUES
           (@p_msd_id,@p_pk);
	IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
GO
;

CREATE PROCEDURE INSERT_MSD(
 @p_id varchar(50),
    @p_version  varchar(50),
    @p_agency varchar(50),
    @p_valid_from datetime = NULL,
    @p_valid_to datetime = NULL,
    @p_is_final bigint = NULL,
    @p_uri  nvarchar(50),
    @p_last_modified datetime = NULL,
 @p_pk  bigint OUT)
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

	EXEC INSERT_ARTEFACT @p_id = @p_id,@p_version = @p_version,@p_agency = @p_agency,@p_valid_from = @p_valid_from,@p_valid_to = @p_valid_to,@p_is_final = @p_is_final,@p_uri = @p_uri,@p_last_modified = @p_last_modified, @p_pk = @p_pk OUTPUT;
	INSERT INTO METADATA_STRUCTURE_DEFINITION
           (MSD_ID)
     VALUES
           (@p_pk);
	IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
GO
;

CREATE PROCEDURE INSERT_METADATA_TARGET(
 @p_id  varchar(150),
 @p_MSD_ID bigint,
 @p_pk  bigint OUT)
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

	insert into ITEM (ID) VALUES (@p_id);
		set @p_pk = SCOPE_IDENTITY();
		insert into METADATA_TARGET (MDT_ID,MSD_ID) VALUES (@p_pk,@p_MSD_ID);

	IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
GO
;

CREATE PROCEDURE INSERT_TARGET_OBJECT(
 @p_id varchar(50),
 @p_ITEM_SCHEME_ID bigint,
 @p_MDT_ID bigint,
 @p_TYPE varchar(50),
 @p_pk  bigint OUT)
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

		EXEC INSERT_COMPONENT_COMMON @p_pk = @p_pk;
		insert into TARGET_OBJECT (TARGET_OBJ_ID,ID,ITEM_SCHEME_ID,MDT_ID,TYPE) VALUES (@p_pk,@p_id,@p_ITEM_SCHEME_ID,@p_MDT_ID,@p_TYPE);
		set @p_pk = SCOPE_IDENTITY();
	IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
GO
;

CREATE PROCEDURE INSERT_REPORT_STRUCTURE(
 @p_id  varchar(150),
 @p_MSD_ID bigint,
 @p_pk  bigint OUT)
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION
	insert into ITEM (ID) VALUES (@p_id);
		set @p_pk = SCOPE_IDENTITY();
		insert into REPORT_STRUCTURE (RS_ID,MSD_ID) VALUES (@p_pk,@p_MSD_ID);
	IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
GO
;

CREATE PROCEDURE INSERT_METADATA_ATTRIBUTE(
 @p_id varchar(50),
 @p_MIN_OCCURS  int,
 @p_MAX_OCCURS  int,
 @p_IS_PRESENTATIONAL  Smallint,
 @p_PARENT  bigint,
 @p_CON_ID  bigint,
 @p_CL_ID  bigint,
 @p_RS_ID  bigint,
 @p_pk  bigint OUT)
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

	EXEC INSERT_COMPONENT_COMMON @p_pk = @p_pk;
	INSERT INTO METADATA_ATTRIBUTE
           (MTD_ATTR_ID,ID,MIN_OCCURS,MAX_OCCURS,IS_PRESENTATIONAL,PARENT,CON_ID,CL_ID,RS_ID)
     VALUES
           (@p_pk,@p_id,@p_MIN_OCCURS,@p_MAX_OCCURS,@p_IS_PRESENTATIONAL,@p_PARENT,@p_CON_ID,@p_CL_ID,@p_RS_ID);

	 IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
	
END
;
GO
;

CREATE PROCEDURE INSERT_PROVISION_AGREEMENT(
 @p_id varchar(50),
    @p_version  varchar(50),
    @p_agency varchar(50),
    @p_valid_from datetime = NULL,
    @p_valid_to datetime = NULL,
    @p_is_final bigint = NULL,
    @p_uri  nvarchar(50),
 @p_last_modified  datetime = NULL,
 @p_su_id bigint,
 @p_dp_id bigint,
 @p_pk  bigint OUT)
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

	EXEC INSERT_ARTEFACT @p_id = @p_id,@p_version = @p_version,@p_agency = @p_agency,@p_valid_from = @p_valid_from,@p_valid_to = @p_valid_to,@p_is_final = @p_is_final,@p_uri = @p_uri,@p_last_modified = @p_last_modified, @p_pk = @p_pk OUTPUT;
	INSERT INTO PROVISION_AGREEMENT
           (PA_ID,SU_ID,DP_ID)
     VALUES
           (@p_pk,@p_su_id,@p_dp_id);
	 IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH	
END
;
GO
;

CREATE PROCEDURE INSERT_CATEGORY_SCHEME
	@p_id varchar(50),
	@p_version varchar(50),
	@p_agency varchar(50),
	@p_valid_from datetime = NULL,
	@p_valid_to datetime= NULL,
	@p_is_final int = NULL,
	@p_uri nvarchar(255),
	@p_last_modified datetime = NULL,
    @p_is_partial bit = 0,
	@p_pk bigint out
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

	exec INSERT_ITEM_SCHEME @p_id = @p_id,@p_version = @p_version,@p_agency = @p_agency,@p_valid_from = @p_valid_from,@p_valid_to = @p_valid_to,@p_is_final = @p_is_final,@p_uri=@p_uri,@p_last_modified = @p_last_modified,@p_pk = @p_pk OUTPUT;
	
	INSERT INTO CATEGORY_SCHEME
           (CAT_SCH_ID, IS_PARTIAL)
     VALUES
           (@p_pk, @p_is_partial);

	IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
GO
;

CREATE PROCEDURE INSERT_CODELIST
	@p_id varchar(50),
	@p_version varchar(50),
	@p_agency varchar(50),
	@p_valid_from datetime = NULL,
	@p_valid_to datetime= NULL,
	@p_is_final int = NULL,
	@p_uri nvarchar(255),
	@p_last_modified datetime = NULL,
    @p_is_partial bit = 0,
	@p_pk bigint out
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

	exec INSERT_ITEM_SCHEME @p_id = @p_id,@p_version = @p_version,@p_agency = @p_agency,@p_valid_from = @p_valid_from,@p_valid_to = @p_valid_to,@p_is_final = @p_is_final,@p_uri = @p_uri,@p_last_modified = @p_last_modified,@p_pk = @p_pk OUTPUT;
	
	INSERT INTO CODELIST
           (CL_ID, IS_PARTIAL)
     VALUES
           (@p_pk, @p_is_partial);

	IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
GO
;

CREATE PROCEDURE INSERT_CONCEPT_SCHEME
	@p_id varchar(50),
	@p_version varchar(50),
	@p_agency varchar(50),
	@p_valid_from datetime = NULL,
	@p_valid_to datetime= NULL,
	@p_is_final int = NULL,
	@p_uri nvarchar(255),
	@p_last_modified datetime = NULL,
    @p_is_partial bit = 0,
	@p_pk bigint out
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

	exec INSERT_ITEM_SCHEME @p_id = @p_id,@p_version = @p_version,@p_agency = @p_agency,@p_valid_from = @p_valid_from,@p_valid_to = @p_valid_to,@p_is_final = @p_is_final,@p_uri = @p_uri, @p_last_modified = @p_last_modified,@p_pk = @p_pk OUTPUT;
	
	INSERT INTO CONCEPT_SCHEME
           (CON_SCH_ID, IS_PARTIAL)
     VALUES
           (@p_pk, @p_is_partial);

	IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
GO
;

CREATE PROCEDURE INSERT_AGENCY_SCHEME
	@p_id varchar(50),
	@p_version varchar(50),
	@p_agency varchar(50),
	@p_valid_from datetime = NULL,
	@p_valid_to datetime= NULL,
	@p_is_final int = NULL,
	@p_uri nvarchar(255),
	@p_last_modified datetime = NULL,
    @p_is_partial bit = 0,
	@p_pk bigint out
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

	exec INSERT_ITEM_SCHEME @p_id = @p_id,@p_version = @p_version,@p_agency = @p_agency,@p_valid_from = @p_valid_from,@p_valid_to = @p_valid_to,@p_is_final = @p_is_final,@p_uri = @p_uri, @p_last_modified = @p_last_modified,@p_pk = @p_pk OUTPUT;
	
	INSERT INTO AGENCY_SCHEME
           (AG_SCH_ID, IS_PARTIAL)
     VALUES
           (@p_pk, @p_is_partial);

	IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
GO
;

CREATE PROCEDURE INSERT_DATACONSUMER_SCHEME
	@p_id varchar(50),
	@p_version varchar(50),
	@p_agency varchar(50),
	@p_valid_from datetime = NULL,
	@p_valid_to datetime= NULL,
	@p_is_final int = NULL,
	@p_uri nvarchar(255),
	@p_last_modified datetime = NULL,
    @p_is_partial bit = 0,
	@p_pk bigint out
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

	exec INSERT_ITEM_SCHEME @p_id = @p_id,@p_version = @p_version,@p_agency = @p_agency,@p_valid_from = @p_valid_from,@p_valid_to = @p_valid_to,@p_is_final = @p_is_final,@p_uri = @p_uri, @p_last_modified = @p_last_modified,@p_pk = @p_pk OUTPUT;
	
	INSERT INTO DATACONSUMER_SCHEME
           (DC_SCH_ID, IS_PARTIAL)
     VALUES
           (@p_pk, @p_is_partial);

	IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
GO
;

CREATE PROCEDURE INSERT_DATAPROVIDER_SCHEME
	@p_id varchar(50),
	@p_version varchar(50),
	@p_agency varchar(50),
	@p_valid_from datetime = NULL,
	@p_valid_to datetime= NULL,
	@p_is_final int = NULL,
	@p_uri nvarchar(255),
	@p_last_modified datetime = NULL,
    @p_is_partial bit = 0,
	@p_pk bigint out
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

	exec INSERT_ITEM_SCHEME @p_id = @p_id,@p_version = @p_version,@p_agency = @p_agency,@p_valid_from = @p_valid_from,@p_valid_to = @p_valid_to,@p_is_final = @p_is_final,@p_uri = @p_uri, @p_last_modified = @p_last_modified,@p_pk = @p_pk OUTPUT;
	
	INSERT INTO DATAPROVIDER_SCHEME
           (DP_SCH_ID, IS_PARTIAL)
     VALUES
           (@p_pk, @p_is_partial);

	IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
GO
;

CREATE PROCEDURE INSERT_ORGANISATION_UNIT_SCH
	@p_id varchar(50),
	@p_version varchar(50),
	@p_agency varchar(50),
	@p_valid_from datetime = NULL,
	@p_valid_to datetime= NULL,
	@p_is_final int = NULL,
	@p_uri nvarchar(255),
	@p_last_modified datetime = NULL,
    @p_is_partial bit = 0,
	@p_pk bigint out
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

	exec INSERT_ITEM_SCHEME @p_id = @p_id,@p_version = @p_version,@p_agency = @p_agency,@p_valid_from = @p_valid_from,@p_valid_to = @p_valid_to,@p_is_final = @p_is_final,@p_uri = @p_uri, @p_last_modified = @p_last_modified,@p_pk = @p_pk OUTPUT;
	
	INSERT INTO ORGANISATION_UNIT_SCHEME
           (ORG_UNIT_SCH_ID, IS_PARTIAL)
     VALUES
           (@p_pk, @p_is_partial);

	IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
GO
;

CREATE PROCEDURE INSERT_DATAFLOW
	@p_id varchar(50),
	@p_version varchar(50),
	@p_agency varchar(50),
	@p_valid_from datetime = NULL,
	@p_valid_to datetime= NULL,
	@p_is_final int = NULL,
	@p_uri nvarchar(255),
	@p_last_modified datetime = NULL,
	@p_dsd_id bigint,
    @p_map_set_id bigint = null,
	@p_pk bigint out
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

	exec INSERT_STRUCTURE_USAGE @p_id = @p_id,@p_version = @p_version,@p_agency = @p_agency,@p_valid_from = @p_valid_from,@p_valid_to = @p_valid_to,@p_is_final = @p_is_final,@p_uri = @p_uri,@p_last_modified = @p_last_modified,@p_pk = @p_pk OUTPUT;
	
INSERT INTO DATAFLOW
           (DF_ID
           ,DSD_ID
           ,MAP_SET_ID)
     VALUES
           (@p_pk
           ,@p_dsd_id
           ,@p_map_set_id);

	IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
GO
;
CREATE TABLE REGISTRATION (
	DR_ID BIGINT NOT NULL IDENTITY,
	ID VARCHAR(50) NULL,
	VALID_FROM datetime NULL,
    VALID_TO datetime NULL,
    LAST_UPDATED datetime NULL,
    INDEX_TIME_SERIES BIT NOT NULL DEFAULT 0,
    INDEX_DATA_SET BIT NOT NULL DEFAULT 0,
    INDEX_ATTRIBUTES BIT NOT NULL DEFAULT 0,
    INDEX_REPORTING_PERIOD BIT NOT NULL DEFAULT 0,
    PA_ID bigint NOT NULL,
    DATA_SOURCE_ID bigint NOT NULL,
    PRIMARY KEY(DR_ID)
) 
;
GO
;
CREATE TABLE DATA_SOURCE (
   DATA_SOURCE_ID bigint NOT NULL IDENTITY,
   -- ID VARCHAR(50) NULL, Doubled checked and doesn't appear to be a need for an ID.
   DATA_URL nvarchar(500) NOT NULL,
   WSDL_URL nvarchar(500) NULL,
   WADL_URL nvarchar(500) NULL,
   IS_SIMPLE BIT NOT NULL,
   IS_REST BIT NOT NULL,
   IS_WS BIT NOT NULL,
   PRIMARY KEY(DATA_SOURCE_ID)
)
;
GO
;
alter table REGISTRATION add constraint FK_REGISTRATION_PROVISION foreign key (PA_ID) 
references PROVISION_AGREEMENT (PA_ID)
;
GO
;
alter table REGISTRATION add constraint FK_REGISTRATION_DATASOURCE foreign key (DATA_SOURCE_ID)
references DATA_SOURCE (DATA_SOURCE_ID)
ON DELETE CASCADE
;
GO
;
create procedure INSERT_REGISTRATION
	@p_id varchar(50),
	@p_valid_from datetime,
	@p_valid_to datetime,
	@p_last_updated datetime,
    @p_index_ts bit,
    @p_index_ds bit,
    @p_index_attributes bit,
    @p_index_reporting_period bit,
    @p_pa_id bigint,
    @p_datasource_id bigint,
	@p_pk bigint OUT
AS
BEGIN
	SET NOCOUNT ON;
	insert into REGISTRATION (ID, VALID_FROM, VALID_TO, LAST_UPDATED, INDEX_TIME_SERIES, INDEX_DATA_SET, INDEX_ATTRIBUTES, INDEX_REPORTING_PERIOD, PA_ID, DATA_SOURCE_ID) 
    values (@p_id, @p_valid_from, @p_valid_to, @p_last_updated, @p_index_ts, @p_index_ds, @p_index_attributes, @p_index_reporting_period, @p_pa_id, @p_datasource_id);
	set @p_pk = SCOPE_IDENTITY();
END
;
GO
;
create procedure INSERT_DATA_SOURCE
	@p_data_url nvarchar(500),
	@p_wsdl_url nvarchar(500),
	@p_wadl_url nvarchar(500),
	@p_is_simple bit,
	@p_is_rest bit,
	@p_is_ws bit,
	@p_pk bigint OUT
AS
BEGIN
	SET NOCOUNT ON;
	insert into DATA_SOURCE (DATA_URL, WSDL_URL, WADL_URL, IS_SIMPLE, IS_REST, IS_WS) 
    values (@p_data_url, @p_wsdl_url, @p_wadl_url, @p_is_simple, @p_is_rest, @p_is_ws);
	set @p_pk = SCOPE_IDENTITY();
END
;
GO
;
update DB_VERSION SET VERSION = '5.0'
;
