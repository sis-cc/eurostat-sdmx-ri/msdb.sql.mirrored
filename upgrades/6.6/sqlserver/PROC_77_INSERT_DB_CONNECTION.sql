IF EXISTS (SELECT * FROM dbo.SYSOBJECTS WHERE id = object_id('INSERT_DB_CONNECTION') AND  OBJECTPROPERTY(id, 'IsProcedure') = 1)
DROP PROC INSERT_DB_CONNECTION
;

CREATE PROCEDURE INSERT_DB_CONNECTION
@p_db_name varchar(1000) ,
@p_db_type varchar(50) ,
@p_name varchar(50) ,
@p_db_password varchar(50) ,
@p_db_port int ,
@p_db_server varchar(100) ,
@p_db_user varchar(50) ,
@p_ado_connection_string varchar(2000) ,
@p_jdbc_connection_string varchar(2000) ,
@p_properties varchar(1000) ,
@p_user_id bigint ,
@p_pk bigint OUT
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

		INSERT INTO DB_CONNECTION (DB_NAME,DB_TYPE,NAME,DB_PASSWORD,DB_PORT,DB_SERVER,DB_USER,ADO_CONNECTION_STRING,JDBC_CONNECTION_STRING,PROPERTIES,USER_ID) VALUES (@p_db_name, @p_db_type, @p_name, @p_db_password, @p_db_port, @p_db_server, @p_db_user, @p_ado_connection_string, @p_jdbc_connection_string, @p_properties, @p_user_id);
		set @p_pk = SCOPE_IDENTITY();
	IF @starttrancount = 0
	COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	RAISERROR (@ErrorMessage, -- Message text.
				@ErrorSeverity, -- Severity.
				@ErrorState -- State.
				);
	END CATCH
END
;
