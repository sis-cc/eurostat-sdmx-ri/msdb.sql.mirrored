
Create table ATTACH_TYPES(
ATTACH_TYPE Varchar(20) Not Null,
PRIMARY KEY (ATTACH_TYPE))

;

Insert into ATTACH_TYPES values
('DataProvider'),
('DataSet'),
('MetadataSet'),
('SimpleDataSource'),
('DataStructure'),
('MetadataStructure'),
('Dataflow'),
('Metadataflow'),
('ProvisionAgreement')
;

