
DROP PROCEDURE IF EXISTS INSERT_CONTENT_CONSTRAINT
;
CREATE PROCEDURE INSERT_CONTENT_CONSTRAINT(
      IN p_id varchar(50),
      IN p_version  varchar(50),
      IN p_agency varchar(50),
      IN p_valid_from   datetime,
      IN p_valid_to   datetime,
      IN p_is_final  bigint,
      IN p_uri  nvarchar(255),
      IN p_last_modified  datetime,
      IN p_actual_data bigint,
      IN p_periodicity varchar(50),
      IN p_offset varchar(50),
      IN p_tolerance varchar(50),
	  IN p_attach_type varchar(20),
	  IN dp_id bigint,
	  IN set_id varchar(255),
	  IN simple_data_source nvarchar(1024),
      OUT p_pk bigint )
BEGIN

	call INSERT_ARTEFACT(p_id,p_version,p_agency,p_valid_from,p_valid_to,p_is_final,p_uri,p_last_modified, p_pk);

       INSERT INTO CONTENT_CONSTRAINT
           (CONT_CONS_ID, ACTUAL_DATA, PERIODICITY, OFFSET, TOLERANCE,ATTACH_TYPE,DP_ID,SET_ID,SIMPLE_DATA_SOURCE)
     VALUES
           (p_pk, p_actual_data, p_periodicity, p_offset, p_tolerance,p_attach_type,dp_id,set_id,simple_data_source);
END
;


CREATE TEMPORARY TABLE ATTACH_TYPE_TEMP
(
ATTACH_TYPE VARCHAR(30),
CONT_CONS_ID VARCHAR(30)
)
;

INSERT INTO ATTACH_TYPE_TEMP SELECT DISTINCT
                                                   CASE
                                                   WHEN C.DSD_ID IS NOT NULL THEN 'DataStructure'
                                                   WHEN D.DF_ID IS NOT NULL THEN 'Dataflow'
                                                   WHEN PA.PA_ID IS NOT NULL THEN 'ProvisionAgreement'
                                                   WHEN MD.MDF_ID IS NOT NULL THEN 'Metadataflow'
                                                   WHEN MSD.MSD_ID IS NOT NULL THEN 'MetadataStructure'

                                                   END AS ATTACH_TYPE,
                                                   A.CONT_CONS_ID
                                                       FROM CONTENT_CONSTRAINT_ATTACHMENT A
                                                   INNER JOIN ARTEFACT_VIEW B
                                                   ON A.ART_ID = B.ART_ID
                                                       LEFT OUTER JOIN DSD C
                                                   ON B.ART_ID = C.DSD_ID
                                                       LEFT OUTER JOIN DATAFLOW D
                                                   ON B.ART_ID = D.DF_ID
                                                       LEFT OUTER JOIN PROVISION_AGREEMENT PA
                                                   ON B.ART_ID = PA.PA_ID
                                                       LEFT OUTER JOIN METADATAFLOW MD
                                                   ON B.ART_ID = MD.MDF_ID
                                                       LEFT OUTER JOIN METADATA_STRUCTURE_DEFINITION MSD
                                                   ON B.ART_ID = MSD.MSD_ID
                                                       INNER JOIN CONTENT_CONSTRAINT E
                                                       ON A.CONT_CONS_ID = E.CONT_CONS_ID
                                                       WHERE A.CONT_CONS_ID = E.CONT_CONS_ID
;
Update CONTENT_CONSTRAINT set CONTENT_CONSTRAINT.ATTACH_TYPE = (select ATTACH_TYPE from ATTACH_TYPE_TEMP where CONTENT_CONSTRAINT.CONT_CONS_ID = ATTACH_TYPE_TEMP.CONT_CONS_ID limit 1)
;
Drop table ATTACH_TYPE_TEMP
;

-- always last
update DB_VERSION SET VERSION = '6.6'
;
