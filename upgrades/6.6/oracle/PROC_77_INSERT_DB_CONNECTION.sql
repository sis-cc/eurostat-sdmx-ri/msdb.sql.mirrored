


CREATE OR REPLACE PROCEDURE INSERT_DB_CONNECTION(
p_db_name IN varchar2,
p_db_type IN varchar2,
p_name IN varchar2,
p_db_password IN varchar2,
p_db_port IN number,
p_db_server IN varchar2,
p_db_user IN varchar2,
p_ado_connection_string IN varchar2,
p_jdbc_connection_string IN varchar2,
P_properties IN varchar2,
p_user_id IN number,
p_pk OUT number)
AS
BEGIN
		INSERT INTO DB_CONNECTION (DB_NAME,DB_TYPE,NAME,DB_PASSWORD,DB_PORT,DB_SERVER,DB_USER,ADO_CONNECTION_STRING,JDBC_CONNECTION_STRING,PROPERTIES,USER_ID) VALUES (p_db_name, p_db_type, p_name, p_db_password, p_db_port, p_db_server, p_db_user, p_ado_connection_string, p_jdbc_connection_string, p_properties, p_user_id) RETURNING CONNECTION_ID INTO p_pk;
END;
/
;
