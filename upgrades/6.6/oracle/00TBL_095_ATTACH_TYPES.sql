
Create table ATTACH_TYPES(
ATTACH_TYPE varchar2(20) Not Null,
PRIMARY KEY (ATTACH_TYPE))
;

Insert into ATTACH_TYPES values ('DataProvider')
;
Insert into ATTACH_TYPES values ('DataSet')
;
Insert into ATTACH_TYPES values ('MetadataSet')
;
Insert into ATTACH_TYPES values ('SimpleDataSource')
;
Insert into ATTACH_TYPES values ('DataStructure')
;
Insert into ATTACH_TYPES values ('MetadataStructure')
;
Insert into ATTACH_TYPES values ('Dataflow')
;
Insert into ATTACH_TYPES values ('Metadataflow')
;
Insert into ATTACH_TYPES values ('ProvisionAgreement')
;