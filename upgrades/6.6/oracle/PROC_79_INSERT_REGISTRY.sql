

CREATE OR REPLACE PROCEDURE INSERT_REGISTRY(
  p_url IN varchar2,
  p_username IN varchar2,
  p_password IN varchar2,
  p_description IN varchar2,
  p_technology IN varchar2,
  p_name IN varchar2,
  p_is_public IN NUMBER,
  p_upgrades IN NUMBER,
  p_proxy IN NUMBER,
  p_pk OUT number)
AS
BEGIN
   INSERT INTO REGISTRY (URL,USERNAME,PASSWORD,DESCRIPTION,TECHNOLOGY,NAME,IS_PUBLIC,UPGRADES,PROXY) VALUES (p_url,p_username, p_password,p_description,p_technology,p_name,p_is_public,p_upgrades,p_proxy) RETURNING ID INTO p_pk;
END;
/
;
