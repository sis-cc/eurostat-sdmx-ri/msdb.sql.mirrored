CREATE OR REPLACE PROCEDURE INSERT_PROXY_SETTINGS(
  p_enable_proxy IN NUMBER,
  p_url IN varchar2,
  p_authentication IN NUMBER,
  p_username IN varchar2,
  p_password IN varchar2,
  p_pk OUT number)
AS
BEGIN
   INSERT INTO PROXY_SETTINGS (ENABLE_PROXY,URL,AUTHENTICATION,USERNAME,PASSWORD) VALUES (p_enable_proxy,p_url,p_authentication,p_username,p_password) RETURNING ID INTO p_pk;
END;
/
;
