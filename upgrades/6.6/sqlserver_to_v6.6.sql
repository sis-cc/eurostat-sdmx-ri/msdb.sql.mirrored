DROP PROCEDURE INSERT_CONTENT_CONSTRAINT
;
GO
;
CREATE PROCEDURE INSERT_CONTENT_CONSTRAINT
       @p_id varchar(50),
       @p_version varchar(50),
       @p_agency varchar(50),
       @p_valid_from datetime = NULL,
       @p_valid_to datetime = NULL,
       @p_is_final bigint = NULL,
       @p_uri nvarchar(255),
       @p_last_modified  datetime = NULL,
       @p_actual_data bigint = 1,
       @p_periodicity varchar(50) = NULL,
       @p_offset varchar(50) = NULL,
       @p_tolerance varchar(50) = NULL,
       @p_attach_type VARCHAR(20) = 'Dataflow',
	   @dp_id bigint = null,
	   @set_id varchar(255) = null,
	   @simple_data_source nvarchar(1024) = null,
	   @p_pk bigint OUT

AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0
			BEGIN TRANSACTION

	   exec INSERT_ARTEFACT @p_id = @p_id,@p_version = @p_version,@p_agency = @p_agency,@p_valid_from = @p_valid_from,@p_valid_to = @p_valid_to,@p_is_final = @p_is_final,@p_uri = @p_uri, @p_last_modified = @p_last_modified, @p_pk = @p_pk OUTPUT;

       INSERT INTO CONTENT_CONSTRAINT
           (CONT_CONS_ID, ACTUAL_DATA, PERIODICITY, OFFSET, TOLERANCE,ATTACH_TYPE,DP_ID,SET_ID,SIMPLE_DATA_SOURCE)
     VALUES
           (@p_pk, @p_actual_data, @p_periodicity, @p_offset, @p_tolerance,@p_attach_type,@dp_id,@set_id,@simple_data_source);
IF @starttrancount = 0
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
GO
;

CREATE TABLE #ATTACH_TYPE
(
ATTACH_TYPE VARCHAR(30),
CONT_CONS_ID VARCHAR(30),
)
;
GO
;
INSERT INTO #ATTACH_TYPE SELECT DISTINCT
                                                   CASE
                                                   WHEN C.DSD_ID IS NOT NULL THEN 'DataStructure'
                                                   WHEN D.DF_ID IS NOT NULL THEN 'Dataflow'
                                                   WHEN PA.PA_ID IS NOT NULL THEN 'ProvisionAgreement'
                                                   WHEN MD.MDF_ID IS NOT NULL THEN 'Metadataflow'
                                                   WHEN MSD.MSD_ID IS NOT NULL THEN 'MetadataStructure'

                                                   END AS ATTACH_TYPE,
                                                   A.CONT_CONS_ID
                                                       FROM CONTENT_CONSTRAINT_ATTACHMENT A
                                                   INNER JOIN ARTEFACT_VIEW B
                                                   ON A.ART_ID = B.ART_ID
                                                       LEFT OUTER JOIN DSD C
                                                   ON B.ART_ID = C.DSD_ID
                                                       LEFT OUTER JOIN DATAFLOW D
                                                   ON B.ART_ID = D.DF_ID
                                                       LEFT OUTER JOIN PROVISION_AGREEMENT PA
                                                   ON B.ART_ID = PA.PA_ID
                                                       LEFT OUTER JOIN METADATAFLOW MD
                                                   ON B.ART_ID = MD.MDF_ID
                                                       LEFT OUTER JOIN METADATA_STRUCTURE_DEFINITION MSD
                                                   ON B.ART_ID = MSD.MSD_ID
                                                       INNER JOIN CONTENT_CONSTRAINT E
                                                       ON A.CONT_CONS_ID = E.CONT_CONS_ID
                                                       WHERE A.CONT_CONS_ID = E.CONT_CONS_ID
;
GO
;
Update CONTENT_CONSTRAINT set CONTENT_CONSTRAINT.ATTACH_TYPE = (select TOP 1 ATTACH_TYPE from #ATTACH_TYPE where CONTENT_CONSTRAINT.CONT_CONS_ID = #ATTACH_TYPE.CONT_CONS_ID )
;
GO
;
Drop table #ATTACH_TYPE
;
GO
;
-- always last
update DB_VERSION SET VERSION = '6.6'
;

