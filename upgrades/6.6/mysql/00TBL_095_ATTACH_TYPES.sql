
Create table ATTACH_TYPES(
ATTACH_TYPE Varchar(20) Not Null,
PRIMARY KEY (ATTACH_TYPE))
ENGINE=InnoDB DEFAULT CHARSET=utf8
;

Insert into ATTACH_TYPES values ('DataProvider')
;
Insert into ATTACH_TYPES values ('DataSet')
;
Insert into ATTACH_TYPES values ('MetadataSet')
;
Insert into ATTACH_TYPES values ('SimpleDataSource')
;
Insert into ATTACH_TYPES values ('DataStructure')
;
Insert into ATTACH_TYPES values ('MetadataStructure')
;
Insert into ATTACH_TYPES values ('Dataflow')
;
Insert into ATTACH_TYPES values ('Metadataflow')
;
Insert into ATTACH_TYPES values ('ProvisionAgreement')
;