-- Added columns to REGISTRY.
Alter table REGISTRY
ADD (NAME NVARCHAR(250) NULL,
	IS_PUBLIC BOOL NOT NULL DEFAULT 0,
	UPGRADES BOOL NOT NULL DEFAULT 0,
	PROXY BOOL NOT NULL DEFAULT 0)
;