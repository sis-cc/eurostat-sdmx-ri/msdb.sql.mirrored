CREATE OR REPLACE PROCEDURE INSERT_CONTENT_CONSTRAINT(
	p_id IN varchar2,
	p_version IN varchar2,
	p_agency IN varchar2,
	p_valid_from IN  timestamp DEFAULT NULL,
	p_valid_to IN  timestamp DEFAULT NULL,
	p_is_final IN number DEFAULT NULL,
	p_uri IN nvarchar2,
	p_last_modified IN timestamp DEFAULT NULL,
	p_actual_data NUMBER DEFAULT 1,
	p_periodicity varchar2 DEFAULT NULL,
	p_offset varchar2 DEFAULT NULL,
	p_tolerance varchar2 DEFAULT NULL,
	p_attach_type varchar2 DEFAULT 'Dataflow',
	p_dp_id number DEFAULT null,
	p_set_id varchar2 DEFAULT null,
	p_simple_data_source varchar2 DEFAULT null,
	p_pk OUT NUMBER)
AS
BEGIN
    INSERT_ARTEFACT(p_id => p_id,p_version => p_version,p_agency => p_agency,p_valid_from => p_valid_from,p_valid_to => p_valid_to,p_is_final => p_is_final,p_uri => p_uri,p_last_modified => p_last_modified,p_pk => p_pk);

	INSERT INTO CONTENT_CONSTRAINT
           (CONT_CONS_ID, ACTUAL_DATA, PERIODICITY, OFFSET, TOLERANCE,ATTACH_TYPE,DP_ID,SET_ID,SIMPLE_DATA_SOURCE)
     VALUES
           (p_pk, p_actual_data, p_periodicity, p_offset, p_tolerance,p_attach_type,p_dp_id,p_set_id,p_simple_data_source);
END;
/


CREATE TABLE ATTACH_TYPE_TEMP
(
ATTACH_TYPE VARCHAR2(30),
CONT_CONS_ID VARCHAR2(30)
)
;

INSERT INTO ATTACH_TYPE_TEMP SELECT DISTINCT
                                                   CASE
                                                   WHEN C.dsd_id IS NOT NULL THEN 'DataStructure'
                                                   WHEN D.df_id IS NOT NULL THEN 'Dataflow'
                                                   WHEN PA.pa_id IS NOT NULL THEN 'ProvisionAgreement'
                                                   WHEN MD.mdf_id IS NOT NULL THEN 'Metadataflow'
                                                   WHEN MSD.msd_id IS NOT NULL THEN 'MetadataStructure'

                                                   END AS ATTACH_TYPE,
                                                   A.CONT_CONS_ID
                                                       FROM CONTENT_CONSTRAINT_ATTACHMENT A
                                                   INNER JOIN ARTEFACT_VIEW B
                                                   ON A.ART_ID = B.ART_ID
                                                       LEFT OUTER JOIN DSD C
                                                   ON B.ART_ID = C.DSD_ID
                                                       LEFT OUTER JOIN DATAFLOW D
                                                   ON B.ART_ID = D.DF_ID
                                                       LEFT OUTER JOIN PROVISION_AGREEMENT PA
                                                   ON B.ART_ID = PA.PA_ID
                                                       LEFT OUTER JOIN METADATAFLOW MD
                                                   ON B.ART_ID = MD.MDF_ID
                                                       LEFT OUTER JOIN METADATA_STRUCTURE_DEFINITION MSD
                                                   ON B.ART_ID = MSD.MSD_ID
                                                       INNER JOIN CONTENT_CONSTRAINT E
                                                       ON A.CONT_CONS_ID = E.CONT_CONS_ID
                                                       WHERE A.CONT_CONS_ID = E.CONT_CONS_ID
;

Update CONTENT_CONSTRAINT set CONTENT_CONSTRAINT.ATTACH_TYPE = (select ATTACH_TYPE from ATTACH_TYPE_TEMP where CONTENT_CONSTRAINT.CONT_CONS_ID = ATTACH_TYPE_TEMP.CONT_CONS_ID and ROWNUM = 1)
;

Drop table ATTACH_TYPE_TEMP
;

-- always last
update DB_VERSION SET VERSION = '6.6'
;

