CREATE OR REPLACE PROCEDURE INSERT_COMPONENT_MAPPING(
p_map_set_id IN number,
p_type IN varchar2,
p_constant IN nvarchar2,
p_default_value IN nvarchar2,
p_is_metadata IN number,
p_pk OUT number)
AS
BEGIN
		INSERT INTO COMPONENT_MAPPING (MAP_SET_ID,TYPE,CONSTANT,DEFAULT_VALUE,IS_METADATA) VALUES (p_map_set_id, p_type, p_constant, p_default_value, p_is_metadata) RETURNING MAP_ID INTO p_pk;
END;
/
;
