CREATE TABLE META_COL_MAPPING_COMPONENT
(
	MAP_ID BIGINT NOT NULL,
	SDMX_ID VARCHAR(255) NOT NULL,
	CONSTRAINT PK_META_COL_MAPPING_COMPONENT PRIMARY KEY CLUSTERED(MAP_ID, SDMX_ID),
	CONSTRAINT FK_META_COL_MAPPING_COMPONENT FOREIGN KEY (MAP_ID) REFERENCES COMPONENT_MAPPING (MAP_ID) ON DELETE CASCADE
)
;
