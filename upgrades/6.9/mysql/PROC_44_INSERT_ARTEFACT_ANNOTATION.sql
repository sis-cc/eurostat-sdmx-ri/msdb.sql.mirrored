DROP PROCEDURE IF EXISTS INSERT_ARTEFACT_ANNOTATION
;
CREATE PROCEDURE INSERT_ARTEFACT_ANNOTATION 
(
	IN p_art_id bigint,
	IN p_id nvarchar(50),
	IN p_title nvarchar(4000),
	IN p_type nvarchar(50),
	IN p_url nvarchar(1000),
	OUT p_pk bigint) 
BEGIN
	CALL INSERT_ANNOTATION (p_id, p_title, p_type, p_url, p_pk);
	insert into ARTEFACT_ANNOTATION (ANN_ID, ART_ID) VALUES (p_pk, p_art_id);
END
;
