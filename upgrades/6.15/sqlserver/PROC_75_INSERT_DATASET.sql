-- SDMXRI-1693
-- Update Stored procedures
IF EXISTS (SELECT * FROM dbo.SYSOBJECTS WHERE id = object_id('INSERT_DATASET') AND  OBJECTPROPERTY(id, 'IsProcedure') = 1)
DROP PROC INSERT_DATASET
;

CREATE PROCEDURE INSERT_DATASET
@p_name varchar(255) ,
@p_query text ,
@p_order_by_clause text ,
@p_connection_id bigint ,
@p_description varchar(1024) ,
@p_xml_query text ,
@p_user_id bigint ,
@p_editor_type varchar(250) ,
@p_pk bigint OUT
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

		INSERT INTO DATASET (NAME,QUERY,ORDER_BY_CLAUSE,CONNECTION_ID,DESCRIPTION,XML_QUERY,USER_ID,EDITOR_TYPE) VALUES (@p_name, @p_query, @p_order_by_clause, @p_connection_id, @p_description, @p_xml_query, @p_user_id, @p_editor_type);
		set @p_pk = SCOPE_IDENTITY();
	IF @starttrancount = 0
	COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	RAISERROR (@ErrorMessage, -- Message text.
				@ErrorSeverity, -- Severity.
				@ErrorState -- State.
				);
	END CATCH
END
;

