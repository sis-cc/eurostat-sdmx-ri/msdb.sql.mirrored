DROP PROCEDURE IF EXISTS INSERT_GROUP_ANNOTATION
;
CREATE PROCEDURE INSERT_GROUP_ANNOTATION 
(
	IN p_gr_id bigint,
	IN p_id nvarchar(50),
	IN p_title nvarchar(4000),
	IN p_type nvarchar(50),
	IN p_url nvarchar(1000),
	OUT p_pk bigint) 
BEGIN
	CALL INSERT_ANNOTATION (p_id, p_title, p_type, p_url, p_pk);
	insert into GROUP_ANNOTATION (ANN_ID, GR_ID) VALUES (p_pk, p_gr_id);
END
;
