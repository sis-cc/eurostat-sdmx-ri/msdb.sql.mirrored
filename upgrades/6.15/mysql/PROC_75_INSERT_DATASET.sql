
DROP PROCEDURE IF EXISTS INSERT_DATASET
;
CREATE PROCEDURE INSERT_DATASET(
IN p_name varchar(255),
IN p_query text,
IN p_order_by_clause text,
IN p_connection_id bigint,
IN p_description varchar(1024),
IN p_xml_query text,
IN p_user_id bigint,
IN p_editor_type varchar(250),
OUT p_pk bigint)
BEGIN
		INSERT INTO DATASET (NAME,QUERY,ORDER_BY_CLAUSE,CONNECTION_ID,DESCRIPTION,XML_QUERY,USER_ID,EDITOR_TYPE) VALUES (p_name, p_query, p_order_by_clause, p_connection_id, p_description, p_xml_query, p_user_id, p_editor_type);
		set p_pk = LAST_INSERT_ID();
END;

;
