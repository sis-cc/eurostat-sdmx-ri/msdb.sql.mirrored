ALTER TABLE CONCEPT ADD CONSTRAINT CON_ID 
	FOREIGN KEY (CON_ID) REFERENCES ITEM (ITEM_ID)
ON DELETE CASCADE
;
ALTER TABLE CONCEPT ADD CONSTRAINT CON_SCH_ID 
	FOREIGN KEY (CON_SCH_ID) REFERENCES CONCEPT_SCHEME (CON_SCH_ID)
ON DELETE CASCADE
;
ALTER TABLE CONCEPT ADD CONSTRAINT FK_CONCEPT_ITEM 
	FOREIGN KEY (PARENT_CONCEPT_ID) REFERENCES CONCEPT (CON_ID)
;
ALTER TABLE CONCEPT ADD CONSTRAINT CONCEPT_CODELIST 
	FOREIGN KEY (CL_ID) REFERENCES CODELIST (CL_ID)
;
