IF EXISTS (SELECT * FROM dbo.SYSOBJECTS WHERE id = object_id('INSERT_COMPONENT') AND  OBJECTPROPERTY(id, 'IsProcedure') = 1)
   DROP PROC INSERT_COMPONENT
;
CREATE PROCEDURE INSERT_COMPONENT
	@p_type varchar(50) ,
    @p_id varchar(255)=NULL,
	@p_dsd_id bigint,
	@p_con_id bigint,
	@p_cl_id bigint=NULL,
	@p_con_sch_id bigint=NULL,
	@p_is_freq_dim int=NULL,
	@p_is_measure_dim int=NULL,
	@p_att_ass_level varchar(11)=NULL,
	@p_att_status varchar(11)=NULL,
	@p_att_is_time_format int=NULL,
	@p_xs_attlevel_ds int=NULL,
	@p_xs_attlevel_group int=NULL,
	@p_xs_attlevel_section int=NULL,
	@p_xs_attlevel_obs int=NULL,
	@p_xs_measure_code varchar(50)=NULL,
	@p_pk bigint OUT  
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

		exec INSERT_COMPONENT_COMMON @p_pk = @p_pk OUTPUT ;
		insert into COMPONENT (COMP_ID,ID, TYPE, DSD_ID, CON_ID, CL_ID, IS_FREQ_DIM, IS_MEASURE_DIM, ATT_ASS_LEVEL, ATT_STATUS, ATT_IS_TIME_FORMAT, XS_ATTLEVEL_DS, XS_ATTLEVEL_GROUP, XS_ATTLEVEL_SECTION, XS_ATTLEVEL_OBS, XS_MEASURE_CODE, CON_SCH_ID) 
		values (@p_pk,@p_id, @p_type, @p_dsd_id, @p_con_id, @p_cl_id, @p_is_freq_dim, @p_is_measure_dim, @p_att_ass_level, @p_att_status, @p_att_is_time_format, @p_xs_attlevel_ds, @p_xs_attlevel_group, @p_xs_attlevel_section, @p_xs_attlevel_obs, @p_xs_measure_code, @p_con_sch_id);
		IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
