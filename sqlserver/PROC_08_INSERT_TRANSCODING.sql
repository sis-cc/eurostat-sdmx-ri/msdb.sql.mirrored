IF EXISTS (SELECT * FROM dbo.SYSOBJECTS WHERE id = object_id('INSERT_TRANSCODING') AND  OBJECTPROPERTY(id, 'IsProcedure') = 1)
   DROP PROC INSERT_TRANSCODING
;
CREATE PROCEDURE INSERT_TRANSCODING
	@p_map_id bigint, 
	@p_expression varchar(150) = null,
	@p_pk bigint out
AS
BEGIN
	SET NOCOUNT ON;
	insert into TRANSCODING (MAP_ID, EXPRESSION) values (@p_map_id,@p_expression);
	set @p_pk = SCOPE_IDENTITY();
END
;
