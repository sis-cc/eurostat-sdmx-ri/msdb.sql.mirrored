IF EXISTS (SELECT * FROM dbo.SYSOBJECTS WHERE id = object_id('INSERT_CUBE_REGION') AND  OBJECTPROPERTY(id, 'IsProcedure') = 1)
   DROP PROC INSERT_CUBE_REGION
;
CREATE PROCEDURE INSERT_CUBE_REGION
       @p_cont_cons_id bigint,
       @p_include bigint,
       @p_pk bigint OUT 
AS
BEGIN
       INSERT INTO CUBE_REGION
           (CONT_CONS_ID, INCLUDE)
     VALUES
           (@p_cont_cons_id, @p_include);
       set @p_pk = SCOPE_IDENTITY();

END
;
