IF EXISTS (SELECT * FROM dbo.SYSOBJECTS WHERE id = object_id('INSERT_REGISTRATION') AND  OBJECTPROPERTY(id, 'IsProcedure') = 1)
   DROP PROC INSERT_REGISTRATION
;
create procedure INSERT_REGISTRATION
	@p_id varchar(50),
	@p_valid_from datetime,
	@p_valid_to datetime,
	@p_last_updated datetime,
    @p_index_ts bit,
    @p_index_ds bit,
    @p_index_attributes bit,
    @p_index_reporting_period bit,
    @p_pa_id bigint,
    @p_datasource_id bigint,
	@p_pk bigint OUT
AS
BEGIN
	SET NOCOUNT ON;
	insert into REGISTRATION (ID, VALID_FROM, VALID_TO, LAST_UPDATED, INDEX_TIME_SERIES, INDEX_DATA_SET, INDEX_ATTRIBUTES, INDEX_REPORTING_PERIOD, PA_ID, DATA_SOURCE_ID) 
    values (@p_id, @p_valid_from, @p_valid_to, @p_last_updated, @p_index_ts, @p_index_ds, @p_index_attributes, @p_index_reporting_period, @p_pa_id, @p_datasource_id);
	set @p_pk = SCOPE_IDENTITY();
END
;
