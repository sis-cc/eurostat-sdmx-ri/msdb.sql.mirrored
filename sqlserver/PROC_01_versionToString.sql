IF OBJECT_ID (N'versionToString', N'FN') IS NOT NULL
    DROP FUNCTION versionToString
;
GO
;
create function versionToString(@particle1 bigint, @particle2 bigint, @particle3 bigint) 
RETURNS varchar(50)
BEGIN
	declare @version varchar(50);
	if @particle1 is null
		set @particle1 = 0;

	if @particle2 is null
		set @particle2 = 0;
	
	if @particle3 is null
		set @version = cast(@particle1 as varchar) +'.' + cast(@particle2 as varchar);
	else
		set @version = cast(@particle1 as varchar) + '.' + cast(@particle2 as varchar) + '.' + cast(@particle3 as varchar);

	return @version;
END
;
