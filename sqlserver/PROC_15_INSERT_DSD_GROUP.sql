IF EXISTS (SELECT * FROM dbo.SYSOBJECTS WHERE id = object_id('INSERT_DSD_GROUP') AND  OBJECTPROPERTY(id, 'IsProcedure') = 1)
   DROP PROC INSERT_DSD_GROUP
;
CREATE PROCEDURE INSERT_DSD_GROUP
	@p_id varchar(50) ,
	@p_dsd_id bigint ,
	@p_pk bigint OUT 
AS
BEGIN
	SET NOCOUNT ON;
	insert into DSD_GROUP (ID, DSD_ID) 
		values (@p_id, @p_dsd_id);
	set @p_pk = SCOPE_IDENTITY();
END
;
