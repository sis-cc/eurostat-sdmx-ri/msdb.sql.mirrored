IF EXISTS (SELECT * FROM dbo.SYSOBJECTS WHERE id = object_id('INSERT_DESC_SOURCE') AND  OBJECTPROPERTY(id, 'IsProcedure') = 1)
DROP PROC INSERT_DESC_SOURCE
;

CREATE PROCEDURE INSERT_DESC_SOURCE
@p_desc_table varchar(255) ,
@p_related_field varchar(255) ,
@p_desc_field varchar(255) ,
@p_col_id bigint ,
@p_pk bigint OUT
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

		INSERT INTO DESC_SOURCE (DESC_TABLE,RELATED_FIELD,DESC_FIELD,COL_ID) VALUES (@p_desc_table, @p_related_field, @p_desc_field, @p_col_id);
		set @p_pk = SCOPE_IDENTITY();
	IF @starttrancount = 0
	COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	RAISERROR (@ErrorMessage, -- Message text.
				@ErrorSeverity, -- Severity.
				@ErrorState -- State.
				);
	END CATCH
END
;
