IF EXISTS (SELECT * FROM dbo.SYSOBJECTS WHERE id = object_id('INSERT_DATA_SOURCE') AND  OBJECTPROPERTY(id, 'IsProcedure') = 1)
   DROP PROC INSERT_DATA_SOURCE
;
create procedure INSERT_DATA_SOURCE
	@p_data_url nvarchar(500),
	@p_wsdl_url nvarchar(500),
	@p_wadl_url nvarchar(500),
	@p_is_simple bit,
	@p_is_rest bit,
	@p_is_ws bit,
	@p_pk bigint OUT
AS
BEGIN
	SET NOCOUNT ON;
	insert into DATA_SOURCE (DATA_URL, WSDL_URL, WADL_URL, IS_SIMPLE, IS_REST, IS_WS) 
    values (@p_data_url, @p_wsdl_url, @p_wadl_url, @p_is_simple, @p_is_rest, @p_is_ws);
	set @p_pk = SCOPE_IDENTITY();
END
;
