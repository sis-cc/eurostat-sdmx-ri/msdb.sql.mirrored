IF EXISTS (SELECT * FROM dbo.SYSOBJECTS WHERE id = object_id('INSERT_TRANSCODING_SCRIPT') AND  OBJECTPROPERTY(id, 'IsProcedure') = 1)
   DROP PROC INSERT_TRANSCODING_SCRIPT
;
CREATE PROCEDURE INSERT_TRANSCODING_SCRIPT
	@p_tr_id bigint, 
	@p_title varchar(128),
	@p_content text,
	@p_pk bigint out
AS
BEGIN
	SET NOCOUNT ON;
	insert into TRANSCODING_SCRIPT (TR_ID, SCRIPT_TITLE, SCRIPT_CONTENT) values (@p_tr_id, @p_title, @p_content);
	set @p_pk=SCOPE_IDENTITY();
END
;
