IF EXISTS (SELECT * FROM dbo.SYSOBJECTS WHERE id = object_id('INSERT_COMPONENT_MAP') AND  OBJECTPROPERTY(id, 'IsProcedure') = 1)
   DROP PROC INSERT_COMPONENT_MAP
;
CREATE PROCEDURE INSERT_COMPONENT_MAP
	@p_sm_id bigint,
	@p_source_comp_id bigint,
	@p_target_comp_id bigint,
	@p_pk bigint OUT  
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION
		insert into COMPONENT_MAP (SM_ID, SOURCE_COMP_ID, TARGET_COMP_ID) VALUES (@p_sm_id, @p_source_comp_id, @p_target_comp_id);
		set @p_pk = SCOPE_IDENTITY();

    IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
