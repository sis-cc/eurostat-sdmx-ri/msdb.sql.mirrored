
CREATE TABLE ARTEFACT (
	ART_ID bigint identity(1,1) NOT NULL,
	ID varchar(255) NULL,
	VERSION1 bigint NOT NULL DEFAULT 0,
	VERSION2 bigint NOT NULL DEFAULT 0,
	VERSION3 bigint NULL DEFAULT NULL,
	AGENCY varchar(50) NULL,
	VALID_FROM datetime NULL,
	VALID_TO datetime NULL,
	IS_FINAL int NULL,
	URI nvarchar(255) NULL,
	LAST_MODIFIED datetime NULL,
	IS_STUB bit NOT NULL DEFAULT 0,
	SERVICE_URL nvarchar(255) NULL,
	STRUCTURE_URL nvarchar(255) NULL,
	ARTEFACT_TYPE varchar(50) NULL
)
;

ALTER TABLE ARTEFACT ADD CONSTRAINT PK_ARTEFACT
	PRIMARY KEY CLUSTERED (ART_ID)
;
