IF EXISTS (SELECT * FROM dbo.SYSOBJECTS WHERE id = object_id('INSERT_LOCALISED_STRING') AND  OBJECTPROPERTY(id, 'IsProcedure') = 1)
   DROP PROC INSERT_LOCALISED_STRING
;
CREATE PROCEDURE INSERT_LOCALISED_STRING
	@p_item_id bigint = null,
	@p_art_id bigInt = null,
	@p_text nvarchar(4000),
	@p_type varchar(10),
	@p_language varchar(50),
	@p_pk bigint OUT 
AS
BEGIN
	SET NOCOUNT ON;
	insert into LOCALISED_STRING (ART_ID, ITEM_ID, TEXT, TYPE, LANGUAGE) values (@p_art_id, @p_item_id, @p_text, @p_type, @p_language);
	set @p_pk = SCOPE_IDENTITY();
END
;
