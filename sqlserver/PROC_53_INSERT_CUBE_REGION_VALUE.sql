IF EXISTS (SELECT * FROM dbo.SYSOBJECTS WHERE id = object_id('INSERT_CUBE_REGION_VALUE') AND  OBJECTPROPERTY(id, 'IsProcedure') = 1)
   DROP PROC INSERT_CUBE_REGION_VALUE
;
CREATE PROCEDURE INSERT_CUBE_REGION_VALUE(
	@p_cube_region_key_value_id bigint,
	@p_member_value varchar(150),
	@p_include bigint)
AS
BEGIN
	INSERT INTO CUBE_REGION_VALUE
           (CUBE_REGION_KEY_VALUE_ID, MEMBER_VALUE, INCLUDE)
     VALUES
           (@p_cube_region_key_value_id, @p_member_value, @p_include);
END
;
