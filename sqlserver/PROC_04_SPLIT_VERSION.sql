-- STORED PROCECURES
IF EXISTS (SELECT * FROM dbo.SYSOBJECTS WHERE id = object_id('SPLIT_VERSION') AND  OBJECTPROPERTY(id, 'IsProcedure') = 1)
   DROP PROC SPLIT_VERSION
;

CREATE PROCEDURE SPLIT_VERSION(
@p_version varchar(50), 
@p_version1 bigint out, 
@p_version2 bigint out,
@p_version3 bigint out)
AS
BEGIN
declare @point1 int;
declare @point2 int;
declare @l int;
set @p_version1 = dbo.getVersion(@p_version, 1);
set @p_version2 = dbo.getVersion(@p_version, 2);
set @p_version3 = dbo.getVersion(@p_version, 3);
if @p_version1 is null
   set @p_version1 = 0;
    
if @p_version2 is null
   set @p_version2 = 0;
END
;
