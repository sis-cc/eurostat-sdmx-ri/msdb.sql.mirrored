IF EXISTS (SELECT * FROM dbo.SYSOBJECTS WHERE id = object_id('INSERT_DATASET_COLUMN') AND  OBJECTPROPERTY(id, 'IsProcedure') = 1)
DROP PROC INSERT_DATASET_COLUMN
;

CREATE PROCEDURE INSERT_DATASET_COLUMN
@p_name varchar(255) ,
@p_description varchar(1024) ,
@p_ds_id bigint ,
@p_lastretrieval datetime ,
@p_pk bigint OUT
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

		INSERT INTO DATASET_COLUMN (NAME,DESCRIPTION,DS_ID,LASTRETRIEVAL) VALUES (@p_name, @p_description, @p_ds_id, @p_lastretrieval);
		set @p_pk = SCOPE_IDENTITY();
	IF @starttrancount = 0
	COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	RAISERROR (@ErrorMessage, -- Message text.
				@ErrorSeverity, -- Severity.
				@ErrorState -- State.
				);
	END CATCH
END
;
