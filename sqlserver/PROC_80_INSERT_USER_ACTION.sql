IF EXISTS (SELECT * FROM dbo.SYSOBJECTS WHERE id = object_id('INSERT_USER_ACTION') AND  OBJECTPROPERTY(id, 'IsProcedure') = 1)
DROP PROC INSERT_USER_ACTION
;

CREATE PROCEDURE INSERT_USER_ACTION
@p_action_when datetime ,
@p_operation_type varchar(50) ,
@p_username nvarchar(50) ,
@p_entity_type varchar(50) ,
@p_entity_id bigint ,
@p_entity_name nvarchar(255) ,
@p_pk bigint OUT
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

		INSERT INTO USER_ACTION (ACTION_WHEN,OPERATION_TYPE,USERNAME,ENTITY_TYPE,ENTITY_ID,ENTITY_NAME) VALUES (@p_action_when, @p_operation_type, @p_username, @p_entity_type, @p_entity_id, @p_entity_name);
		set @p_pk = SCOPE_IDENTITY();
	IF @starttrancount = 0
	COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	RAISERROR (@ErrorMessage, -- Message text.
				@ErrorSeverity, -- Severity.
				@ErrorState -- State.
				);
	END CATCH
END
;
