IF EXISTS (SELECT * FROM dbo.SYSOBJECTS WHERE id = object_id('INSERT_ARTEFACT') AND  OBJECTPROPERTY(id, 'IsProcedure') = 1)
   DROP PROC INSERT_ARTEFACT
;
CREATE PROCEDURE INSERT_ARTEFACT
	@p_id varchar(255),
	@p_version varchar(50),
	@p_agency varchar(50),
	@p_valid_from datetime = NULL,
	@p_valid_to datetime = NULL,
	@p_is_final int = NULL,
	@p_uri nvarchar(255),
	@p_last_modified datetime = NULL,
	@p_is_stub bit ,
	@p_service_url nvarchar(255),
	@p_structure_url nvarchar(255),
	@p_artefact_type varchar(50),
	@p_pk bigint out
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;

    DECLARE	@p_version1 bigint,
		@p_version2 bigint,
		@p_version3 bigint;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0
			BEGIN TRANSACTION

        exec SPLIT_VERSION @p_version = @p_version, @p_version1 = @p_version1 OUTPUT, @p_version2 = @p_version2 OUTPUT, @p_version3 = @p_version3 OUTPUT;
		insert into  ARTEFACT (ID, VERSION1, VERSION2, VERSION3, AGENCY, VALID_FROM, VALID_TO, IS_FINAL, URI, LAST_MODIFIED, IS_STUB, SERVICE_URL, STRUCTURE_URL, ARTEFACT_TYPE) VALUES (@p_id,  @p_version1,  @p_version2,  @p_version3,  @p_agency,  @p_valid_from,  @p_valid_to,  @p_is_final,  @p_uri,  @p_last_modified,  @p_is_stub,  @p_service_url,  @p_structure_url,  @p_artefact_type);
	    set @p_pk=SCOPE_IDENTITY();
        IF @starttrancount = 0
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
