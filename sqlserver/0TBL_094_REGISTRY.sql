
CREATE TABLE REGISTRY (
    ID bigint IDENTITY(1,1) NOT NULL,
    URL nvarchar(250) NOT NULL,
    USERNAME nvarchar(50),
    PASSWORD nvarchar(250),
	DESCRIPTION nvarchar(250),
	TECHNOLOGY nvarchar(250),
	NAME nvarchar(250) NULL,
	IS_PUBLIC BIT NOT NULL DEFAULT 0,
	UPGRADES BIT NOT NULL DEFAULT 0,
	PROXY BIT NOT NULL DEFAULT 0,
    PRIMARY KEY (ID)
)
;
