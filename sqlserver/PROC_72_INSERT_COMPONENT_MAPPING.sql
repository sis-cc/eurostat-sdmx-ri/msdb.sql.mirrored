IF EXISTS (SELECT * FROM dbo.SYSOBJECTS WHERE id = object_id('INSERT_COMPONENT_MAPPING') AND  OBJECTPROPERTY(id, 'IsProcedure') = 1)
DROP PROC INSERT_COMPONENT_MAPPING
;

CREATE PROCEDURE INSERT_COMPONENT_MAPPING
@p_map_set_id bigint ,
@p_type varchar(10) ,
@p_constant nvarchar(255) ,
@p_default_value nvarchar(255) ,
@p_is_metadata bit = 0,
@p_pk bigint OUT
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

		INSERT INTO COMPONENT_MAPPING (MAP_SET_ID,TYPE,CONSTANT,DEFAULT_VALUE,IS_METADATA) VALUES (@p_map_set_id, @p_type, @p_constant, @p_default_value,@p_is_metadata);
		set @p_pk = SCOPE_IDENTITY();
	IF @starttrancount = 0
	COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	RAISERROR (@ErrorMessage, -- Message text.
				@ErrorSeverity, -- Severity.
				@ErrorState -- State.
				);
	END CATCH
END
;
