IF EXISTS (SELECT * FROM dbo.SYSOBJECTS WHERE id = object_id('INSERT_DATASET_PROPERTY') AND OBJECTPROPERTY(id, 'IsProcedure') = 1)
   DROP PROC INSERT_DATASET_PROPERTY
;

CREATE PROCEDURE INSERT_DATASET_PROPERTY
	@p_dataset_id bigint, 
	@p_property_type_enum bigint, 
	@p_property_value nvarchar(4000) = null,
	@p_pk bigint out
AS
BEGIN
	SET NOCOUNT ON;
	
	INSERT 
	  INTO DATASET_PROPERTY
	       (DATASET_ID, PROPERTY_TYPE_ENUM, PROPERTY_VALUE) 
	VALUES (@p_dataset_id, @p_property_type_enum, @p_property_value)
	
	SET @p_pk = SCOPE_IDENTITY();
END
;
