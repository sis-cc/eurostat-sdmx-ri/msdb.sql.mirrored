CREATE TABLE HCL_CODE 
    ( 
     HCODE_ID bigint  NOT NULL , 
     PARENT_HCODE_ID bigint , 
     LCD_ID bigint  NOT NULL , 
     H_ID bigint  NOT NULL , 
     LEVEL_ID bigint NULL
    )  
;

ALTER TABLE HCL_CODE 
    ADD CONSTRAINT HCL_CODE_PK PRIMARY KEY ( HCODE_ID ) 
;
