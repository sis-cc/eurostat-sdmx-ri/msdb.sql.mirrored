ALTER TABLE ATT_GROUP ADD CONSTRAINT FK_ATT_GROUP_COMPONENT 
	FOREIGN KEY (COMP_ID) REFERENCES COMPONENT (COMP_ID)
ON DELETE CASCADE
;
ALTER TABLE ATT_GROUP ADD CONSTRAINT FK_ATT_GROUP_DSD_GROUP 
	FOREIGN KEY (GR_ID) REFERENCES DSD_GROUP (GR_ID)
;
