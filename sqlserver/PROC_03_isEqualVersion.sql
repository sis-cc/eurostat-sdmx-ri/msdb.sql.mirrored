IF OBJECT_ID (N'isEqualVersion', N'FN') IS NOT NULL
    DROP FUNCTION isEqualVersion
;
GO
;
create function isEqualVersion(@x1 bigint, @y1 bigint, @z1 bigint, @x2 bigint, @y2 bigint, @z2 bigint)
RETURNS BIT
begin
    declare @returnValue bit;
    set @returnValue = 0;
    if @x1 is null
		set @x1 = 0;
	if @x2 is null
		set @x2 = 0;
	if @y1 is null
		set @y1 = 0;
	if @y2 is null
		set @y2 = 0;
    if ( @x1 = @x2 ) and ( @y1 = @y2 ) and (((@z1 is null) and (@z2 is null)) or (((@z1 is not null) and (@z2 is not null)) and ( @z1 = @z2 )))
        set @returnValue = 1;
    
    return @returnValue;
END
;
