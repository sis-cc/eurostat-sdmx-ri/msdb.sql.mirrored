
CREATE TABLE CONCEPT_SCHEME ( 
	CON_SCH_ID bigint NOT NULL,
    IS_PARTIAL BIT NOT NULL DEFAULT 0
)
;

ALTER TABLE CONCEPT_SCHEME ADD CONSTRAINT PK_CONCEPT_SCHEME 
	PRIMARY KEY CLUSTERED (CON_SCH_ID)
;
