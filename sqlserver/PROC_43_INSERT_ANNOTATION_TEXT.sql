IF EXISTS (SELECT * FROM dbo.SYSOBJECTS WHERE id = object_id('INSERT_ANNOTATION_TEXT') AND  OBJECTPROPERTY(id, 'IsProcedure') = 1)
   DROP PROC INSERT_ANNOTATION_TEXT
;
CREATE PROCEDURE INSERT_ANNOTATION_TEXT 
	@p_ann_id bigint,
	@p_language varchar(50),
	@p_text nvarchar(4000),
	@p_pk bigint out
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

	insert into ANNOTATION_TEXT (ANN_ID, LANGUAGE, TEXT) VALUES (@p_ann_id, @p_language, @p_text);
	set @p_pk = SCOPE_IDENTITY();

IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
