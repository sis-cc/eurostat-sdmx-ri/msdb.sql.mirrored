IF EXISTS (SELECT * FROM dbo.SYSOBJECTS WHERE id = object_id('INSERT_TARGET_OBJECT') AND  OBJECTPROPERTY(id, 'IsProcedure') = 1)
   DROP PROC INSERT_TARGET_OBJECT
;
CREATE PROCEDURE INSERT_TARGET_OBJECT(
 @p_id varchar(255),
 @p_ITEM_SCHEME_ID bigint,
 @p_MDT_ID bigint,
 @p_TYPE varchar(50),
 @p_pk  bigint OUT)
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

		EXEC INSERT_COMPONENT_COMMON @p_pk = @p_pk OUTPUT;
		insert into TARGET_OBJECT (TARGET_OBJ_ID,ID,ITEM_SCHEME_ID,MDT_ID,TYPE) VALUES (@p_pk,@p_id,@p_ITEM_SCHEME_ID,@p_MDT_ID,@p_TYPE);
	IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
