IF EXISTS (SELECT * FROM dbo.SYSOBJECTS WHERE id = object_id('INSERT_TEMPLATE_MAPPING') AND  OBJECTPROPERTY(id, 'IsProcedure') = 1)
   DROP PROC INSERT_TEMPLATE_MAPPING
;
CREATE PROCEDURE INSERT_TEMPLATE_MAPPING
@p_column_name varchar(50) ,
@p_col_desc nvarchar(255) ,
@p_component_id varchar(50) ,
@p_component_type varchar(50) ,
@p_con_id bigint ,
@p_item_scheme_id bigint ,
@p_pk bigint OUT
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

		EXEC INSERT_COMPONENT_COMMON @p_pk = @p_pk OUTPUT;
		INSERT INTO TEMPLATE_MAPPING (TEMPLATE_ID,COLUMN_NAME,COL_DESC,COMPONENT_ID,COMPONENT_TYPE,CON_ID,ITEM_SCHEME_ID) VALUES (@p_pk, @p_column_name, @p_col_desc, @p_component_id, @p_component_type, @p_con_id, @p_item_scheme_id);
	IF @starttrancount = 0
	COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	RAISERROR (@ErrorMessage, -- Message text.
				@ErrorSeverity, -- Severity.
				@ErrorState -- State.
				);
	END CATCH
END
;
