IF EXISTS (SELECT * FROM dbo.SYSOBJECTS WHERE id = object_id('INSERT_HEADER_LOCALISED_STRING') AND  OBJECTPROPERTY(id, 'IsProcedure') = 1)
DROP PROC INSERT_HEADER_LOCALISED_STRING
;

CREATE PROCEDURE INSERT_HEADER_LOCALISED_STRING
@p_type varchar(50) ,
@p_header_id bigint ,
@p_party_id bigint ,
@p_contact_id bigint ,
@p_language varchar(50) ,
@p_text varchar(255) ,
@p_pk bigint OUT
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

		INSERT INTO HEADER_LOCALISED_STRING (TYPE,HEADER_ID,PARTY_ID,CONTACT_ID,LANGUAGE,TEXT) VALUES (@p_type, @p_header_id, @p_party_id, @p_contact_id, @p_language, @p_text);
		set @p_pk = SCOPE_IDENTITY();
	IF @starttrancount = 0
	COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	RAISERROR (@ErrorMessage, -- Message text.
				@ErrorSeverity, -- Severity.
				@ErrorState -- State.
				);
	END CATCH
END
;
