
-- version 2.1 changes start
CREATE TABLE TRANSCODING_SCRIPT (
	TR_SCRIPT_ID bigint IDENTITY(1,1) NOT NULL,
	TR_ID bigint NOT NULL,
	SCRIPT_TITLE varchar(128) NOT NULL,
	SCRIPT_CONTENT text NOT NULL
)
;

ALTER TABLE TRANSCODING_SCRIPT ADD CONSTRAINT PK_TRANSCODING_SCRIPT
	PRIMARY KEY CLUSTERED (TR_SCRIPT_ID)
;
