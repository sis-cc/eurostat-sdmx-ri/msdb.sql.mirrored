IF EXISTS (SELECT * FROM dbo.SYSOBJECTS WHERE id = object_id('INSERT_PROXY_SETTINGS') AND  OBJECTPROPERTY(id, 'IsProcedure') = 1)
   DROP PROC INSERT_PROXY_SETTINGS
;
create procedure INSERT_PROXY_SETTINGS
	@p_enable_proxy bit,
	@p_url varchar(250),
	@p_authentication bit,
	@p_username varchar(50),
	@p_password varchar(250),
	@p_pk bigint OUT
AS
BEGIN
	SET NOCOUNT ON;
	insert into PROXY_SETTINGS (ENABLE_PROXY, URL, AUTHENTICATION, USERNAME, PASSWORD) 
    values (@p_enable_proxy, @p_url, @p_authentication, @p_username, @p_password);
	set @p_pk = SCOPE_IDENTITY();
END
;
