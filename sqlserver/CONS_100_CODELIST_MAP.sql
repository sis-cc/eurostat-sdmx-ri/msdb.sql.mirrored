ALTER TABLE CODELIST_MAP ADD CONSTRAINT FK_CL_MAP_ITEM FOREIGN KEY(CLM_ID) REFERENCES ITEM(ITEM_ID) ON DELETE CASCADE
;
ALTER TABLE CODELIST_MAP ADD CONSTRAINT FK_CL_MAP_SS FOREIGN KEY(SS_ID) REFERENCES STRUCTURE_SET(SS_ID) ON DELETE CASCADE
;
ALTER TABLE CODELIST_MAP ADD CONSTRAINT FK_CL_MAP_SOURCE FOREIGN KEY(SOURCE_CL_ID) REFERENCES CODELIST(CL_ID)
;
ALTER TABLE CODELIST_MAP ADD CONSTRAINT FK_CL_MAP_TARGET FOREIGN KEY(TARGET_CL_ID) REFERENCES CODELIST(CL_ID)
;
