
CREATE TABLE DATA_SOURCE (
   DATA_SOURCE_ID bigint NOT NULL IDENTITY,
   -- ID VARCHAR(50) NULL, Doubled checked and doesn't appear to be a need for an ID.
   DATA_URL nvarchar(500) NOT NULL,
   WSDL_URL nvarchar(500) NULL,
   WADL_URL nvarchar(500) NULL,
   IS_SIMPLE BIT NOT NULL,
   IS_REST BIT NOT NULL,
   IS_WS BIT NOT NULL,
   PRIMARY KEY(DATA_SOURCE_ID)
)
;
