CREATE TABLE ARTEFACT_ANNOTATION
(
	ANN_ID bigint NOT NULL, 
	ART_ID bigint NOT NULL
) 
;

ALTER TABLE ARTEFACT_ANNOTATION ADD CONSTRAINT PK_ARTEFACT_ANNOTATION
	PRIMARY KEY CLUSTERED (ANN_ID, ART_ID)
;
