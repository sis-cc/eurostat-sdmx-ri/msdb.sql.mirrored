IF EXISTS (SELECT * FROM dbo.SYSOBJECTS WHERE id = object_id('INSERT_CONCEPT_TEXT_FORMAT') AND  OBJECTPROPERTY(id, 'IsProcedure') = 1)
   DROP PROC INSERT_CONCEPT_TEXT_FORMAT
;
CREATE PROCEDURE [dbo].[INSERT_CONCEPT_TEXT_FORMAT]
    @p_conceptid bigint, 
    @p_facet_type_enum bigint, 
    @p_facet_value varchar(51) = null
AS
BEGIN
    SET NOCOUNT ON;
    insert into CONCEPT_TEXT_FORMAT (CON_ID, FACET_TYPE_ENUM, FACET_VALUE) values (@p_conceptid, @p_facet_type_enum, @p_facet_value);
END
;
