IF EXISTS (SELECT * FROM dbo.SYSOBJECTS WHERE id = object_id('INSERT_TEXT_FORMAT') AND  OBJECTPROPERTY(id, 'IsProcedure') = 1)
   DROP PROC INSERT_TEXT_FORMAT
;
CREATE PROCEDURE INSERT_TEXT_FORMAT
	@p_compid bigint, 
	@p_facet_type_enum bigint, 
	@p_facet_value varchar(51) = null,
	@p_pk bigint out
AS
BEGIN
	SET NOCOUNT ON;
	insert into TEXT_FORMAT (COMP_ID, FACET_TYPE_ENUM, FACET_VALUE) values (@p_compid, @p_facet_type_enum, @p_facet_value);
	set @p_pk = SCOPE_IDENTITY();
END
;
