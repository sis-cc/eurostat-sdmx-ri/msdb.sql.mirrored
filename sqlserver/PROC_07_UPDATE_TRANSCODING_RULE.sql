IF EXISTS (SELECT * FROM dbo.SYSOBJECTS WHERE id = object_id('UPDATE_TRANSCODING_RULE') AND  OBJECTPROPERTY(id, 'IsProcedure') = 1)
   DROP PROC UPDATE_TRANSCODING_RULE
;
CREATE PROCEDURE UPDATE_TRANSCODING_RULE
	-- Add the parameters for the stored procedure here 
	@p_tr_id bigint,
	@p_tr_rule_id bigint,
	@p_lcd_id bigint,
	@p_cd_id bigint,
	@p_uncoded_value nvarchar(255),
	@p_pk bigint OUT 
AS
BEGIN
	
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

		-- Insert statements for procedure here
		if @p_tr_rule_id is null
		begin
			INSERT INTO TRANSCODING_RULE (TR_ID,UNCODED_VALUE) VALUES (@p_tr_id, @p_uncoded_value);
			set @p_pk = SCOPE_IDENTITY();
		end
		else
		begin
			-- clean up
			delete from TRANSCODING_RULE_DSD_CODE where TR_RULE_ID = @p_tr_rule_id;
			delete from TRANSCODING_RULE_LOCAL_CODE where TR_RULE_ID = @p_tr_rule_id;
			update TRANSCODING_RULE SET UNCODED_VALUE=@p_uncoded_value where TR_RULE_ID = @p_tr_rule_id;
			set @p_pk = @p_tr_rule_id
		end
		
		-- p_cd_id could be null if p_uncoded_value is used
		if @p_cd_id is not null
			insert into TRANSCODING_RULE_DSD_CODE (TR_RULE_ID, CD_ID) VALUES (@p_pk, @p_cd_id);
		insert into TRANSCODING_RULE_LOCAL_CODE (TR_RULE_ID, LCD_ID) VALUES (@p_pk, @p_lcd_id);
		
		IF @starttrancount = 0 
			COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );	
	END CATCH
END
;
