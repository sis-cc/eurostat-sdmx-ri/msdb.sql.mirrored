
CREATE TABLE DSD_CODE ( 
	LCD_ID bigint NOT NULL,
	CL_ID bigint NOT NULL,
	PARENT_CODE_ID bigint NULL
)
;

ALTER TABLE DSD_CODE ADD CONSTRAINT PK_DSD_CODE 
	PRIMARY KEY CLUSTERED (LCD_ID)
;
