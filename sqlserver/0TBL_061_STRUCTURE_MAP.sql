CREATE TABLE STRUCTURE_MAP
(
	SM_ID bigint NOT NULL,
	SS_ID bigint NOT NULL,
	SOURCE_STR_ID bigint NOT NULL,
	TARGET_STR_ID bigint NOT NULL
) 
;

ALTER TABLE STRUCTURE_MAP ADD CONSTRAINT PK_STRUCTURE_MAP
	PRIMARY KEY CLUSTERED (SM_ID)
;
