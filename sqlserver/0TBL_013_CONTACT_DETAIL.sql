
CREATE TABLE CONTACT_DETAIL ( 
	CD_ID bigint identity(1,1)  NOT NULL,
	CONTACT_ID bigint,
	TYPE varchar(50),
	VALUE varchar(255)
)
;

ALTER TABLE CONTACT_DETAIL ADD CONSTRAINT PK_CONTACT_DETAIL 
	PRIMARY KEY CLUSTERED (CD_ID)
;
