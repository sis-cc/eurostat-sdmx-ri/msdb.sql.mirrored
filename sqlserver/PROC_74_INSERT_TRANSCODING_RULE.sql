IF EXISTS (SELECT * FROM dbo.SYSOBJECTS WHERE id = object_id('INSERT_TRANSCODING_RULE') AND  OBJECTPROPERTY(id, 'IsProcedure') = 1)
DROP PROC INSERT_TRANSCODING_RULE
;

CREATE PROCEDURE INSERT_TRANSCODING_RULE
@p_tr_id bigint ,
@p_uncoded_value nvarchar(255) ,
@p_pk bigint OUT
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

		INSERT INTO TRANSCODING_RULE (TR_ID,UNCODED_VALUE) VALUES (@p_tr_id, @p_uncoded_value);
		set @p_pk = SCOPE_IDENTITY();
	IF @starttrancount = 0
	COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	RAISERROR (@ErrorMessage, -- Message text.
				@ErrorSeverity, -- Severity.
				@ErrorState -- State.
				);
	END CATCH
END
;
