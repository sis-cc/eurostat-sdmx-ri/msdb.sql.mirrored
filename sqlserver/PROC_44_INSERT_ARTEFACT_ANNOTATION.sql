IF EXISTS (SELECT * FROM dbo.SYSOBJECTS WHERE id = object_id('INSERT_ARTEFACT_ANNOTATION') AND  OBJECTPROPERTY(id, 'IsProcedure') = 1)
   DROP PROC INSERT_ARTEFACT_ANNOTATION
;
GO
;
CREATE PROCEDURE INSERT_ARTEFACT_ANNOTATION 
	@p_art_id bigint,
	@p_id nvarchar(50),
	@p_title nvarchar(4000),
	@p_type nvarchar(50),
	@p_url nvarchar(1000),
	@p_pk bigint OUT
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;
	DECLARE @starttrancount int;
	SET @starttrancount = @@TRANCOUNT;
	BEGIN TRY
		IF @starttrancount = 0 
			BEGIN TRANSACTION

	exec INSERT_ANNOTATION @p_id = @p_id, @p_title = @p_title, @p_type = @p_type, @p_url = @p_url, @p_pk = @p_pk OUTPUT;
	insert into ARTEFACT_ANNOTATION (ANN_ID, ART_ID) VALUES (@p_pk, @p_art_id);

	IF @starttrancount = 0 
	        COMMIT TRANSACTION

	END TRY
	BEGIN CATCH
		DECLARE @ErrorMessage NVARCHAR(4000);
		DECLARE @ErrorSeverity INT;
		DECLARE @ErrorState INT;

		SELECT 
			@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
		IF XACT_STATE() <> 0 AND @starttrancount = 0 
			ROLLBACK TRANSACTION
	   RAISERROR (@ErrorMessage, -- Message text.
				   @ErrorSeverity, -- Severity.
				   @ErrorState -- State.
				   );
	END CATCH
END
;
