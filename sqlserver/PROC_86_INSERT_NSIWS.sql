IF EXISTS (SELECT * FROM dbo.SYSOBJECTS WHERE id = object_id('INSERT_NSIWS') AND  OBJECTPROPERTY(id, 'IsProcedure') = 1)
 DROP PROC INSERT_NSIWS
;


CREATE PROCEDURE INSERT_NSIWS
@p_url varchar(250),
@p_username varchar(250),
@p_password varchar(250),
@p_description varchar(250),
@p_technology varchar(250),
@p_name varchar(250),
@p_proxy bit,
@p_pk bigint out
AS
BEGIN
  SET NOCOUNT ON;
  SET XACT_ABORT ON;
  DECLARE @starttrancount int;
  SET @starttrancount = @@TRANCOUNT;
  BEGIN TRY
    IF @starttrancount = 0 
      BEGIN TRANSACTION

    INSERT INTO NSIWS (URL,USERNAME,PASSWORD,DESCRIPTION,TECHNOLOGY,NAME,PROXY) VALUES (@p_url,@p_username, @p_password,@p_description,@p_technology,@p_name,@p_proxy);
    set @p_pk = SCOPE_IDENTITY();
    IF @starttrancount = 0
      COMMIT TRANSACTION
  END TRY
  BEGIN CATCH
      DECLARE @ErrorMessage NVARCHAR(4000);
      DECLARE @ErrorSeverity INT;
      DECLARE @ErrorState INT;

      SELECT 
        @ErrorMessage = ERROR_MESSAGE(),
        @ErrorSeverity = ERROR_SEVERITY(),
        @ErrorState = ERROR_STATE();
      IF XACT_STATE() <> 0 AND @starttrancount = 0 
        ROLLBACK TRANSACTION
      RAISERROR (@ErrorMessage, -- Message text.
          @ErrorSeverity, -- Severity.
          @ErrorState -- State.
        );
    END CATCH
END
;
