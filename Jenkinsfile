#!groovy

pipeline {
  agent any
    parameters {
      booleanParam(defaultValue: false, description: 'Deploys in Nexus and creates Tag', name: 'release')
      string(defaultValue: '', description: 'Release version', name: 'releaseVersion')

    }
    tools { 
      // For some reason when Jenkins tries to download maven it gets 403. The same link works from a browser
      // maven 'maven3.6.0' 
      jdk 'jdk8'
    }
  environment {
    CITNET_USER = credentials('citnet_user_pass')
  }
  stages {
    stage('Initialize') {
      steps {
        echo "JAVA_HOME=${env.JAVA_HOME}"
        echo "PATH=${env.PATH}"
        echo "GIT_REPO_SDMXRI=${env.GIT_REPO_SDMXRI}"
        echo "GIT_REPO_SERVER=${env.GIT_REPO_SERVER}"
        sh 'java -version'
      }
    }
    stage('BuildAndInstall') {
      steps {
        sh 'mvn clean install -Dmaven.javadoc.skip=true -Dmaven.test.skip=true'
      }
    }

    stage('Release deploy') {
      when {
        branch 'master'
        expression {return params.release && currentBuild.currentResult == 'SUCCESS' && params.releaseVersion != null && params.releaseVersion != ''}

      }
      steps {
        sh 'mvn deploy -Dmaven.test.skip=true'

		sh 'git config credential.helper store'
        sh 'echo -n https://${CITNET_USER_USR}:${CITNET_USER_PSW} > ~/.git-credentials'
        sh "sed -i 's/@/%40/' ~/.git-credentials"
        sh "echo @${env.GIT_REPO_SERVER} >> ~/.git-credentials"
		sh "chmod 600 ~/.git-credentials"

		sh "git tag rel-${params.releaseVersion}"
		sh "git push origin rel-${params.releaseVersion}"
      }
    }

    stage('Support Release 6.10.1 deploy') {
      when {
        branch '6.10.1'
        expression {return params.release && currentBuild.currentResult == 'SUCCESS' && params.releaseVersion != null && params.releaseVersion != ''}

      }
      steps {
        sh 'mvn deploy -Dmaven.test.skip=true'

        sh 'git config credential.helper store'
        sh "echo -n https://${CITNET_USER_USR}:${CITNET_USER_PSW} > ~/.git-credentials"
        sh "sed -i 's/@/%40/' ~/.git-credentials"
        sh "echo @${env.GIT_REPO_SERVER} >> ~/.git-credentials"
        sh "chmod 600 ~/.git-credentials"

        sh "git tag rel-${params.releaseVersion}"
        sh "git push origin rel-${params.releaseVersion}"
      }
    }

    stage('Develop deploy') {
      when {
        branch 'develop'
        expression { return !params.release}

      }
      steps {
        sh 'mvn deploy -Dmaven.test.skip=true'
      }
    }
  }
}
