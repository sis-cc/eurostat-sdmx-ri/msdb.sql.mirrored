# HISTORY

## msdb.sql v6.20.1 (2022-12-22)

### Details v6.20.1

1. Fixed script to escape OFFSET which caused problems on mariadb 10.6

### Tickets v6.20.1

- SDMXRI-1866: MariaDB 10.6.* not compatible with MAWEB

## msdb.sql v6.20 (2022-12-01)

### Details v6.20

- Changes to constraint name FK_DATASET_PROPERTY_PROPERTY_TYPE to FK_DATASET_PROPERTY_PROP_TYPE inside CONS_188_DATASET_PROPERTY.sql because Oracle 11g allows up to 30 characters identifiers.
- fix missing names and editor_type from old MA: added sql scripts  FIX-ARTEFACT-NAMES.sql, FIX-DATASET-EDITOR-TYPE.sql and FIX-DATASET-EDITOR-TYPE.sql for mariadb/sqlserver and oracle.

### Tickets v6.20

The following support ticket fixed:

- SDMXRI-1993: issues with UK mapping store

## msdb.sql v6.19 (2022-06-29)

### Details v6.19

The following new tables were added in order to store extra configuration for `DATASET`:

| `DATASET_PROPERY`     | Type    | size | Null | Primary Key          | Foreign Key                         |
|-----------------------|---------|------|------|----------------------|-------------------------------------|
| `DATASET_PROPERTY_ID` | BIGINT  |   64 | No   | Yes (AUTO_INCREMENT) |                                     |
| `DATASET_ID`          | BIGINT  |   64 | No   |                      | `DATASET.DS_ID`                     |
| `PROPERTY_TYPE_ENUM`  | BIGINT  |   64 | No   |                      | `DATASET_PROPERTY_TYPE.PROPERTY_ID` |
| `PROPERTY_VALUE`      | VARCHAR | 4000 | Yes  |                      |                                     |

| `DATASET_PROPERY_TYPE` | Type    | size | Null | Primary Key          | Foreign Key                       |
|------------------------|---------|------|------|----------------------|-----------------------------------|
| `DATASET_PROPERTY_ID`  | BIGINT  |  64  | No   | Yes                  |                                   |
| `PROPERTY_NAME`        | VARCHAR | 150  | No   |                      |                                   |
| `PROPERTY_DATATYPE`    | VARCHAR |  20  | No   |                      |                                   |

### Tickets v6.19

The following new features added:

- SDMXRI-1758: Improve the performance of firstNObserverions queries and fix the result set when a range is requested. (OECD,PULL_REQUEST)

## msdb.sql v6.18 (2021-12-10)

### Tickets v6.18

The following new features added:

- SDMXRI-1772: Mapping set for Metadata

In the existing table `MAPPING_SET` the following new fields were added.

| New field     | Type     | Size | NULL or default |
|---------------|----------|------|-----------------|
| IS_METADATA   | bit      |      | 0               |

In the existing table `COMPONENT_MAPPING` the following new fields were added.

| New field     | Type     | Size | NULL or default |
|---------------|----------|------|-----------------|
| IS_METADATA   | bit     |      | 0               |

New table `META_COL_MAPPING_COMPONENT` was added

| New field     | Type     | Size | NULL or default |
|---------------|----------|------|-----------------|
| MAP_ID        | bigint   |      |                 |
| SDMX_ID       | varchar  |255   |                 |

The following store procedures have been updated:

1. `INSERT_MAPPING_SET`
1. `INSERT_COMPONENT_MAPPING`

## msdb.sql v6.17.1

### Tickets 6.17.1

- SDMXRI-1561: IREF autonumber

### Details 6.17.1

- Fix MariaDB/MySQL upgrade script which failed on MariaDB 10.3

## msdb.sql v6.17 (2021-09-30)

### Tickets 6.17.1

The following new features added:

- SDMXRI-1561: IREF autonumber (future-QTM)

### Details 6.17.1

- Introduce a new SEQUENCE `IREF_SEQ` for storing the current IREF number in Header ID for data requests.

## msdb.sql v6.16 ()

The following new features added:

- SDMXRI-1564: integrate the Test Client inside the MA WEB.

The following bugfixes added:

- SDMXRI-1737: MS Migration

### Details v6.16

#### SDMXRI-1564

For SDMXRI-1564 the following changes were made.

A new table NSIWS was added to store one record with the proxy settings.

| NSIWS | Type    | size | Null | Primary Key          | Foreign Key |
|----------------|---------|------|------|----------------------|-------------|
| ID             | BIGINT  | 64   | No   | Yes (AUTO_INCREMENT) |             |
| URL            | VARCHAR | 250  | No   |                      |             |
| USERNAME       | VARCHAR |  50  | Yes  |                      |             |
| PASSWORD       | VARCHAR | 250  | Yes  |                      |             |
| DESCRIPTION    | VARCHAR | 250  | Yes  |                      |             |
| TECHNOLOGY     | VARCHAR |  10  | Yes  |                      |             |
| NAME           | VARCHAR | 250  | Yes  |                      |             |
| PROXY          | BOOLEAN |      | Yes  |                      |             |

The following store procedures have been created:

1. `INSERT_NSIWS`

#### SDMXRI-1737

Copy the link between `DATAFLOW` and `MAPPING_SET` from `DATAFLOW.MAP_SET_ID` to `MAPPING_SET.DF_ID`.

## msdb.sql v6.15 (2021-07-08)

### Details v6.15

1. A new column `ORDER_BY_CLAUSE` was added to `DATASET` table.

In the existing table `DATASET` the following new field was added.

| New field       | Type | Size   | NULL or default |
|-----------------|------|--------|-----------------|
| ORDER_BY_CLAUSE | TEXT | 2^31-1 | NULL            |

The following store procedures have been updated:

1. `INSERT_DATASET`

1. Length of annotation title increased to 4000 for items, components and groups.
The following stored procedures have been updated:

1. `INSERT_ITEM_ANNOTANION`
1. `INSERT_COMPONENT_ANNOTATION`
1. `INSERT_GROUP_ANNOTATION`

### Tickets v6.15

The following new features added:

- SDMXRI-1693: Introduce SQL pagination for range requests (OECD).

The following bugs were corrected:

- SDMXRI-1707: Annotation Title is still to 70 characters at items, components and groups (OECD)

## msdb.sql v6.14 (2021-05-28)

### Details v6.14

1. Fixed script for upgrading from v6.5 to v6.6. The script assumed that there content constraints attached only to one artefact
1. A new table was created `TIME_PRE_FORMATED`.

| New field     | Type    | Size  | NULL or default | Primary Key | Foreign key            |
|---------------|---------|-------|-----------------|-------------|------------------------|
| MAP_SET_ID    | BIGINT  | 64bit | NOT NULL        | yes         | MAPPING_SET.MAP_SET_ID |
| OUTPUT_COLUMN | BIGINT  | 64bit | NOT NULL        |             | DATASET_COLUMN.COL_ID  |
| FROM_COLUMN   | BIGINT  | 64bit | NOT NULL        |             | DATASET_COLUMN.COL_ID  |
| TO_COLUMN     | BIGINT  | 64bit | NOT NULL        |             | DATASET_COLUMN.COL_ID  |

### Tickets v6.14

The following new features added:

- SDMXRI-1538: Enrich existing data retriever to take advantage of a mapped query with Time range columns
- SDMXRI-1671: MAWEB NET: impossible to update MS 6.5

## msdb.sql v6.13 (2021-04-28)

### Details v6.13

1. A new nullable field in `HEADER` table, the `STRUCTURE_TYPE`
	1. Should take values "DataStructure" or "Dataflow"
1. A new nullable field in `HEADER` table, the `DATASET_ID`
	1. Should take the DSD ID
1. A new nullable field in `HEADER` table, the `DATASET_ACTION`
	1. Should take values "Append", "Replace", "Delete", "Information"

In the existing table `HEADER` the following new fields were added.

| New field      | Type     | Size | NULL or default |
|----------------|----------|------|-----------------|
| STRUCTURE_TYPE | VARCHAR  | 50   | NULL            |
| DATASET_ID     | VARCHAR  | 250  | NULL            |
| DATASET_ACTION | VARCHAR  | 50   | NULL            |

The following store procedures have been updated:

1. `INSERT_HEADER`

### Tickets v6.13

The following new features added:

- SDMXRI-1430: Make StructureUsage/DataSetID/DataSetAction configurable per dataflow (QTM10-2019.0281)

The following bugs were corrected:

- SDMXRI-1616: Fix 01ALTER_MAPPING_SET.sql script on all database types (upgrades folder of 6.12) causing upgrade Mapping Store operation to throw exception on MaEntityRetriever.java. Semicolon should be on a separate line, otherwise Java backend throws exception.
- SDMXRI-1616: Remove _COLUMN_ keyword and _NOT NULL_ constraint from 01ALTER_MAPPING_SET.sql script of oracle database folder of 6.12 folder. The _COLUMN_ keyword does not exist on Oracle DB, and _NOT NULL_ constraint already exists from the Table creation.
- SDMXRI-1616: Create PL/SQL code (script ADD_FK_MAPPING_SET_DATASET.sql) on upgrades folder of 6.13 version and Oracle database. This code fixes the issue where a user could delete a dataset that was used by a mapping set when the database is Oracle.

## msdb.sql v6.12 (2020-11-18)

### Details v6.12

1. Fix a syntax error in MariaDB/Mysql upgrade script that targets 6.11
1. MAPPING_SET table ID field size increased to 1024 characters.

The following store procedures have been updated:

1. `INSERT_MAPPING_SET`

### Tickets v6.12

The following new features added:

- SDMXRI-1516: Extend the length of MAPPING_SET table's ID column from 50 to 1024 (.NET) (OECD)

The following bugs were corrected:

- SDMXRI-1517: MSDB MariaDB 6.11 upgrade script syntax error

## msdb.sql v6.11 (2020-07-13)

### Tickets v6.11

The following new features added:

- SDMXRI-1336: Point in Time support (.NET) (OECD)

In the existing table `MAPPING_SET` the following new fields were added.

| New field     | Type     | Size | NULL or default |
|---------------|----------|------|-----------------|
| VALID_FROM    | datetime |      | NULL            |
| VALID_TO      | datetime |      | NULL            |

The following store procedures have been updated:

1. `INSERT_MAPPING_SET`

## msdb.sql v6.10 (2020-06-15)

### Details v6.10

1. Starting from v6.10, MSDB contains a list of predefined SDMX Registries
   1. Euro Registry (SOAP v2.0)
   1. Euro Registry (SOAP v2.0 extended)
   1. Euro Registry (SOAP v2.1)
   1. Fusion Registry (REST)
   1. Global Registry (REST)
   1. IMF Registry (REST)
1. The field `NAME` in table `DATASET` is now unique
   1. In case a duplicate is found during upgrade, the text " Duplicate entry " followed by a UUID is appended to the name
   1. In case it exceeds the the size the original value in `NAME` is truncated.
1. The size of the field `NAME` in table `DB_CONNECTION` has increased to 255 bytes from 50.
1. The field `NAME` in table `DB_CONNECTION` is now unique
   1. In case a duplicate is found during upgrade, the text " Duplicate entry " followed by a UUID is appended to the name
1. The primary key of `CONCEPT_TEXT_FORMAT` has been modified to composed by two fields `CON_ID`,`FACET_TYPE_ENUM`. It was only `CON_ID`
1. The foreign key constraint from the `CONCEPT_TEXT_FORMAT.CON_ID` to `CONCEPT.CON_ID` has been modified to include `ON DELETE CASCADE`.
1. New store procedure `INSERT_CONCEPT_TEXT_FORMAT` was added.

### Tickets v6.10

The following new features added:

- SDMXRI-1255: MAWEB: Registries WS listed (QTM6-2019.0281)
- SDMXRI-1365: MSDB add name unique constraints for DDB, Dataset tables (QTM6-2019.0281)
- SDMXRI-1265: Support Concept Scheme Core representation store/retrieval/references (.NET) (OECD,QTM6-2019.0281)

## msdb.sql v6.9 (2020-03-19)

## Details v6.9

1. Minor change in Annotation store procedure modified on `SqlServer` and `Mariadb`. Increased the size of the Annotation Title parameter to 4000 characters
   1. `INSERT_ARTEFACT_ANNOTATION`
1. A new nullable field in `DATAFLOW` table, the `DATA_SOURCE_ID`. Its type is the same as the existing field `DATA_SOURCE_ID` in `DATA_SOURCE` table.
1. A new foreign key constraint between `DATAFLOW` table, `DATA_SOURCE_ID` field and the field `DATA_SOURCE_ID` in `DATA_SOURCE` table

The following table has the new field in `DATAFLOW` table.

| New field        | Type     | Size | NULL or default |
|------------------|----------|------|-----------------|
| `DATA_SOURCE_ID` | BIGINT   | 64bit| Yes             |

## Tickets v6.9

The following new features added:

- SDMXRI-968: Implementation of Data Registation/monitor: Configure data source per dataflow (SQL) (QTM2-2019.0281)

The following bugs have been corrected:

- SDMXRI-1290: Annotation Title is still fixed to 70 characters

## msdb.sql v6.8 (2020-01-14)

## Details v6.8

1. Increase SDMX ID size to 255 characters. This affects all SDMX tables and store procedures that store a SDMX ID. Including `ITEM`, `ARTEFACT`, `COMPONENT` and others
1. Store procudures related to `referencepartial` feature were not filtering out allowed content constraints
1. Added missing DROP statements for Store Procedures in SQL Server

## Tickets v6.8

The following new features added:

- SDMXRI-1208: SDMX ID increase to 255 characters in MSDB (Oracle/MariaDB) (OECD,sync-needed)
- SDMXRI-1052: SDMX ID increase to 255 characters in MSDB (SQL) (OECD,QTM2-2018-SC000941)

The following bugs have been corrected:

- SDMXRI-1222: Referencepartial parameter for request with references doesn't work anymore
- SDMXRI-1123: Enhancements for command line MAAPI Tool (OECD,QTM2-2019.0281) (Partial only missing DROP statements were fixed)

## 6.7

### Tickets v6.7

The following new features added:

- SDMXRI-1100: MSDB Increase size of MEMBER_VALUE field in CUBE_REGION_VALUE table (SQL) (OECD)
- SDMXRI-897: Support for Content Constraint Reference Period and Time Range (MSDB) (COLLAB,OECD,QTM2-2018-SC000941,SDMXTOOLSTF)
- SDMXRI-1036: performance issues while retrieving artefacts (QTM2-2018-SC000941)
- SDMXRI-1077: SDMX RI support Stubs (SQL) (ISTAT,OECD)
- SDMXRI-1081: MSDB Increase size of field TITLE in table ANNOTATION (SQL) (ISTAT,OECD)

The following bugs have been corrected:

- SDMXRI-1088: Internal release of SDMX RI .NET has old MSDB v6.6 schema

### Detailed changes 6.7

#### SDMXRI-1100

For SDMXRI-1100 the following changes were made.

In the existing table `CUBE_REGION_VALUE` the existing field `MEMBER_VALUE` size increased from 50 to 150 bytes.

| `CUBE_REGION_VALUE`  | Type    | new size | old size |
|----------------------|---------|----------|----------|
| `MEMBER_VALUE`       | VARCHAR | 150      | 50       |

The following store procedures have been updated:

1. `INSERT_CUBE_REGION_VALUE`

#### SDMXRI-897

For SDMXRI-897 the following changes were made.

In the existing table `CONTENT_CONSTRAINT` the following new fields were added.

| CONTENT_CONSTRAINT | Type     | size | Null | Foreign Key |
|--------------------|----------|------|------|-------------|
| START_TIME         | DateTime |      | Yes  |             |
| END_TIME           | DateTime |      | Yes  |             |

In the existing table `CUBE_REGION_KEY_VALUE` the following new fields were added.

| CUBE_REGION_KEY_VALUE | Type        | size | Null | Foreign Key |
|-----------------------|-------------|------|------|-------------|
| START_PERIOD          | VARCHAR(50) |      | Yes  |             |
| END_PERIOD            | VARCHAR(50) |      | Yes  |             |
| START_INCLUSIVE       | bit         | 1    | No   |             |
| END_INCLUSIVE         | bit         | 1    | No   |             |

The following store procedures have been updated:

1. `INSERT_CONTENT_CONSTRAINT`
1. `INSERT_CUBE_REGION_KEY_VALUE`

#### SDMXRI-1036

New indexes from ISTAT were merged:

1. `INDEX_ANNOTATION_TEXT` on table `ANNOTATION_TEXT`
1. `INDEX_ITEM_ANNOTATION` on table `ITEM_ANNOTATION`

#### SDMXRI-1077

In the existing table `ARTEFACT` the following new fields were added.

| New field     | Type     | Size | NULL or default |
|---------------|----------|------|-----------------|
| IS_STUB       | Bit      | 1    | 0               |
| SERVICE_URL   | NVARCHAR | 255  | NULL            |
| STRUCTURE_URL | NVARCHAR | 255  | NULL            |
| ARTEFACT_TYPE | VARCHAR  | 50   | NULL            |

The following store procedures have been updated:

1. `INSERT_ARTEFACT`
1. `INSERT_STRUCTURE_USAGE`
1. `INSERT_ITEM_SCHEME`
1. While store procedures were updated, their signature remained the same

#### SDMXRI-1081

In the existing table `ANNOTATION` the existing field `TITLE` size increased from 50 to 150 bytes.

| `ANNOTATION`  | Type     | new size | old size |
|---------------|----------|---------|-----------|
| `TITLE`       | NVARCHAR | 4000    | 70        |

The following store procedures have been updated:

1. `INSERT_ANNOTATION`

## 6.6

### Tickets 6.6

- SDMXRI-764: Mapping Store upgrade for JDBC driver class (sql) (QTM6-2018.SC000641)
- SDMXRI-930: Content Constraint support attaching to Item and Data Sources (SQL) (QTM6-2018.SC000641)
- SDMXRI-828: Store proxy/new registry settings in MSDB and MAAPI/WS (sql) (QTM6-2018.SC000641)

### Detailed changes 6.6

#### SDMXRI-828

For SDMXRI-828 the following changes were made.

In the existing table REGISTRY the following new fields were added.

| REGISTRY  | Type    | size | Null | Foreign Key |
|-----------|---------|------|------|-------------|
| ​NAME      | VARCHAR | 250  | Yes  |             |
| IS_PUBLIC | BOOLEAN |      | No   |             |
| UPGRADES  | BOOLEAN |      | No   |             |
| PROXY     | BOOLEAN |      | No   |             |

A new table PROXY_SETTINGS was added to store one record with the proxy settings.

| PROXY_SETTINGS | Type    | size | Null | Primary Key          | Foreign Key |
|----------------|---------|------|------|----------------------|-------------|
| ID             | BIGINT  | 64   | No   | Yes (AUTO_INCREMENT) |             |
| ​ENABLE_PROXY   | BOOLEAN |      | No   |                      |             |
| URL            | VARCHAR | 250  | Yes  |                      |             |
| AUTHENTICATION | BOOLEAN |      | No   |                      |             |
| USERNAME       | VARCHAR | 50   | Yes  |                      |             |
| PASSWORD       | VARCHAR | 250  | Yes  |                      |             |

The following store procedures have been updated:

1. `INSERT_REGISTRY`

#### SDMXRI-930

For SDMXRI-930 the following changes were made.

In the existing table `CONTENT_CONSTRAINT` the following new fields were added.

| CONTENT_CONSTRAINT | Type     | size  | Null | Foreign Key              |
|--------------------|----------|-------|------|--------------------------|
| ​DP_ID              | BIGINT   | 64bit | Yes  | DATAPROVIDER.DP_ID       |
| ATTACH_TYPE        | VARCHAR  | 20    | No   | ATTACH_TYPES.ATTACH_TYPE |
| SET_ID             | VARCHAR  | 255   | Yes  |                          |
| SIMPLE_DATA_SOURCE | NVARCHAR | 1024  | Yes  |                          |

A new table was created `CONTENT_CONSTRAINT_SOURCE`

| CONTENT_CONSTRAINT_SOURCE | Type   | size  | Null | Foreign Key                     |
|---------------------------|--------|-------|------|---------------------------------|
| DATA_SOURCE_ID            | BIGINT | 64bit | Yes  | DATA_SOURCE.DATA_SOURCE_ID      |
| CONT_CONS_ID              | BIGINT | 64bit | Yes  | CONTENT_CONSTRAINT.CONT_CONS_ID |

A new table was created `ATTACH_TYPES`

| ATTACH_TYPES | Type    | size | Null | Primary Key |
|--------------|---------|------|------|-------------|
| ATTACH_TYPE  | VARCHAR | 20   | No   | Yes         |

The ATTACH_TYPE table has the following predefined values.

| ATTACH_TYPES       |
|--------------------|
| DataProvider       |
| DataSet            |
| MetadataSet        |
| SimpleDataSource   |
| DataStructure      |
| MetadataStructure  |
| Dataflow           |
| Metadataflow       |
| ProvisionAgreement |

The following store procedures have been updated:

1. `INSERT_CONTENT_CONSTRAINT`

#### SDMXRI-764

In the existing table DB_CONNECTION the following new fields were added.

| DB_CONNECTION  | Type    | size  | Null | Foreign Key     |
|----------------|---------|-------|------|-----------------|
| PROPERTIES     | VARCHAR | 1000  | Yes  |                 |

The store procudure `INSERT_DB_CONNECTION` has been updated.

## 6.5

*NOTE* MariaDB 10.2 or greater is required
*NOTE* Major changes in the structure of the files

### Tickets 6.5

- SDMXRI-667: SDMX v2.1 Time Range support
- SDMXRI-637: Split Mapping Store DDL scripts
- SDMXRI-733: ISTAT New indexes in the Mapping Store

### Detailed changes 6.5

Listed below are the new tables and stored procedures for Time Transcoding for SDMX v2.1 Time Range support.

- Table: `DB_TIME_FORMAT`: Look up table for the DB Formats
- Table: `PERIOD_RULES`: Store the transcoding between local period codes e.g. `JAN` to SDMX e.g. 01.
- Table: `TIME_COLUMN_CONFIG` Store the column and highlight part of a year or period column
- Table: `TIME_COLUMN_TYPE` Store the type of column Year, Period, Date.
- Table: `TIME_FORMAT_CONFIG` Store the output tme format, duration mapping, the criteria value i.e. when to use this output format and the start and/or end period
- Table: `TIME_PARTICLE_CONFIG` The DDB time format
- Table: `TIME_TRANSCODING_TR` The new root Time Transcoding table for Time Range and other more advanced scenarios.
- Stored Procedure:`INSERT_TIME_PARTICLE_CONFIG` Insert a new DDB format
- Stored Procedure:`INSERT_TIME_TRANSCODING_TR` insert a new Time Transcoding for Time Range

Listed below are the changes in the file structure:

- `oracle.sql` split into separate files in `oracle` folder, each having few closely related SQL statements
- `mysql.sql` split into separate files in `mysql` folder, each having few closely related SQL statements
- `sqlserver.sql` split into separate files in `sqlserver` folder, each having few closely related SQL statements

New indexes were added for tables `LOCALISED_STRING` and `DSD_CODE`.

## 6.4

*NOTE* MariaDB 10.1 or greater is required

### Tickets 6.4

- SDMXRI-625: Create index in MSDB to improve delete performance
- Bugfix: In mysql & SQL Server the length of URI parameter was too short

### Detailed changes 6.4

- New Index added to `LOCALISED_STRING.ITEM_ID` to improve delete code performance
- For MySQL/MariaDB & SqlServer scripts only, increased the length of the `p_uri` parameter from `50` to `255`
  - `INSERT_PROVISION_AGREEMENT`
  - `INSERT_MSD`
  - `INSERT_METADATAFLOW`
  - `INSERT_CONTENT_CONSTRAINT`
  - `INSERT_STRUCTURE_USAGE`

## 6.3

### Tickets 6.3

- SDMXRI-208: On the fly DSD

### Detailed changes 6.3

- For supporing on the fly DSD (SDMXRI-208) the following changes were applied
  - New table `TEMPLATE_MAPPING` created to store Template Mappings
  - New table `TM_TRANSCODING` created to store transcodings for Template Mappings
  - New table `TM_TIME_TRANSCODING` created to store time transcodings for Template Mappings
  - New stored procedure `INSERT_TEMPLATE_MAPPING` was created

## 6.2

### Tickets 6.2

- SDMXRI-498: User Actions

### Detailed changes 6.2

- For supporing user actions (SDMXRI-498) the following changes were applied
  - New table `USER_ACTION` created
  - New stored procedure `INSERT_USER_ACTION` was created

## 6.1

### Tickets 6.1

- SDMXRI-468: Support of transcoding/mapping of emtpy values
- SDMXRI-469: Transcoding for uncoded DSD components
- SDMXRI-497: Migration of N components to 1 columns mappings, where N > 1, to 1 to 1 mappings. Reimplemented in SQL, was in C# code only.

### Detailed changes 6.1

- For supporing mapping of empty/null values (SDXMRI-468)
  - Added new field DEFAULT_VALUE in COMPONENT_MAPPING
  - Updated INSERT_COMPONENT_MAPPING store procedure to include an extra parameter for the new DEFAULT_VALUE
- For supporting the Transcoding for uncoded DSD components (SDMXRI-469) the following changes were made
  - Added new field `UNCODED_VALUE` in `TRANSCODING_RULE` table
  - Updated the following store procedures to include an extra parameter for the new `UNCODED_VALUE` field
    - `INSERT_TRANSCODING_RULE`. New parameter `p_uncoded_value`.
    - `UPDATE_TRANSCODING_RULE`. New parameter `p_uncoded_value`. Also some logic was added to handle the possibility of a null `p_cd_id` at the parameter
- (Upgrade to 6.1 only) One shot SQL query code to migrate the N components to 1 columns mappings, where N > 1, to 1 to 1 mappings. Reimplemented in SQL, was in C# code only.

## 6.0 bug-fix relase

Fixed bugs related with Oracle scripts. Due to the nature of the bugs, it was not possible to upgrade
to Oracle v6.0. As a result the version is not incremented.

## 6.0

### Tickets 6.0

- SDMXRI-454: Added internal SDMX Artefacts (XML files)
- SDMXRI-499: MA_USER table updates and migration
- SDMXRI-155: Registry Table and store procedure

### Detailed changes 6.0

- Internal SDMX RI SDMX artefacts are now under folder sdmx in SDMX format
  - CL_UPDATE_STATUS.xml
  - ESTAT+COMPONENTS_ROLES+1.0.xml
  - MA-PERIOD.xml
- Registry settings related changes
  - Added new table REGISTRY
  - Added new stored procedure INSERT_REGISTRY
- Mapping Assistant related changes
  - Added new columns SALT and ALGORITHM to MA_USER
  - Added new store procedure INSERT_MA_USER
  - Migrate MA_USER.USER_TYPE values from 1 to 127 and from 2 to 15

## 5.3

*WARNING* THIS IS THE LAST VERSION SUPPORTED BY Mapping Assistant desktop tool

### Tickets 5.3

- SDMXRI-471: New column in Dataflow table
- SDMXRI-466: Fixes related to MSD/MDF tables
- SDMXRI-82: Change case of tables to work with MariaDB on Linux

### Detailed changes 5.3

- SQL Server use safer method to drop tables, constraints and store procedures
- Bugfixes in stored procedures related to MSD
- Bugfixes in foreign key constraints related to MSD and synchronize names between providers
- Add new column DATA_LAST_UPDATE to DATAFLOW table

## 5.2

### Tickets 5.2

- SDMXRI-454: Add new stored procedures

### Detailed changes 5.2

- Added new store procedure INSERT_COMPONENT_MAPPING
- Added new store procedure INSERT_CONTACT
- Added new store procedure INSERT_CONTACT_DETAIL
- Added new store procedure INSERT_DATASET
- Added new store procedure INSERT_DATASET_COLUMN
- Added new store procedure INSERT_DB_CONNECTION
- Added new store procedure INSERT_DESC_SOURCE
- Added new store procedure INSERT_HEADER
- Added new store procedure INSERT_HEADER_LOCALISED_STRING
- Added new store procedure INSERT_MAPPING_SET
- Added new store procedure INSERT_PARTY
- Added new store procedure INSERT_TRANSCODING_RULE
