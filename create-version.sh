#!/bin/bash

# Requires bash, sed and grep. On Linux everything should already be there.
# On Windows it requires either Git for Windows or Cygwin or something similar

set -e
ver="$1"
set -u

if [ "x$ver" = 'x' ]; then
  echo "Usage $0 <version>"
  exit -1
fi

if echo $ver | grep -v -q '^[0-9]\+\.[0-9]\+$'; then
  echo "Invalid version: '$ver'"
  echo "Usage $0 <version>"
  exit -1
fi

upgradeDir="upgrades/$ver"

if [ ! -d "$upgradeDir" ]; then
  mkdir "$upgradeDir"
fi

echo "Creating upgrade versions"
for i in mysql sqlserver oracle; do
  mkdir -p "$upgradeDir/$i"
  u="${upgradeDir}/${i}_to_v${ver}.sql"
   if [ ! -f "${u}" ]; then
    cat > "${u}" <<EOT
-- updates for $ver
     
-- always last
update DB_VERSION SET VERSION = '$ver'
;

EOT

   fi
done 

echo "Updating initilize version to $ver"
sed -i -e "s/\(INSERT INTO DB_VERSION (VERSION) VALUES ('\)[0-9.]\+\(')\)/\1$ver\2/" version.sql
echo "Updating java version to $ver-test"
mvn -Dhttps.protocols=TLSv1.2 versions:set -DnewVersion="$ver-test"
echo "Updating dotnet version to $ver"
sed -i -e "s#\(<VersionPrefix>\)[^<]\+\(</VersionPrefix>\)#\1$ver\2#" src/dotnet/Directory.Build.props

